CREATE TABLE IF NOT EXISTS `Armor`(
	`id_armor` INT AUTO_INCREMENT PRIMARY KEY,
	`armor_type`  SET('Light armor', 'Medium armor', 'Heavy armor', 'Shields', 'Extras'),
	`acBonus` INT,
	`maxDex` INT,
	`checkPenalty` INT,
	`spellFailure` DECIMAL,
	`weigth` DECIMAL,
	`isMetal` BOOLEAN,
	`specialPropierties` VARCHAR(100),
	 CONSTRAINT `spell_Failure` CHECK (`spellFailure` BETWEEN 0 AND 100)
);
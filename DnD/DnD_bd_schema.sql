-- ------------------------------------------------------------------------------------------
-- -----------------------------------  NOTES  ----------------------------------------------
-- FOR THE NAME OF THE IDS PLEASE USE THIS STANDARD: id_[tablename]
-- FOR SETS WE HAVE TO USE EXACTLY THE SAME FORMAT OTHERWISE IF WE HAVE TO MAKE CHANGES IT
-- WIL BE MORE DIFICULT TO CHANGE THEM. SO PLEASE COPY PASTE THIS:
--  SET('STR', 'DEX','CON','INT','WIS','CHA') ,
--  `nameClass` SET('Barbarian' ,'Bard', 'Cleric', 'Druid', 'Fighter', 'Monk', 'Paladin', 'Ranger', 'Rogue', 'Sorcerer', 'Wizard')

-- ------------------------------------------------------------------------------------------
-- ------------------------------	TABLE RELATIONS -------------------------------------------

-- -------------------------------------------------------------------------------------------
-- -------------------------------------	CLASS ----------------------------------------------
-- ------------------------------ Author: Sara Henriques -------------------------------------
--	page 21 player handbook                                                                 --
-- -------------------------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS `Classes`(
	`id_class` INT AUTO_INCREMENT PRIMARY KEY,
	`lvl` INT(2),
	`baseAttack` varchar(20),
	`FortSave` int,
	`RefSave` int,
	`WillSave` int,
	`Special` varchar(50),
	`flurryBlows` varchar(50),
	`unarmedDamage` SET('1d6', '1d8','1d10','2d6','2d8','2d10'),
	`acBonus` int,
	`unarmedSpeed` int,
	`Spells0` int,
	`Spells1` int,
	`Spells2` int,
	`Spells3` int,
	`Spells4` int,
	`Spells5` int,
	`Spells6` int,
	`Spells7` int,
	`Spells8` int,
	`Spells9` int,
	`skillPointsPerLvl` int,
	`nameClass` SET('Barbarian' ,'Bard', 'Cleric', 'Druid', 'Fighter', 'Monk', 'Paladin', 'Ranger', 'Rogue', 'Sorcerer', 'Wizard')
);

-- --------------------------------------------------------------------------------------------
-- ------------------------------	PrestigeClass ----------------------------------------------
-- ------------------------------ Author: Sara Henriques --------------------------------------
-- page 176 master handbook
-- Quien haga los inserts por favor vuelva a mirar todos los campos y si es posible o si hay tiempo
-- hacer tablas de relaciones
-- --------------------------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS `PrestigeClass`(
	`id_prestigeClass` INT AUTO_INCREMENT PRIMARY KEY,
	`name`  VARCHAR(30) NOT NULL,
	`hitDie` SET('d4', 'd6','d8','d10','d12'),
	`reqRace`  VARCHAR(100), -- maybe table of relations?
	`reqBaseAttack`  int,
	`reqFeats`  VARCHAR(100),  -- maybe table of relations?
	`reqSpells`  VARCHAR(100), -- maybe table of relations?
	`reqAlignment` VARCHAR(100), -- usually in the description it puts the one that can't be. If it's any nonLawful it means it's Chaotic or neutral
	`reqSpecial`  VARCHAR(100),
	`reqLanguages`  VARCHAR(100),
	`reqWeaponProficiency`  VARCHAR(100), -- usually it says weapon group,
	`skillPointsPerLvl` int,
	`shortDescription` varchar(200)
);

-- --------------------------------------------------------------------------------------------
-- ------------------------------	PrestigePerLvl ---------------------------------------------
-- ------------------------------ Author: Sara Henriques --------------------------------------
-- page 176 master handbook
-- --------------------------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS `PrestigeClassPerLvl`(
	`id_prestigeClassPerLvl` INT AUTO_INCREMENT PRIMARY KEY,
	`id_prestigeClass_fk` INT,
	`lvl` int(2),
	`AttackBonus` varchar(20),
	`FortSave` int,
	`RefSave` int,
	`WillSave` int,
	`Special` varchar(50),
	`SpellsPerDay` varchar(50),
	`Spells1` int,
	`Spells2` int,
	`Spells3` int,
	`Spells4` int,
	`bonusSpell` int,
	`acBonus` int,
	FOREIGN KEY (id_prestigeClass_fk) REFERENCES PrestigeClass(id_prestigeClass)
);


-- --------------------------------------------------------------------------------------------
-- --------------------------------  	SKILL   ------------------------------------------------
-- ------------------------------ Author: Sara Henriques --------------------------------------
--	page 63 player handbook for a table                                                     --
-- --------------------------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS `Skill`(
	`id_skill` INT AUTO_INCREMENT PRIMARY KEY,
	`skillName`  VARCHAR(50) NOT NULL,
	`keyAbility` SET('STR', 'DEX','CON','INT','WIS','CHA', 'None'),
	`isUsableUntrained` BOOLEAN NOT NULL, -- true is equal yes false is no
	`isArmorPenalty`  BOOLEAN NOT NULL -- true is equal yes false is no
);

CREATE TABLE IF NOT EXISTS `ClassSkills` (
	`id_classSkills` INT AUTO_INCREMENT PRIMARY KEY,
	`id_skill_fk` INT,
	`nameClass` SET('Barbarian' ,'Bard', 'Cleric', 'Druid', 'Fighter', 'Monk', 'Paladin', 'Ranger', 'Rogue', 'Sorcerer', 'Wizard') ,
   FOREIGN KEY (id_skill_fk) REFERENCES Skill(id_skill)
);

CREATE TABLE IF NOT EXISTS `PrstClassSkills` (
	`id_prstClassSkills` INT AUTO_INCREMENT PRIMARY KEY,
	`id_skill_fk` INT,
	`id_prestigeClass_fk` INT ,
   FOREIGN KEY (id_skill_fk) REFERENCES Skill(id_skill),
   FOREIGN KEY (id_prestigeClass_fk) REFERENCES PrestigeClass(id_prestigeClass)
);

CREATE TABLE IF NOT EXISTS `ReqPrstClassSkills` (
	`id_reqPrstClassSkills` INT AUTO_INCREMENT PRIMARY KEY,
	`id_skill_fk` INT,
	`id_prestigeClass_fk` INT ,
	`ranks` INT,
   FOREIGN KEY (id_skill_fk) REFERENCES Skill(id_skill),
   FOREIGN KEY (id_prestigeClass_fk) REFERENCES PrestigeClass(id_prestigeClass)
);

-- --------------------------------------------------------------------------------------------
-- -------------------------------------	RACE -------------------------------------------------
-- ------------------------------ Author: Sara Henriques --------------------------------------
--	page 11 player handbook                                                                 --
-- --------------------------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS `Race` (
	`id_race` INT AUTO_INCREMENT PRIMARY KEY,
	`name` varchar(30),
	`abilityAdjustments` VARCHAR(30),
	`traits` TEXT,
	`size` SET('Medium' ,'Small'),
	`speedMod` SET('30' ,'20'),
	`favoredClass` varchar(30),
	`automaticLanguages` varchar(50),
	`bonusLanguages` varchar(50)
);

-- --------------------------------------------------------------------------------------------
-- ------------------------------------	FEAT -------------------------------------------------
-- ------------------------------ Author: Sara Henriques --------------------------------------
-- page 87 player handbook
-- --------------------------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS `Feat`(
	`id_feat` INT AUTO_INCREMENT PRIMARY KEY,
	`featName` varchar(60),
	`featType` SET('General', 'Item Creation', 'Metamagic Feats', 'Special') NOT NULL,
	`prerequisites` varchar(100),
	`benefit` text,
	`normal` text,
	`special` text,
	`shortDescription` varchar(100),
	`isFighterChoice` BOOLEAN,
	`isGainedMultipleTimes` BOOLEAN,
	`itsEffectsStack` BOOLEAN
);

-- --------------------------------------------------------------------------------------------
-- ------------------------------	ARMOR TABLE	-------------------------------------------------
-- --------------------------------------------------------------------------------------------
-- --------------------------	Player handbook 3.5	---------------------------------------------
-- ------------------------------	Page: 123	---------------------------------------------------
---------------------	Author: Eric Amoros & Núria Alcalà	-------------------------------------
-- --------------------------------------------------------------------------------------------
-- Inserts podemos sacar de aqui:  http://www.dandwiki.com/wiki/SRD:Armor                   --
-- El campo 'armor_type' hay dos opciones: 																								  --
--		1) Las armaduras personalizadas por el usuario seran catalogadas como extras					--
--		2) El usuario elegira el tipo de armadura que es																			--
--		3) Campo se quedara vacio, no sera obligatorio																				--
--	El campo specialProperties es texto, no lo controlaremos nosotros												--
--	El campo speed lo calcularemos nosotros en la entidad																		--
-- --------------------------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS `Armor`(
	`id_armor` INT AUTO_INCREMENT PRIMARY KEY,
	`name` varchar(30),
	`armorType`  SET('Light armor', 'Medium armor', 'Heavy armor', 'Shields', 'Extras'),
	`acBonus` INT,
	`maxDex` INT,
	`checkPenalty` INT,
	`spellFailure` DECIMAL,
	`weight` DECIMAL,
	`isMetal` BOOLEAN,
	`specialProperties` VARCHAR(100),
	 CONSTRAINT `spell_Failure` CHECK (`spellFailure` BETWEEN 0 AND 100)
);


-- --------------------------------------------------------------------------------------------
-- ------------------------------	WEAPON TABLE	----------------------------------------------
-- --------------------------------------------------------------------------------------------
-- --------------------------	Player handbook 3.5 --------------------------------------------
-- ------------------------------	Page: 116, 117	--------------------------------------------
-- ----------------------	Author: Eric Amoros & Nuria Alcalà----------------------------------
-- --------------------------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS `Weapon`(
	`id_weapon` int primary key auto_increment,
	`name` varchar(50),
	`damageS` varchar(5),
	`damageM` varchar(5),
	`critical` varchar(10),
	 wpRange int(3),
	`weight` decimal,
	`weaponGroup` varchar(50),
	`weaponType` varchar(50),
	`damageType` varchar(50)
);


-- --------------------------------------------------------------------------------------------
-- -----------------------------------	Languages ---------------------------------------------
-- ------------------------------ Author: Sara Henriques --------------------------------------
--	page 82 player handbook                                                                  --
-- --------------------------------------------------------------------------------------------


-- CREATE TABLE IF NOT EXISTS `Language`(
-- 	`id_language` int primary key auto_increment,
-- 	`name` varchar(50)
-- );

-- --------------------------------------------------------------------------------------------
-- ----------------------------------	Spells --------------------------------------------------
-- ------------------------------ Author: Sara Henriques --------------------------------------
--	page 181 player handbook                                                                 --
-- --------------------------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS `Spells`(
	`id_spell` int primary key auto_increment,
	`nameClass` SET('Barbarian' ,'Bard', 'Cleric', 'Druid', 'Fighter', 'Monk', 'Paladin', 'Ranger', 'Rogue', 'Sorcerer', 'Wizard'),
	`name` varchar(50),
	`level` int(2),
	`description` varchar(50),
	`school` varchar(50),
	`subschool` varchar(50),
	`domain` SET('Air', 'Animal', 'Chaos', 'Death', 'Destruction', 'Earth', 'Evil', 'Fire', 'Good', 'Healing', 'Knowledge', 'Law', 'Luck', 'Magic', 'Plant', 'Protection', 'Strength', 'Sun', 'Travel', 'Trickery', 'War', 'Water'), -- page174,
	`components` varchar(50),
	`castingTime` varchar(50),
	spRange varchar(50),
	`targetEffectArea` varchar(50),
	`duration` varchar(50),
	`saveThrow` varchar(50),
	`spellResistance` varchar(50),
  `page` varchar(50),
	`book` varchar(50)
);

-- --------------------------------------------------------------------------------------------
-- ------------------------------------	Build -------------------------------------------------
-- ------------------------------ Author: Sara Henriques --------------------------------------
--	page 21 player handbook                                                                 --
-- --------------------------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS `Build`(
	`id_build` int primary key auto_increment,
	`id_user_fk` int,
	`id_race_fk` int,
	`STR` int(2),
	`DEX` int(2),
	`CON` int(2),
	`INT` int(2),
	`WIS` int(2),
  `CHA` int(2),
	`deity` varchar(50),
	`alignment` SET('Lawful Good', 'Neutral Good', 'Chaotic Good', 'Lawful Neutral', 'Neutral', 'Chaotic Neutral', 'Lawful Evil', 'Neutral Evil', 'Chaotic Evil'),
	`SpellResistance` varchar(50),
	`Language` varchar(150),
	FOREIGN KEY (id_user_fk) REFERENCES User(id),
	FOREIGN KEY (id_race_fk) REFERENCES Race(id_race)
);

-- ------------------------------------------------------------------------------------------
-- ------------------------------	TABLE RELATIONS -------------------------------------------

-- Each Build has many skills, and each skill has many builds (many to many)
CREATE TABLE IF NOT EXISTS `SkillBuild` (
	`id_skillbuild` INT AUTO_INCREMENT PRIMARY KEY,
	`id_skill_fk` INT,
	`id_build_fk` INT,
	`skillModifier` INT,
	`miscModifier` INT,
	`ranks` INT,
	FOREIGN KEY (id_skill_fk) REFERENCES Skill(id_skill),
	FOREIGN KEY (id_build_fk) REFERENCES Build(id_build)
	);

-- Each Build has many classes, and each class has many builds (many to many)
CREATE TABLE IF NOT EXISTS `ClassBuild`(
	`id_classbuild` INT AUTO_INCREMENT PRIMARY KEY,
	`id_build_fk` INT,
	`nameClass` SET('Barbarian' ,'Bard', 'Cleric', 'Druid', 'Fighter', 'Monk', 'Paladin', 'Ranger', 'Rogue', 'Sorcerer', 'Wizard'),
	`levels` INT(2),
	FOREIGN KEY (id_build_fk) REFERENCES Build(id_build)
	);

-- Each Build has many classes, and each class has many builds (many to many)
CREATE TABLE IF NOT EXISTS `FeatBuild`(
	`id_featbuild` INT AUTO_INCREMENT PRIMARY KEY,
	`id_build_fk` INT,
	`id_feat_fk` INT,
	FOREIGN KEY (id_build_fk) REFERENCES Build(id_build),
	FOREIGN KEY (id_feat_fk) REFERENCES Feat(id_feat)
	);


CREATE TABLE IF NOT EXISTS `ArmorBuild`(
	`id_armorbuild` INT AUTO_INCREMENT PRIMARY KEY,
	`id_armor_fk` int,
	`id_build_fk` int,
	FOREIGN KEY (id_armor_fk) REFERENCES `Armor`(`id_armor`),
	FOREIGN KEY (id_build_fk) REFERENCES `Build`(`id_build`)
);


CREATE TABLE IF NOT EXISTS `WeaponBuild`(
	`id_weaponbuild` INT AUTO_INCREMENT PRIMARY KEY,
	`id_weapon_fk` int,
	`id_build_fk` int,
	`attack` int(3),
	`attackBonus` int(3),
	`notes` text,
	FOREIGN KEY (id_weapon_fk) REFERENCES `Weapon`(`id_weapon`),
	FOREIGN KEY (id_build_fk) REFERENCES `Build`(`id_build`)
);

-- CREATE TABLE IF NOT EXISTS `LanguageBuild`(
-- 	`id_language_fk` INT,
--	`id_build_fk` INT,
--	FOREIGN KEY (id_build_fk) REFERENCES Build(id_build),
--	FOREIGN KEY (id_language_fk) REFERENCES Language(id_language)
--	);


-- --------------------------------------------------------------------------------------------
-- ----------------------------------	Possessions ---------------------------------------------
-- ------------------------------ Author: Sara Henriques --------------------------------------
--	page 111 player handbook                                                                 --
-- --------------------------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS `Possessions`(
	`id_possessions` int primary key auto_increment,
	`id_build_fk` int,
	`itemName` varchar(60),
	`page` char(3),
	`weight` varchar(5),
  FOREIGN KEY (id_build_fk) REFERENCES `Build`(`id_build`)
);


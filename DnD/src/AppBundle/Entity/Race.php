<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Race
 *
 * @ORM\Table(name="Race")
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RaceRepository")
 */
class Race
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id_race", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO") 
     */
    protected $idRace;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=30, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="abilityAdjustments", type="string", length=30, nullable=true)
     */
    private $abilityadjustments;

    /**
     * @var string
     *
     * @ORM\Column(name="traits", type="text", length=65535, nullable=true)
     */
    private $traits;

    /**
     * @var array
     *
     * @ORM\Column(name="size", type="simple_array", nullable=true)
     */
    private $size;

    /**
     * @var array
     *
     * @ORM\Column(name="speedMod", type="simple_array", nullable=true)
     */
    private $speedmod;

    /**
     * @var string
     *
     * @ORM\Column(name="favoredClass", type="string", length=30, nullable=true)
     */
    private $favoredclass;

    /**
     * @var string
     *
     * @ORM\Column(name="automaticLanguages", type="string", length=50, nullable=true)
     */
    private $automaticlanguages;

    /**
     * @var string
     *
     * @ORM\Column(name="bonusLanguages", type="string", length=50, nullable=true)
     */
    private $bonuslanguages;



    /**
     * Set idRace
     *
     * @param integer $idRace
     *
     * @return Race
     */
    public function setIdRace($idRace)
    {
        $this->idRace = $idRace;

        return $this;
    }

    /**
     * Get idRace
     *
     * @return integer
     */
    public function getIdRace()
    {
        return $this->idRace;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Race
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set abilityadjustments
     *
     * @param string $abilityadjustments
     *
     * @return Race
     */
    public function setAbilityadjustments($abilityadjustments)
    {
        $this->abilityadjustments = $abilityadjustments;

        return $this;
    }

    /**
     * Get abilityadjustments
     *
     * @return string
     */
    public function getAbilityadjustments()
    {
        return $this->abilityadjustments;
    }

    /**
     * Set traits
     *
     * @param string $traits
     *
     * @return Race
     */
    public function setTraits($traits)
    {
        $this->traits = $traits;

        return $this;
    }

    /**
     * Get traits
     *
     * @return string
     */
    public function getTraits()
    {
        return $this->traits;
    }

    /**
     * Set size
     *
     * @param array $size
     *
     * @return Race
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size
     *
     * @return array
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set speedmod
     *
     * @param array $speedmod
     *
     * @return Race
     */
    public function setSpeedmod($speedmod)
    {
        $this->speedmod = $speedmod;

        return $this;
    }

    /**
     * Get speedmod
     *
     * @return array
     */
    public function getSpeedmod()
    {
        return $this->speedmod;
    }

    /**
     * Set favoredclass
     *
     * @param string $favoredclass
     *
     * @return Race
     */
    public function setFavoredclass($favoredclass)
    {
        $this->favoredclass = $favoredclass;

        return $this;
    }

    /**
     * Get favoredclass
     *
     * @return string
     */
    public function getFavoredclass()
    {
        return $this->favoredclass;
    }

    /**
     * Set automaticlanguages
     *
     * @param string $automaticlanguages
     *
     * @return Race
     */
    public function setAutomaticlanguages($automaticlanguages)
    {
        $this->automaticlanguages = $automaticlanguages;

        return $this;
    }

    /**
     * Get automaticlanguages
     *
     * @return string
     */
    public function getAutomaticlanguages()
    {
        return $this->automaticlanguages;
    }

    /**
     * Set bonuslanguages
     *
     * @param string $bonuslanguages
     *
     * @return Race
     */
    public function setBonuslanguages($bonuslanguages)
    {
        $this->bonuslanguages = $bonuslanguages;

        return $this;
    }

    /**
     * Get bonuslanguages
     *
     * @return string
     */
    public function getBonuslanguages()
    {
        return $this->bonuslanguages;
    }
}

<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Spells
 *
 * @ORM\Table(name="Spells")
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SpellsRepository")
 */
class Spells
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id_spell", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $idSpell;

    /**
     * @var array
     *
     * @ORM\Column(name="nameClass", type="simple_array", nullable=true)
     */
    private $nameclass;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=true)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="level", type="integer", nullable=true)
     */
    private $level;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=50, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="school", type="string", length=50, nullable=true)
     */
    private $school;

    /**
     * @var string
     *
     * @ORM\Column(name="subschool", type="string", length=50, nullable=true)
     */
    private $subschool;

    /**
     * @var array
     *
     * @ORM\Column(name="domain", type="simple_array", nullable=true)
     */
    private $domain;

    /**
     * @var string
     *
     * @ORM\Column(name="components", type="string", length=50, nullable=true)
     */
    private $components;

    /**
     * @var string
     *
     * @ORM\Column(name="castingTime", type="string", length=50, nullable=true)
     */
    private $castingtime;

    /**
     * @var string
     *
     * @ORM\Column(name="spRange", type="string", length=50, nullable=true)
     */
    private $sprange;

    /**
     * @var string
     *
     * @ORM\Column(name="targetEffectArea", type="string", length=50, nullable=true)
     */
    private $targeteffectarea;

    /**
     * @var string
     *
     * @ORM\Column(name="duration", type="string", length=50, nullable=true)
     */
    private $duration;

    /**
     * @var string
     *
     * @ORM\Column(name="saveThrow", type="string", length=50, nullable=true)
     */
    private $savethrow;

    /**
     * @var string
     *
     * @ORM\Column(name="spellResistance", type="string", length=50, nullable=true)
     */
    private $spellresistance;

    /**
     * @var string
     *
     * @ORM\Column(name="page", type="string", length=50, nullable=true)
     */
    private $page;

    /**
     * @var string
     *
     * @ORM\Column(name="book", type="string", length=50, nullable=true)
     */
    private $book;



    /**
     * Set idSpell
     *
     * @param integer $idSpell
     *
     * @return Spells
     */
    public function setIdSpell($idSpell)
    {
        $this->idSpell = $idSpell;

        return $this;
    }

    /**
     * Get idSpell
     *
     * @return integer
     */
    public function getIdSpell()
    {
        return $this->idSpell;
    }

    /**
     * Set nameclass
     *
     * @param array $nameclass
     *
     * @return Spells
     */
    public function setNameclass($nameclass)
    {
        $this->nameclass = $nameclass;

        return $this;
    }

    /**
     * Get nameclass
     *
     * @return array
     */
    public function getNameclass()
    {
        return $this->nameclass;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Spells
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set level
     *
     * @param integer $level
     *
     * @return Spells
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return integer
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Spells
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set school
     *
     * @param string $school
     *
     * @return Spells
     */
    public function setSchool($school)
    {
        $this->school = $school;

        return $this;
    }

    /**
     * Get school
     *
     * @return string
     */
    public function getSchool()
    {
        return $this->school;
    }

    /**
     * Set subschool
     *
     * @param string $subschool
     *
     * @return Spells
     */
    public function setSubschool($subschool)
    {
        $this->subschool = $subschool;

        return $this;
    }

    /**
     * Get subschool
     *
     * @return string
     */
    public function getSubschool()
    {
        return $this->subschool;
    }

    /**
     * Set domain
     *
     * @param array $domain
     *
     * @return Spells
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * Get domain
     *
     * @return array
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Set components
     *
     * @param string $components
     *
     * @return Spells
     */
    public function setComponents($components)
    {
        $this->components = $components;

        return $this;
    }

    /**
     * Get components
     *
     * @return string
     */
    public function getComponents()
    {
        return $this->components;
    }

    /**
     * Set castingtime
     *
     * @param string $castingtime
     *
     * @return Spells
     */
    public function setCastingtime($castingtime)
    {
        $this->castingtime = $castingtime;

        return $this;
    }

    /**
     * Get castingtime
     *
     * @return string
     */
    public function getCastingtime()
    {
        return $this->castingtime;
    }

    /**
     * Set sprange
     *
     * @param string $sprange
     *
     * @return Spells
     */
    public function setSprange($sprange)
    {
        $this->sprange = $sprange;

        return $this;
    }

    /**
     * Get sprange
     *
     * @return string
     */
    public function getSprange()
    {
        return $this->sprange;
    }

    /**
     * Set targeteffectarea
     *
     * @param string $targeteffectarea
     *
     * @return Spells
     */
    public function setTargeteffectarea($targeteffectarea)
    {
        $this->targeteffectarea = $targeteffectarea;

        return $this;
    }

    /**
     * Get targeteffectarea
     *
     * @return string
     */
    public function getTargeteffectarea()
    {
        return $this->targeteffectarea;
    }

    /**
     * Set duration
     *
     * @param string $duration
     *
     * @return Spells
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return string
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set savethrow
     *
     * @param string $savethrow
     *
     * @return Spells
     */
    public function setSavethrow($savethrow)
    {
        $this->savethrow = $savethrow;

        return $this;
    }

    /**
     * Get savethrow
     *
     * @return string
     */
    public function getSavethrow()
    {
        return $this->savethrow;
    }

    /**
     * Set spellresistance
     *
     * @param string $spellresistance
     *
     * @return Spells
     */
    public function setSpellresistance($spellresistance)
    {
        $this->spellresistance = $spellresistance;

        return $this;
    }

    /**
     * Get spellresistance
     *
     * @return string
     */
    public function getSpellresistance()
    {
        return $this->spellresistance;
    }

    /**
     * Set page
     *
     * @param string $page
     *
     * @return Spells
     */
    public function setPage($page)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * Get page
     *
     * @return string
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Set book
     *
     * @param string $book
     *
     * @return Spells
     */
    public function setBook($book)
    {
        $this->book = $book;

        return $this;
    }

    /**
     * Get book
     *
     * @return string
     */
    public function getBook()
    {
        return $this->book;
    }
}

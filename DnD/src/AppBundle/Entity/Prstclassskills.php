<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Prstclassskills
 *
 * @ORM\Table(name="PrstClassSkills", indexes={@ORM\Index(name="id_skill_fk", columns={"id_skill_fk"}), @ORM\Index(name="id_prestigeClass_fk", columns={"id_prestigeClass_fk"})})
 * @ORM\Entity
 */
class Prstclassskills
{
    
     /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $idPrstClassSkills;
    
    /**
     * @var \AppBundle\Entity\Skill
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Skill")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_skill_fk", referencedColumnName="id_skill")
     * })
     */
    private $idSkillFk;

    /**
     * @var \AppBundle\Entity\Prestigeclass
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Prestigeclass")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_prestigeClass_fk", referencedColumnName="id_prestige_class")
     * })
     */
    private $id_prestigeClass_fk;



    /**
     * Set idSkillFk
     *
     * @param \AppBundle\Entity\Skill $idSkillFk
     *
     * @return Prstclassskills
     */
    public function setIdSkillFk(\AppBundle\Entity\Skill $idSkillFk = null)
    {
        $this->idSkillFk = $idSkillFk;

        return $this;
    }

    /**
     * Get idSkillFk
     *
     * @return \AppBundle\Entity\Skill
     */
    public function getIdSkillFk()
    {
        return $this->idSkillFk;
    }

    /**
     * Set idPrestigeclassFk
     *
     * @param \AppBundle\Entity\Prestigeclass $idPrestigeclassFk
     *
     * @return Prstclassskills
     */
    public function setIdPrestigeclassFk(\AppBundle\Entity\Prestigeclass $idPrestigeclassFk = null)
    {
        $this->idPrestigeclassFk = $idPrestigeclassFk;

        return $this;
    }

    /**
     * Get idPrestigeclassFk
     *
     * @return \AppBundle\Entity\Prestigeclass
     */
    public function getIdPrestigeclassFk()
    {
        return $this->idPrestigeclassFk;
    }

    /**
     * Get idPrstClassSkills
     *
     * @return integer
     */
    public function getIdPrstClassSkills()
    {
        return $this->idPrstClassSkills;
    }
}

<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Skill
 * @ORM\Entity
 * @ORM\Table(name="Skill")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SkillRepository")
 */
class Skill
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $idSkill;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $skillname;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $keyability;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isusableuntrained;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isarmorpenalty;


    /**
     * Set idSkill
     *
     * @param integer $idSkill
     *
     * @return Skill
     */
    public function setIdSkill($idSkill)
    {
        $this->idSkill = $idSkill;

        return $this;
    }

    /**
     * Get idSkill
     *
     * @return integer
     */
    public function getIdSkill()
    {
        return $this->idSkill;
    }

    /**
     * Set skillname
     *
     * @param string $skillname
     *
     * @return Skill
     */
    public function setSkillname($skillname)
    {
        $this->skillname = $skillname;

        return $this;
    }

    /**
     * Get skillname
     *
     * @return string
     */
    public function getSkillname()
    {
        return $this->skillname;
    }

    /**
     * Set keyability
     *
     * @param array $keyability
     *
     * @return Skill
     */
    public function setKeyability($keyability)
    {
        $this->keyability = $keyability;

        return $this;
    }

    /**
     * Get keyability
     *
     * @return array
     */
    public function getKeyability()
    {
        return $this->keyability;
    }

    /**
     * Set isusableuntrained
     *
     * @param boolean $isusableuntrained
     *
     * @return Skill
     */
    public function setIsusableuntrained($isusableuntrained)
    {
        $this->isusableuntrained = $isusableuntrained;

        return $this;
    }

    /**
     * Get isusableuntrained
     *
     * @return boolean
     */
    public function getIsusableuntrained()
    {
        return $this->isusableuntrained;
    }

    /**
     * Set isarmorpenalty
     *
     * @param boolean $isarmorpenalty
     *
     * @return Skill
     */
    public function setIsarmorpenalty($isarmorpenalty)
    {
        $this->isarmorpenalty = $isarmorpenalty;

        return $this;
    }

    /**
     * Get isarmorpenalty
     *
     * @return boolean
     */
    public function getIsarmorpenalty()
    {
        return $this->isarmorpenalty;
    }
}

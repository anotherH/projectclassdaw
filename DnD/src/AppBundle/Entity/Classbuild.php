<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Classbuild
 * @ORM\Entity
 * @ORM\Table(name="ClassBuild")
 */
class Classbuild
{
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $idClassbuild;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $nameclass;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $levels;

    /**
     * @var \AppBundle\Entity\Build
     */
    private $idBuildFk;


    /**
     * Set nameclass
     *
     * @param array $nameclass
     *
     * @return Classbuild
     */
    public function setNameclass($nameclass)
    {
        $this->nameclass = $nameclass;

        return $this;
    }

    /**
     * Get nameclass
     *
     * @return array
     */
    public function getNameclass()
    {
        return $this->nameclass;
    }

    /**
     * Set levels
     *
     * @param integer $levels
     *
     * @return Classbuild
     */
    public function setLevels($levels)
    {
        $this->levels = $levels;

        return $this;
    }

    /**
     * Get levels
     *
     * @return integer
     */
    public function getLevels()
    {
        return $this->levels;
    }

    /**
     * Set idBuildFk
     *
     * @param \AppBundle\Entity\Build $idBuildFk
     *
     * @return Classbuild
     */
    public function setIdBuildFk(\AppBundle\Entity\Build $idBuildFk = null)
    {
        $this->idBuildFk = $idBuildFk;

        return $this;
    }

    /**
     * Get idBuildFk
     *
     * @return \AppBundle\Entity\Build
     */
    public function getIdBuildFk()
    {
        return $this->idBuildFk;
    }

    /**
     * Get idClassbuild
     *
     * @return integer
     */
    public function getIdClassbuild()
    {
        return $this->idClassbuild;
    }
}

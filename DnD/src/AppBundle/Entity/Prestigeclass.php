<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Prestigeclass
 * @ORM\Entity
 * @ORM\Table(name="PrestigeClass")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PrestigeclassRepository")
 */
class Prestigeclass
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id_prestigeClass;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $hitdie;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $reqrace;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $reqbaseattack;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $reqfeats;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $reqspells;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $reqalignment;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $reqspecial;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $reqlanguages;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $reqweaponproficiency;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $skillpointsperlvl;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $shortdescription;


    /**
     * Set idPrestigeclass
     *
     * @param integer $idPrestigeclass
     *
     * @return Prestigeclass
     */
    public function setIdPrestigeclass($idPrestigeclass)
    {
        $this->idPrestigeclass = $idPrestigeclass;

        return $this;
    }

    /**
     * Get idPrestigeclass
     *
     * @return integer
     */
    public function getIdPrestigeclass()
    {
        return $this->idPrestigeclass;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Prestigeclass
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set hitdie
     *
     * @param array $hitdie
     *
     * @return Prestigeclass
     */
    public function setHitdie($hitdie)
    {
        $this->hitdie = $hitdie;

        return $this;
    }

    /**
     * Get hitdie
     *
     * @return array
     */
    public function getHitdie()
    {
        return $this->hitdie;
    }

    /**
     * Set reqrace
     *
     * @param string $reqrace
     *
     * @return Prestigeclass
     */
    public function setReqrace($reqrace)
    {
        $this->reqrace = $reqrace;

        return $this;
    }

    /**
     * Get reqrace
     *
     * @return string
     */
    public function getReqrace()
    {
        return $this->reqrace;
    }

    /**
     * Set reqbaseattack
     *
     * @param integer $reqbaseattack
     *
     * @return Prestigeclass
     */
    public function setReqbaseattack($reqbaseattack)
    {
        $this->reqbaseattack = $reqbaseattack;

        return $this;
    }

    /**
     * Get reqbaseattack
     *
     * @return integer
     */
    public function getReqbaseattack()
    {
        return $this->reqbaseattack;
    }

    /**
     * Set reqfeats
     *
     * @param string $reqfeats
     *
     * @return Prestigeclass
     */
    public function setReqfeats($reqfeats)
    {
        $this->reqfeats = $reqfeats;

        return $this;
    }

    /**
     * Get reqfeats
     *
     * @return string
     */
    public function getReqfeats()
    {
        return $this->reqfeats;
    }

    /**
     * Set reqspells
     *
     * @param string $reqspells
     *
     * @return Prestigeclass
     */
    public function setReqspells($reqspells)
    {
        $this->reqspells = $reqspells;

        return $this;
    }

    /**
     * Get reqspells
     *
     * @return string
     */
    public function getReqspells()
    {
        return $this->reqspells;
    }

    /**
     * Set reqalignment
     *
     * @param string $reqalignment
     *
     * @return Prestigeclass
     */
    public function setReqalignment($reqalignment)
    {
        $this->reqalignment = $reqalignment;

        return $this;
    }

    /**
     * Get reqalignment
     *
     * @return string
     */
    public function getReqalignment()
    {
        return $this->reqalignment;
    }

    /**
     * Set reqspecial
     *
     * @param string $reqspecial
     *
     * @return Prestigeclass
     */
    public function setReqspecial($reqspecial)
    {
        $this->reqspecial = $reqspecial;

        return $this;
    }

    /**
     * Get reqspecial
     *
     * @return string
     */
    public function getReqspecial()
    {
        return $this->reqspecial;
    }

    /**
     * Set reqlanguages
     *
     * @param string $reqlanguages
     *
     * @return Prestigeclass
     */
    public function setReqlanguages($reqlanguages)
    {
        $this->reqlanguages = $reqlanguages;

        return $this;
    }

    /**
     * Get reqlanguages
     *
     * @return string
     */
    public function getReqlanguages()
    {
        return $this->reqlanguages;
    }

    /**
     * Set reqweaponproficiency
     *
     * @param string $reqweaponproficiency
     *
     * @return Prestigeclass
     */
    public function setReqweaponproficiency($reqweaponproficiency)
    {
        $this->reqweaponproficiency = $reqweaponproficiency;

        return $this;
    }

    /**
     * Get reqweaponproficiency
     *
     * @return string
     */
    public function getReqweaponproficiency()
    {
        return $this->reqweaponproficiency;
    }

    /**
     * Set skillpointsperlvl
     *
     * @param integer $skillpointsperlvl
     *
     * @return Prestigeclass
     */
    public function setSkillpointsperlvl($skillpointsperlvl)
    {
        $this->skillpointsperlvl = $skillpointsperlvl;

        return $this;
    }

    /**
     * Get skillpointsperlvl
     *
     * @return integer
     */
    public function getSkillpointsperlvl()
    {
        return $this->skillpointsperlvl;
    }

    /**
     * Set shortdescription
     *
     * @param string $shortdescription
     *
     * @return Prestigeclass
     */
    public function setShortdescription($shortdescription)
    {
        $this->shortdescription = $shortdescription;

        return $this;
    }

    /**
     * Get shortdescription
     *
     * @return string
     */
    public function getShortdescription()
    {
        return $this->shortdescription;
    }
}

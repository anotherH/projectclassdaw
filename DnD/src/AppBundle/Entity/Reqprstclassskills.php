<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reqprstclassskills
 *
 * @ORM\Table(name="ReqPrstClassSkills", indexes={@ORM\Index(name="id_skill_fk", columns={"id_skill_fk"}), @ORM\Index(name="id_prestigeClass_fk", columns={"id_prestigeClass_fk"})})
 * @ORM\Entity
 */
class Reqprstclassskills
{
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $idReqprstclassskills;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="ranks", type="integer", nullable=true)
     */
    private $ranks;

    /**
     * @var \AppBundle\Entity\Skill
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Skill")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_skill_fk", referencedColumnName="id_skill")
     * })
     */
    private $idSkillFk;

    /**
     * @var \AppBundle\Entity\Prestigeclass
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Prestigeclass")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_prestigeClass_fk", referencedColumnName="id_prestige_class")
     * })
     */
    private $idPrestigeclassFk;



    /**
     * Set ranks
     *
     * @param integer $ranks
     *
     * @return Reqprstclassskills
     */
    public function setRanks($ranks)
    {
        $this->ranks = $ranks;

        return $this;
    }

    /**
     * Get ranks
     *
     * @return integer
     */
    public function getRanks()
    {
        return $this->ranks;
    }

    /**
     * Set idSkillFk
     *
     * @param \AppBundle\Entity\Skill $idSkillFk
     *
     * @return Reqprstclassskills
     */
    public function setIdSkillFk(\AppBundle\Entity\Skill $idSkillFk = null)
    {
        $this->idSkillFk = $idSkillFk;

        return $this;
    }

    /**
     * Get idSkillFk
     *
     * @return \AppBundle\Entity\Skill
     */
    public function getIdSkillFk()
    {
        return $this->idSkillFk;
    }

    /**
     * Set idPrestigeclassFk
     *
     * @param \AppBundle\Entity\Prestigeclass $idPrestigeclassFk
     *
     * @return Reqprstclassskills
     */
    public function setIdPrestigeclassFk(\AppBundle\Entity\Prestigeclass $idPrestigeclassFk = null)
    {
        $this->idPrestigeclassFk = $idPrestigeclassFk;

        return $this;
    }

    /**
     * Get idPrestigeclassFk
     *
     * @return \AppBundle\Entity\Prestigeclass
     */
    public function getIdPrestigeclassFk()
    {
        return $this->idPrestigeclassFk;
    }

    /**
     * Get idReqprstclassskills
     *
     * @return integer
     */
    public function getIdReqprstclassskills()
    {
        return $this->idReqprstclassskills;
    }
}

<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Feat
 * @ORM\Entity
 * @ORM\Table(name="Feat")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FeatRepository")
 */
class Feat
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $idFeat;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $featname;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $feattype;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $prerequisites;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $benefit;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $normal;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $special;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $shortdescription;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isfighterchoice;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isgainedmultipletimes;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $itseffectsstack;


    /**
     * Set idFeat
     *
     * @param integer $idFeat
     *
     * @return Feat
     */
    public function setIdFeat($idFeat)
    {
        $this->idFeat = $idFeat;

        return $this;
    }

    /**
     * Get idFeat
     *
     * @return integer
     */
    public function getIdFeat()
    {
        return $this->idFeat;
    }

    /**
     * Set featname
     *
     * @param string $featname
     *
     * @return Feat
     */
    public function setFeatname($featname)
    {
        $this->featname = $featname;

        return $this;
    }

    /**
     * Get featname
     *
     * @return string
     */
    public function getFeatname()
    {
        return $this->featname;
    }

    /**
     * Set feattype
     *
     * @param array $feattype
     *
     * @return Feat
     */
    public function setFeattype($feattype)
    {
        $this->feattype = $feattype;

        return $this;
    }

    /**
     * Get feattype
     *
     * @return array
     */
    public function getFeattype()
    {
        return $this->feattype;
    }

    /**
     * Set prerequisites
     *
     * @param string $prerequisites
     *
     * @return Feat
     */
    public function setPrerequisites($prerequisites)
    {
        $this->prerequisites = $prerequisites;

        return $this;
    }

    /**
     * Get prerequisites
     *
     * @return string
     */
    public function getPrerequisites()
    {
        return $this->prerequisites;
    }

    /**
     * Set benefit
     *
     * @param string $benefit
     *
     * @return Feat
     */
    public function setBenefit($benefit)
    {
        $this->benefit = $benefit;

        return $this;
    }

    /**
     * Get benefit
     *
     * @return string
     */
    public function getBenefit()
    {
        return $this->benefit;
    }

    /**
     * Set normal
     *
     * @param string $normal
     *
     * @return Feat
     */
    public function setNormal($normal)
    {
        $this->normal = $normal;

        return $this;
    }

    /**
     * Get normal
     *
     * @return string
     */
    public function getNormal()
    {
        return $this->normal;
    }

    /**
     * Set special
     *
     * @param string $special
     *
     * @return Feat
     */
    public function setSpecial($special)
    {
        $this->special = $special;

        return $this;
    }

    /**
     * Get special
     *
     * @return string
     */
    public function getSpecial()
    {
        return $this->special;
    }

    /**
     * Set shortdescription
     *
     * @param string $shortdescription
     *
     * @return Feat
     */
    public function setShortdescription($shortdescription)
    {
        $this->shortdescription = $shortdescription;

        return $this;
    }

    /**
     * Get shortdescription
     *
     * @return string
     */
    public function getShortdescription()
    {
        return $this->shortdescription;
    }

    /**
     * Set isfighterchoice
     *
     * @param boolean $isfighterchoice
     *
     * @return Feat
     */
    public function setIsfighterchoice($isfighterchoice)
    {
        $this->isfighterchoice = $isfighterchoice;

        return $this;
    }

    /**
     * Get isfighterchoice
     *
     * @return boolean
     */
    public function getIsfighterchoice()
    {
        return $this->isfighterchoice;
    }

    /**
     * Set isgainedmultipletimes
     *
     * @param boolean $isgainedmultipletimes
     *
     * @return Feat
     */
    public function setIsgainedmultipletimes($isgainedmultipletimes)
    {
        $this->isgainedmultipletimes = $isgainedmultipletimes;

        return $this;
    }

    /**
     * Get isgainedmultipletimes
     *
     * @return boolean
     */
    public function getIsgainedmultipletimes()
    {
        return $this->isgainedmultipletimes;
    }

    /**
     * Set itseffectsstack
     *
     * @param boolean $itseffectsstack
     *
     * @return Feat
     */
    public function setItseffectsstack($itseffectsstack)
    {
        $this->itseffectsstack = $itseffectsstack;

        return $this;
    }

    /**
     * Get itseffectsstack
     *
     * @return boolean
     */
    public function getItseffectsstack()
    {
        return $this->itseffectsstack;
    }
    
    /** 
     * Comprueba que strings solo tengan letras, números y los carácteres () / . , -
     * 
     * @return boolean
     * 
     */
     
    public function checkFeatName($name){
         if(preg_match('~[^A-Za-z\ \-]~', $name)){
             return false;
         } else {
             return true;
         }
    }
    
    
    
}

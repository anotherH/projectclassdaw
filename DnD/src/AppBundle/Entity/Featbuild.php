<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Featbuild
 *
 * @ORM\Table(name="FeatBuild", indexes={@ORM\Index(name="id_build_fk", columns={"id_build_fk"}), @ORM\Index(name="id_feat_fk", columns={"id_feat_fk"})})
 * @ORM\Entity
 */
class Featbuild
{
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $idFeatbuild;
    
    /**
     * @var \AppBundle\Entity\Build
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Build")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_build_fk", referencedColumnName="id_build")
     * })
     */
    private $idBuildFk;

    /**
     * @var \AppBundle\Entity\Feat
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Feat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_feat_fk", referencedColumnName="id_feat")
     * })
     */
    private $idFeatFk;



    /**
     * Set idBuildFk
     *
     * @param \AppBundle\Entity\Build $idBuildFk
     *
     * @return Featbuild
     */
    public function setIdBuildFk(\AppBundle\Entity\Build $idBuildFk = null)
    {
        $this->idBuildFk = $idBuildFk;

        return $this;
    }

    /**
     * Get idBuildFk
     *
     * @return \AppBundle\Entity\Build
     */
    public function getIdBuildFk()
    {
        return $this->idBuildFk;
    }

    /**
     * Set idFeatFk
     *
     * @param \AppBundle\Entity\Feat $idFeatFk
     *
     * @return Featbuild
     */
    public function setIdFeatFk(\AppBundle\Entity\Feat $idFeatFk = null)
    {
        $this->idFeatFk = $idFeatFk;

        return $this;
    }

    /**
     * Get idFeatFk
     *
     * @return \AppBundle\Entity\Feat
     */
    public function getIdFeatFk()
    {
        return $this->idFeatFk;
    }

    /**
     * Get idFeatbuild
     *
     * @return integer
     */
    public function getIdFeatbuild()
    {
        return $this->idFeatbuild;
    }
}

<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Skillbuild
 * @ORM\Entity
 * @ORM\Table(name="SkillBuild")
 */
class Skillbuild
{
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $idSkillbuild;
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $skillmodifier;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $miscmodifier;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $ranks;

    /**
     * @var \AppBundle\Entity\Skill
     */
    private $idSkillFk;

    /**
     * @var \AppBundle\Entity\Build
     */
    private $idBuildFk;


    /**
     * Set skillmodifier
     *
     * @param integer $skillmodifier
     *
     * @return Skillbuild
     */
    public function setSkillmodifier($skillmodifier)
    {
        $this->skillmodifier = $skillmodifier;

        return $this;
    }

    /**
     * Get skillmodifier
     *
     * @return integer
     */
    public function getSkillmodifier()
    {
        return $this->skillmodifier;
    }

    /**
     * Set miscmodifier
     *
     * @param integer $miscmodifier
     *
     * @return Skillbuild
     */
    public function setMiscmodifier($miscmodifier)
    {
        $this->miscmodifier = $miscmodifier;

        return $this;
    }

    /**
     * Get miscmodifier
     *
     * @return integer
     */
    public function getMiscmodifier()
    {
        return $this->miscmodifier;
    }

    /**
     * Set ranks
     *
     * @param integer $ranks
     *
     * @return Skillbuild
     */
    public function setRanks($ranks)
    {
        $this->ranks = $ranks;

        return $this;
    }

    /**
     * Get ranks
     *
     * @return integer
     */
    public function getRanks()
    {
        return $this->ranks;
    }

    /**
     * Set idSkillFk
     *
     * @param \AppBundle\Entity\Skill $idSkillFk
     *
     * @return Skillbuild
     */
    public function setIdSkillFk(\AppBundle\Entity\Skill $idSkillFk = null)
    {
        $this->idSkillFk = $idSkillFk;

        return $this;
    }

    /**
     * Get idSkillFk
     *
     * @return \AppBundle\Entity\Skill
     */
    public function getIdSkillFk()
    {
        return $this->idSkillFk;
    }

    /**
     * Set idBuildFk
     *
     * @param \AppBundle\Entity\Build $idBuildFk
     *
     * @return Skillbuild
     */
    public function setIdBuildFk(\AppBundle\Entity\Build $idBuildFk = null)
    {
        $this->idBuildFk = $idBuildFk;

        return $this;
    }

    /**
     * Get idBuildFk
     *
     * @return \AppBundle\Entity\Build
     */
    public function getIdBuildFk()
    {
        return $this->idBuildFk;
    }

    /**
     * Get idSkillbuild
     *
     * @return integer
     */
    public function getIdSkillbuild()
    {
        return $this->idSkillbuild;
    }
}

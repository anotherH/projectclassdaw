<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Classskills
 * @ORM\Entity
 * @ORM\Table(name="ClassSkills")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ClassSkillsRepository")
 */
class Classskills
{
     /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $idClassSkills;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $nameclass;

    /**
     * @var \AppBundle\Entity\Skill
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Skill")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="id_skill_fk", referencedColumnName="id_skill")
     * })
     */
    private $id_skill_fk;


    /**
     * Set nameclass
     *
     * @param array $nameclass
     *
     * @return Classskills
     */
    public function setNameclass($nameclass)
    {
        $this->nameclass = $nameclass;

        return $this;
    }

    /**
     * Get nameclass
     *
     * @return array
     */
    public function getNameclass()
    {
        return $this->nameclass;
    }

    /**
     * Set idSkillFk
     *
     * @param \AppBundle\Entity\Skill $idSkillFk
     *
     * @return Classskills
     */
    public function setIdSkillFk(\AppBundle\Entity\Skill $idSkillFk = null)
    {
        $this->idSkillFk = $idSkillFk;

        return $this;
    }

    /**
     * Get idSkillFk
     *
     * @return \AppBundle\Entity\Skill
     */
    public function getIdSkillFk()
    {
        return $this->idSkillFk;
    }

    /**
     * Get idClassSkills
     *
     * @return integer
     */
    public function getIdClassSkills()
    {
        return $this->idClassSkills;
    }
}

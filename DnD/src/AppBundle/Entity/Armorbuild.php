<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Armorbuild
 * @ORM\Entity
 * @ORM\Table(name="ArmorBuild")
 */

class Armorbuild
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $idArmorBuild;
    
    /**
     * @var \AppBundle\Entity\Armor
     */
    private $idArmorFk;

    /**
     * @var \AppBundle\Entity\Build
     */
    private $idBuildFk;


    /**
     * Set idArmorFk
     *
     * @param \AppBundle\Entity\Armor $idArmorFk
     *
     * @return Armorbuild
     */
    public function setIdArmorFk(\AppBundle\Entity\Armor $idArmorFk = null)
    {
        $this->idArmorFk = $idArmorFk;

        return $this;
    }

    /**
     * Get idArmorFk
     *
     * @return \AppBundle\Entity\Armor
     */
    public function getIdArmorFk()
    {
        return $this->idArmorFk;
    }

    /**
     * Set idBuildFk
     *
     * @param \AppBundle\Entity\Build $idBuildFk
     *
     * @return Armorbuild
     */
    public function setIdBuildFk(\AppBundle\Entity\Build $idBuildFk = null)
    {
        $this->idBuildFk = $idBuildFk;

        return $this;
    }

    /**
     * Get idBuildFk
     *
     * @return \AppBundle\Entity\Build
     */
    public function getIdBuildFk()
    {
        return $this->idBuildFk;
    }

    /**
     * Get idArmorBuild
     *
     * @return integer
     */
    public function getIdArmorBuild()
    {
        return $this->idArmorBuild;
    }
}

<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Weapon
 * @ORM\Entity
 * @ORM\Table(name="Weapon")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\WeaponRepository")
 */
class Weapon
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $idWeapon;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $damages;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $damagem;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $critical;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $wprange;

    /**
     * @ORM\Column(type="decimal", nullable=true)
     */
    private $weight;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $weapongroup;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $weapontype;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $damagetype;


    /**
     * Set idWeapon
     *
     * @param integer $idWeapon
     *
     * @return Weapon
     */
    public function setIdWeapon($idWeapon)
    {
        $this->idWeapon = $idWeapon;

        return $this;
    }

    /**
     * Get idWeapon
     *
     * @return integer
     */
    public function getIdWeapon()
    {
        return $this->idWeapon;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Weapon
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set damages
     *
     * @param string $damages
     *
     * @return Weapon
     */
    public function setDamages($damages)
    {
        $this->damages = $damages;

        return $this;
    }

    /**
     * Get damages
     *
     * @return string
     */
    public function getDamages()
    {
        return $this->damages;
    }

    /**
     * Set damagem
     *
     * @param string $damagem
     *
     * @return Weapon
     */
    public function setDamagem($damagem)
    {
        $this->damagem = $damagem;

        return $this;
    }

    /**
     * Get damagem
     *
     * @return string
     */
    public function getDamagem()
    {
        return $this->damagem;
    }

    /**
     * Set critical
     *
     * @param string $critical
     *
     * @return Weapon
     */
    public function setCritical($critical)
    {
        $this->critical = $critical;

        return $this;
    }

    /**
     * Get critical
     *
     * @return string
     */
    public function getCritical()
    {
        return $this->critical;
    }

    /**
     * Set wprange
     *
     * @param integer $wprange
     *
     * @return Weapon
     */
    public function setWprange($wprange)
    {
        $this->wprange = $wprange;

        return $this;
    }

    /**
     * Get wprange
     *
     * @return integer
     */
    public function getWprange()
    {
        return $this->wprange;
    }

    /**
     * Set weight
     *
     * @param string $weight
     *
     * @return Weapon
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return string
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set weapongroup
     *
     * @param string $weapongroup
     *
     * @return Weapon
     */
    public function setWeapongroup($weapongroup)
    {
        if(checkWeaponGroup){
            $this->weapongroup = $weapongroup;
        }

        return $this;
    }

    /**
     * Get weapongroup
     *
     * @return string
     */
    public function getWeapongroup()
    {
        return $this->weapongroup;
    }

    /**
     * Set weapontype
     *
     * @param string $weapontype
     *
     * @return Weapon
     */
    public function setWeapontype($weapontype)
    {
        $this->weapontype = $weapontype;

        return $this;
    }

    /**
     * Get weapontype
     *
     * @return string
     */
    public function getWeapontype()
    {
        return $this->weapontype;
    }

    /**
     * Set damagetype
     *
     * @param string $damagetype
     *
     * @return Weapon
     */
    public function setDamagetype($damagetype)
    {
        $this->damagetype = $damagetype;

        return $this;
    }

    /**
     * Get damagetype
     *
     * @return string
     */
    public function getDamagetype(){
        return $this->damagetype;
    }
    
    /** 
     * Comprueba que strings solo tengan letras, números y los carácteres () / . , -
     * 
     * @return boolean
     */
     
    public function checkName($name){
         if(preg_match('~[^A-Za-z0-9\(\)\ \,\.\-]~', $name)){
             return false;
         } else {
             return true;
         }
    }
    
    
    /**
     * Comprobar que damageS es o de estilo "1dX", "1dX/1dX", "X" o NULL
     * 
     * 
    
    public function checkDamageS($damage){
         if(preg_match('(^1d)[0-9]{1,2}$|((^1d)[0-9]{1,2}[\/](1d)[0-9]{1,2})|NULL|[0-9]'', $name)){
             return true;
         } else {
             return false;
         }
    }*/
    
    /**
     * Comprobar que weaponGroup es Simple, Martial o Exotic
     * 
     * @return boolean
     * 
     */
    
    public function checkWeaponGroup($wg){
        if(strcmp($wg, "Simple")||strcmp($wg, "Martial")||strcmp($wg, "Exotic")){
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Comprobar que weaponType es Unarmed Attacks, Light Melee Weapons, One-Handed Melee Weapons, Two-Handed Melee Weapons, Ranged Weapons
     * 
     * @return boolean
     */
    
    public function checkWeaponType($wt){
        if(strcmp($wt, "Unarmed Attacks")||strcmp($tg, "Light Melee Weapons")||strcmp($wt, "One-Handed Melee Weapons")||strcmp($wt, "Two-Handed Melee Weapons")||strcmp($wt, "Ranged Melee Weapons")){
            return true;
        } else {
            return false;
        }
    }
}

<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Classes
 * @ORM\Entity
 * @ORM\Table(name="Classes")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ClassesRepository")
 */
class Classes
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $idClass;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $lvl;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $baseattack;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $fortsave;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $refsave;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $willsave;

    /**
     *@ORM\Column(type="integer", nullable=true)
     */
    private $special;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $flurryblows;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $unarmeddamage;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $acbonus;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $unarmedspeed;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $spells0;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $spells1;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $spells2;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $spells3;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $spells4;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $spells5;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $spells6;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $spells7;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $spells8;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $spells9;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $skillpointsperlvl;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $nameclass;


    /**
     * Set idClass
     *
     * @param integer $idClass
     *
     * @return Classes
     */
    public function setIdClass($idClass)
    {
        $this->idClass = $idClass;

        return $this;
    }

    /**
     * Get idClass
     *
     * @return integer
     */
    public function getIdClass()
    {
        return $this->idClass;
    }

    /**
     * Set lvl
     *
     * @param integer $lvl
     *
     * @return Classes
     */
    public function setLvl($lvl)
    {
        $this->lvl = $lvl;

        return $this;
    }

    /**
     * Get lvl
     *
     * @return integer
     */
    public function getLvl()
    {
        return $this->lvl;
    }

    /**
     * Set baseattack
     *
     * @param string $baseattack
     *
     * @return Classes
     */
    public function setBaseattack($baseattack)
    {
        $this->baseattack = $baseattack;

        return $this;
    }

    /**
     * Get baseattack
     *
     * @return string
     */
    public function getBaseattack()
    {
        return $this->baseattack;
    }

    /**
     * Set fortsave
     *
     * @param integer $fortsave
     *
     * @return Classes
     */
    public function setFortsave($fortsave)
    {
        $this->fortsave = $fortsave;

        return $this;
    }

    /**
     * Get fortsave
     *
     * @return integer
     */
    public function getFortsave()
    {
        return $this->fortsave;
    }

    /**
     * Set refsave
     *
     * @param integer $refsave
     *
     * @return Classes
     */
    public function setRefsave($refsave)
    {
        $this->refsave = $refsave;

        return $this;
    }

    /**
     * Get refsave
     *
     * @return integer
     */
    public function getRefsave()
    {
        return $this->refsave;
    }

    /**
     * Set willsave
     *
     * @param integer $willsave
     *
     * @return Classes
     */
    public function setWillsave($willsave)
    {
        $this->willsave = $willsave;

        return $this;
    }

    /**
     * Get willsave
     *
     * @return integer
     */
    public function getWillsave()
    {
        return $this->willsave;
    }

    /**
     * Set special
     *
     * @param string $special
     *
     * @return Classes
     */
    public function setSpecial($special)
    {
        $this->special = $special;

        return $this;
    }

    /**
     * Get special
     *
     * @return string
     */
    public function getSpecial()
    {
        return $this->special;
    }

    /**
     * Set flurryblows
     *
     * @param string $flurryblows
     *
     * @return Classes
     */
    public function setFlurryblows($flurryblows)
    {
        $this->flurryblows = $flurryblows;

        return $this;
    }

    /**
     * Get flurryblows
     *
     * @return string
     */
    public function getFlurryblows()
    {
        return $this->flurryblows;
    }

    /**
     * Set unarmeddamage
     *
     * @param array $unarmeddamage
     *
     * @return Classes
     */
    public function setUnarmeddamage($unarmeddamage)
    {
        $this->unarmeddamage = $unarmeddamage;

        return $this;
    }

    /**
     * Get unarmeddamage
     *
     * @return array
     */
    public function getUnarmeddamage()
    {
        return $this->unarmeddamage;
    }

    /**
     * Set acbonus
     *
     * @param integer $acbonus
     *
     * @return Classes
     */
    public function setAcbonus($acbonus)
    {
        $this->acbonus = $acbonus;

        return $this;
    }

    /**
     * Get acbonus
     *
     * @return integer
     */
    public function getAcbonus()
    {
        return $this->acbonus;
    }

    /**
     * Set unarmedspeed
     *
     * @param integer $unarmedspeed
     *
     * @return Classes
     */
    public function setUnarmedspeed($unarmedspeed)
    {
        $this->unarmedspeed = $unarmedspeed;

        return $this;
    }

    /**
     * Get unarmedspeed
     *
     * @return integer
     */
    public function getUnarmedspeed()
    {
        return $this->unarmedspeed;
    }

    /**
     * Set spells0
     *
     * @param integer $spells0
     *
     * @return Classes
     */
    public function setSpells0($spells0)
    {
        $this->spells0 = $spells0;

        return $this;
    }

    /**
     * Get spells0
     *
     * @return integer
     */
    public function getSpells0()
    {
        return $this->spells0;
    }

    /**
     * Set spells1
     *
     * @param integer $spells1
     *
     * @return Classes
     */
    public function setSpells1($spells1)
    {
        $this->spells1 = $spells1;

        return $this;
    }

    /**
     * Get spells1
     *
     * @return integer
     */
    public function getSpells1()
    {
        return $this->spells1;
    }

    /**
     * Set spells2
     *
     * @param integer $spells2
     *
     * @return Classes
     */
    public function setSpells2($spells2)
    {
        $this->spells2 = $spells2;

        return $this;
    }

    /**
     * Get spells2
     *
     * @return integer
     */
    public function getSpells2()
    {
        return $this->spells2;
    }

    /**
     * Set spells3
     *
     * @param integer $spells3
     *
     * @return Classes
     */
    public function setSpells3($spells3)
    {
        $this->spells3 = $spells3;

        return $this;
    }

    /**
     * Get spells3
     *
     * @return integer
     */
    public function getSpells3()
    {
        return $this->spells3;
    }

    /**
     * Set spells4
     *
     * @param integer $spells4
     *
     * @return Classes
     */
    public function setSpells4($spells4)
    {
        $this->spells4 = $spells4;

        return $this;
    }

    /**
     * Get spells4
     *
     * @return integer
     */
    public function getSpells4()
    {
        return $this->spells4;
    }

    /**
     * Set spells5
     *
     * @param integer $spells5
     *
     * @return Classes
     */
    public function setSpells5($spells5)
    {
        $this->spells5 = $spells5;

        return $this;
    }

    /**
     * Get spells5
     *
     * @return integer
     */
    public function getSpells5()
    {
        return $this->spells5;
    }

    /**
     * Set spells6
     *
     * @param integer $spells6
     *
     * @return Classes
     */
    public function setSpells6($spells6)
    {
        $this->spells6 = $spells6;

        return $this;
    }

    /**
     * Get spells6
     *
     * @return integer
     */
    public function getSpells6()
    {
        return $this->spells6;
    }

    /**
     * Set spells7
     *
     * @param integer $spells7
     *
     * @return Classes
     */
    public function setSpells7($spells7)
    {
        $this->spells7 = $spells7;

        return $this;
    }

    /**
     * Get spells7
     *
     * @return integer
     */
    public function getSpells7()
    {
        return $this->spells7;
    }

    /**
     * Set spells8
     *
     * @param integer $spells8
     *
     * @return Classes
     */
    public function setSpells8($spells8)
    {
        $this->spells8 = $spells8;

        return $this;
    }

    /**
     * Get spells8
     *
     * @return integer
     */
    public function getSpells8()
    {
        return $this->spells8;
    }

    /**
     * Set spells9
     *
     * @param integer $spells9
     *
     * @return Classes
     */
    public function setSpells9($spells9)
    {
        $this->spells9 = $spells9;

        return $this;
    }

    /**
     * Get spells9
     *
     * @return integer
     */
    public function getSpells9()
    {
        return $this->spells9;
    }

    /**
     * Set skillpointsperlvl
     *
     * @param integer $skillpointsperlvl
     *
     * @return Classes
     */
    public function setSkillpointsperlvl($skillpointsperlvl)
    {
        $this->skillpointsperlvl = $skillpointsperlvl;

        return $this;
    }

    /**
     * Get skillpointsperlvl
     *
     * @return integer
     */
    public function getSkillpointsperlvl()
    {
        return $this->skillpointsperlvl;
    }

    /**
     * Set nameclass
     *
     * @param array $nameclass
     *
     * @return Classes
     */
    public function setNameclass($nameclass)
    {
        $this->nameclass = $nameclass;

        return $this;
    }

    /**
     * Get nameclass
     *
     * @return array
     */
    public function getNameclass()
    {
        return $this->nameclass;
    }
}

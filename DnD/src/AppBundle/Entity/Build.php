<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Build
 *
 * @ORM\Table(name="Build", indexes={@ORM\Index(name="id_race_fk", columns={"id_race_fk"})})
 * @ORM\Entity
 */
class Build
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id_build", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $idBuild;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_user_fk", type="integer", nullable=true)
     */
    private $idUserFk;

    /**
     * @var integer
     *
     * @ORM\Column(name="STR", type="integer", nullable=true)
     */
    private $str;

    /**
     * @var integer
     *
     * @ORM\Column(name="DEX", type="integer", nullable=true)
     */
    private $dex;

    /**
     * @var integer
     *
     * @ORM\Column(name="CON", type="integer", nullable=true)
     */
    private $con;

    /**
     * @var integer
     *
     * @ORM\Column(name="INT", type="integer", nullable=true)
     */
    private $int;

    /**
     * @var integer
     *
     * @ORM\Column(name="WIS", type="integer", nullable=true)
     */
    private $wis;

    /**
     * @var integer
     *
     * @ORM\Column(name="CHA", type="integer", nullable=true)
     */
    private $cha;

    /**
     * @var string
     *
     * @ORM\Column(name="deity", type="string", length=50, nullable=true)
     */
    private $deity;

    /**
     * @var array
     *
     * @ORM\Column(name="alignment", type="simple_array", nullable=true)
     */
    private $alignment;

    /**
     * @var string
     *
     * @ORM\Column(name="SpellResistance", type="string", length=50, nullable=true)
     */
    private $spellresistance;

    /**
     * @var \AppBundle\Entity\Race
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Race")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_race_fk", referencedColumnName="id_race")
     * })
     */
    private $idRaceFk;

     /**
     * @var string
     *
     * @ORM\Column(name="languages", type="string", nullable=true)
     */
    private $languages;


    /**
     * Set languages
     *
     * @param array $languages
     *
     * @return Build
     */
    public function setLanguages($languages) {
        $this->alignment = $languages;

        return $this;
    }

    /**
     * Get languages
     *
     * @return string
     */
    public function getLanguages() {
        return $this->languages;
    }



    /**
     * Set idBuild
     *
     * @param integer $idBuild
     *
     * @return Build
     */
    public function setIdBuild($idBuild)
    {
        $this->idBuild = $idBuild;

        return $this;
    }

    /**
     * Get idBuild
     *
     * @return integer
     */
    public function getIdBuild()
    {
        return $this->idBuild;
    }

    /**
     * Set idUserFk
     *
     * @param integer $idUserFk
     *
     * @return Build
     */
    public function setIdUserFk($idUserFk)
    {
        $this->idUserFk = $idUserFk;

        return $this;
    }

    /**
     * Get idUserFk
     *
     * @return integer
     */
    public function getIdUserFk()
    {
        return $this->idUserFk;
    }

    /**
     * Set str
     *
     * @param integer $str
     *
     * @return Build
     */
    public function setStr($str)
    {
        $this->str = $str;

        return $this;
    }

    /**
     * Get str
     *
     * @return integer
     */
    public function getStr()
    {
        return $this->str;
    }

    /**
     * Set dex
     *
     * @param integer $dex
     *
     * @return Build
     */
    public function setDex($dex)
    {
        $this->dex = $dex;

        return $this;
    }

    /**
     * Get dex
     *
     * @return integer
     */
    public function getDex()
    {
        return $this->dex;
    }

    /**
     * Set con
     *
     * @param integer $con
     *
     * @return Build
     */
    public function setCon($con)
    {
        $this->con = $con;

        return $this;
    }

    /**
     * Get con
     *
     * @return integer
     */
    public function getCon()
    {
        return $this->con;
    }

    /**
     * Set int
     *
     * @param integer $int
     *
     * @return Build
     */
    public function setInt($int)
    {
        $this->int = $int;

        return $this;
    }

    /**
     * Get int
     *
     * @return integer
     */
    public function getInt()
    {
        return $this->int;
    }

    /**
     * Set wis
     *
     * @param integer $wis
     *
     * @return Build
     */
    public function setWis($wis)
    {
        $this->wis = $wis;

        return $this;
    }

    /**
     * Get wis
     *
     * @return integer
     */
    public function getWis()
    {
        return $this->wis;
    }

    /**
     * Set cha
     *
     * @param integer $cha
     *
     * @return Build
     */
    public function setCha($cha)
    {
        $this->cha = $cha;

        return $this;
    }

    /**
     * Get cha
     *
     * @return integer
     */
    public function getCha()
    {
        return $this->cha;
    }

    /**
     * Set deity
     *
     * @param string $deity
     *
     * @return Build
     */
    public function setDeity($deity)
    {
        $this->deity = $deity;

        return $this;
    }

    /**
     * Get deity
     *
     * @return string
     */
    public function getDeity()
    {
        return $this->deity;
    }

    /**
     * Set alignment
     *
     * @param array $alignment
     *
     * @return Build
     */
    public function setAlignment($alignment)
    {
        $this->alignment = $alignment;

        return $this;
    }

    /**
     * Get alignment
     *
     * @return array
     */
    public function getAlignment()
    {
        return $this->alignment;
    }

    /**
     * Set spellresistance
     *
     * @param string $spellresistance
     *
     * @return Build
     */
    public function setSpellresistance($spellresistance)
    {
        $this->spellresistance = $spellresistance;

        return $this;
    }

    /**
     * Get spellresistance
     *
     * @return string
     */
    public function getSpellresistance()
    {
        return $this->spellresistance;
    }

    /**
     * Set idRaceFk
     *
     * @param \AppBundle\Entity\Race $idRaceFk
     *
     * @return Build
     */
    public function setIdRaceFk(\AppBundle\Entity\Race $idRaceFk = null)
    {
        $this->idRaceFk = $idRaceFk;

        return $this;
    }

    /**
     * Get idRaceFk
     *
     * @return \AppBundle\Entity\Race
     */
    public function getIdRaceFk()
    {
        return $this->idRaceFk;
    }
}

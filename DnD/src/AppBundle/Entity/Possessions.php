<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Possessions
 * @ORM\Entity
 * @ORM\Table(name="Possessions")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PossessionsRepository")
 */
class Possessions
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $idPossessions;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $itemname;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $page;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $weight;

    /**
     * @var \AppBundle\Entity\Build
     */
    private $idBuildFk;


    /**
     * Set idPossessions
     *
     * @param integer $idPossessions
     *
     * @return Possessions
     */
    public function setIdPossessions($idPossessions)
    {
        $this->idPossessions = $idPossessions;

        return $this;
    }

    /**
     * Get idPossessions
     *
     * @return integer
     */
    public function getIdPossessions()
    {
        return $this->idPossessions;
    }

    /**
     * Set itemname
     *
     * @param string $itemname
     *
     * @return Possessions
     */
    public function setItemname($itemname)
    {
        $this->itemname = $itemname;

        return $this;
    }

    /**
     * Get itemname
     *
     * @return string
     */
    public function getItemname()
    {
        return $this->itemname;
    }

    /**
     * Set page
     *
     * @param string $page
     *
     * @return Possessions
     */
    public function setPage($page)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * Get page
     *
     * @return string
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Set weight
     *
     * @param string $weight
     *
     * @return Possessions
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return string
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set idBuildFk
     *
     * @param \AppBundle\Entity\Build $idBuildFk
     *
     * @return Possessions
     */
    public function setIdBuildFk(\AppBundle\Entity\Build $idBuildFk = null)
    {
        $this->idBuildFk = $idBuildFk;

        return $this;
    }

    /**
     * Get idBuildFk
     *
     * @return \AppBundle\Entity\Build
     */
    public function getIdBuildFk()
    {
        return $this->idBuildFk;
    }
}

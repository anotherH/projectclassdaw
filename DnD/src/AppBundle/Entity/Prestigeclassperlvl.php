<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Prestigeclassperlvl
 * @ORM\Entity
 * @ORM\Table(name="PrestigeClassPerLvl")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ArmorRepository")
 */
class Prestigeclassperlvl
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idPrestigeclassperlvl;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $lvl;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $attackbonus;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $fortsave;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $refsave;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $willsave;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $special;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $spellsperday;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $spells1;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $spells2;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $spells3;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $spells4;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $bonusspell;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $acbonus;

    /**
     * @var \AppBundle\Entity\Prestigeclass
     * 
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Prestigeclass")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_prestigeclass_fk", referencedColumnName="id_prestige_class")
     * })
     */
    private $idPrestigeclassFk;


    /**
     * Set idPrestigeclassperlvl
     *
     * @param integer $idPrestigeclassperlvl
     *
     * @return Prestigeclassperlvl
     */
    public function setIdPrestigeclassperlvl($idPrestigeclassperlvl)
    {
        $this->idPrestigeclassperlvl = $idPrestigeclassperlvl;

        return $this;
    }

    /**
     * Get idPrestigeclassperlvl
     *
     * @return integer
     */
    public function getIdPrestigeclassperlvl()
    {
        return $this->idPrestigeclassperlvl;
    }

    /**
     * Set lvl
     *
     * @param integer $lvl
     *
     * @return Prestigeclassperlvl
     */
    public function setLvl($lvl)
    {
        $this->lvl = $lvl;

        return $this;
    }

    /**
     * Get lvl
     *
     * @return integer
     */
    public function getLvl()
    {
        return $this->lvl;
    }

    /**
     * Set attackbonus
     *
     * @param string $attackbonus
     *
     * @return Prestigeclassperlvl
     */
    public function setAttackbonus($attackbonus)
    {
        $this->attackbonus = $attackbonus;

        return $this;
    }

    /**
     * Get attackbonus
     *
     * @return string
     */
    public function getAttackbonus()
    {
        return $this->attackbonus;
    }

    /**
     * Set fortsave
     *
     * @param integer $fortsave
     *
     * @return Prestigeclassperlvl
     */
    public function setFortsave($fortsave)
    {
        $this->fortsave = $fortsave;

        return $this;
    }

    /**
     * Get fortsave
     *
     * @return integer
     */
    public function getFortsave()
    {
        return $this->fortsave;
    }

    /**
     * Set refsave
     *
     * @param integer $refsave
     *
     * @return Prestigeclassperlvl
     */
    public function setRefsave($refsave)
    {
        $this->refsave = $refsave;

        return $this;
    }

    /**
     * Get refsave
     *
     * @return integer
     */
    public function getRefsave()
    {
        return $this->refsave;
    }

    /**
     * Set willsave
     *
     * @param integer $willsave
     *
     * @return Prestigeclassperlvl
     */
    public function setWillsave($willsave)
    {
        $this->willsave = $willsave;

        return $this;
    }

    /**
     * Get willsave
     *
     * @return integer
     */
    public function getWillsave()
    {
        return $this->willsave;
    }

    /**
     * Set special
     *
     * @param string $special
     *
     * @return Prestigeclassperlvl
     */
    public function setSpecial($special)
    {
        $this->special = $special;

        return $this;
    }

    /**
     * Get special
     *
     * @return string
     */
    public function getSpecial()
    {
        return $this->special;
    }

    /**
     * Set spellsperday
     *
     * @param string $spellsperday
     *
     * @return Prestigeclassperlvl
     */
    public function setSpellsperday($spellsperday)
    {
        $this->spellsperday = $spellsperday;

        return $this;
    }

    /**
     * Get spellsperday
     *
     * @return string
     */
    public function getSpellsperday()
    {
        return $this->spellsperday;
    }

    /**
     * Set spells1
     *
     * @param integer $spells1
     *
     * @return Prestigeclassperlvl
     */
    public function setSpells1($spells1)
    {
        $this->spells1 = $spells1;

        return $this;
    }

    /**
     * Get spells1
     *
     * @return integer
     */
    public function getSpells1()
    {
        return $this->spells1;
    }

    /**
     * Set spells2
     *
     * @param integer $spells2
     *
     * @return Prestigeclassperlvl
     */
    public function setSpells2($spells2)
    {
        $this->spells2 = $spells2;

        return $this;
    }

    /**
     * Get spells2
     *
     * @return integer
     */
    public function getSpells2()
    {
        return $this->spells2;
    }

    /**
     * Set spells3
     *
     * @param integer $spells3
     *
     * @return Prestigeclassperlvl
     */
    public function setSpells3($spells3)
    {
        $this->spells3 = $spells3;

        return $this;
    }

    /**
     * Get spells3
     *
     * @return integer
     */
    public function getSpells3()
    {
        return $this->spells3;
    }

    /**
     * Set spells4
     *
     * @param integer $spells4
     *
     * @return Prestigeclassperlvl
     */
    public function setSpells4($spells4)
    {
        $this->spells4 = $spells4;

        return $this;
    }

    /**
     * Get spells4
     *
     * @return integer
     */
    public function getSpells4()
    {
        return $this->spells4;
    }

    /**
     * Set bonusspell
     *
     * @param integer $bonusspell
     *
     * @return Prestigeclassperlvl
     */
    public function setBonusspell($bonusspell)
    {
        $this->bonusspell = $bonusspell;

        return $this;
    }

    /**
     * Get bonusspell
     *
     * @return integer
     */
    public function getBonusspell()
    {
        return $this->bonusspell;
    }

    /**
     * Set acbonus
     *
     * @param integer $acbonus
     *
     * @return Prestigeclassperlvl
     */
    public function setAcbonus($acbonus)
    {
        $this->acbonus = $acbonus;

        return $this;
    }

    /**
     * Get acbonus
     *
     * @return integer
     */
    public function getAcbonus()
    {
        return $this->acbonus;
    }

    /**
     * Set idPrestigeclassFk
     *
     * @param \AppBundle\Entity\Prestigeclass $idPrestigeclassFk
     *
     * @return Prestigeclassperlvl
     */
    public function setIdPrestigeclassFk(\AppBundle\Entity\Prestigeclass $idPrestigeclassFk = null)
    {
        $this->idPrestigeclassFk = $idPrestigeclassFk;

        return $this;
    }

    /**
     * Get idPrestigeclassFk
     *
     * @return \AppBundle\Entity\Prestigeclass
     */
    public function getIdPrestigeclassFk()
    {
        return $this->idPrestigeclassFk;
    }
}

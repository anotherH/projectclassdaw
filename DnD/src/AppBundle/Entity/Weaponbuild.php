<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Weaponbuild
 * @ORM\Entity
 * @ORM\Table(name="WeaponBuild")
 */
class Weaponbuild
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $idWeaponBuild;
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $attack;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $attackbonus;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $notes;

    /**
     * @var \AppBundle\Entity\Weapon
     */
    private $idWeaponFk;

    /**
     * @var \AppBundle\Entity\Build
     */
    private $idBuildFk;


    /**
     * Set attack
     *
     * @param integer $attack
     *
     * @return Weaponbuild
     */
    public function setAttack($attack)
    {
        $this->attack = $attack;

        return $this;
    }

    /**
     * Get attack
     *
     * @return integer
     */
    public function getAttack()
    {
        return $this->attack;
    }

    /**
     * Set attackbonus
     *
     * @param integer $attackbonus
     *
     * @return Weaponbuild
     */
    public function setAttackbonus($attackbonus)
    {
        $this->attackbonus = $attackbonus;

        return $this;
    }

    /**
     * Get attackbonus
     *
     * @return integer
     */
    public function getAttackbonus()
    {
        return $this->attackbonus;
    }

    /**
     * Set notes
     *
     * @param string $notes
     *
     * @return Weaponbuild
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes
     *
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set idWeaponFk
     *
     * @param \AppBundle\Entity\Weapon $idWeaponFk
     *
     * @return Weaponbuild
     */
    public function setIdWeaponFk(\AppBundle\Entity\Weapon $idWeaponFk = null)
    {
        $this->idWeaponFk = $idWeaponFk;

        return $this;
    }

    /**
     * Get idWeaponFk
     *
     * @return \AppBundle\Entity\Weapon
     */
    public function getIdWeaponFk()
    {
        return $this->idWeaponFk;
    }

    /**
     * Set idBuildFk
     *
     * @param \AppBundle\Entity\Build $idBuildFk
     *
     * @return Weaponbuild
     */
    public function setIdBuildFk(\AppBundle\Entity\Build $idBuildFk = null)
    {
        $this->idBuildFk = $idBuildFk;

        return $this;
    }

    /**
     * Get idBuildFk
     *
     * @return \AppBundle\Entity\Build
     */
    public function getIdBuildFk()
    {
        return $this->idBuildFk;
    }

    /**
     * Get idWeaponBuild
     *
     * @return integer
     */
    public function getIdWeaponBuild()
    {
        return $this->idWeaponBuild;
    }
}

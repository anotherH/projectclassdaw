<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Armor
 * @ORM\Entity
 * @ORM\Table(name="Armor")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ArmorRepository")
 */
 
class Armor
{
   /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $idArmor;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $armortype;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $acbonus;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $maxdex;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $checkpenalty;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $spellfailure;

    /**
     * @ORM\Column(type="decimal", nullable=true)
     */
    private $weight;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $ismetal;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $specialproperties;


    /**
     * Set idArmor
     *
     * @param integer $idArmor
     *
     * @return Armor
     */
    public function setIdArmor($idArmor)
    {
        $this->idArmor = $idArmor;

        return $this;
    }

    /**
     * Get idArmor
     *
     * @return integer
     */
    public function getIdArmor()
    {
        return $this->idArmor;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Armor
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set armortype
     *
     * @param array $armortype
     *
     * @return Armor
     */
    public function setArmortype($armortype)
    {
        $this->armortype = $armortype;

        return $this;
    }

    /**
     * Get armortype
     *
     * @return array
     */
    public function getArmortype()
    {
        return $this->armortype;
    }

    /**
     * Set acbonus
     *
     * @param integer $acbonus
     *
     * @return Armor
     */
    public function setAcbonus($acbonus)
    {
        $this->acbonus = $acbonus;

        return $this;
    }

    /**
     * Get acbonus
     *
     * @return integer
     */
    public function getAcbonus()
    {
        return $this->acbonus;
    }

    /**
     * Set maxdex
     *
     * @param integer $maxdex
     *
     * @return Armor
     */
    public function setMaxdex($maxdex)
    {
        $this->maxdex = $maxdex;

        return $this;
    }

    /**
     * Get maxdex
     *
     * @return integer
     */
    public function getMaxdex()
    {
        return $this->maxdex;
    }

    /**
     * Set checkpenalty
     *
     * @param integer $checkpenalty
     *
     * @return Armor
     */
    public function setCheckpenalty($checkpenalty)
    {
        $this->checkpenalty = $checkpenalty;

        return $this;
    }

    /**
     * Get checkpenalty
     *
     * @return integer
     */
    public function getCheckpenalty()
    {
        return $this->checkpenalty;
    }

    /**
     * Set spellfailure
     *
     * @param string $spellfailure
     *
     * @return Armor
     */
    public function setSpellfailure($spellfailure)
    {
        $this->spellfailure = $spellfailure;

        return $this;
    }

    /**
     * Get spellfailure
     *
     * @return string
     */
    public function getSpellfailure()
    {
        return $this->spellfailure;
    }

    /**
     * Set weight
     *
     * @param string $weight
     *
     * @return Armor
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return string
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set ismetal
     *
     * @param boolean $ismetal
     *
     * @return Armor
     */
    public function setIsmetal($ismetal)
    {
        $this->ismetal = $ismetal;

        return $this;
    }

    /**
     * Get ismetal
     *
     * @return boolean
     */
    public function getIsmetal()
    {
        return $this->ismetal;
    }

    /**
     * Set specialproperties
     *
     * @param string $specialproperties
     *
     * @return Armor
     */
    public function setSpecialproperties($specialproperties)
    {
        $this->specialproperties = $specialproperties;

        return $this;
    }

    /**
     * Get specialproperties
     *
     * @return string
     */
    public function getSpecialproperties()
    {
        return $this->specialproperties;
    }
}

<?php
	
	namespace AppBundle\Repository;
	
	use Doctrine\ORM\EntityRepository;
	
	class ClassSkillsRepository extends EntityRepository{
		
		function selectIdByClass($name){
			return $this->getEntityManager()->createQuery("SELECT DISTINCT IDENTITY(classSkills.id_skill_fk) as skillId FROM AppBundle:Classskills classSkills WHERE classSkills.nameclass = '".$name."'")->getArrayResult();
		}
	}
?>
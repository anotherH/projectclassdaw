<?php
	
	namespace AppBundle\Repository;
	
	use Doctrine\ORM\EntityRepository;
	
	class FeatRepository extends EntityRepository{
		function getFeatInfo(){
			return $this->getEntityManager()->createQuery("SELECT DISTINCT feat FROM AppBundle:Feat feat")->getArrayResult();
		}
		
		function getFeatName($name){
			return $this->getEntityManager()->createQuery("SELECT DISTINCT feat FROM AppBundle:Feat feat WHERE feat.shortdescription LIKE '%". $name ."%'")->getArrayResult();
		}
		
		function getFeatType($type){
			return $this->getEntityManager()->createQuery("SELECT DISTINCT feat FROM AppBundle:Feat feat WHERE feat.feattype = '". $type ."'")->getArrayResult();
		}
		
		function getFeatFullSearch($name, $type){
			return $this->getEntityManager()->createQuery("SELECT DISTINCT feat FROM AppBundle:Feat feat WHERE feat.shortdescription LIKE '%". $name ."%' AND feat.feattype = '". $type ."'")->getArrayResult();
		}
	}
	
?>
<?php
	
	namespace AppBundle\Repository;
	
	use Doctrine\ORM\EntityRepository;
	
	class ArmorRepository extends EntityRepository{
		function selectNames(){
			return $this->getEntityManager()->createQuery("SELECT DISTINCT armor.name FROM AppBundle:Armor armor")->getArrayResult();
		}
		
		function selectByName($name){
			return $this->getEntityManager()->createQuery("SELECT DISTINCT armor FROM AppBundle:Armor armor WHERE armor.name = '". $name ."'")->getArrayResult();
		}
	}
	
?>
<?php
	
	namespace AppBundle\Repository;
	
	use Doctrine\ORM\EntityRepository;
	
	class SkillRepository extends EntityRepository{
		
		function selectNamesById($id){
			return $this->getEntityManager()->createQuery("SELECT DISTINCT skill.skillname FROM AppBundle:Skill skill WHERE skill.idSkill = " . $id['skillId'])->getArrayResult();
		}
	}
?>
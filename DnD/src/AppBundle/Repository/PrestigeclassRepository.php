<?php
	
	namespace AppBundle\Repository;
	
	use Doctrine\ORM\EntityRepository;
	
	class PrestigeclassRepository extends EntityRepository{
		function getPrestigeInfo(){
			return $this->getEntityManager()->createQuery("SELECT DISTINCT prestige FROM AppBundle:Prestigeclass prestige")->getArrayResult();
		}
		
		function selectHitDie(){
			return $this->getEntityManager()->createQuery("SELECT DISTINCT prestige.hitdie FROM AppBundle:Prestigeclass prestige")->getArrayResult();
		}
		
		function getPrestigeName($name){
			return $this->getEntityManager()->createQuery("SELECT DISTINCT prestige FROM AppBundle:Prestigeclass prestige WHERE prestige.name LIKE '%". $name ."%'")->getArrayResult();	
		}
		
		function getPrestigeHitDie($hitdie){
			return $this->getEntityManager()->createQuery("SELECT DISTINCT prestige FROM AppBundle:Prestigeclass prestige WHERE prestige.hitdie IN ('". $hitdie ."')")->getArrayResult();	
		}
		
		function getPrestigeFullSearch($name, $hitdie){
			return $this->getEntityManager()->createQuery("SELECT DISTINCT prestige FROM AppBundle:Prestigeclass prestige WHERE prestige.name LIKE '%". $name. "%' AND prestige.hitdie IN ('". $hitdie ."')")->getArrayResult();	
		}
	}
	
?>
<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * @author Eric Amoros
 */
class ClassesRepository extends EntityRepository {

    function selectAllNames() {
        return $this->getEntityManager()->createQuery("SELECT DISTINCT class.nameclass FROM AppBundle:Classes class")->getArrayResult();
    }

    function selectLvl() {
        return $this->getEntityManager()->createQuery("SELECT DISTINCT class.lvl FROM AppBundle:Classes class")->getArrayResult();
    }

    function getClassParametersPerLvl($name, $lvl) {
        return $this->getEntityManager()->createQuery("SELECT class FROM AppBundle:Classes class WHERE class.nameclass = '" . $name . "' AND class.lvl = '" . $lvl . "'")->getArrayResult();
    }

}

?>
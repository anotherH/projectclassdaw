<?php
	
	namespace AppBundle\Repository;
	
	use Doctrine\ORM\EntityRepository;
	
	class WeaponRepository extends EntityRepository{
		function selectNames(){
			return $this->getEntityManager()->createQuery("SELECT DISTINCT weapon.name FROM AppBundle:Weapon weapon")->getArrayResult();
		}
		
		function selectByName($name){
			return $this->getEntityManager()->createQuery("SELECT DISTINCT weapon FROM AppBundle:Weapon weapon WHERE weapon.name = '". $name ."'")->getArrayResult();
		}
	}
?>
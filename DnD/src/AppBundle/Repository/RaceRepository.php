<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Repository for  Race
 * @author Sara Henriques
 */
class RaceRepository extends EntityRepository {

    /**
     * @return array with race names
     */
    function selectAllRaces() {
        return $this->getEntityManager()->createQuery("SELECT race.name"
                        . " FROM AppBundle:Race race")->getArrayResult();
    }

    /**
     * @param string $name of the race
     * @return array with race traits
     */
    function getTraits($name) {
        return $this->getEntityManager()->createQuery("SELECT race.traits"
                        . " FROM AppBundle:Race race WHERE race.name = '" . $name . "'")->getArrayResult();
    }

    /**
     * @param string $name of the race
     * @return array with race size
     */
    function getSize($name) {
        return $this->getEntityManager()->createQuery("SELECT race.size"
                        . " FROM AppBundle:Race race WHERE race.name = '" . $name . "'")->getArrayResult();
    }

    /**
     * @param string $name of the race
     * @return array with race speed
     */
    function getSpeed($name) {
        return $this->getEntityManager()->createQuery("SELECT race.speedmod"
                        . " FROM AppBundle:Race race WHERE race.name = '" . $name . "'")->getArrayResult();
    }

    /**
     * @param string $name of the race
     * @return array with favored class
     */
    function getFavClass($name) {
        return $this->getEntityManager()->createQuery("SELECT race.favoredclass"
                        . " FROM AppBundle:Race race WHERE race.name = '" . $name . "'")->getArrayResult();
    }

    /**
     * @param string $name of the race
     * @return array with race automatic languages
     */
    function getAutomaticLanguages($name) {
        return $this->getEntityManager()->createQuery("SELECT race.automaticlanguages"
                        . " FROM AppBundle:Race race WHERE race.name = '" . $name . "'")->getArrayResult();
    }

    
    /**
     * @param string $name of the race
     * @return array with race bonus languages
     */
    function getBonusLanguages($name) {
        return $this->getEntityManager()->createQuery("SELECT race.bonuslanguages"
                        . " FROM AppBundle:Race race WHERE race.name = '" . $name . "'")->getArrayResult();
    }

    
    /**
     * @param string $name of the race
     * @return array with race adjustments
     */
    function getRaceAdjustments($name) {
        return $this->getEntityManager()->createQuery("SELECT race.abilityadjustments"
                        . " FROM AppBundle:Race race WHERE race.name = '" . $name . "'")->getArrayResult();
    }

}

?>
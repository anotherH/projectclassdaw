<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller {

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request) {

        // replace this example code with whatever you need
        return $this->render('default/index.html.twig');
    }

    /**
     * Page to create a new build, edit a new one or view one.
     * @Route("/build/{id}", name="createbuild")
     * @param type $id
     * @return view
     * @author Sara Henriques
     */
    public function buildAction($id = null) {
        //get sesion para ver si esta editando 
        $races = $this->getDoctrine()->getRepository('AppBundle:Race')->selectAllRaces();

        $names = $this->getDoctrine()->getRepository('AppBundle:Classes')->selectAllNames();

        //lvls depend of the class. Mostly because of prestige classes, otherwise it's always lvl 20.
        $lvls = $this->getDoctrine()->getRepository('AppBundle:Classes')->selectLvl();

        return $this->render('default/createBuild.html.twig', array(
                    'races' => $races,
                    'classes' => $names,
                    'lvls' => $lvls
        ));
    }

    /**
     * Save new Build
     * @Route("/saveBuild")
     * @return \Symfony\Component\HttpFoundation\Response
     * @author Sara Henriques
     */
    public function saveBuild(Request $request) {
        $request = Request::createFromGlobals();
        $title = $request->request->get('titleOfBuild');
        $race = $request->request->get('race');
        $alignment = $request->request->get('alignment');
        $deity = $request->request->get('deity');
        $classes = $request->request->get('classes');
        $classlvls = $request->request->get('classlvls');
        $str = $request->request->get('str');
        $strOther = $request->request->get('strOther');
        $dex = $request->request->get('dex');
        $dexOther = $request->request->get('dexOther');
        $con = $request->request->get('con');
        $conOther = $request->request->get('conOther');
        $int = $request->request->get('int');
        $intOther = $request->request->get('intOther');
        $wis = $request->request->get('wis');
        $wisOther = $request->request->get('wisOther');
        $cha = $request->request->get('cha');
        $chaOther = $request->request->get('chaOther');
        $acNatural = $request->request->get('$acNatural');
        $acDeflect = $request->request->get('acDeflect');
        $acMisc = $request->request->get('acMisc');
        $iniciativeMisc = $request->request->get('iniciativeMisc');
        $fortitudeMisc = $request->request->get('fortitudeMisc');
        $reflexMisc = $request->request->get('reflexMisc');
        $willMisc = $request->request->get('willMisc');
        $grapple = $request->request->get('grapple');
        $possessions = $request->request->get('possessions');
        $bonusLanguages = $request->request->get('bonusLanguages');
        $buildExplanation = $request->request->get('buildExplanation');


        if (empty($title)) {
            $title = 'untitled';
        }

        $response = new Response(json_encode(array($title,
                    $race,
                    $alignment,
                    $deity,
                    $classes,
                    $classlvls,
                    $str,
                    $strOther,
                    $dex,
                    $dexOther,
                    $con,
                    $conOther,
                    $int,
                    $intOther,
                    $wis,
                    $wisOther,
                    $cha,
                    $chaOther,
                    $acNatural,
                    $acDeflect,
                    $acMisc,
                    $iniciativeMisc,
                    $fortitudeMisc,
                    $reflexMisc,
                    $willMisc,
                    $grapple,
                    $possessions,
                    $bonusLanguages,
                    $buildExplanation)));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /** RACE SERVICES FOR AJAX * */

    /**
     * Service that returns the traits of the selected race
     * @Route("/racetraits/{race}")
     * @param $race
     * @return \Symfony\Component\HttpFoundation\Response
     * @author Sara Henriques
     */
    public function getRaceTraits($race) {
        $traits = $this->getDoctrine()->getRepository('AppBundle:Race')->getTraits($race);

        $response = new Response(json_encode($traits[0]['traits']));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * Service that returns the size of the selected race
     * @Route("/racesize/{race}")
     * @param $race
     * @return \Symfony\Component\HttpFoundation\Response
     * @author Sara Henriques
     */
    public function getRaceSize($race) {
        $size = $this->getDoctrine()->getRepository('AppBundle:Race')->getSize($race);

        $response = new Response(json_encode($size[0]["size"]));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * Service that returns the speed of the selected race
     * @Route("/racespeed/{race}")
     * @param $race
     * @return \Symfony\Component\HttpFoundation\Response
     * @author Sara Henriques
     */
    public function getRaceSpeed($race) {
        $speed = $this->getDoctrine()->getRepository('AppBundle:Race')->getSpeed($race);

        $response = new Response(json_encode($speed[0]["speedmod"]));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     *  Service that returns the favored class of the selected race
     * @Route("/racefavclass/{race}")
     * @param $race
     * @return \Symfony\Component\HttpFoundation\Response
     * @author Sara Henriques
     */
    public function getFavClass($race) {
        $favClass = $this->getDoctrine()->getRepository('AppBundle:Race')->getFavClass($race);

        $response = new Response(json_encode($favClass[0]["favoredclass"]));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * Service that returns the race languages of the selected race
     * @Route("/raceLanguages/{race}")
     * @param $race
     * @return \Symfony\Component\HttpFoundation\Response
     * @author Sara Henriques
     */
    public function getRaceLanguages($race) {
        $automaticlanguages = $this->getDoctrine()->getRepository('AppBundle:Race')->getAutomaticLanguages($race);
        $bonuslanguages = $this->getDoctrine()->getRepository('AppBundle:Race')->getBonusLanguages($race);

        //cleaning up white spaces
        $automaticlanguages = str_replace(' ', '', $automaticlanguages[0]["automaticlanguages"]);
        $bonuslanguages = str_replace(' ', '', $bonuslanguages[0]["bonuslanguages"]);

        //extract it to array
        $autoArray = explode(',', $automaticlanguages);
        $bonusArray = explode(',', $bonuslanguages);

        $languages = array("auto" => $autoArray, "bonus" => $bonusArray);


        $response = new Response(json_encode($languages));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * Service that returns the race adjustments of the selected race
     * @Route("/raceAdjustments/{race}")
     * @param $race
     * @return \Symfony\Component\HttpFoundation\Response
     * @author Sara Henriques
     */
    public function getRaceAdjustments($race) {
        $adjustments = $this->getDoctrine()->getRepository('AppBundle:Race')->getRaceAdjustments($race);

        //cleaning up white spaces
        $adjustments = str_replace(' ', '', $adjustments[0]["abilityadjustments"]);

        //extract it to array
        $autoArray = explode(',', $adjustments);


        $response = new Response(json_encode($autoArray));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /** CLASSES SERVICES FOR AJAX * */

    /**
     * @Route("/buildClass/{name},{lvl}")
     * @param $name
     * @param $lvl
     * @return \Symfony\Component\HttpFoundation\Response
     * @author Eric Amoros
     */
    public function buildClass($name, $lvl) {
        $stats = $this->getDoctrine()->getRepository('AppBundle:Classes')->getClassParametersPerLvl($name, $lvl);

        $response = new Response(json_encode($stats));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}

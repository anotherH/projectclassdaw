<?php

	namespace AppBundle\Controller;

	use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
	use Symfony\Bundle\FrameworkBundle\Controller\Controller;
	use Symfony\Component\HttpFoundation\Response;
	
	class BuildController extends Controller{

	    /**
	     * @Route("/class", name = "classPage")
	     */
	     
	    public function classesAction(){
	    	$names = $this->getDoctrine()->getRepository('AppBundle:Classes')->selectAllNames();
	    	$lvls = $this->getDoctrine()->getRepository('AppBundle:Classes')->selectLvl();
	      
	      return $this->render('default/build.html.twig', array(
            'classes' => $names,
            'lvls' => $lvls
        ));
	    }
	    
	    /**
	     * @Route("/buildClass/{name},{lvl}")
	     * @param $name
	     * @param $lvl
	     * @return \Symfony\Component\HttpFoundation\Response
	     */
	     
	    public function buildClass($name, $lvl){
	    	$stats = $this->getDoctrine()->getRepository('AppBundle:Classes')->getClassParametersPerLvl($name, $lvl);
	      
	      $response = new Response(json_encode($stats));
    		$response->headers->set('Content-Type', 'application/json');

    		return $response;
	    }
	    
	   /**
     * @Route("/ClassSkills/{name}", name = "classSkillPage")
     * @param $name
     * @return \Symfony\Component\HttpFoundation\Response
     */
     
    public function classSkillAction($name){
    	$skillsId = $this->getDoctrine()->getRepository('AppBundle:Classskills')->selectIdByClass($name);
			$skills = [];
			
			foreach($skillsId as $id){
				$skill = $this->getDoctrine()->getRepository('AppBundle:Skill')->selectNamesById($id);
				$skills[] = $skill;
			}
			
			$response = new Response(json_encode($skills));
  		$response->headers->set('Content-Type', 'application/json');

  		return $response;
    }
    
    /**
     * @Route("/weapons", name = "weaponsPage")
     * @return \Symfony\Component\HttpFoundation\Response
     */
     
    public function weaponAction(){
    	$weapons = $this->getDoctrine()->getRepository('AppBundle:Weapon')->selectNames();
    	
    	$response = new Response(json_encode($weapons));
  		$response->headers->set('Content-Type', 'application/json');

  		return $response;
    }
    
    /**
     * @Route("/weapon/{name}", name = "weaponNamePage")
     * @param $name
     * @return \Symfony\Component\HttpFoundation\Response
     */
     
    public function weaponNameAction($name){
    	$weapon = $this->getDoctrine()->getRepository('AppBundle:Weapon')->selectByName($name);
    	
		$response = new Response(json_encode($weapon));
  		$response->headers->set('Content-Type', 'application/json');

  		return $response;
    }
    
    /**
     * @Route("/armors", name = "armorPage")
     * @return \Symfony\Component\HttpFoundation\Response
     */
     
    public function armorAction(){
    	$armors = $this->getDoctrine()->getRepository('AppBundle:Armor')->selectNames();
    	
    	$response = new Response(json_encode($armors));
  		$response->headers->set('Content-Type', 'application/json');

  		return $response;
    }
    
    /**
     * @Route("/armor/{name}", name = "armorNamePage")
     * @param $name
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function armorNameAction($name){
    	$armor = $this->getDoctrine()->getRepository('AppBundle:Armor')->selectByName($name);
			
			$response = new Response(json_encode($armor));
  		$response->headers->set('Content-Type', 'application/json');

  		return $response;
    }
	}
?>
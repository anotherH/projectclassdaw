<?php

	namespace AppBundle\Controller;

	use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
	use Symfony\Bundle\FrameworkBundle\Controller\Controller;
	use Symfony\Component\HttpFoundation\Response;
	
	class SearcherController extends Controller{

        /**
         * @Route("/prestigeSearcher", name = "prestigeSearcherPage")
         */
     
        public function prestigeSearcherAction(){
        	$hitdie = $this->getDoctrine()->getRepository('AppBundle:Prestigeclass')->selectHitDie();
          $prestige = $this->getDoctrine()->getRepository('AppBundle:Prestigeclass')->getPrestigeInfo();
    			
    			for($i = 0; $i < count($hitdie); $i++)
    				$hitdie[$i] = $hitdie[$i]['hitdie'];
    				
          return $this->render('default/prestigeSearcher.html.twig', array(
              'hitdies' => $hitdie,
              'prestiges' => $prestige
          ));
        }
        
        /**
         * @Route("/prestigeclassname/{name}") 
         * @param $name
         * @return \Symfony\Component\HttpFoundation\Response
         */
         
        public function prestigeNameAction($name){
            $prestige = $this->getDoctrine()->getRepository('AppBundle:Prestigeclass')->getPrestigeName($name);
            
            $response = new Response(json_encode($prestige));
    		$response->headers->set('Content-Type', 'application/json');

    		return $response;
        }
        
        /**
         * @Route("/prestigeclasshitdie/{hitdie}") 
         * @param $hitdie
         * @return \Symfony\Component\HttpFoundation\Response
         */
         
        public function prestigeHitDieAction($hitdie){
            $prestige = $this->getDoctrine()->getRepository('AppBundle:Prestigeclass')->getPrestigeHitDie($hitdie);
            
            $response = new Response(json_encode($prestige));
    		$response->headers->set('Content-Type', 'application/json');

    		return $response;
        }
        
        /**
         * @Route("/prestigeclass/{name}&{hitdie}") 
         * @param $name
         * @param $hitdie
         * @return \Symfony\Component\HttpFoundation\Response
         */
         
        public function prestigeFullSearchAction($name, $hitdie){
            $prestige = $this->getDoctrine()->getRepository('AppBundle:Prestigeclass')->getPrestigeFullSearch($name, $hitdie);
            
            $response = new Response(json_encode($prestige));
    		$response->headers->set('Content-Type', 'application/json');

    		return $response;
        }
        
      /**
       * @Route("/featSearcher", name = "featSearcherPage")
       */
      
      public function featSearcher() {
            $feats = $this->getDoctrine()->getRepository('AppBundle:Feat')->getFeatInfo();
      
            return $this->render('default/featSearcher.html.twig', array(
                  'feats' => $feats
            ));
      }
	
	/**
         * @Route("/featname/{name}") 
         * @param $name
         * @return \Symfony\Component\HttpFoundation\Response
         */
         
        public function featNameAction($name){
            $feat = $this->getDoctrine()->getRepository('AppBundle:Feat')->getFeatName($name);
            
            $response = new Response(json_encode($feat));
    		$response->headers->set('Content-Type', 'application/json');

    		return $response;
        }
        
        /**
         * @Route("/feattype/{type}") 
         * @param type
         * @return \Symfony\Component\HttpFoundation\Response
         */
         
        public function featTypeAction($type){
            $feat = $this->getDoctrine()->getRepository('AppBundle:Feat')->getFeatType($type);
            
            $response = new Response(json_encode($feat));
    		$response->headers->set('Content-Type', 'application/json');

    		return $response;
        }
        
        /**
         * @Route("/feat/{name}&{type}") 
         * @param name
         * @param type
         * @return \Symfony\Component\HttpFoundation\Response
         */
         
        public function featFullSearchAction($name, $type){
            $feat = $this->getDoctrine()->getRepository('AppBundle:Feat')->getFeatFullSearch($name, $type);
            
            $response = new Response(json_encode($feat));
    		$response->headers->set('Content-Type', 'application/json');

    		return $response;
        }
	}
?>
<?php

/* TwigBundle:Exception:error.json.twig */
class __TwigTemplate_d73d72bb387d09bbd29390611e34093c3fcb42863a296c0a67f700e2ba817366 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c79d4f45973d5a3779d9bfa7bb5d16f03cbbba5187f41203c73d101ed288fa69 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c79d4f45973d5a3779d9bfa7bb5d16f03cbbba5187f41203c73d101ed288fa69->enter($__internal_c79d4f45973d5a3779d9bfa7bb5d16f03cbbba5187f41203c73d101ed288fa69_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.json.twig"));

        $__internal_e40e144401d6599ba724d88141a59f48d30ca7fac9c828e2170dd63ad9c1bd9d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e40e144401d6599ba724d88141a59f48d30ca7fac9c828e2170dd63ad9c1bd9d->enter($__internal_e40e144401d6599ba724d88141a59f48d30ca7fac9c828e2170dd63ad9c1bd9d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => ($context["status_code"] ?? $this->getContext($context, "status_code")), "message" => ($context["status_text"] ?? $this->getContext($context, "status_text")))));
        echo "
";
        
        $__internal_c79d4f45973d5a3779d9bfa7bb5d16f03cbbba5187f41203c73d101ed288fa69->leave($__internal_c79d4f45973d5a3779d9bfa7bb5d16f03cbbba5187f41203c73d101ed288fa69_prof);

        
        $__internal_e40e144401d6599ba724d88141a59f48d30ca7fac9c828e2170dd63ad9c1bd9d->leave($__internal_e40e144401d6599ba724d88141a59f48d30ca7fac9c828e2170dd63ad9c1bd9d_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ { 'error': { 'code': status_code, 'message': status_text } }|json_encode|raw }}
", "TwigBundle:Exception:error.json.twig", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.json.twig");
    }
}

<?php

/* TwigBundle:Exception:exception.json.twig */
class __TwigTemplate_ff7795255c015fcd99bedc10277f82a3daa4ccbc57ca7059b843a848208b40d1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e56aad08d03b6f3a7e9516bcd16b4815d7cd597ba8ea84e24869ce3d408080c1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e56aad08d03b6f3a7e9516bcd16b4815d7cd597ba8ea84e24869ce3d408080c1->enter($__internal_e56aad08d03b6f3a7e9516bcd16b4815d7cd597ba8ea84e24869ce3d408080c1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.json.twig"));

        $__internal_93aa4eaf820860c35c677c01a0fa9029cf020b1fe980bd22b8c410162eda8afd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_93aa4eaf820860c35c677c01a0fa9029cf020b1fe980bd22b8c410162eda8afd->enter($__internal_93aa4eaf820860c35c677c01a0fa9029cf020b1fe980bd22b8c410162eda8afd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => ($context["status_code"] ?? $this->getContext($context, "status_code")), "message" => ($context["status_text"] ?? $this->getContext($context, "status_text")), "exception" => $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "toarray", array()))));
        echo "
";
        
        $__internal_e56aad08d03b6f3a7e9516bcd16b4815d7cd597ba8ea84e24869ce3d408080c1->leave($__internal_e56aad08d03b6f3a7e9516bcd16b4815d7cd597ba8ea84e24869ce3d408080c1_prof);

        
        $__internal_93aa4eaf820860c35c677c01a0fa9029cf020b1fe980bd22b8c410162eda8afd->leave($__internal_93aa4eaf820860c35c677c01a0fa9029cf020b1fe980bd22b8c410162eda8afd_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ { 'error': { 'code': status_code, 'message': status_text, 'exception': exception.toarray } }|json_encode|raw }}
", "TwigBundle:Exception:exception.json.twig", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.json.twig");
    }
}

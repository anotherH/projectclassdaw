<?php

/* EasyAdminBundle:default:field_object.html.twig */
class __TwigTemplate_023cff3de945912562a67970f0b7d99ad2670d1a4b613c8f53e7c9d5e6fcdc74 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_722f18077c9a7fe08790af37a6da49189a0c69722f02d3de2f5768f76fccbb58 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_722f18077c9a7fe08790af37a6da49189a0c69722f02d3de2f5768f76fccbb58->enter($__internal_722f18077c9a7fe08790af37a6da49189a0c69722f02d3de2f5768f76fccbb58_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_object.html.twig"));

        $__internal_e8bf19397b87fb581579f45748a1926e95ab7fc04712225cd1a91da508e51287 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e8bf19397b87fb581579f45748a1926e95ab7fc04712225cd1a91da508e51287->enter($__internal_e8bf19397b87fb581579f45748a1926e95ab7fc04712225cd1a91da508e51287_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_object.html.twig"));

        // line 1
        echo "<span class=\"label\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("label.object", array(), "EasyAdminBundle"), "html", null, true);
        echo "</span>
";
        
        $__internal_722f18077c9a7fe08790af37a6da49189a0c69722f02d3de2f5768f76fccbb58->leave($__internal_722f18077c9a7fe08790af37a6da49189a0c69722f02d3de2f5768f76fccbb58_prof);

        
        $__internal_e8bf19397b87fb581579f45748a1926e95ab7fc04712225cd1a91da508e51287->leave($__internal_e8bf19397b87fb581579f45748a1926e95ab7fc04712225cd1a91da508e51287_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:field_object.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<span class=\"label\">{{ 'label.object'|trans(domain = 'EasyAdminBundle') }}</span>
", "EasyAdminBundle:default:field_object.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/field_object.html.twig");
    }
}

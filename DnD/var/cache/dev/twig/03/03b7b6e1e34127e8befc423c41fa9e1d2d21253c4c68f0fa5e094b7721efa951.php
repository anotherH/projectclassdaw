<?php

/* WebProfilerBundle:Profiler:toolbar_redirect.html.twig */
class __TwigTemplate_bba3baef1b9c4cf717be3af912b13046ae9de0af818b9d385ae945df77c6ce00 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8f53087ecae61d47dcd7e965dddad22fd830642a3bd19faeab3f8c3e9cfa68af = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8f53087ecae61d47dcd7e965dddad22fd830642a3bd19faeab3f8c3e9cfa68af->enter($__internal_8f53087ecae61d47dcd7e965dddad22fd830642a3bd19faeab3f8c3e9cfa68af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $__internal_de87f5a30dc30873af92e90f76b507c6348663904b429f65eff5990d693b0fc7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_de87f5a30dc30873af92e90f76b507c6348663904b429f65eff5990d693b0fc7->enter($__internal_de87f5a30dc30873af92e90f76b507c6348663904b429f65eff5990d693b0fc7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8f53087ecae61d47dcd7e965dddad22fd830642a3bd19faeab3f8c3e9cfa68af->leave($__internal_8f53087ecae61d47dcd7e965dddad22fd830642a3bd19faeab3f8c3e9cfa68af_prof);

        
        $__internal_de87f5a30dc30873af92e90f76b507c6348663904b429f65eff5990d693b0fc7->leave($__internal_de87f5a30dc30873af92e90f76b507c6348663904b429f65eff5990d693b0fc7_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_58cf3d362d1ecea7865193e9645d2d5b5bc9aa06285013bc7750c3300d1dbcb8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_58cf3d362d1ecea7865193e9645d2d5b5bc9aa06285013bc7750c3300d1dbcb8->enter($__internal_58cf3d362d1ecea7865193e9645d2d5b5bc9aa06285013bc7750c3300d1dbcb8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_ad6a85c288502a75f50b81c750f6be8092806a13270c109386452302a925f6ba = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ad6a85c288502a75f50b81c750f6be8092806a13270c109386452302a925f6ba->enter($__internal_ad6a85c288502a75f50b81c750f6be8092806a13270c109386452302a925f6ba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_ad6a85c288502a75f50b81c750f6be8092806a13270c109386452302a925f6ba->leave($__internal_ad6a85c288502a75f50b81c750f6be8092806a13270c109386452302a925f6ba_prof);

        
        $__internal_58cf3d362d1ecea7865193e9645d2d5b5bc9aa06285013bc7750c3300d1dbcb8->leave($__internal_58cf3d362d1ecea7865193e9645d2d5b5bc9aa06285013bc7750c3300d1dbcb8_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_a4c6c5f2532ed36510d770260289f79371ac24808c2e59df719b0d9f6920d76e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a4c6c5f2532ed36510d770260289f79371ac24808c2e59df719b0d9f6920d76e->enter($__internal_a4c6c5f2532ed36510d770260289f79371ac24808c2e59df719b0d9f6920d76e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_6d7536f2268a98d2b2bf2cd63dd29a4352b2905a99e465886ea204cf4c54bb62 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6d7536f2268a98d2b2bf2cd63dd29a4352b2905a99e465886ea204cf4c54bb62->enter($__internal_6d7536f2268a98d2b2bf2cd63dd29a4352b2905a99e465886ea204cf4c54bb62_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, ($context["location"] ?? $this->getContext($context, "location")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, ($context["location"] ?? $this->getContext($context, "location")), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_6d7536f2268a98d2b2bf2cd63dd29a4352b2905a99e465886ea204cf4c54bb62->leave($__internal_6d7536f2268a98d2b2bf2cd63dd29a4352b2905a99e465886ea204cf4c54bb62_prof);

        
        $__internal_a4c6c5f2532ed36510d770260289f79371ac24808c2e59df719b0d9f6920d76e->leave($__internal_a4c6c5f2532ed36510d770260289f79371ac24808c2e59df719b0d9f6920d76e_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 8,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block title 'Redirection Intercepted' %}

{% block body %}
    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"{{ location }}\">{{ location }}</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
{% endblock %}
", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/toolbar_redirect.html.twig");
    }
}

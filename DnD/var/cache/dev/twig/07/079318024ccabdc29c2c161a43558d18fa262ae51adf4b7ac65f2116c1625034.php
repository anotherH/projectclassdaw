<?php

/* FOSUserBundle:Registration:confirmed.html.twig */
class __TwigTemplate_99ac3af1cb04666ff24737f799ec7557f2ccd36c92addeb4e3def3332ec18da7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "FOSUserBundle:Registration:confirmed.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_12e5beb8f43f4c15f8cf586d3c4b9e7b9da76a9b1f6cda44aa352c4121bb93e6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_12e5beb8f43f4c15f8cf586d3c4b9e7b9da76a9b1f6cda44aa352c4121bb93e6->enter($__internal_12e5beb8f43f4c15f8cf586d3c4b9e7b9da76a9b1f6cda44aa352c4121bb93e6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:confirmed.html.twig"));

        $__internal_c20ccf5adf2064ab9fbaa780682311a7c45b80a1476f8854e9cb67e7a9780d52 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c20ccf5adf2064ab9fbaa780682311a7c45b80a1476f8854e9cb67e7a9780d52->enter($__internal_c20ccf5adf2064ab9fbaa780682311a7c45b80a1476f8854e9cb67e7a9780d52_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:confirmed.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_12e5beb8f43f4c15f8cf586d3c4b9e7b9da76a9b1f6cda44aa352c4121bb93e6->leave($__internal_12e5beb8f43f4c15f8cf586d3c4b9e7b9da76a9b1f6cda44aa352c4121bb93e6_prof);

        
        $__internal_c20ccf5adf2064ab9fbaa780682311a7c45b80a1476f8854e9cb67e7a9780d52->leave($__internal_c20ccf5adf2064ab9fbaa780682311a7c45b80a1476f8854e9cb67e7a9780d52_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_f5d025a5c5fbd7f8fe537f48de512543044e08d3497fef5770713136c638e481 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f5d025a5c5fbd7f8fe537f48de512543044e08d3497fef5770713136c638e481->enter($__internal_f5d025a5c5fbd7f8fe537f48de512543044e08d3497fef5770713136c638e481_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_6b9d2cfb269ff689ad86b61b8e5cf7c6d7e8bbf1370fe1ccdd185ea1838ceedb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6b9d2cfb269ff689ad86b61b8e5cf7c6d7e8bbf1370fe1ccdd185ea1838ceedb->enter($__internal_6b9d2cfb269ff689ad86b61b8e5cf7c6d7e8bbf1370fe1ccdd185ea1838ceedb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.confirmed", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
    ";
        // line 7
        if (($context["targetUrl"] ?? $this->getContext($context, "targetUrl"))) {
            // line 8
            echo "    <p><a href=\"/\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.back", array(), "FOSUserBundle"), "html", null, true);
            echo "</a></p>
    ";
        }
        
        $__internal_6b9d2cfb269ff689ad86b61b8e5cf7c6d7e8bbf1370fe1ccdd185ea1838ceedb->leave($__internal_6b9d2cfb269ff689ad86b61b8e5cf7c6d7e8bbf1370fe1ccdd185ea1838ceedb_prof);

        
        $__internal_f5d025a5c5fbd7f8fe537f48de512543044e08d3497fef5770713136c638e481->leave($__internal_f5d025a5c5fbd7f8fe537f48de512543044e08d3497fef5770713136c638e481_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:confirmed.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 8,  54 => 7,  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
    <p>{{ 'registration.confirmed'|trans({'%username%': user.username}) }}</p>
    {% if targetUrl %}
    <p><a href=\"/\">{{ 'registration.back'|trans }}</a></p>
    {% endif %}
{% endblock fos_user_content %}
", "FOSUserBundle:Registration:confirmed.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/confirmed.html.twig");
    }
}

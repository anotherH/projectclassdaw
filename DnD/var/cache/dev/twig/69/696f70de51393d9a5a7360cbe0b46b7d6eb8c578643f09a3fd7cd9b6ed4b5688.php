<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_5a243955ea4042bd8d8df3fb2efe70f1c1d395ac329b5707110c4f7b3ece7b74 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3646c76752004d5d8132b28c9007656656db4ecc2c1059b1c447ed8d741dbde9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3646c76752004d5d8132b28c9007656656db4ecc2c1059b1c447ed8d741dbde9->enter($__internal_3646c76752004d5d8132b28c9007656656db4ecc2c1059b1c447ed8d741dbde9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_a8435320e771de733c6acd17ce50da06500fb7c233c91618fdbebbe63b4320a5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a8435320e771de733c6acd17ce50da06500fb7c233c91618fdbebbe63b4320a5->enter($__internal_a8435320e771de733c6acd17ce50da06500fb7c233c91618fdbebbe63b4320a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3646c76752004d5d8132b28c9007656656db4ecc2c1059b1c447ed8d741dbde9->leave($__internal_3646c76752004d5d8132b28c9007656656db4ecc2c1059b1c447ed8d741dbde9_prof);

        
        $__internal_a8435320e771de733c6acd17ce50da06500fb7c233c91618fdbebbe63b4320a5->leave($__internal_a8435320e771de733c6acd17ce50da06500fb7c233c91618fdbebbe63b4320a5_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_51fdf80a9e0b9d2a5e64f7dfb4920fe8e22b0d13bd6e4400f76097a9c3b4deaf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_51fdf80a9e0b9d2a5e64f7dfb4920fe8e22b0d13bd6e4400f76097a9c3b4deaf->enter($__internal_51fdf80a9e0b9d2a5e64f7dfb4920fe8e22b0d13bd6e4400f76097a9c3b4deaf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_a26a93cde9ea091e761c6d72ab423d11e7d1bbbd80e3259db76b2c3033bc8e91 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a26a93cde9ea091e761c6d72ab423d11e7d1bbbd80e3259db76b2c3033bc8e91->enter($__internal_a26a93cde9ea091e761c6d72ab423d11e7d1bbbd80e3259db76b2c3033bc8e91_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_a26a93cde9ea091e761c6d72ab423d11e7d1bbbd80e3259db76b2c3033bc8e91->leave($__internal_a26a93cde9ea091e761c6d72ab423d11e7d1bbbd80e3259db76b2c3033bc8e91_prof);

        
        $__internal_51fdf80a9e0b9d2a5e64f7dfb4920fe8e22b0d13bd6e4400f76097a9c3b4deaf->leave($__internal_51fdf80a9e0b9d2a5e64f7dfb4920fe8e22b0d13bd6e4400f76097a9c3b4deaf_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_e41715e9e8894eaac105ea6d4948d936e45c6fa8a5a454cb260f9ff9e002af98 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e41715e9e8894eaac105ea6d4948d936e45c6fa8a5a454cb260f9ff9e002af98->enter($__internal_e41715e9e8894eaac105ea6d4948d936e45c6fa8a5a454cb260f9ff9e002af98_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_ed0b70aa739e85bd1290bf2fce863de01370b71954ecabb7e1d458606b801fbb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ed0b70aa739e85bd1290bf2fce863de01370b71954ecabb7e1d458606b801fbb->enter($__internal_ed0b70aa739e85bd1290bf2fce863de01370b71954ecabb7e1d458606b801fbb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_ed0b70aa739e85bd1290bf2fce863de01370b71954ecabb7e1d458606b801fbb->leave($__internal_ed0b70aa739e85bd1290bf2fce863de01370b71954ecabb7e1d458606b801fbb_prof);

        
        $__internal_e41715e9e8894eaac105ea6d4948d936e45c6fa8a5a454cb260f9ff9e002af98->leave($__internal_e41715e9e8894eaac105ea6d4948d936e45c6fa8a5a454cb260f9ff9e002af98_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_58f042955c3496a9d08b8beef8b9595485291e5222d8eca9312da7d1fca3ddf2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_58f042955c3496a9d08b8beef8b9595485291e5222d8eca9312da7d1fca3ddf2->enter($__internal_58f042955c3496a9d08b8beef8b9595485291e5222d8eca9312da7d1fca3ddf2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_3436ce36bcdcfcd48087ae22ebc3397aa7f0f0204082c288c1ac83d43848186c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3436ce36bcdcfcd48087ae22ebc3397aa7f0f0204082c288c1ac83d43848186c->enter($__internal_3436ce36bcdcfcd48087ae22ebc3397aa7f0f0204082c288c1ac83d43848186c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_3436ce36bcdcfcd48087ae22ebc3397aa7f0f0204082c288c1ac83d43848186c->leave($__internal_3436ce36bcdcfcd48087ae22ebc3397aa7f0f0204082c288c1ac83d43848186c_prof);

        
        $__internal_58f042955c3496a9d08b8beef8b9595485291e5222d8eca9312da7d1fca3ddf2->leave($__internal_58f042955c3496a9d08b8beef8b9595485291e5222d8eca9312da7d1fca3ddf2_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}

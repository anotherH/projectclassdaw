<?php

/* TwigBundle:Exception:error.js.twig */
class __TwigTemplate_3cad35b61f4b5f3302e31e5672d00edfa0302aad099eef54768970190bb409a3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d18ea2f9cb5f87a80a8864aeeffcbd3947bfb74f4a8b2255791e55e84c92afc3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d18ea2f9cb5f87a80a8864aeeffcbd3947bfb74f4a8b2255791e55e84c92afc3->enter($__internal_d18ea2f9cb5f87a80a8864aeeffcbd3947bfb74f4a8b2255791e55e84c92afc3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.js.twig"));

        $__internal_3c66e02b05331229788a55c2920774e766d8bbc827279856173e93b66d36874f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3c66e02b05331229788a55c2920774e766d8bbc827279856173e93b66d36874f->enter($__internal_3c66e02b05331229788a55c2920774e766d8bbc827279856173e93b66d36874f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "js", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "js", null, true);
        echo "

*/
";
        
        $__internal_d18ea2f9cb5f87a80a8864aeeffcbd3947bfb74f4a8b2255791e55e84c92afc3->leave($__internal_d18ea2f9cb5f87a80a8864aeeffcbd3947bfb74f4a8b2255791e55e84c92afc3_prof);

        
        $__internal_3c66e02b05331229788a55c2920774e766d8bbc827279856173e93b66d36874f->leave($__internal_3c66e02b05331229788a55c2920774e766d8bbc827279856173e93b66d36874f_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ status_code }} {{ status_text }}

*/
", "TwigBundle:Exception:error.js.twig", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.js.twig");
    }
}

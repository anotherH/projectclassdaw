<?php

/* @Framework/Form/email_widget.html.php */
class __TwigTemplate_b3c65b9a72efaad7545fb752023e25bba4b284ecc13504a97f36eda8b2c66d33 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_69f220ffe5140dad93daa27c39a1f449578f30b94e0568ef9421a0bef84c2ae6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_69f220ffe5140dad93daa27c39a1f449578f30b94e0568ef9421a0bef84c2ae6->enter($__internal_69f220ffe5140dad93daa27c39a1f449578f30b94e0568ef9421a0bef84c2ae6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        $__internal_18151b4a3744a85dd2e029e9a7b5fcad6c1b72e84878ad035339ad716b0c360e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_18151b4a3744a85dd2e029e9a7b5fcad6c1b72e84878ad035339ad716b0c360e->enter($__internal_18151b4a3744a85dd2e029e9a7b5fcad6c1b72e84878ad035339ad716b0c360e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
";
        
        $__internal_69f220ffe5140dad93daa27c39a1f449578f30b94e0568ef9421a0bef84c2ae6->leave($__internal_69f220ffe5140dad93daa27c39a1f449578f30b94e0568ef9421a0bef84c2ae6_prof);

        
        $__internal_18151b4a3744a85dd2e029e9a7b5fcad6c1b72e84878ad035339ad716b0c360e->leave($__internal_18151b4a3744a85dd2e029e9a7b5fcad6c1b72e84878ad035339ad716b0c360e_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/email_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
", "@Framework/Form/email_widget.html.php", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/email_widget.html.php");
    }
}

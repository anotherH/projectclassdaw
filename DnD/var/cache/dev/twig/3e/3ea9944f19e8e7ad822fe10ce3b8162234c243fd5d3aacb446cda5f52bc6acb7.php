<?php

/* TwigBundle:Exception:error.xml.twig */
class __TwigTemplate_9790ec6d81473222b118aca0f59a5d3a4c1224a585e30444a69732032f213f66 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9634d787b241a6e0c2ddd23cbbdc3e37c9e9493ecc5eddadb260398a47be41f6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9634d787b241a6e0c2ddd23cbbdc3e37c9e9493ecc5eddadb260398a47be41f6->enter($__internal_9634d787b241a6e0c2ddd23cbbdc3e37c9e9493ecc5eddadb260398a47be41f6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.xml.twig"));

        $__internal_439081342da08f3ba4d746e6829adb3784c75479883cabe09c2e3befeddabfd5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_439081342da08f3ba4d746e6829adb3784c75479883cabe09c2e3befeddabfd5->enter($__internal_439081342da08f3ba4d746e6829adb3784c75479883cabe09c2e3befeddabfd5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.xml.twig"));

        // line 1
        echo "<?xml version=\"1.0\" encoding=\"";
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" ?>

<error code=\"";
        // line 3
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "html", null, true);
        echo "\" message=\"";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "html", null, true);
        echo "\" />
";
        
        $__internal_9634d787b241a6e0c2ddd23cbbdc3e37c9e9493ecc5eddadb260398a47be41f6->leave($__internal_9634d787b241a6e0c2ddd23cbbdc3e37c9e9493ecc5eddadb260398a47be41f6_prof);

        
        $__internal_439081342da08f3ba4d746e6829adb3784c75479883cabe09c2e3befeddabfd5->leave($__internal_439081342da08f3ba4d746e6829adb3784c75479883cabe09c2e3befeddabfd5_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.xml.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 3,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?xml version=\"1.0\" encoding=\"{{ _charset }}\" ?>

<error code=\"{{ status_code }}\" message=\"{{ status_text }}\" />
", "TwigBundle:Exception:error.xml.twig", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.xml.twig");
    }
}

<?php

/* EasyAdminBundle:default:field_integer.html.twig */
class __TwigTemplate_fcfa7c3d01d11cf75647b819e94ba092cc7819cab5d239ae0a9c97645c760a49 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_21ffbf8b9348462db4811bf2a202384fa0febc4b38e4a9b2ab65e8f96eccc000 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_21ffbf8b9348462db4811bf2a202384fa0febc4b38e4a9b2ab65e8f96eccc000->enter($__internal_21ffbf8b9348462db4811bf2a202384fa0febc4b38e4a9b2ab65e8f96eccc000_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_integer.html.twig"));

        $__internal_8acfd3c82ab0054e490ec418471ceeef5d070567b7e4a53b7bc9f9cf373705fd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8acfd3c82ab0054e490ec418471ceeef5d070567b7e4a53b7bc9f9cf373705fd->enter($__internal_8acfd3c82ab0054e490ec418471ceeef5d070567b7e4a53b7bc9f9cf373705fd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_integer.html.twig"));

        // line 1
        if ($this->getAttribute(($context["field_options"] ?? $this->getContext($context, "field_options")), "format", array())) {
            // line 2
            echo "    ";
            echo twig_escape_filter($this->env, sprintf($this->getAttribute(($context["field_options"] ?? $this->getContext($context, "field_options")), "format", array()), ($context["value"] ?? $this->getContext($context, "value"))), "html", null, true);
            echo "
";
        } else {
            // line 4
            echo "    ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, ($context["value"] ?? $this->getContext($context, "value"))), "html", null, true);
            echo "
";
        }
        
        $__internal_21ffbf8b9348462db4811bf2a202384fa0febc4b38e4a9b2ab65e8f96eccc000->leave($__internal_21ffbf8b9348462db4811bf2a202384fa0febc4b38e4a9b2ab65e8f96eccc000_prof);

        
        $__internal_8acfd3c82ab0054e490ec418471ceeef5d070567b7e4a53b7bc9f9cf373705fd->leave($__internal_8acfd3c82ab0054e490ec418471ceeef5d070567b7e4a53b7bc9f9cf373705fd_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:field_integer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  33 => 4,  27 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if field_options.format %}
    {{ field_options.format|format(value) }}
{% else %}
    {{ value|number_format }}
{% endif %}
", "EasyAdminBundle:default:field_integer.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/field_integer.html.twig");
    }
}

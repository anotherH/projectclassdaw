<?php

/* TwigBundle:Exception:exception.js.twig */
class __TwigTemplate_44fa04712d528a01111fa91dbb8bde78c94dadc66c4135682f00aa0745c3566f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9fdade8826ae4f74d3e10a7cb2af156833cb9c46ca2c1842e11325ea5030f12b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9fdade8826ae4f74d3e10a7cb2af156833cb9c46ca2c1842e11325ea5030f12b->enter($__internal_9fdade8826ae4f74d3e10a7cb2af156833cb9c46ca2c1842e11325ea5030f12b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.js.twig"));

        $__internal_054f39fd44e646b3eed2bb8644b740dd8fb082e34a56bfa80766b8f9672b2fa8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_054f39fd44e646b3eed2bb8644b740dd8fb082e34a56bfa80766b8f9672b2fa8->enter($__internal_054f39fd44e646b3eed2bb8644b740dd8fb082e34a56bfa80766b8f9672b2fa8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("@Twig/Exception/exception.txt.twig", "TwigBundle:Exception:exception.js.twig", 2)->display(array_merge($context, array("exception" => ($context["exception"] ?? $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_9fdade8826ae4f74d3e10a7cb2af156833cb9c46ca2c1842e11325ea5030f12b->leave($__internal_9fdade8826ae4f74d3e10a7cb2af156833cb9c46ca2c1842e11325ea5030f12b_prof);

        
        $__internal_054f39fd44e646b3eed2bb8644b740dd8fb082e34a56bfa80766b8f9672b2fa8->leave($__internal_054f39fd44e646b3eed2bb8644b740dd8fb082e34a56bfa80766b8f9672b2fa8_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 3,  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{% include '@Twig/Exception/exception.txt.twig' with { 'exception': exception } %}
*/
", "TwigBundle:Exception:exception.js.twig", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.js.twig");
    }
}

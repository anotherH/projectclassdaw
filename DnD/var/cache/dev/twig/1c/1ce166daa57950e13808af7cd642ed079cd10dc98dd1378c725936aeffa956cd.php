<?php

/* @Framework/FormTable/form_row.html.php */
class __TwigTemplate_e2493c7672184a10316b2d872a39aa2b315300810e3811edd1f418209f8ca19e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_015062a91440534ae86ca3624f0c8005cf2126cd58bce14ee1b0b5b4e928fd36 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_015062a91440534ae86ca3624f0c8005cf2126cd58bce14ee1b0b5b4e928fd36->enter($__internal_015062a91440534ae86ca3624f0c8005cf2126cd58bce14ee1b0b5b4e928fd36_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_row.html.php"));

        $__internal_e49a2f596c3750f2c3af7bbb6b41895a21994dc05af67913054e1f3fda30835a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e49a2f596c3750f2c3af7bbb6b41895a21994dc05af67913054e1f3fda30835a->enter($__internal_e49a2f596c3750f2c3af7bbb6b41895a21994dc05af67913054e1f3fda30835a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_row.html.php"));

        // line 1
        echo "<tr>
    <td>
        <?php echo \$view['form']->label(\$form) ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form) ?>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_015062a91440534ae86ca3624f0c8005cf2126cd58bce14ee1b0b5b4e928fd36->leave($__internal_015062a91440534ae86ca3624f0c8005cf2126cd58bce14ee1b0b5b4e928fd36_prof);

        
        $__internal_e49a2f596c3750f2c3af7bbb6b41895a21994dc05af67913054e1f3fda30835a->leave($__internal_e49a2f596c3750f2c3af7bbb6b41895a21994dc05af67913054e1f3fda30835a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr>
    <td>
        <?php echo \$view['form']->label(\$form) ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form) ?>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
", "@Framework/FormTable/form_row.html.php", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/form_row.html.php");
    }
}

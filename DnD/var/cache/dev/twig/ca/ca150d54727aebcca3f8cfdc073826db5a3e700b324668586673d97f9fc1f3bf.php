<?php

/* EasyAdminBundle:form:bootstrap_3_horizontal_layout.html.twig */
class __TwigTemplate_160fd3416089590128422ee262e1bef55d23762b45f60ed0f1b0424b4da61c3a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $_trait_0 = $this->loadTemplate("@EasyAdmin/form/bootstrap_3_layout.html.twig", "EasyAdminBundle:form:bootstrap_3_horizontal_layout.html.twig", 1);
        // line 1
        if (!$_trait_0->isTraitable()) {
            throw new Twig_Error_Runtime('Template "'."@EasyAdmin/form/bootstrap_3_layout.html.twig".'" cannot be used as a trait.');
        }
        $_trait_0_blocks = $_trait_0->getBlocks();

        $this->traits = $_trait_0_blocks;

        $this->blocks = array_merge(
            $this->traits,
            array(
                'form_start' => array($this, 'block_form_start'),
                'form_label' => array($this, 'block_form_label'),
                'form_label_class' => array($this, 'block_form_label_class'),
                'form_row' => array($this, 'block_form_row'),
                'checkbox_row' => array($this, 'block_checkbox_row'),
                'radio_row' => array($this, 'block_radio_row'),
                'checkbox_radio_row' => array($this, 'block_checkbox_radio_row'),
                'submit_row' => array($this, 'block_submit_row'),
                'reset_row' => array($this, 'block_reset_row'),
                'form_group_class' => array($this, 'block_form_group_class'),
            )
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8f1e97634be4432db712fd5569f5986a16f0373e409d1aa80df756c9d48b4016 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8f1e97634be4432db712fd5569f5986a16f0373e409d1aa80df756c9d48b4016->enter($__internal_8f1e97634be4432db712fd5569f5986a16f0373e409d1aa80df756c9d48b4016_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:form:bootstrap_3_horizontal_layout.html.twig"));

        $__internal_f0a93070148be02ee3e192b9ffd90d90ac79076e95c58482724ed793daf71ef3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f0a93070148be02ee3e192b9ffd90d90ac79076e95c58482724ed793daf71ef3->enter($__internal_f0a93070148be02ee3e192b9ffd90d90ac79076e95c58482724ed793daf71ef3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:form:bootstrap_3_horizontal_layout.html.twig"));

        // line 2
        echo "
";
        // line 3
        $this->displayBlock('form_start', $context, $blocks);
        // line 7
        echo "
";
        // line 9
        echo "
";
        // line 10
        $this->displayBlock('form_label', $context, $blocks);
        // line 20
        echo "
";
        // line 21
        $this->displayBlock('form_label_class', $context, $blocks);
        // line 22
        echo "
";
        // line 24
        echo "
";
        // line 25
        $this->displayBlock('form_row', $context, $blocks);
        // line 52
        echo "
";
        // line 53
        $this->displayBlock('checkbox_row', $context, $blocks);
        // line 56
        echo "
";
        // line 57
        $this->displayBlock('radio_row', $context, $blocks);
        // line 60
        echo "
";
        // line 61
        $this->displayBlock('checkbox_radio_row', $context, $blocks);
        // line 72
        echo "
";
        // line 73
        $this->displayBlock('submit_row', $context, $blocks);
        // line 83
        echo "
";
        // line 84
        $this->displayBlock('reset_row', $context, $blocks);
        // line 94
        echo "
";
        // line 95
        $this->displayBlock('form_group_class', $context, $blocks);
        
        $__internal_8f1e97634be4432db712fd5569f5986a16f0373e409d1aa80df756c9d48b4016->leave($__internal_8f1e97634be4432db712fd5569f5986a16f0373e409d1aa80df756c9d48b4016_prof);

        
        $__internal_f0a93070148be02ee3e192b9ffd90d90ac79076e95c58482724ed793daf71ef3->leave($__internal_f0a93070148be02ee3e192b9ffd90d90ac79076e95c58482724ed793daf71ef3_prof);

    }

    // line 3
    public function block_form_start($context, array $blocks = array())
    {
        $__internal_e314d4badeef87a1f6fc96e69587522caed74dc0ac47adb0c88812a248a6481d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e314d4badeef87a1f6fc96e69587522caed74dc0ac47adb0c88812a248a6481d->enter($__internal_e314d4badeef87a1f6fc96e69587522caed74dc0ac47adb0c88812a248a6481d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_start"));

        $__internal_c1aa24185f6ff589972f7d9d90c18687848fd591d4f54efc7cdddba2c53e7d63 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c1aa24185f6ff589972f7d9d90c18687848fd591d4f54efc7cdddba2c53e7d63->enter($__internal_c1aa24185f6ff589972f7d9d90c18687848fd591d4f54efc7cdddba2c53e7d63_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_start"));

        // line 4
        $context["_easyadmin_form_type"] = "horizontal";
        // line 5
        $this->displayParentBlock("form_start", $context, $blocks);
        
        $__internal_c1aa24185f6ff589972f7d9d90c18687848fd591d4f54efc7cdddba2c53e7d63->leave($__internal_c1aa24185f6ff589972f7d9d90c18687848fd591d4f54efc7cdddba2c53e7d63_prof);

        
        $__internal_e314d4badeef87a1f6fc96e69587522caed74dc0ac47adb0c88812a248a6481d->leave($__internal_e314d4badeef87a1f6fc96e69587522caed74dc0ac47adb0c88812a248a6481d_prof);

    }

    // line 10
    public function block_form_label($context, array $blocks = array())
    {
        $__internal_1922c8c4ee6a00fa8d45528836e5707aef366e38808de58ad97c0d2ead3bad48 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1922c8c4ee6a00fa8d45528836e5707aef366e38808de58ad97c0d2ead3bad48->enter($__internal_1922c8c4ee6a00fa8d45528836e5707aef366e38808de58ad97c0d2ead3bad48_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        $__internal_8f79e3bd1a6526739f6e7211c2cf973f7c15e22c7e4fc679495544eebc325ffd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8f79e3bd1a6526739f6e7211c2cf973f7c15e22c7e4fc679495544eebc325ffd->enter($__internal_8f79e3bd1a6526739f6e7211c2cf973f7c15e22c7e4fc679495544eebc325ffd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        // line 11
        ob_start();
        // line 12
        echo "    ";
        if ((($context["label"] ?? $this->getContext($context, "label")) === false)) {
            // line 13
            echo "        <div class=\"";
            $this->displayBlock("form_label_class", $context, $blocks);
            echo "\"></div>
    ";
        } else {
            // line 15
            echo "        ";
            $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => twig_trim_filter((((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")) . " ") .             $this->renderBlock("form_label_class", $context, $blocks)))));
            // line 16
            $this->displayParentBlock("form_label", $context, $blocks);
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_8f79e3bd1a6526739f6e7211c2cf973f7c15e22c7e4fc679495544eebc325ffd->leave($__internal_8f79e3bd1a6526739f6e7211c2cf973f7c15e22c7e4fc679495544eebc325ffd_prof);

        
        $__internal_1922c8c4ee6a00fa8d45528836e5707aef366e38808de58ad97c0d2ead3bad48->leave($__internal_1922c8c4ee6a00fa8d45528836e5707aef366e38808de58ad97c0d2ead3bad48_prof);

    }

    // line 21
    public function block_form_label_class($context, array $blocks = array())
    {
        $__internal_21f5fb3bf6d701d6911995dd132eeac66fa3822c5266349afd6ef4125e58d194 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_21f5fb3bf6d701d6911995dd132eeac66fa3822c5266349afd6ef4125e58d194->enter($__internal_21f5fb3bf6d701d6911995dd132eeac66fa3822c5266349afd6ef4125e58d194_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label_class"));

        $__internal_170d0a5948db4eddea7608ee0794fe51d57334c4ae28d8dabd49ac83ac65b260 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_170d0a5948db4eddea7608ee0794fe51d57334c4ae28d8dabd49ac83ac65b260->enter($__internal_170d0a5948db4eddea7608ee0794fe51d57334c4ae28d8dabd49ac83ac65b260_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label_class"));

        echo "col-sm-2";
        
        $__internal_170d0a5948db4eddea7608ee0794fe51d57334c4ae28d8dabd49ac83ac65b260->leave($__internal_170d0a5948db4eddea7608ee0794fe51d57334c4ae28d8dabd49ac83ac65b260_prof);

        
        $__internal_21f5fb3bf6d701d6911995dd132eeac66fa3822c5266349afd6ef4125e58d194->leave($__internal_21f5fb3bf6d701d6911995dd132eeac66fa3822c5266349afd6ef4125e58d194_prof);

    }

    // line 25
    public function block_form_row($context, array $blocks = array())
    {
        $__internal_5d54bc4cf9024b356960461ff0555d603ecb8d005b50d289c1fc8d515eb5c64a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5d54bc4cf9024b356960461ff0555d603ecb8d005b50d289c1fc8d515eb5c64a->enter($__internal_5d54bc4cf9024b356960461ff0555d603ecb8d005b50d289c1fc8d515eb5c64a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        $__internal_0341fe01cec531aff1d9e3e545d6badb9d0cf66c76e14c8a1c09d6ae7a18063d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0341fe01cec531aff1d9e3e545d6badb9d0cf66c76e14c8a1c09d6ae7a18063d->enter($__internal_0341fe01cec531aff1d9e3e545d6badb9d0cf66c76e14c8a1c09d6ae7a18063d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        // line 26
        ob_start();
        // line 27
        echo "    ";
        $context["_field_type"] = (($this->getAttribute($this->getAttribute(($context["easyadmin"] ?? null), "field", array(), "any", false, true), "fieldType", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute(($context["easyadmin"] ?? null), "field", array(), "any", false, true), "fieldType", array()), "default")) : ("default"));
        // line 28
        echo "    <div class=\"form-group ";
        if ((( !($context["compound"] ?? $this->getContext($context, "compound")) || ((array_key_exists("force_error", $context)) ? (_twig_default_filter(($context["force_error"] ?? $this->getContext($context, "force_error")), false)) : (false))) &&  !($context["valid"] ?? $this->getContext($context, "valid")))) {
            echo "has-error";
        }
        echo " field-";
        echo twig_escape_filter($this->env, twig_first($this->env, twig_slice($this->env, ($context["block_prefixes"] ?? $this->getContext($context, "block_prefixes")),  -2)), "html", null, true);
        echo "\">
        ";
        // line 29
        $context["_field_label"] = (($this->getAttribute($this->getAttribute(($context["easyadmin"] ?? null), "field", array(), "any", false, true), "label", array(), "array", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute(($context["easyadmin"] ?? null), "field", array(), "any", false, true), "label", array(), "array"), null)) : (null));
        // line 30
        echo "        ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label', array("translation_domain" => $this->getAttribute($this->getAttribute(($context["easyadmin"] ?? $this->getContext($context, "easyadmin")), "entity", array()), "translation_domain", array())) + (twig_test_empty($_label_ = ($context["_field_label"] ?? $this->getContext($context, "_field_label"))) ? array() : array("label" => $_label_)));
        echo "
        <div class=\"";
        // line 31
        $this->displayBlock("form_group_class", $context, $blocks);
        echo "\">
            ";
        // line 32
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "

            ";
        // line 34
        if ((twig_in_filter(($context["_field_type"] ?? $this->getContext($context, "_field_type")), array(0 => "datetime", 1 => "date", 2 => "time", 3 => "birthday")) && (($this->getAttribute($this->getAttribute(($context["easyadmin"] ?? null), "field", array(), "any", false, true), "nullable", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute(($context["easyadmin"] ?? null), "field", array(), "any", false, true), "nullable", array()), false)) : (false)))) {
            // line 35
            echo "                <div class=\"nullable-control\">
                    <label>
                        <input type=\"checkbox\" ";
            // line 37
            if ((null === ($context["data"] ?? $this->getContext($context, "data")))) {
                echo "checked=\"checked\"";
            }
            echo ">
                        ";
            // line 38
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("label.nullable_field", array(), "EasyAdminBundle"), "html", null, true);
            echo "
                    </label>
                </div>
            ";
        }
        // line 42
        echo "
            ";
        // line 43
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
        echo "

            ";
        // line 45
        if (((($this->getAttribute($this->getAttribute(($context["easyadmin"] ?? null), "field", array(), "any", false, true), "help", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute(($context["easyadmin"] ?? null), "field", array(), "any", false, true), "help", array()), "")) : ("")) != "")) {
            // line 46
            echo "                <span class=\"help-block\"><i class=\"fa fa-info-circle\"></i> ";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute($this->getAttribute(($context["easyadmin"] ?? $this->getContext($context, "easyadmin")), "field", array()), "help", array()), array(), $this->getAttribute($this->getAttribute(($context["easyadmin"] ?? $this->getContext($context, "easyadmin")), "entity", array()), "translation_domain", array()));
            echo "</span>
            ";
        }
        // line 48
        echo "        </div>
    </div>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_0341fe01cec531aff1d9e3e545d6badb9d0cf66c76e14c8a1c09d6ae7a18063d->leave($__internal_0341fe01cec531aff1d9e3e545d6badb9d0cf66c76e14c8a1c09d6ae7a18063d_prof);

        
        $__internal_5d54bc4cf9024b356960461ff0555d603ecb8d005b50d289c1fc8d515eb5c64a->leave($__internal_5d54bc4cf9024b356960461ff0555d603ecb8d005b50d289c1fc8d515eb5c64a_prof);

    }

    // line 53
    public function block_checkbox_row($context, array $blocks = array())
    {
        $__internal_4960625d4f3a57dc8b578b920d98989e4fe0a501b91d2f4ba19362a554066335 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4960625d4f3a57dc8b578b920d98989e4fe0a501b91d2f4ba19362a554066335->enter($__internal_4960625d4f3a57dc8b578b920d98989e4fe0a501b91d2f4ba19362a554066335_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_row"));

        $__internal_8dcb7d6f2e4341eed3abfe261a2525173137895f1dccbed937cc993b73ea6748 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8dcb7d6f2e4341eed3abfe261a2525173137895f1dccbed937cc993b73ea6748->enter($__internal_8dcb7d6f2e4341eed3abfe261a2525173137895f1dccbed937cc993b73ea6748_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_row"));

        // line 54
        $this->displayBlock("checkbox_radio_row", $context, $blocks);
        
        $__internal_8dcb7d6f2e4341eed3abfe261a2525173137895f1dccbed937cc993b73ea6748->leave($__internal_8dcb7d6f2e4341eed3abfe261a2525173137895f1dccbed937cc993b73ea6748_prof);

        
        $__internal_4960625d4f3a57dc8b578b920d98989e4fe0a501b91d2f4ba19362a554066335->leave($__internal_4960625d4f3a57dc8b578b920d98989e4fe0a501b91d2f4ba19362a554066335_prof);

    }

    // line 57
    public function block_radio_row($context, array $blocks = array())
    {
        $__internal_82df7e6aab3d2b10e0d08e3806e62016e7510733f34f1a27010acb909abbb4f1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_82df7e6aab3d2b10e0d08e3806e62016e7510733f34f1a27010acb909abbb4f1->enter($__internal_82df7e6aab3d2b10e0d08e3806e62016e7510733f34f1a27010acb909abbb4f1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_row"));

        $__internal_f4148101cca66fb172e243c9cd1372f576e80e464bfb57705f9c983593934140 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f4148101cca66fb172e243c9cd1372f576e80e464bfb57705f9c983593934140->enter($__internal_f4148101cca66fb172e243c9cd1372f576e80e464bfb57705f9c983593934140_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_row"));

        // line 58
        $this->displayBlock("checkbox_radio_row", $context, $blocks);
        
        $__internal_f4148101cca66fb172e243c9cd1372f576e80e464bfb57705f9c983593934140->leave($__internal_f4148101cca66fb172e243c9cd1372f576e80e464bfb57705f9c983593934140_prof);

        
        $__internal_82df7e6aab3d2b10e0d08e3806e62016e7510733f34f1a27010acb909abbb4f1->leave($__internal_82df7e6aab3d2b10e0d08e3806e62016e7510733f34f1a27010acb909abbb4f1_prof);

    }

    // line 61
    public function block_checkbox_radio_row($context, array $blocks = array())
    {
        $__internal_b2e9c579ff71c5bc3bd9f45de66e562dd6c3a70d05065d55391fdac4a24a2f99 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b2e9c579ff71c5bc3bd9f45de66e562dd6c3a70d05065d55391fdac4a24a2f99->enter($__internal_b2e9c579ff71c5bc3bd9f45de66e562dd6c3a70d05065d55391fdac4a24a2f99_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_radio_row"));

        $__internal_c1ba80571a96c2506b63d4a42b5b4e08862b73035967eed3f19f77af8d0c4aae = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c1ba80571a96c2506b63d4a42b5b4e08862b73035967eed3f19f77af8d0c4aae->enter($__internal_c1ba80571a96c2506b63d4a42b5b4e08862b73035967eed3f19f77af8d0c4aae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_radio_row"));

        // line 62
        ob_start();
        // line 63
        echo "    <div class=\"form-group ";
        if ( !($context["valid"] ?? $this->getContext($context, "valid"))) {
            echo "has-error";
        }
        echo " field-";
        echo twig_escape_filter($this->env, twig_first($this->env, twig_slice($this->env, ($context["block_prefixes"] ?? $this->getContext($context, "block_prefixes")),  -2)), "html", null, true);
        echo "\">
        <div class=\"";
        // line 64
        $this->displayBlock("form_label_class", $context, $blocks);
        echo "\"></div>
        <div class=\"";
        // line 65
        $this->displayBlock("form_group_class", $context, $blocks);
        echo "\">
            ";
        // line 66
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
            ";
        // line 67
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
        echo "
        </div>
    </div>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_c1ba80571a96c2506b63d4a42b5b4e08862b73035967eed3f19f77af8d0c4aae->leave($__internal_c1ba80571a96c2506b63d4a42b5b4e08862b73035967eed3f19f77af8d0c4aae_prof);

        
        $__internal_b2e9c579ff71c5bc3bd9f45de66e562dd6c3a70d05065d55391fdac4a24a2f99->leave($__internal_b2e9c579ff71c5bc3bd9f45de66e562dd6c3a70d05065d55391fdac4a24a2f99_prof);

    }

    // line 73
    public function block_submit_row($context, array $blocks = array())
    {
        $__internal_7698e7fdfee78e01f24be38a7d7f535e54e4bac5401b688f0157ecd88ec33cf1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7698e7fdfee78e01f24be38a7d7f535e54e4bac5401b688f0157ecd88ec33cf1->enter($__internal_7698e7fdfee78e01f24be38a7d7f535e54e4bac5401b688f0157ecd88ec33cf1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "submit_row"));

        $__internal_361eae472f64d1ef9ea22f13e6d0930fd9c246470388563c91af915672130aef = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_361eae472f64d1ef9ea22f13e6d0930fd9c246470388563c91af915672130aef->enter($__internal_361eae472f64d1ef9ea22f13e6d0930fd9c246470388563c91af915672130aef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "submit_row"));

        // line 74
        ob_start();
        // line 75
        echo "    <div class=\"form-group field-";
        echo twig_escape_filter($this->env, twig_first($this->env, twig_slice($this->env, ($context["block_prefixes"] ?? $this->getContext($context, "block_prefixes")),  -2)), "html", null, true);
        echo "\">
        <div class=\"";
        // line 76
        $this->displayBlock("form_label_class", $context, $blocks);
        echo "\"></div>
        <div class=\"";
        // line 77
        $this->displayBlock("form_group_class", $context, $blocks);
        echo "\">
            ";
        // line 78
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
        </div>
    </div>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_361eae472f64d1ef9ea22f13e6d0930fd9c246470388563c91af915672130aef->leave($__internal_361eae472f64d1ef9ea22f13e6d0930fd9c246470388563c91af915672130aef_prof);

        
        $__internal_7698e7fdfee78e01f24be38a7d7f535e54e4bac5401b688f0157ecd88ec33cf1->leave($__internal_7698e7fdfee78e01f24be38a7d7f535e54e4bac5401b688f0157ecd88ec33cf1_prof);

    }

    // line 84
    public function block_reset_row($context, array $blocks = array())
    {
        $__internal_b2316634e936ea38cd55aae069229f6b066aaee4adb2d9c81db6ebadaf1904ae = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b2316634e936ea38cd55aae069229f6b066aaee4adb2d9c81db6ebadaf1904ae->enter($__internal_b2316634e936ea38cd55aae069229f6b066aaee4adb2d9c81db6ebadaf1904ae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "reset_row"));

        $__internal_340312b9d2a79e8f23e04968f951b9894525cc52c9e11c1f4b6322bfdac7a98c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_340312b9d2a79e8f23e04968f951b9894525cc52c9e11c1f4b6322bfdac7a98c->enter($__internal_340312b9d2a79e8f23e04968f951b9894525cc52c9e11c1f4b6322bfdac7a98c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "reset_row"));

        // line 85
        ob_start();
        // line 86
        echo "    <div class=\"form-group\">
        <div class=\"";
        // line 87
        $this->displayBlock("form_label_class", $context, $blocks);
        echo "\"></div>
        <div class=\"";
        // line 88
        $this->displayBlock("form_group_class", $context, $blocks);
        echo "\">
            ";
        // line 89
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
        </div>
    </div>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_340312b9d2a79e8f23e04968f951b9894525cc52c9e11c1f4b6322bfdac7a98c->leave($__internal_340312b9d2a79e8f23e04968f951b9894525cc52c9e11c1f4b6322bfdac7a98c_prof);

        
        $__internal_b2316634e936ea38cd55aae069229f6b066aaee4adb2d9c81db6ebadaf1904ae->leave($__internal_b2316634e936ea38cd55aae069229f6b066aaee4adb2d9c81db6ebadaf1904ae_prof);

    }

    // line 95
    public function block_form_group_class($context, array $blocks = array())
    {
        $__internal_7f07401007cb926950e402a713dd837acde9b12679f1d5506b9b8ce2465b8c12 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7f07401007cb926950e402a713dd837acde9b12679f1d5506b9b8ce2465b8c12->enter($__internal_7f07401007cb926950e402a713dd837acde9b12679f1d5506b9b8ce2465b8c12_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_group_class"));

        $__internal_0cd460957bfa8b60f97456041fa40f0728a943ec40f42fc397b3e763c97dc5b9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0cd460957bfa8b60f97456041fa40f0728a943ec40f42fc397b3e763c97dc5b9->enter($__internal_0cd460957bfa8b60f97456041fa40f0728a943ec40f42fc397b3e763c97dc5b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_group_class"));

        echo "col-sm-10";
        
        $__internal_0cd460957bfa8b60f97456041fa40f0728a943ec40f42fc397b3e763c97dc5b9->leave($__internal_0cd460957bfa8b60f97456041fa40f0728a943ec40f42fc397b3e763c97dc5b9_prof);

        
        $__internal_7f07401007cb926950e402a713dd837acde9b12679f1d5506b9b8ce2465b8c12->leave($__internal_7f07401007cb926950e402a713dd837acde9b12679f1d5506b9b8ce2465b8c12_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:form:bootstrap_3_horizontal_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  433 => 95,  418 => 89,  414 => 88,  410 => 87,  407 => 86,  405 => 85,  396 => 84,  381 => 78,  377 => 77,  373 => 76,  368 => 75,  366 => 74,  357 => 73,  342 => 67,  338 => 66,  334 => 65,  330 => 64,  321 => 63,  319 => 62,  310 => 61,  300 => 58,  291 => 57,  281 => 54,  272 => 53,  259 => 48,  253 => 46,  251 => 45,  246 => 43,  243 => 42,  236 => 38,  230 => 37,  226 => 35,  224 => 34,  219 => 32,  215 => 31,  210 => 30,  208 => 29,  199 => 28,  196 => 27,  194 => 26,  185 => 25,  167 => 21,  155 => 16,  152 => 15,  146 => 13,  143 => 12,  141 => 11,  132 => 10,  122 => 5,  120 => 4,  111 => 3,  101 => 95,  98 => 94,  96 => 84,  93 => 83,  91 => 73,  88 => 72,  86 => 61,  83 => 60,  81 => 57,  78 => 56,  76 => 53,  73 => 52,  71 => 25,  68 => 24,  65 => 22,  63 => 21,  60 => 20,  58 => 10,  55 => 9,  52 => 7,  50 => 3,  47 => 2,  14 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% use \"@EasyAdmin/form/bootstrap_3_layout.html.twig\" %}

{% block form_start -%}
    {% set _easyadmin_form_type = 'horizontal' %}
    {{- parent() -}}
{%- endblock form_start %}

{# Labels #}

{% block form_label -%}
{% spaceless %}
    {% if label is same as(false) %}
        <div class=\"{{ block('form_label_class') }}\"></div>
    {% else %}
        {% set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' ' ~ block('form_label_class'))|trim}) %}
        {{- parent() -}}
    {% endif %}
{% endspaceless %}
{%- endblock form_label %}

{% block form_label_class 'col-sm-2' %}

{# Rows #}

{% block form_row -%}
{% spaceless %}
    {% set _field_type = easyadmin.field.fieldType|default('default') %}
    <div class=\"form-group {% if (not compound or force_error|default(false)) and not valid %}has-error{% endif %} field-{{ block_prefixes|slice(-2)|first }}\">
        {% set _field_label = easyadmin.field['label']|default(null) %}
        {{ form_label(form, _field_label, { translation_domain: easyadmin.entity.translation_domain }) }}
        <div class=\"{{ block('form_group_class') }}\">
            {{ form_widget(form) }}

            {% if _field_type in ['datetime', 'date', 'time', 'birthday'] and easyadmin.field.nullable|default(false) %}
                <div class=\"nullable-control\">
                    <label>
                        <input type=\"checkbox\" {% if data is null %}checked=\"checked\"{% endif %}>
                        {{ 'label.nullable_field'|trans({}, 'EasyAdminBundle')}}
                    </label>
                </div>
            {% endif %}

            {{ form_errors(form) }}

            {% if easyadmin.field.help|default('') != '' %}
                <span class=\"help-block\"><i class=\"fa fa-info-circle\"></i> {{ easyadmin.field.help|trans(domain = easyadmin.entity.translation_domain)|raw }}</span>
            {% endif %}
        </div>
    </div>
{% endspaceless %}
{%- endblock form_row %}

{% block checkbox_row -%}
    {{- block('checkbox_radio_row') -}}
{%- endblock checkbox_row %}

{% block radio_row -%}
    {{- block('checkbox_radio_row') -}}
{%- endblock radio_row %}

{% block checkbox_radio_row -%}
{% spaceless %}
    <div class=\"form-group {% if not valid %}has-error{% endif %} field-{{ block_prefixes|slice(-2)|first }}\">
        <div class=\"{{ block('form_label_class') }}\"></div>
        <div class=\"{{ block('form_group_class') }}\">
            {{ form_widget(form) }}
            {{ form_errors(form) }}
        </div>
    </div>
{% endspaceless %}
{%- endblock checkbox_radio_row %}

{% block submit_row -%}
{% spaceless %}
    <div class=\"form-group field-{{ block_prefixes|slice(-2)|first }}\">
        <div class=\"{{ block('form_label_class') }}\"></div>
        <div class=\"{{ block('form_group_class') }}\">
            {{ form_widget(form) }}
        </div>
    </div>
{% endspaceless %}
{% endblock submit_row %}

{% block reset_row -%}
{% spaceless %}
    <div class=\"form-group\">
        <div class=\"{{ block('form_label_class') }}\"></div>
        <div class=\"{{ block('form_group_class') }}\">
            {{ form_widget(form) }}
        </div>
    </div>
{% endspaceless %}
{% endblock reset_row %}

{% block form_group_class 'col-sm-10' %}
", "EasyAdminBundle:form:bootstrap_3_horizontal_layout.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/javiereguiluz/easyadmin-bundle/Resources/views/form/bootstrap_3_horizontal_layout.html.twig");
    }
}

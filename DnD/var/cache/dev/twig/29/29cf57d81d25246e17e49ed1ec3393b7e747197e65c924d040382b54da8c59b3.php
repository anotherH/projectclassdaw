<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_39d5c82ffd564c2a5e6a1d5ae5ff9bbe189c466601212ae79cb931157784f1ee extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e476e0bfbcf3a73eb6be724d80177dd6d6f0e4d73dbc70c2de1fe7ec6f5ece3d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e476e0bfbcf3a73eb6be724d80177dd6d6f0e4d73dbc70c2de1fe7ec6f5ece3d->enter($__internal_e476e0bfbcf3a73eb6be724d80177dd6d6f0e4d73dbc70c2de1fe7ec6f5ece3d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $__internal_9fc56b04bacd1f1ee096d879fd29eff62fc5865f89c368bdb3baab187c295407 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9fc56b04bacd1f1ee096d879fd29eff62fc5865f89c368bdb3baab187c295407->enter($__internal_9fc56b04bacd1f1ee096d879fd29eff62fc5865f89c368bdb3baab187c295407_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e476e0bfbcf3a73eb6be724d80177dd6d6f0e4d73dbc70c2de1fe7ec6f5ece3d->leave($__internal_e476e0bfbcf3a73eb6be724d80177dd6d6f0e4d73dbc70c2de1fe7ec6f5ece3d_prof);

        
        $__internal_9fc56b04bacd1f1ee096d879fd29eff62fc5865f89c368bdb3baab187c295407->leave($__internal_9fc56b04bacd1f1ee096d879fd29eff62fc5865f89c368bdb3baab187c295407_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_44692b27be5017d1ecdc66280b9239e543c84933a5f17a518a10840e2e04711e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_44692b27be5017d1ecdc66280b9239e543c84933a5f17a518a10840e2e04711e->enter($__internal_44692b27be5017d1ecdc66280b9239e543c84933a5f17a518a10840e2e04711e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_386e43683ab1419686aa6a0ed6c54d899ccbf206c75cda9da9e4438412d63e5b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_386e43683ab1419686aa6a0ed6c54d899ccbf206c75cda9da9e4438412d63e5b->enter($__internal_386e43683ab1419686aa6a0ed6c54d899ccbf206c75cda9da9e4438412d63e5b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
";
        
        $__internal_386e43683ab1419686aa6a0ed6c54d899ccbf206c75cda9da9e4438412d63e5b->leave($__internal_386e43683ab1419686aa6a0ed6c54d899ccbf206c75cda9da9e4438412d63e5b_prof);

        
        $__internal_44692b27be5017d1ecdc66280b9239e543c84933a5f17a518a10840e2e04711e->leave($__internal_44692b27be5017d1ecdc66280b9239e543c84933a5f17a518a10840e2e04711e_prof);

    }

    // line 136
    public function block_title($context, array $blocks = array())
    {
        $__internal_8ac6d4c078d6c1228ade0035ab39ccb717af637b7c8d4c50ba1732446f9f745d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8ac6d4c078d6c1228ade0035ab39ccb717af637b7c8d4c50ba1732446f9f745d->enter($__internal_8ac6d4c078d6c1228ade0035ab39ccb717af637b7c8d4c50ba1732446f9f745d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_dad75aefe0809ea41c1cbcc3e898045110be7e4e84fd9427a9236eeda8eabc91 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dad75aefe0809ea41c1cbcc3e898045110be7e4e84fd9427a9236eeda8eabc91->enter($__internal_dad75aefe0809ea41c1cbcc3e898045110be7e4e84fd9427a9236eeda8eabc91_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 137
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_dad75aefe0809ea41c1cbcc3e898045110be7e4e84fd9427a9236eeda8eabc91->leave($__internal_dad75aefe0809ea41c1cbcc3e898045110be7e4e84fd9427a9236eeda8eabc91_prof);

        
        $__internal_8ac6d4c078d6c1228ade0035ab39ccb717af637b7c8d4c50ba1732446f9f745d->leave($__internal_8ac6d4c078d6c1228ade0035ab39ccb717af637b7c8d4c50ba1732446f9f745d_prof);

    }

    // line 140
    public function block_body($context, array $blocks = array())
    {
        $__internal_d863a66a29e0e883324c9a920b0ac62382bd0fb9fca31cdcb97fe8d0a0812f80 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d863a66a29e0e883324c9a920b0ac62382bd0fb9fca31cdcb97fe8d0a0812f80->enter($__internal_d863a66a29e0e883324c9a920b0ac62382bd0fb9fca31cdcb97fe8d0a0812f80_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_00411becf92f4fd3cffd9aaba3bd872e43ae68a8453e5fb6f0607de1e0fb8ee1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_00411becf92f4fd3cffd9aaba3bd872e43ae68a8453e5fb6f0607de1e0fb8ee1->enter($__internal_00411becf92f4fd3cffd9aaba3bd872e43ae68a8453e5fb6f0607de1e0fb8ee1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 141
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 141)->display($context);
        
        $__internal_00411becf92f4fd3cffd9aaba3bd872e43ae68a8453e5fb6f0607de1e0fb8ee1->leave($__internal_00411becf92f4fd3cffd9aaba3bd872e43ae68a8453e5fb6f0607de1e0fb8ee1_prof);

        
        $__internal_d863a66a29e0e883324c9a920b0ac62382bd0fb9fca31cdcb97fe8d0a0812f80->leave($__internal_d863a66a29e0e883324c9a920b0ac62382bd0fb9fca31cdcb97fe8d0a0812f80_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 141,  217 => 140,  200 => 137,  191 => 136,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "TwigBundle:Exception:exception_full.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception_full.html.twig");
    }
}

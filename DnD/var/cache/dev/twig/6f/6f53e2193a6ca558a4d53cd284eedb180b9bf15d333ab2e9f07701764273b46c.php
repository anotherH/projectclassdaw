<?php

/* @Framework/Form/form_row.html.php */
class __TwigTemplate_ffe27ea71f8330649e136b928075aa48051ae1d8add3e2142cfe1861e8467574 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f747a56f9d24316b3a83b7ea77b908a9ef4359f88a8d5f79f4c860da5ee981fb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f747a56f9d24316b3a83b7ea77b908a9ef4359f88a8d5f79f4c860da5ee981fb->enter($__internal_f747a56f9d24316b3a83b7ea77b908a9ef4359f88a8d5f79f4c860da5ee981fb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        $__internal_ffba991af0c270020b1cc390efab69fb18016682a3cdf7085ef57a7c1fa7a961 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ffba991af0c270020b1cc390efab69fb18016682a3cdf7085ef57a7c1fa7a961->enter($__internal_ffba991af0c270020b1cc390efab69fb18016682a3cdf7085ef57a7c1fa7a961_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_f747a56f9d24316b3a83b7ea77b908a9ef4359f88a8d5f79f4c860da5ee981fb->leave($__internal_f747a56f9d24316b3a83b7ea77b908a9ef4359f88a8d5f79f4c860da5ee981fb_prof);

        
        $__internal_ffba991af0c270020b1cc390efab69fb18016682a3cdf7085ef57a7c1fa7a961->leave($__internal_ffba991af0c270020b1cc390efab69fb18016682a3cdf7085ef57a7c1fa7a961_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
", "@Framework/Form/form_row.html.php", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_row.html.php");
    }
}

<?php

/* EasyAdminBundle:default:field_url.html.twig */
class __TwigTemplate_34332e8a3015335bd449e51cc209a93c08a80f564779e8064f14748c8d093c0e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9ed84ef809e9de9802f569416542c4cb79b2ba06a0b5325e3bee55d7aa5631f4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9ed84ef809e9de9802f569416542c4cb79b2ba06a0b5325e3bee55d7aa5631f4->enter($__internal_9ed84ef809e9de9802f569416542c4cb79b2ba06a0b5325e3bee55d7aa5631f4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_url.html.twig"));

        $__internal_12cec3670c235447f0b322a0ea00c249cb606ea8a24d4708fcd124e2767c0d13 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_12cec3670c235447f0b322a0ea00c249cb606ea8a24d4708fcd124e2767c0d13->enter($__internal_12cec3670c235447f0b322a0ea00c249cb606ea8a24d4708fcd124e2767c0d13_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_url.html.twig"));

        // line 1
        if ((($context["view"] ?? $this->getContext($context, "view")) == "show")) {
            // line 2
            echo "    <a target=\"_blank\" href=\"";
            echo twig_escape_filter($this->env, ($context["value"] ?? $this->getContext($context, "value")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, ($context["value"] ?? $this->getContext($context, "value")), "html", null, true);
            echo "</a>
";
        } else {
            // line 4
            echo "    <a target=\"_blank\" href=\"";
            echo twig_escape_filter($this->env, ($context["value"] ?? $this->getContext($context, "value")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->truncateText($this->env, twig_replace_filter(($context["value"] ?? $this->getContext($context, "value")), array("https://" => "", "http://" => ""))), "html", null, true);
            echo "</a>
";
        }
        
        $__internal_9ed84ef809e9de9802f569416542c4cb79b2ba06a0b5325e3bee55d7aa5631f4->leave($__internal_9ed84ef809e9de9802f569416542c4cb79b2ba06a0b5325e3bee55d7aa5631f4_prof);

        
        $__internal_12cec3670c235447f0b322a0ea00c249cb606ea8a24d4708fcd124e2767c0d13->leave($__internal_12cec3670c235447f0b322a0ea00c249cb606ea8a24d4708fcd124e2767c0d13_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:field_url.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 4,  27 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if view == 'show' %}
    <a target=\"_blank\" href=\"{{ value }}\">{{ value }}</a>
{% else %}
    <a target=\"_blank\" href=\"{{ value }}\">{{ value|replace({ 'https://': '', 'http://': '' })|easyadmin_truncate }}</a>
{% endif %}
", "EasyAdminBundle:default:field_url.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/field_url.html.twig");
    }
}

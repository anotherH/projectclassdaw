<?php

/* @Framework/Form/choice_widget_expanded.html.php */
class __TwigTemplate_cb9286d39e135173f8f20921fa3ea1c07bf392bca430ffc0b379b01023c30837 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_67a44b8dabf1f88b3e9672ff0ed544c18d7a2164923cd81595f1dd902b63888d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_67a44b8dabf1f88b3e9672ff0ed544c18d7a2164923cd81595f1dd902b63888d->enter($__internal_67a44b8dabf1f88b3e9672ff0ed544c18d7a2164923cd81595f1dd902b63888d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget_expanded.html.php"));

        $__internal_39dc27d0d3fdd476463a428ab7fcb1aec706331b97cc8e9297c08507ad97e8e1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_39dc27d0d3fdd476463a428ab7fcb1aec706331b97cc8e9297c08507ad97e8e1->enter($__internal_39dc27d0d3fdd476463a428ab7fcb1aec706331b97cc8e9297c08507ad97e8e1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget_expanded.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
<?php foreach (\$form as \$child): ?>
    <?php echo \$view['form']->widget(\$child) ?>
    <?php echo \$view['form']->label(\$child, null, array('translation_domain' => \$choice_translation_domain)) ?>
<?php endforeach ?>
</div>
";
        
        $__internal_67a44b8dabf1f88b3e9672ff0ed544c18d7a2164923cd81595f1dd902b63888d->leave($__internal_67a44b8dabf1f88b3e9672ff0ed544c18d7a2164923cd81595f1dd902b63888d_prof);

        
        $__internal_39dc27d0d3fdd476463a428ab7fcb1aec706331b97cc8e9297c08507ad97e8e1->leave($__internal_39dc27d0d3fdd476463a428ab7fcb1aec706331b97cc8e9297c08507ad97e8e1_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget_expanded.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
<?php foreach (\$form as \$child): ?>
    <?php echo \$view['form']->widget(\$child) ?>
    <?php echo \$view['form']->label(\$child, null, array('translation_domain' => \$choice_translation_domain)) ?>
<?php endforeach ?>
</div>
", "@Framework/Form/choice_widget_expanded.html.php", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_widget_expanded.html.php");
    }
}

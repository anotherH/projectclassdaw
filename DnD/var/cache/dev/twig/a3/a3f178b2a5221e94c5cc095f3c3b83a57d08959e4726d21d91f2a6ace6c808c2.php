<?php

/* EasyAdminBundle:default:field_float.html.twig */
class __TwigTemplate_a7d9b0a44203bb1ffea6cc87cae95491029d063b69263bf2423ee89278815235 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6ce45a8e195d96f4a6c14d2172978b2eda6cfe66db034bac845862ccf1ec4404 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6ce45a8e195d96f4a6c14d2172978b2eda6cfe66db034bac845862ccf1ec4404->enter($__internal_6ce45a8e195d96f4a6c14d2172978b2eda6cfe66db034bac845862ccf1ec4404_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_float.html.twig"));

        $__internal_61617a4c54884d664900afa8ff9cc57960e502f12a9081f8abfb84a9f37ddf91 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_61617a4c54884d664900afa8ff9cc57960e502f12a9081f8abfb84a9f37ddf91->enter($__internal_61617a4c54884d664900afa8ff9cc57960e502f12a9081f8abfb84a9f37ddf91_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_float.html.twig"));

        // line 1
        if ($this->getAttribute(($context["field_options"] ?? $this->getContext($context, "field_options")), "format", array())) {
            // line 2
            echo "    ";
            echo twig_escape_filter($this->env, sprintf($this->getAttribute(($context["field_options"] ?? $this->getContext($context, "field_options")), "format", array()), ($context["value"] ?? $this->getContext($context, "value"))), "html", null, true);
            echo "
";
        } else {
            // line 4
            echo "    ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, ($context["value"] ?? $this->getContext($context, "value")), 2), "html", null, true);
            echo "
";
        }
        
        $__internal_6ce45a8e195d96f4a6c14d2172978b2eda6cfe66db034bac845862ccf1ec4404->leave($__internal_6ce45a8e195d96f4a6c14d2172978b2eda6cfe66db034bac845862ccf1ec4404_prof);

        
        $__internal_61617a4c54884d664900afa8ff9cc57960e502f12a9081f8abfb84a9f37ddf91->leave($__internal_61617a4c54884d664900afa8ff9cc57960e502f12a9081f8abfb84a9f37ddf91_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:field_float.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  33 => 4,  27 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if field_options.format %}
    {{ field_options.format|format(value) }}
{% else %}
    {{ value|number_format(2) }}
{% endif %}
", "EasyAdminBundle:default:field_float.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/field_float.html.twig");
    }
}

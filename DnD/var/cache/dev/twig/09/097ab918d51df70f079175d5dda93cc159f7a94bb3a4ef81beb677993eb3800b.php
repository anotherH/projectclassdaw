<?php

/* @Framework/Form/money_widget.html.php */
class __TwigTemplate_2a2808024720c58e283b02fded2faef61a4608add25d6b3fec9e097985d5cf8a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4f139c7c2009eebbdabd511b9f0108d2599f45afd7a5148d0ff1ec6b01d5e82c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4f139c7c2009eebbdabd511b9f0108d2599f45afd7a5148d0ff1ec6b01d5e82c->enter($__internal_4f139c7c2009eebbdabd511b9f0108d2599f45afd7a5148d0ff1ec6b01d5e82c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/money_widget.html.php"));

        $__internal_8c1948442da7ba2ce394a237cc4bc48620baecbfb6b98efae0a0f8e72d7ce932 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8c1948442da7ba2ce394a237cc4bc48620baecbfb6b98efae0a0f8e72d7ce932->enter($__internal_8c1948442da7ba2ce394a237cc4bc48620baecbfb6b98efae0a0f8e72d7ce932_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/money_widget.html.php"));

        // line 1
        echo "<?php echo str_replace('";
        echo twig_escape_filter($this->env, ($context["widget"] ?? $this->getContext($context, "widget")), "html", null, true);
        echo "', \$view['form']->block(\$form, 'form_widget_simple'), \$money_pattern) ?>
";
        
        $__internal_4f139c7c2009eebbdabd511b9f0108d2599f45afd7a5148d0ff1ec6b01d5e82c->leave($__internal_4f139c7c2009eebbdabd511b9f0108d2599f45afd7a5148d0ff1ec6b01d5e82c_prof);

        
        $__internal_8c1948442da7ba2ce394a237cc4bc48620baecbfb6b98efae0a0f8e72d7ce932->leave($__internal_8c1948442da7ba2ce394a237cc4bc48620baecbfb6b98efae0a0f8e72d7ce932_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/money_widget.html.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo str_replace('{{ widget }}', \$view['form']->block(\$form, 'form_widget_simple'), \$money_pattern) ?>
", "@Framework/Form/money_widget.html.php", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/money_widget.html.php");
    }
}

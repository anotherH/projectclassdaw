<?php

/* FOSUserBundle:Resetting:check_email.html.twig */
class __TwigTemplate_8dd69979554d5d949d63364097b55f3e232be04e7d92310787ea817a0ad16d95 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:check_email.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d621d8a60f404b9e5be776dc4767199db4a70b59b4a7b42112d7a28c06081184 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d621d8a60f404b9e5be776dc4767199db4a70b59b4a7b42112d7a28c06081184->enter($__internal_d621d8a60f404b9e5be776dc4767199db4a70b59b4a7b42112d7a28c06081184_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:check_email.html.twig"));

        $__internal_92a1fa9c12f6489da12383da16815d0638238b6d49b36729dc0358f8220e0751 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_92a1fa9c12f6489da12383da16815d0638238b6d49b36729dc0358f8220e0751->enter($__internal_92a1fa9c12f6489da12383da16815d0638238b6d49b36729dc0358f8220e0751_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:check_email.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d621d8a60f404b9e5be776dc4767199db4a70b59b4a7b42112d7a28c06081184->leave($__internal_d621d8a60f404b9e5be776dc4767199db4a70b59b4a7b42112d7a28c06081184_prof);

        
        $__internal_92a1fa9c12f6489da12383da16815d0638238b6d49b36729dc0358f8220e0751->leave($__internal_92a1fa9c12f6489da12383da16815d0638238b6d49b36729dc0358f8220e0751_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_aaed71d0a0d221d0610f56b55ba42c9ece58c830b71fa0a8cb3eecfaa9550448 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_aaed71d0a0d221d0610f56b55ba42c9ece58c830b71fa0a8cb3eecfaa9550448->enter($__internal_aaed71d0a0d221d0610f56b55ba42c9ece58c830b71fa0a8cb3eecfaa9550448_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_0b795b9071c1d519b5681a8eac05dbde47af1796bcbd9e6c3e8139f9ff145b3a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0b795b9071c1d519b5681a8eac05dbde47af1796bcbd9e6c3e8139f9ff145b3a->enter($__internal_0b795b9071c1d519b5681a8eac05dbde47af1796bcbd9e6c3e8139f9ff145b3a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "<p>
";
        // line 7
        echo nl2br(twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.check_email", array("%tokenLifetime%" => ($context["tokenLifetime"] ?? $this->getContext($context, "tokenLifetime"))), "FOSUserBundle"), "html", null, true));
        echo "
</p>
";
        
        $__internal_0b795b9071c1d519b5681a8eac05dbde47af1796bcbd9e6c3e8139f9ff145b3a->leave($__internal_0b795b9071c1d519b5681a8eac05dbde47af1796bcbd9e6c3e8139f9ff145b3a_prof);

        
        $__internal_aaed71d0a0d221d0610f56b55ba42c9ece58c830b71fa0a8cb3eecfaa9550448->leave($__internal_aaed71d0a0d221d0610f56b55ba42c9ece58c830b71fa0a8cb3eecfaa9550448_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:check_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 7,  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
<p>
{{ 'resetting.check_email'|trans({'%tokenLifetime%': tokenLifetime})|nl2br }}
</p>
{% endblock %}
", "FOSUserBundle:Resetting:check_email.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/friendsofsymfony/user-bundle/Resources/views/Resetting/check_email.html.twig");
    }
}

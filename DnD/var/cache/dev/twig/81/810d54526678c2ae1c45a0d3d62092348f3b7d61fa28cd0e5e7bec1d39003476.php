<?php

/* @Framework/Form/choice_options.html.php */
class __TwigTemplate_4d68c9eb69615a416cbbe4b5eac94bbfa03d81b4f203389070a7908c9ba63d35 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1ca852eabeee9a91d8a3b27e08115442c69584f1cd6541b1c25c1f63b0b12042 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1ca852eabeee9a91d8a3b27e08115442c69584f1cd6541b1c25c1f63b0b12042->enter($__internal_1ca852eabeee9a91d8a3b27e08115442c69584f1cd6541b1c25c1f63b0b12042_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_options.html.php"));

        $__internal_2313a477df1fd560534d62ed057ea575dcb982fb63eaebd40368c3d36be9bf7b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2313a477df1fd560534d62ed057ea575dcb982fb63eaebd40368c3d36be9bf7b->enter($__internal_2313a477df1fd560534d62ed057ea575dcb982fb63eaebd40368c3d36be9bf7b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_options.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
";
        
        $__internal_1ca852eabeee9a91d8a3b27e08115442c69584f1cd6541b1c25c1f63b0b12042->leave($__internal_1ca852eabeee9a91d8a3b27e08115442c69584f1cd6541b1c25c1f63b0b12042_prof);

        
        $__internal_2313a477df1fd560534d62ed057ea575dcb982fb63eaebd40368c3d36be9bf7b->leave($__internal_2313a477df1fd560534d62ed057ea575dcb982fb63eaebd40368c3d36be9bf7b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_options.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
", "@Framework/Form/choice_options.html.php", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_options.html.php");
    }
}

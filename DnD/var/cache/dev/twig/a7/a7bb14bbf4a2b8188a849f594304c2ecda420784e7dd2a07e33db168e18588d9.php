<?php

/* @Framework/Form/attributes.html.php */
class __TwigTemplate_79a56bc3ff38f0f5ff5fd61ceb1c84d38116f8bed5d0f394777c828ca7a2c885 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7712505bbeeefa079f96b516792bcd8ea2f0015b344c106bed09be9f66345ff1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7712505bbeeefa079f96b516792bcd8ea2f0015b344c106bed09be9f66345ff1->enter($__internal_7712505bbeeefa079f96b516792bcd8ea2f0015b344c106bed09be9f66345ff1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/attributes.html.php"));

        $__internal_6075c6677786fd53c8f3ff3e1025d3f9c9efa0ec186aa47f0ac8e667155e519f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6075c6677786fd53c8f3ff3e1025d3f9c9efa0ec186aa47f0ac8e667155e519f->enter($__internal_6075c6677786fd53c8f3ff3e1025d3f9c9efa0ec186aa47f0ac8e667155e519f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
";
        
        $__internal_7712505bbeeefa079f96b516792bcd8ea2f0015b344c106bed09be9f66345ff1->leave($__internal_7712505bbeeefa079f96b516792bcd8ea2f0015b344c106bed09be9f66345ff1_prof);

        
        $__internal_6075c6677786fd53c8f3ff3e1025d3f9c9efa0ec186aa47f0ac8e667155e519f->leave($__internal_6075c6677786fd53c8f3ff3e1025d3f9c9efa0ec186aa47f0ac8e667155e519f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
", "@Framework/Form/attributes.html.php", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/attributes.html.php");
    }
}

<?php

/* @Framework/Form/button_widget.html.php */
class __TwigTemplate_091af4d207c0ca0453c85c28fe59d430414e3c2780845fd55c6f620d4b0dff98 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4b96b8ba8fc08e97b89db5bf91c1a87742416c12a13b0eba4eb5d8e39c6e35ba = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4b96b8ba8fc08e97b89db5bf91c1a87742416c12a13b0eba4eb5d8e39c6e35ba->enter($__internal_4b96b8ba8fc08e97b89db5bf91c1a87742416c12a13b0eba4eb5d8e39c6e35ba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_widget.html.php"));

        $__internal_dc100b02cabfeebb808fb2bace4d38f7f59d6d0ff0061ff005872c537df4c0d4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dc100b02cabfeebb808fb2bace4d38f7f59d6d0ff0061ff005872c537df4c0d4->enter($__internal_dc100b02cabfeebb808fb2bace4d38f7f59d6d0ff0061ff005872c537df4c0d4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_widget.html.php"));

        // line 1
        echo "<?php if (!\$label) { \$label = isset(\$label_format)
    ? strtr(\$label_format, array('%name%' => \$name, '%id%' => \$id))
    : \$view['form']->humanize(\$name); } ?>
<button type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'button' ?>\" <?php echo \$view['form']->block(\$form, 'button_attributes') ?>><?php echo \$view->escape(false !== \$translation_domain ? \$view['translator']->trans(\$label, array(), \$translation_domain) : \$label) ?></button>
";
        
        $__internal_4b96b8ba8fc08e97b89db5bf91c1a87742416c12a13b0eba4eb5d8e39c6e35ba->leave($__internal_4b96b8ba8fc08e97b89db5bf91c1a87742416c12a13b0eba4eb5d8e39c6e35ba_prof);

        
        $__internal_dc100b02cabfeebb808fb2bace4d38f7f59d6d0ff0061ff005872c537df4c0d4->leave($__internal_dc100b02cabfeebb808fb2bace4d38f7f59d6d0ff0061ff005872c537df4c0d4_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (!\$label) { \$label = isset(\$label_format)
    ? strtr(\$label_format, array('%name%' => \$name, '%id%' => \$id))
    : \$view['form']->humanize(\$name); } ?>
<button type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'button' ?>\" <?php echo \$view['form']->block(\$form, 'button_attributes') ?>><?php echo \$view->escape(false !== \$translation_domain ? \$view['translator']->trans(\$label, array(), \$translation_domain) : \$label) ?></button>
", "@Framework/Form/button_widget.html.php", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/button_widget.html.php");
    }
}

<?php

/* FOSUserBundle:Security:login_content.html.twig */
class __TwigTemplate_c217087a7e617ee36799573d7ff172b519b3987e44dbe1ee08a71c3ffd449ab4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'head' => array($this, 'block_head'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c05de1f84f70926c75755dc41869ea57e1d871df2d894d82574f4e3d7fbbc430 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c05de1f84f70926c75755dc41869ea57e1d871df2d894d82574f4e3d7fbbc430->enter($__internal_c05de1f84f70926c75755dc41869ea57e1d871df2d894d82574f4e3d7fbbc430_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Security:login_content.html.twig"));

        $__internal_4fb3cbe7ecfe98ed55596940945727afacbe4a97ab7add34488e985bd1b148b3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4fb3cbe7ecfe98ed55596940945727afacbe4a97ab7add34488e985bd1b148b3->enter($__internal_4fb3cbe7ecfe98ed55596940945727afacbe4a97ab7add34488e985bd1b148b3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Security:login_content.html.twig"));

        // line 1
        echo "<html>
    <head>
        ";
        // line 3
        $this->displayBlock('head', $context, $blocks);
        // line 20
        echo "        
        
    </head>
    <body>
        ";
        // line 25
        echo "
        <div id=\"wrapper\">    
            <form action=\"";
        // line 27
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_check");
        echo "\" method=\"post\">
                ";
        // line 28
        if (($context["csrf_token"] ?? $this->getContext($context, "csrf_token"))) {
            // line 29
            echo "                    <input type=\"hidden\" name=\"_csrf_token\" value=\"";
            echo twig_escape_filter($this->env, ($context["csrf_token"] ?? $this->getContext($context, "csrf_token")), "html", null, true);
            echo "\" />
                ";
        }
        // line 31
        echo "    
                <label for=\"username\">";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.username", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>
                <input type=\"text\" id=\"username\" name=\"_username\" value=\"";
        // line 33
        echo twig_escape_filter($this->env, ($context["last_username"] ?? $this->getContext($context, "last_username")), "html", null, true);
        echo "\" required=\"required\" /></br>
    
                <label for=\"password\">";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.password", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>
                <input type=\"password\" id=\"password\" name=\"_password\" required=\"required\" /></br>
    
                <input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\" />
                <label for=\"remember_me\">";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.remember_me", array(), "FOSUserBundle"), "html", null, true);
        echo "</label></br>
        
                <input type=\"submit\" id=\"_submit\" name=\"_submit\" value=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" /></br>
                
                 ";
        // line 43
        if (($context["error"] ?? $this->getContext($context, "error"))) {
            // line 44
            echo "                    <div>";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute(($context["error"] ?? $this->getContext($context, "error")), "messageKey", array()), $this->getAttribute(($context["error"] ?? $this->getContext($context, "error")), "messageData", array()), "security"), "html", null, true);
            echo "</div>
                ";
        }
        // line 46
        echo "            </form>
        </div>
    </body>";
        
        $__internal_c05de1f84f70926c75755dc41869ea57e1d871df2d894d82574f4e3d7fbbc430->leave($__internal_c05de1f84f70926c75755dc41869ea57e1d871df2d894d82574f4e3d7fbbc430_prof);

        
        $__internal_4fb3cbe7ecfe98ed55596940945727afacbe4a97ab7add34488e985bd1b148b3->leave($__internal_4fb3cbe7ecfe98ed55596940945727afacbe4a97ab7add34488e985bd1b148b3_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_ebbf3c2518b9626e175064af731413917ab2290fe954acc1eb9ca0e42fd5550c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ebbf3c2518b9626e175064af731413917ab2290fe954acc1eb9ca0e42fd5550c->enter($__internal_ebbf3c2518b9626e175064af731413917ab2290fe954acc1eb9ca0e42fd5550c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_1d9f1526302434f23a232b47e22a831c01b2f0d671d2c4e059e2f976f9e55827 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1d9f1526302434f23a232b47e22a831c01b2f0d671d2c4e059e2f976f9e55827->enter($__internal_1d9f1526302434f23a232b47e22a831c01b2f0d671d2c4e059e2f976f9e55827_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "          
            <style>
                body{
                    background:#FAFAFA;
                }
                
                #wrapper{
                    text-align: center;
                }
                
                #username, #password, #remember_me, #_submit{
                    margin:5px;
                }
            </style>
            
        ";
        
        $__internal_1d9f1526302434f23a232b47e22a831c01b2f0d671d2c4e059e2f976f9e55827->leave($__internal_1d9f1526302434f23a232b47e22a831c01b2f0d671d2c4e059e2f976f9e55827_prof);

        
        $__internal_ebbf3c2518b9626e175064af731413917ab2290fe954acc1eb9ca0e42fd5550c->leave($__internal_ebbf3c2518b9626e175064af731413917ab2290fe954acc1eb9ca0e42fd5550c_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Security:login_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  112 => 4,  103 => 3,  91 => 46,  85 => 44,  83 => 43,  78 => 41,  73 => 39,  66 => 35,  61 => 33,  57 => 32,  54 => 31,  48 => 29,  46 => 28,  42 => 27,  38 => 25,  32 => 20,  30 => 3,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<html>
    <head>
        {% block head %}
          
            <style>
                body{
                    background:#FAFAFA;
                }
                
                #wrapper{
                    text-align: center;
                }
                
                #username, #password, #remember_me, #_submit{
                    margin:5px;
                }
            </style>
            
        {% endblock %}
        
        
    </head>
    <body>
        {% trans_default_domain 'FOSUserBundle' %}

        <div id=\"wrapper\">    
            <form action=\"{{ path(\"fos_user_security_check\") }}\" method=\"post\">
                {% if csrf_token %}
                    <input type=\"hidden\" name=\"_csrf_token\" value=\"{{ csrf_token }}\" />
                {% endif %}
    
                <label for=\"username\">{{ 'security.login.username'|trans }}</label>
                <input type=\"text\" id=\"username\" name=\"_username\" value=\"{{ last_username }}\" required=\"required\" /></br>
    
                <label for=\"password\">{{ 'security.login.password'|trans }}</label>
                <input type=\"password\" id=\"password\" name=\"_password\" required=\"required\" /></br>
    
                <input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\" />
                <label for=\"remember_me\">{{ 'security.login.remember_me'|trans }}</label></br>
        
                <input type=\"submit\" id=\"_submit\" name=\"_submit\" value=\"{{ 'security.login.submit'|trans }}\" /></br>
                
                 {% if error %}
                    <div>{{ error.messageKey|trans(error.messageData, 'security') }}</div>
                {% endif %}
            </form>
        </div>
    </body>", "FOSUserBundle:Security:login_content.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/friendsofsymfony/user-bundle/Resources/views/Security/login_content.html.twig");
    }
}

<?php

/* @Framework/Form/choice_attributes.html.php */
class __TwigTemplate_feb7f3e2920990679b95adcf88a571d6020610ef9fc60e1051bf82adb0d5bce9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_258d7dff02486c04b5790d06458da03d88008f3db431e870d82d578614f0ab32 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_258d7dff02486c04b5790d06458da03d88008f3db431e870d82d578614f0ab32->enter($__internal_258d7dff02486c04b5790d06458da03d88008f3db431e870d82d578614f0ab32_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_attributes.html.php"));

        $__internal_d8b253048b954756466381511d63a5baff7d43b1773cbf4522042a6e8162d864 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d8b253048b954756466381511d63a5baff7d43b1773cbf4522042a6e8162d864->enter($__internal_d8b253048b954756466381511d63a5baff7d43b1773cbf4522042a6e8162d864_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_attributes.html.php"));

        // line 1
        echo "<?php if (\$disabled): ?>disabled=\"disabled\" <?php endif ?>
<?php foreach (\$choice_attr as \$k => \$v): ?>
<?php if (\$v === true): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$k)) ?>
<?php elseif (\$v !== false): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$v)) ?>
<?php endif ?>
<?php endforeach ?>
";
        
        $__internal_258d7dff02486c04b5790d06458da03d88008f3db431e870d82d578614f0ab32->leave($__internal_258d7dff02486c04b5790d06458da03d88008f3db431e870d82d578614f0ab32_prof);

        
        $__internal_d8b253048b954756466381511d63a5baff7d43b1773cbf4522042a6e8162d864->leave($__internal_d8b253048b954756466381511d63a5baff7d43b1773cbf4522042a6e8162d864_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$disabled): ?>disabled=\"disabled\" <?php endif ?>
<?php foreach (\$choice_attr as \$k => \$v): ?>
<?php if (\$v === true): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$k)) ?>
<?php elseif (\$v !== false): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$v)) ?>
<?php endif ?>
<?php endforeach ?>
", "@Framework/Form/choice_attributes.html.php", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_attributes.html.php");
    }
}

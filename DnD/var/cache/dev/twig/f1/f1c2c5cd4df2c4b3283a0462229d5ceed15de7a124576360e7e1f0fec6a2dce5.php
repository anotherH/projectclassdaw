<?php

/* @Framework/Form/choice_widget.html.php */
class __TwigTemplate_fbc411a17a37c0fa72f2f9b4a25307181e873ed122ed8cce8b51b2a90fd2c19c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6085fc18345201c8cb92469e9b11472d52f4ef37a8f2e70650c5e301acc88bb0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6085fc18345201c8cb92469e9b11472d52f4ef37a8f2e70650c5e301acc88bb0->enter($__internal_6085fc18345201c8cb92469e9b11472d52f4ef37a8f2e70650c5e301acc88bb0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        $__internal_91288411e9e9eddb20559684fc0bca6aee72d8e3ade3187053ebc16cdf5c1bea = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_91288411e9e9eddb20559684fc0bca6aee72d8e3ade3187053ebc16cdf5c1bea->enter($__internal_91288411e9e9eddb20559684fc0bca6aee72d8e3ade3187053ebc16cdf5c1bea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        // line 1
        echo "<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
";
        
        $__internal_6085fc18345201c8cb92469e9b11472d52f4ef37a8f2e70650c5e301acc88bb0->leave($__internal_6085fc18345201c8cb92469e9b11472d52f4ef37a8f2e70650c5e301acc88bb0_prof);

        
        $__internal_91288411e9e9eddb20559684fc0bca6aee72d8e3ade3187053ebc16cdf5c1bea->leave($__internal_91288411e9e9eddb20559684fc0bca6aee72d8e3ade3187053ebc16cdf5c1bea_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
", "@Framework/Form/choice_widget.html.php", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_widget.html.php");
    }
}

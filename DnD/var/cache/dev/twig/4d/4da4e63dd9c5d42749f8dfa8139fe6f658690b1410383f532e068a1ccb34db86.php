<?php

/* @Framework/Form/button_row.html.php */
class __TwigTemplate_b3317620720528c56fac01a0c8e3daa09f3790caef6913c136462ce0f8673961 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7b83529242fa2db2c37ba727cf30e7fd41548a98e9083c3432e7400008478282 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7b83529242fa2db2c37ba727cf30e7fd41548a98e9083c3432e7400008478282->enter($__internal_7b83529242fa2db2c37ba727cf30e7fd41548a98e9083c3432e7400008478282_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        $__internal_6f2ec021ac56b88aeddceb9ecac15ded530f787a14ff1980ac8ee1dafa26e916 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6f2ec021ac56b88aeddceb9ecac15ded530f787a14ff1980ac8ee1dafa26e916->enter($__internal_6f2ec021ac56b88aeddceb9ecac15ded530f787a14ff1980ac8ee1dafa26e916_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_7b83529242fa2db2c37ba727cf30e7fd41548a98e9083c3432e7400008478282->leave($__internal_7b83529242fa2db2c37ba727cf30e7fd41548a98e9083c3432e7400008478282_prof);

        
        $__internal_6f2ec021ac56b88aeddceb9ecac15ded530f787a14ff1980ac8ee1dafa26e916->leave($__internal_6f2ec021ac56b88aeddceb9ecac15ded530f787a14ff1980ac8ee1dafa26e916_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
", "@Framework/Form/button_row.html.php", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/button_row.html.php");
    }
}

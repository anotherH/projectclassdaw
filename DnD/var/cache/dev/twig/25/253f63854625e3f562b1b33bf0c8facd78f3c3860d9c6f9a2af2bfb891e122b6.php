<?php

/* :default:build.html.twig */
class __TwigTemplate_cbce7488055b63db75a6da552996f5085d30a48b3d804601dbfcdb41a4da2361 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":default:build.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4a7092eaac9caba99a3b052647fd7a400e8e1ab72f2ee790c568f433dea19cf0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4a7092eaac9caba99a3b052647fd7a400e8e1ab72f2ee790c568f433dea19cf0->enter($__internal_4a7092eaac9caba99a3b052647fd7a400e8e1ab72f2ee790c568f433dea19cf0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":default:build.html.twig"));

        $__internal_937fe9757f5558ee5ea873bde654a64d16f4c19d1132ae7f68d86de641dad093 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_937fe9757f5558ee5ea873bde654a64d16f4c19d1132ae7f68d86de641dad093->enter($__internal_937fe9757f5558ee5ea873bde654a64d16f4c19d1132ae7f68d86de641dad093_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":default:build.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4a7092eaac9caba99a3b052647fd7a400e8e1ab72f2ee790c568f433dea19cf0->leave($__internal_4a7092eaac9caba99a3b052647fd7a400e8e1ab72f2ee790c568f433dea19cf0_prof);

        
        $__internal_937fe9757f5558ee5ea873bde654a64d16f4c19d1132ae7f68d86de641dad093->leave($__internal_937fe9757f5558ee5ea873bde654a64d16f4c19d1132ae7f68d86de641dad093_prof);

    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_8a3462c62f4dbb0fbf08d47dee41640e0be31eb256e23029e3a2a246487659a5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8a3462c62f4dbb0fbf08d47dee41640e0be31eb256e23029e3a2a246487659a5->enter($__internal_8a3462c62f4dbb0fbf08d47dee41640e0be31eb256e23029e3a2a246487659a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_f63a47ed1f625ab85a7e22f79debda8753acc93468c080890d7e825ff8a4fce1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f63a47ed1f625ab85a7e22f79debda8753acc93468c080890d7e825ff8a4fce1->enter($__internal_f63a47ed1f625ab85a7e22f79debda8753acc93468c080890d7e825ff8a4fce1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 4
        echo "\t\t
\t\t
\t\t#equipment{
\t\t\tmargin-top:10px;
\t\t}
\t\t
\t\t#inputs{
\t\t\tdisplay:inline-block;
\t\t}
\t\t
\t\t.glyphicon{
\t\t\tcursor: pointer;
\t\t}
\t\t
\t\t#tables{
\t\t\tmargin-top:10px;
\t\t\tmargin-bottom: 10px;
\t\t}
\t
\t\t#wTable, #wName, #wGroup, #wType, #dType, #aTable{
\t\t\twidth:100%;
\t\t\ttext-align:center;
\t\t}
\t\t
\t\t#wName th{
\t\t}
\t\t
\t\t #separator{
\t\t \theight:100px;
\t\t }
\t\t
\t\tth{
\t\t\ttext-align: center;
\t\t}
\t\t
";
        
        $__internal_f63a47ed1f625ab85a7e22f79debda8753acc93468c080890d7e825ff8a4fce1->leave($__internal_f63a47ed1f625ab85a7e22f79debda8753acc93468c080890d7e825ff8a4fce1_prof);

        
        $__internal_8a3462c62f4dbb0fbf08d47dee41640e0be31eb256e23029e3a2a246487659a5->leave($__internal_8a3462c62f4dbb0fbf08d47dee41640e0be31eb256e23029e3a2a246487659a5_prof);

    }

    // line 42
    public function block_body($context, array $blocks = array())
    {
        $__internal_6f058ec64c6173b9bdf994c5c316468639df58be00a139cbcbec7ec5a7e09250 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6f058ec64c6173b9bdf994c5c316468639df58be00a139cbcbec7ec5a7e09250->enter($__internal_6f058ec64c6173b9bdf994c5c316468639df58be00a139cbcbec7ec5a7e09250_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_5668d441050255889e600d5c98abd37b4aba675aabe63a634fe69d016f141806 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5668d441050255889e600d5c98abd37b4aba675aabe63a634fe69d016f141806->enter($__internal_5668d441050255889e600d5c98abd37b4aba675aabe63a634fe69d016f141806_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 43
        echo "\t\t<div id=\"content\">
\t\t<div id=\"equipment\">
\t\t\t<div id=\"inputs\">
\t\t\t\t<div class = \"col-md-12 weapon\">\t
\t\t\t\t\t<table id=\"wTable\" border=\"1\" class=\"col-md-6\">
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<th>Name</th>
\t\t\t\t\t\t\t<th>Weapon Group</th>
\t\t\t\t\t\t\t<th>Damage</th>
\t\t\t\t\t\t\t<th>Damage Type</th>
\t\t\t\t\t\t\t<th>Critical</th>
\t\t\t\t\t\t\t<th>Range</th>
\t\t\t\t\t\t\t<th>Type</th>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td><input class=\"wInput\" type=\"text\" placeholder=\"Weapon\"><span class=\"glyphicon glyphicon-plus-sign addWeapon\" aria-hidden=\"true\" onclick=\"checkWeapon(this)\"></span></td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t</table>
\t\t\t\t</div>
\t\t\t\t<div class = \"col-md-12 armor\">
\t\t\t\t\t\t<div id=\"separator\"></div>
\t\t\t\t
\t\t\t\t\t\t<table id=\"aTable\" border=\"1\" class=\"col-md-6\">
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<th>Name</th>
\t\t\t\t\t\t\t\t<th>Type</th>
\t\t\t\t\t\t\t\t<th>AcBonus</th>
\t\t\t\t\t\t\t\t<th>MaxDex</th>
\t\t\t\t\t\t\t\t<th>CheckP</th>
\t\t\t\t\t\t\t\t<th>SpellFailure</th>
\t\t\t\t\t\t\t\t<th>Weight</th>
\t\t\t\t\t\t\t\t<th>Metallic</th>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<td><input class=\"aInput\" type=\"text\" placeholder=\"Armor\"><span class=\"glyphicon glyphicon-plus-sign addArmor\" aria-hidden=\"true\" onclick=\"checkArmor(this)\"></span></td>
\t\t\t\t\t\t\t</tr>\t\t\t
\t\t\t\t\t\t\t\t</table>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t
\t\t\t\t

\t\t\t\t\t
\t\t\t\t\t";
        // line 89
        echo "\t    ";
        // line 90
        echo "\t\t\t\t\t";
        // line 91
        echo "\t\t\t\t\t
\t\t\t\t\t";
        // line 93
        echo "\t    ";
        // line 94
        echo "\t    ";
        // line 95
        echo "\t\t\t\t\t";
        // line 96
        echo "\t\t\t\t\t
\t\t\t\t\t";
        // line 98
        echo "\t\t\t\t\t";
        // line 99
        echo "\t    ";
        // line 100
        echo "\t    ";
        // line 101
        echo "\t    ";
        // line 102
        echo "\t    ";
        // line 103
        echo "\t    ";
        // line 104
        echo "\t    ";
        // line 105
        echo "\t\t\t\t\t";
        // line 106
        echo "\t\t\t\t\t
\t\t\t\t\t";
        // line 108
        echo "\t    ";
        // line 109
        echo "\t    ";
        // line 110
        echo "\t\t\t\t\t";
        // line 111
        echo "\t\t\t\t\t
\t\t\t\t\t";
        // line 113
        echo "\t    ";
        // line 114
        echo "\t\t\t\t\t";
        // line 115
        echo "\t\t\t\t\t
\t\t\t\t\t";
        // line 117
        echo "\t    ";
        // line 118
        echo "\t\t\t\t\t";
        // line 119
        echo "\t\t\t\t\t
\t\t
\t\t\t\t
\t\t\t\t
\t\t\t\t
\t\t\t\t<!-- <table id=\"aTable\" border=\"1\" class=\"col-md-12\">
\t\t\t\t\t<tr id=\"aName\">
\t    \t\t\t<th class=\"col-md-2\" colspan=\"1\">Name</th>
\t\t\t\t\t</tr>
\t\t\t\t\t
\t\t\t\t\t<tr id=\"aType\">
\t    \t\t\t<th class=\"col-md-2\" colspan=\"1\">aType</th>
\t\t\t\t\t</tr>

\t\t\t\t\t<tr id=\"aHeaders\">
\t    \t\t\t<th class=\"col-md-3\">acB</th><th class=\"col-md-3\">maxDex</th><th class=\"col-md-3\">checkPenalty</th><th class=\"col-md-3\">spFail</th>
\t\t\t\t\t</tr>
\t\t\t\t\t
\t\t\t\t\t<tr id=\"aProperties\">
\t\t\t\t\t</tr>
\t\t\t\t\t
\t\t\t\t\t<tr id=\"aHeaders\">
\t    \t\t\t<th class=\"col-md-4\">Weight</th><th class=\"col-md-4\" colspan=\"2\">Has metal</th><th class=\"col-md-4\">More info</th>
\t\t\t\t\t</tr>
\t\t\t\t\t
\t\t\t\t\t<tr id=\"aInfo\">
\t\t\t\t\t</tr>
\t\t\t\t\t
\t\t\t\t</table>
\t\t\t
\t\t</div>
\t</div>-->
  
 ";
        // line 153
        echo "\t";
        // line 154
        echo "\t";
        // line 155
        echo "\t";
        // line 156
        echo "\t";
        // line 157
        echo "\t
\t";
        // line 159
        echo "\t";
        // line 160
        echo "\t";
        // line 161
        echo "\t";
        // line 162
        echo "\t";
        // line 163
        echo "
  ";
        // line 165
        echo "  ";
        // line 166
        echo "  ";
        // line 167
        echo "  ";
        // line 168
        echo "  ";
        // line 169
        echo "  ";
        // line 170
        echo "  ";
        // line 171
        echo "  ";
        // line 172
        echo "  ";
        // line 173
        echo "  ";
        // line 174
        echo "  ";
        // line 175
        echo "  ";
        // line 176
        echo "  ";
        // line 177
        echo "  ";
        // line 178
        echo "  ";
        // line 179
        echo "  ";
        // line 180
        echo "  
  ";
        // line 182
        echo "  ";
        // line 183
        echo "  ";
        // line 184
        echo "  ";
        // line 185
        echo "  ";
        // line 186
        echo "  
  ";
        // line 188
        echo "  ";
        // line 189
        echo "  ";
        // line 190
        echo "  ";
        // line 191
        echo "  ";
        // line 192
        echo "
  
";
        
        $__internal_5668d441050255889e600d5c98abd37b4aba675aabe63a634fe69d016f141806->leave($__internal_5668d441050255889e600d5c98abd37b4aba675aabe63a634fe69d016f141806_prof);

        
        $__internal_6f058ec64c6173b9bdf994c5c316468639df58be00a139cbcbec7ec5a7e09250->leave($__internal_6f058ec64c6173b9bdf994c5c316468639df58be00a139cbcbec7ec5a7e09250_prof);

    }

    // line 196
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_0d7a518f715b439caf30272d595de88cabd5da92782a9783ad5cf4224c17532c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0d7a518f715b439caf30272d595de88cabd5da92782a9783ad5cf4224c17532c->enter($__internal_0d7a518f715b439caf30272d595de88cabd5da92782a9783ad5cf4224c17532c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_21d72c280f3e7adeedc24264f3b77da063ac8babc1ea7fa0fa60ecece0691b46 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_21d72c280f3e7adeedc24264f3b77da063ac8babc1ea7fa0fa60ecece0691b46->enter($__internal_21d72c280f3e7adeedc24264f3b77da063ac8babc1ea7fa0fa60ecece0691b46_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 197
        echo "
\t<script src=\"https://code.jquery.com/ui/1.12.1/jquery-ui.js\"></script>
\t
\t<script>
\t\t\$(document).ready(function(){
\t\t\tcompleteWeapon(); completeArmor();
\t\t// \tgetStats(); getSkills();
\t\t\t
\t\t// \t\$(\"select\").on('change', function() {
\t\t// \t\tgetStats(); clearTable(); getSkills();
\t\t// \t});
\t\t});
\t\t
\t\tfunction getStats(){
\t\t\t\$.ajax({
\t\t\t\t\turl: \"buildClass/\" + \$(\"#class\").val() + \",\" + \$(\"#lvl\").val(),
\t\t\t\t\tmethod: \"POST\",
\t\t\t\t\tdataType: \"JSON\"
\t\t\t}).done(function( data ){
\t\t\t\t\t\$(\"#Attack\").html(data[0]['baseattack']);
\t\t\t\t\t\$(\"#Reflex\").html(data[0]['refsave']);
\t\t\t\t\t\$(\"#Fortitude\").html(data[0]['fortsave']);
          \$(\"#Will\").html(data[0]['willsave']);
          \$(\"#AC\").html(data[0]['acbonus']);
      });
\t\t}
      
      function getSkills(){
\t\t\t\t\$.ajax({
\t\t\t\t\t\turl: \"ClassSkills/\" + \$(\"#class\").val(),
\t\t\t\t\t\tmethod: \"POST\",
\t\t\t\t\t\tdataType: \"JSON\"
\t\t\t\t}).done(function(data){
\t\t\t\t\t\tfor(var i = 0; i < data.length; i++){
\t\t\t\t\t\t\t\$(\"#skills\").append(\"<tr class = 'skill'><td>\" + data[i][0]['skillname'] + \"</td></tr>\");
\t\t\t\t\t\t}
\t      });
      }
      
      function clearTable(){
      \t\$(\".skill\").remove();
      }
      
      function checkWeapon(object){
      \tif(\$(object).hasClass(\"addWeapon\")){
      \t\t\$(object).removeClass(\"addWeapon\").addClass(\"removeWeapon\");
      \t\t\$(object).removeClass(\"glyphicon-plus-sign\").addClass(\"glyphicon-minus-sign\");
      \t\t\$(\"#wTable\").append(\"<tr><td><input class='wInput' type='text' placeholder='Weapon'><span class='glyphicon glyphicon-plus-sign addWeapon' aria-hidden='true' onclick='checkWeapon(this)'></span></td></tr>\");
      \t\tcompleteWeapon();
      \t}
      \t\t
      \telse if(\$(object).hasClass(\"removeWeapon\"))
      \t\t\$(object).parent().parent().remove();
      \t
      }
      
      function checkArmor(object){
      \tif(\$(object).hasClass(\"addArmor\")){
      \t\t\$(object).removeClass(\"addArmor\").addClass(\"removeArmor\");
      \t\t\$(object).removeClass(\"glyphicon-plus-sign\").addClass(\"glyphicon-minus-sign\");
      \t\t\$(\"#aTable\").append(\"<tr><td><input class='aInput' type='text' placeholder='Armor'><span class='glyphicon glyphicon-plus-sign addArmor' aria-hidden='true' onclick='checkArmor(this)'></span></td></tr>\");
      \t\tcompleteArmor();
      \t}
      \t
      \telse if(\$(object).hasClass(\"removeArmor\"))
      \t\t\$(object).parent().parent().remove();
      \t
      }
      
      function completeWeapon(){
      \t\$.ajax({
\t\t\t\t\turl: \"/weapons\",
\t\t\t\t\tdataType: \"JSON\",
\t\t\t\t\tsuccess: function(data){
\t\t\t\t\t\tvar names = [];
\t\t\t\t\t\t
\t\t\t\t\t\tfor(var i = 0; i < data.length; i++){
\t\t\t\t\t\t\tnames.push(data[i]['name']);
\t\t\t\t\t\t}
\t\t\t\t\t\t
\t\t\t\t\t\t\$(\".wInput\").autocomplete({
\t\t\t\t\t\t\tsource: names,
\t\t\t\t\t\t\tresponse: function(event, ui) {
\t\t\t\t\t\t\t\t\$(\".addWeapon\").click(function(){
\t\t\t\t\t\t\t\t\tcheckWeaponName(ui.content, \$(this).parent().find(\".wInput\"));
\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t}
\t\t\t\t\t\t});
\t\t\t\t\t}
\t\t\t\t});
      }
      
      function completeArmor(){
      \t\$.ajax({
\t\t\t\t\turl: \"/armors\",
\t\t\t\t\tdataType: \"JSON\",
\t\t\t\t\tsuccess: function(data){
\t\t\t\t\t\tvar names = [];
\t\t\t\t\t\t
\t\t\t\t\t\tfor(var i = 0; i < data.length; i++){
\t\t\t\t\t\t\tnames.push(data[i]['name']);
\t\t\t\t\t\t}
\t\t\t\t\t\t
\t\t\t\t\t\t\$(\".aInput\").autocomplete({
\t\t\t\t\t\t\tsource: names,
\t\t\t\t\t\t\tresponse: function(event, ui) {
\t\t\t\t\t\t\t\t\$(\".addArmor\").click(function(){
\t\t\t\t\t\t\t\t\tcheckArmorName(ui.content,\$(this).parent().find(\".aInput\"));
\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t}
\t\t\t\t\t\t});
\t\t\t\t\t}
\t\t\t\t});
      }
      
      function checkWeaponName(names, object){
      \tfor(var i = 0; i < names.length; i++){
      \t\tif(names[i]['value'] == \$(object).val()){
     \t\t\t\t\$(object).attr('readonly', 'readonly');
      \t\t\tgetWeapon(\$(object).val(),  \$(object));
      \t\t\treturn ;
      \t\t}
      \t}
      \t
      \t\$(object).attr('readonly', 'readonly');
      }
      
      function getWeapon(name, object){
      \t
      \tconsole.log(object);
      \t\$.ajax({
      \t\turl: \"/weapon/\" + name,
      \t\tdataType: \"JSON\",
      \t\tsuccess: function(data){
      \t\t\tconsole.log(data);
      \t\t\t\$(object).parent().parent().append(
      \t\t\t\t\"<td>\" + data[0]['weapongroup'] + \"</td>\" +
      \t\t\t\t\"<td>\" + data[0]['damages'] + \" | \" + data[0]['damagem'] + \"</td>\" +
      \t\t\t\t\"<td>\" + data[0]['damagetype'] + \"</td>\" +
      \t\t\t\t\"<td>\" + data[0]['critical'] + \"</td>\" +
      \t\t\t\t\"<td>\" + data[0]['wprange'] + \"</td>\" +
      \t\t\t\t\"<td>\" + data[0]['weapontype'] + \"</td>\"
      \t\t\t);
      \t\t}
      \t})
      }
      
      function checkArmorName(names, object){
      \tfor(var i = 0; i < names.length; i++){
      \t\tif(names[i]['value'] == \$(object).val()){
     \t\t\t\t\$(object).attr('readonly', 'readonly');
      \t\t\tgetArmor(\$(object).val(), \$(object));
      \t\t\treturn ;
      \t\t}
      \t}
      \t
      \t\$(object).attr('readonly', 'readonly');
      }
      
      function getArmor(name, object){
      \t\$.ajax({
      \t\turl: \"/armor/\" + name,
      \t\tdataType: \"JSON\",
      \t\tsuccess: function(data){
      \t\t\tconsole.log(data);
      \t\t\t
\t\t\t\t\t\t\$(object).parent().parent().append(
      \t\t\t\t\"<td>\" + data[0]['armortype'] + \"</td>\" + 
      \t\t\t\t\"<td>\" + data[0]['acbonus'] + \"</td>\" + 
      \t\t\t\t\"<td>\" + data[0]['maxdex'] + \"</td>\" + 
      \t\t\t\t\"<td>\" + data[0]['checkpenalty'] + \"</td>\" + 
      \t\t\t\t\"<td>\" + data[0]['spellfailure'] + \"</td>\" +
      \t\t\t\t\"<td>\" + data[0]['weight'] + \"</td>\" +
      \t\t\t\t\"<td>\" + data[0]['ismetal'] + \"</td>\"
      \t\t\t);
      \t\t}
      \t})
      }
\t</script>
";
        
        $__internal_21d72c280f3e7adeedc24264f3b77da063ac8babc1ea7fa0fa60ecece0691b46->leave($__internal_21d72c280f3e7adeedc24264f3b77da063ac8babc1ea7fa0fa60ecece0691b46_prof);

        
        $__internal_0d7a518f715b439caf30272d595de88cabd5da92782a9783ad5cf4224c17532c->leave($__internal_0d7a518f715b439caf30272d595de88cabd5da92782a9783ad5cf4224c17532c_prof);

    }

    public function getTemplateName()
    {
        return ":default:build.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  337 => 197,  328 => 196,  316 => 192,  314 => 191,  312 => 190,  310 => 189,  308 => 188,  305 => 186,  303 => 185,  301 => 184,  299 => 183,  297 => 182,  294 => 180,  292 => 179,  290 => 178,  288 => 177,  286 => 176,  284 => 175,  282 => 174,  280 => 173,  278 => 172,  276 => 171,  274 => 170,  272 => 169,  270 => 168,  268 => 167,  266 => 166,  264 => 165,  261 => 163,  259 => 162,  257 => 161,  255 => 160,  253 => 159,  250 => 157,  248 => 156,  246 => 155,  244 => 154,  242 => 153,  207 => 119,  205 => 118,  203 => 117,  200 => 115,  198 => 114,  196 => 113,  193 => 111,  191 => 110,  189 => 109,  187 => 108,  184 => 106,  182 => 105,  180 => 104,  178 => 103,  176 => 102,  174 => 101,  172 => 100,  170 => 99,  168 => 98,  165 => 96,  163 => 95,  161 => 94,  159 => 93,  156 => 91,  154 => 90,  152 => 89,  105 => 43,  96 => 42,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block stylesheets %}
\t\t
\t\t
\t\t#equipment{
\t\t\tmargin-top:10px;
\t\t}
\t\t
\t\t#inputs{
\t\t\tdisplay:inline-block;
\t\t}
\t\t
\t\t.glyphicon{
\t\t\tcursor: pointer;
\t\t}
\t\t
\t\t#tables{
\t\t\tmargin-top:10px;
\t\t\tmargin-bottom: 10px;
\t\t}
\t
\t\t#wTable, #wName, #wGroup, #wType, #dType, #aTable{
\t\t\twidth:100%;
\t\t\ttext-align:center;
\t\t}
\t\t
\t\t#wName th{
\t\t}
\t\t
\t\t #separator{
\t\t \theight:100px;
\t\t }
\t\t
\t\tth{
\t\t\ttext-align: center;
\t\t}
\t\t
{% endblock %}


{% block body %}
\t\t<div id=\"content\">
\t\t<div id=\"equipment\">
\t\t\t<div id=\"inputs\">
\t\t\t\t<div class = \"col-md-12 weapon\">\t
\t\t\t\t\t<table id=\"wTable\" border=\"1\" class=\"col-md-6\">
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<th>Name</th>
\t\t\t\t\t\t\t<th>Weapon Group</th>
\t\t\t\t\t\t\t<th>Damage</th>
\t\t\t\t\t\t\t<th>Damage Type</th>
\t\t\t\t\t\t\t<th>Critical</th>
\t\t\t\t\t\t\t<th>Range</th>
\t\t\t\t\t\t\t<th>Type</th>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td><input class=\"wInput\" type=\"text\" placeholder=\"Weapon\"><span class=\"glyphicon glyphicon-plus-sign addWeapon\" aria-hidden=\"true\" onclick=\"checkWeapon(this)\"></span></td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t</table>
\t\t\t\t</div>
\t\t\t\t<div class = \"col-md-12 armor\">
\t\t\t\t\t\t<div id=\"separator\"></div>
\t\t\t\t
\t\t\t\t\t\t<table id=\"aTable\" border=\"1\" class=\"col-md-6\">
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<th>Name</th>
\t\t\t\t\t\t\t\t<th>Type</th>
\t\t\t\t\t\t\t\t<th>AcBonus</th>
\t\t\t\t\t\t\t\t<th>MaxDex</th>
\t\t\t\t\t\t\t\t<th>CheckP</th>
\t\t\t\t\t\t\t\t<th>SpellFailure</th>
\t\t\t\t\t\t\t\t<th>Weight</th>
\t\t\t\t\t\t\t\t<th>Metallic</th>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<td><input class=\"aInput\" type=\"text\" placeholder=\"Armor\"><span class=\"glyphicon glyphicon-plus-sign addArmor\" aria-hidden=\"true\" onclick=\"checkArmor(this)\"></span></td>
\t\t\t\t\t\t\t</tr>\t\t\t
\t\t\t\t\t\t\t\t</table>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t
\t\t\t\t

\t\t\t\t\t
\t\t\t\t\t{#<tr id=\"wName\">#}
\t    {#\t\t\t<th class=\"col-md-4\" colspan=\"1\">Name</th>#}
\t\t\t\t\t{#</tr>#}
\t\t\t\t\t
\t\t\t\t\t{#<tr id=\"wGroup\">#}
\t    {#\t\t\t<th class=\"col-md-4\" colspan=\"1\">wGroup</th>#}
\t    {#\t\t\t<td class=\"col-md-8\" colspan=\"4\"></td>#}
\t\t\t\t\t{#</tr>#}
\t\t\t\t\t
\t\t\t\t\t{#<tr id=\"wType\">#}
\t\t\t\t\t{#\t<tr>#}
\t    {#\t\t\t<th class=\"col-md-4\" colspan=\"1\">wType</th>#}
\t    {#\t\t\t</tr>#}
\t    {#\t\t\t<tr>#}
\t    {#\t\t\t<td class=\"col-md-8\" colspan=\"8\">holaaaa</td>#}
\t    {#\t\t\t<td class=\"col-md-8\" colspan=\"8\">adiossss</td>#}
\t    {#\t\t\t</tr>#}
\t\t\t\t\t{#</tr>#}
\t\t\t\t\t
\t\t\t\t\t{#<tr id=\"dType\">#}
\t    {#\t\t\t<th class=\"col-md-4\" colspan=\"1\">dType</th>#}
\t    {#\t\t\t<td class=\"col-md-8\" colspan=\"4\"></td>#}
\t\t\t\t\t{#</tr>#}
\t\t\t\t\t
\t\t\t\t\t{#<tr id=\"wHeaders\">#}
\t    {#\t\t\t<th class=\"col-md-2  col-md-offset-1\">dS</th><th class=\"col-md-2\">dM</th><th class=\"col-md-2\">crit</th><th class=\"col-md-2\">wpR</th><th class=\"col-md-2\">wght</th>#}
\t\t\t\t\t{#</tr>#}
\t\t\t\t\t
\t\t\t\t\t{#<tr id=\"wInfo\">#}
\t    {#\t\t\t<td class=\"col-md-2 col-md-offset-1\">dS</td><td class=\"col-md-2\">dM</td><td class=\"col-md-2\">crit</td><td class=\"col-md-2\">wpR</td><td class=\"col-md-2\">wght</td>#}
\t\t\t\t\t{#</tr>#}
\t\t\t\t\t
\t\t
\t\t\t\t
\t\t\t\t
\t\t\t\t
\t\t\t\t<!-- <table id=\"aTable\" border=\"1\" class=\"col-md-12\">
\t\t\t\t\t<tr id=\"aName\">
\t    \t\t\t<th class=\"col-md-2\" colspan=\"1\">Name</th>
\t\t\t\t\t</tr>
\t\t\t\t\t
\t\t\t\t\t<tr id=\"aType\">
\t    \t\t\t<th class=\"col-md-2\" colspan=\"1\">aType</th>
\t\t\t\t\t</tr>

\t\t\t\t\t<tr id=\"aHeaders\">
\t    \t\t\t<th class=\"col-md-3\">acB</th><th class=\"col-md-3\">maxDex</th><th class=\"col-md-3\">checkPenalty</th><th class=\"col-md-3\">spFail</th>
\t\t\t\t\t</tr>
\t\t\t\t\t
\t\t\t\t\t<tr id=\"aProperties\">
\t\t\t\t\t</tr>
\t\t\t\t\t
\t\t\t\t\t<tr id=\"aHeaders\">
\t    \t\t\t<th class=\"col-md-4\">Weight</th><th class=\"col-md-4\" colspan=\"2\">Has metal</th><th class=\"col-md-4\">More info</th>
\t\t\t\t\t</tr>
\t\t\t\t\t
\t\t\t\t\t<tr id=\"aInfo\">
\t\t\t\t\t</tr>
\t\t\t\t\t
\t\t\t\t</table>
\t\t\t
\t\t</div>
\t</div>-->
  
 {# <select id = \"weapon\">#}
\t{#\t{% for weapon in weapons %}#}
\t{#  \t<option>{{ weapon['name'] }}</option>#}
\t{#  {% endfor %}   #}
\t{#</select>#}
\t
\t{#<select id = \"armor\">#}
\t{#\t{% for armor in armors %}#}
\t{#  \t<option>{{ armor['name'] }}</option>#}
\t{#  {% endfor %}   #}
\t{#</select>#}

  {#<table border = \"1\" style = \"text-align:center;\">#}
  {#\t<tr>#}
  {#\t\t<th>Base Attack</th>#}
  {#\t\t<th>Fortitude</th>#}
  {#\t\t<th>Reflex</th>#}
  {#\t\t<th>Will</th>#}
  {#\t\t<th>AC Bonus</th>#}
  {#\t</tr>#}
  {#\t<tr>#}
  {#\t\t<td id = \"Attack\">0</td>#}
  {#\t\t<td id = \"Fortitude\">0</td>#}
  {#\t\t<td id = \"Reflex\">0</td>#}
  {#\t\t<td id = \"Will\">0</td>#}
  {#\t\t<td id = \"AC\">0</td>#}
  {#\t</tr>#}
  {#</table>#}
  
  {#<table id = \"skills\" border = \"1\" style = \"text-align:center;\">#}
  {#\t<tr>#}
  {#\t\t<th>Skill Name</th>#}
  {#\t</tr>#}
  {#</table>#}
  
  {#<table id = \"weapon\" border = \"1\">#}
  {#\t<tr>#}
  {#\t\t<th>Weapon Name</th>#}
  {#\t</tr>#}
  {#</table>#}

  
{% endblock %}

{% block javascripts %}

\t<script src=\"https://code.jquery.com/ui/1.12.1/jquery-ui.js\"></script>
\t
\t<script>
\t\t\$(document).ready(function(){
\t\t\tcompleteWeapon(); completeArmor();
\t\t// \tgetStats(); getSkills();
\t\t\t
\t\t// \t\$(\"select\").on('change', function() {
\t\t// \t\tgetStats(); clearTable(); getSkills();
\t\t// \t});
\t\t});
\t\t
\t\tfunction getStats(){
\t\t\t\$.ajax({
\t\t\t\t\turl: \"buildClass/\" + \$(\"#class\").val() + \",\" + \$(\"#lvl\").val(),
\t\t\t\t\tmethod: \"POST\",
\t\t\t\t\tdataType: \"JSON\"
\t\t\t}).done(function( data ){
\t\t\t\t\t\$(\"#Attack\").html(data[0]['baseattack']);
\t\t\t\t\t\$(\"#Reflex\").html(data[0]['refsave']);
\t\t\t\t\t\$(\"#Fortitude\").html(data[0]['fortsave']);
          \$(\"#Will\").html(data[0]['willsave']);
          \$(\"#AC\").html(data[0]['acbonus']);
      });
\t\t}
      
      function getSkills(){
\t\t\t\t\$.ajax({
\t\t\t\t\t\turl: \"ClassSkills/\" + \$(\"#class\").val(),
\t\t\t\t\t\tmethod: \"POST\",
\t\t\t\t\t\tdataType: \"JSON\"
\t\t\t\t}).done(function(data){
\t\t\t\t\t\tfor(var i = 0; i < data.length; i++){
\t\t\t\t\t\t\t\$(\"#skills\").append(\"<tr class = 'skill'><td>\" + data[i][0]['skillname'] + \"</td></tr>\");
\t\t\t\t\t\t}
\t      });
      }
      
      function clearTable(){
      \t\$(\".skill\").remove();
      }
      
      function checkWeapon(object){
      \tif(\$(object).hasClass(\"addWeapon\")){
      \t\t\$(object).removeClass(\"addWeapon\").addClass(\"removeWeapon\");
      \t\t\$(object).removeClass(\"glyphicon-plus-sign\").addClass(\"glyphicon-minus-sign\");
      \t\t\$(\"#wTable\").append(\"<tr><td><input class='wInput' type='text' placeholder='Weapon'><span class='glyphicon glyphicon-plus-sign addWeapon' aria-hidden='true' onclick='checkWeapon(this)'></span></td></tr>\");
      \t\tcompleteWeapon();
      \t}
      \t\t
      \telse if(\$(object).hasClass(\"removeWeapon\"))
      \t\t\$(object).parent().parent().remove();
      \t
      }
      
      function checkArmor(object){
      \tif(\$(object).hasClass(\"addArmor\")){
      \t\t\$(object).removeClass(\"addArmor\").addClass(\"removeArmor\");
      \t\t\$(object).removeClass(\"glyphicon-plus-sign\").addClass(\"glyphicon-minus-sign\");
      \t\t\$(\"#aTable\").append(\"<tr><td><input class='aInput' type='text' placeholder='Armor'><span class='glyphicon glyphicon-plus-sign addArmor' aria-hidden='true' onclick='checkArmor(this)'></span></td></tr>\");
      \t\tcompleteArmor();
      \t}
      \t
      \telse if(\$(object).hasClass(\"removeArmor\"))
      \t\t\$(object).parent().parent().remove();
      \t
      }
      
      function completeWeapon(){
      \t\$.ajax({
\t\t\t\t\turl: \"/weapons\",
\t\t\t\t\tdataType: \"JSON\",
\t\t\t\t\tsuccess: function(data){
\t\t\t\t\t\tvar names = [];
\t\t\t\t\t\t
\t\t\t\t\t\tfor(var i = 0; i < data.length; i++){
\t\t\t\t\t\t\tnames.push(data[i]['name']);
\t\t\t\t\t\t}
\t\t\t\t\t\t
\t\t\t\t\t\t\$(\".wInput\").autocomplete({
\t\t\t\t\t\t\tsource: names,
\t\t\t\t\t\t\tresponse: function(event, ui) {
\t\t\t\t\t\t\t\t\$(\".addWeapon\").click(function(){
\t\t\t\t\t\t\t\t\tcheckWeaponName(ui.content, \$(this).parent().find(\".wInput\"));
\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t}
\t\t\t\t\t\t});
\t\t\t\t\t}
\t\t\t\t});
      }
      
      function completeArmor(){
      \t\$.ajax({
\t\t\t\t\turl: \"/armors\",
\t\t\t\t\tdataType: \"JSON\",
\t\t\t\t\tsuccess: function(data){
\t\t\t\t\t\tvar names = [];
\t\t\t\t\t\t
\t\t\t\t\t\tfor(var i = 0; i < data.length; i++){
\t\t\t\t\t\t\tnames.push(data[i]['name']);
\t\t\t\t\t\t}
\t\t\t\t\t\t
\t\t\t\t\t\t\$(\".aInput\").autocomplete({
\t\t\t\t\t\t\tsource: names,
\t\t\t\t\t\t\tresponse: function(event, ui) {
\t\t\t\t\t\t\t\t\$(\".addArmor\").click(function(){
\t\t\t\t\t\t\t\t\tcheckArmorName(ui.content,\$(this).parent().find(\".aInput\"));
\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t}
\t\t\t\t\t\t});
\t\t\t\t\t}
\t\t\t\t});
      }
      
      function checkWeaponName(names, object){
      \tfor(var i = 0; i < names.length; i++){
      \t\tif(names[i]['value'] == \$(object).val()){
     \t\t\t\t\$(object).attr('readonly', 'readonly');
      \t\t\tgetWeapon(\$(object).val(),  \$(object));
      \t\t\treturn ;
      \t\t}
      \t}
      \t
      \t\$(object).attr('readonly', 'readonly');
      }
      
      function getWeapon(name, object){
      \t
      \tconsole.log(object);
      \t\$.ajax({
      \t\turl: \"/weapon/\" + name,
      \t\tdataType: \"JSON\",
      \t\tsuccess: function(data){
      \t\t\tconsole.log(data);
      \t\t\t\$(object).parent().parent().append(
      \t\t\t\t\"<td>\" + data[0]['weapongroup'] + \"</td>\" +
      \t\t\t\t\"<td>\" + data[0]['damages'] + \" | \" + data[0]['damagem'] + \"</td>\" +
      \t\t\t\t\"<td>\" + data[0]['damagetype'] + \"</td>\" +
      \t\t\t\t\"<td>\" + data[0]['critical'] + \"</td>\" +
      \t\t\t\t\"<td>\" + data[0]['wprange'] + \"</td>\" +
      \t\t\t\t\"<td>\" + data[0]['weapontype'] + \"</td>\"
      \t\t\t);
      \t\t}
      \t})
      }
      
      function checkArmorName(names, object){
      \tfor(var i = 0; i < names.length; i++){
      \t\tif(names[i]['value'] == \$(object).val()){
     \t\t\t\t\$(object).attr('readonly', 'readonly');
      \t\t\tgetArmor(\$(object).val(), \$(object));
      \t\t\treturn ;
      \t\t}
      \t}
      \t
      \t\$(object).attr('readonly', 'readonly');
      }
      
      function getArmor(name, object){
      \t\$.ajax({
      \t\turl: \"/armor/\" + name,
      \t\tdataType: \"JSON\",
      \t\tsuccess: function(data){
      \t\t\tconsole.log(data);
      \t\t\t
\t\t\t\t\t\t\$(object).parent().parent().append(
      \t\t\t\t\"<td>\" + data[0]['armortype'] + \"</td>\" + 
      \t\t\t\t\"<td>\" + data[0]['acbonus'] + \"</td>\" + 
      \t\t\t\t\"<td>\" + data[0]['maxdex'] + \"</td>\" + 
      \t\t\t\t\"<td>\" + data[0]['checkpenalty'] + \"</td>\" + 
      \t\t\t\t\"<td>\" + data[0]['spellfailure'] + \"</td>\" +
      \t\t\t\t\"<td>\" + data[0]['weight'] + \"</td>\" +
      \t\t\t\t\"<td>\" + data[0]['ismetal'] + \"</td>\"
      \t\t\t);
      \t\t}
      \t})
      }
\t</script>
{% endblock %}", ":default:build.html.twig", "/home/a14eriamocob/public_html/DnD/app/Resources/views/default/build.html.twig");
    }
}

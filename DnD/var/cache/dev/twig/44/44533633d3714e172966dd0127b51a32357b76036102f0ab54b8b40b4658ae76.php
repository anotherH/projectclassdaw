<?php

/* FOSUserBundle:Resetting:reset.html.twig */
class __TwigTemplate_da94b0d4e010bc9e7ae661619340a53b901a7084bc88cedceca6f769d688815e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:reset.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_132fb41d7ddb852c0ff881f40c355fea997ac048759fd7da73f735ad7d6cd210 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_132fb41d7ddb852c0ff881f40c355fea997ac048759fd7da73f735ad7d6cd210->enter($__internal_132fb41d7ddb852c0ff881f40c355fea997ac048759fd7da73f735ad7d6cd210_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:reset.html.twig"));

        $__internal_ab9fae5166444dee72ddac42643aa0a33861c8d02da4290529bfc3739eda4782 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ab9fae5166444dee72ddac42643aa0a33861c8d02da4290529bfc3739eda4782->enter($__internal_ab9fae5166444dee72ddac42643aa0a33861c8d02da4290529bfc3739eda4782_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:reset.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_132fb41d7ddb852c0ff881f40c355fea997ac048759fd7da73f735ad7d6cd210->leave($__internal_132fb41d7ddb852c0ff881f40c355fea997ac048759fd7da73f735ad7d6cd210_prof);

        
        $__internal_ab9fae5166444dee72ddac42643aa0a33861c8d02da4290529bfc3739eda4782->leave($__internal_ab9fae5166444dee72ddac42643aa0a33861c8d02da4290529bfc3739eda4782_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_bc39848772fd6b7e56ebcc85ab3a4e48a49f9b461aa21d7c6062eb53183d71a6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bc39848772fd6b7e56ebcc85ab3a4e48a49f9b461aa21d7c6062eb53183d71a6->enter($__internal_bc39848772fd6b7e56ebcc85ab3a4e48a49f9b461aa21d7c6062eb53183d71a6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_fb0ee7a59b11dd8ee2edaf60db4bb83c705e7f27b569516ccbe0c7d9853b8849 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fb0ee7a59b11dd8ee2edaf60db4bb83c705e7f27b569516ccbe0c7d9853b8849->enter($__internal_fb0ee7a59b11dd8ee2edaf60db4bb83c705e7f27b569516ccbe0c7d9853b8849_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Resetting/reset_content.html.twig", "FOSUserBundle:Resetting:reset.html.twig", 4)->display($context);
        
        $__internal_fb0ee7a59b11dd8ee2edaf60db4bb83c705e7f27b569516ccbe0c7d9853b8849->leave($__internal_fb0ee7a59b11dd8ee2edaf60db4bb83c705e7f27b569516ccbe0c7d9853b8849_prof);

        
        $__internal_bc39848772fd6b7e56ebcc85ab3a4e48a49f9b461aa21d7c6062eb53183d71a6->leave($__internal_bc39848772fd6b7e56ebcc85ab3a4e48a49f9b461aa21d7c6062eb53183d71a6_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:reset.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Resetting/reset_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Resetting:reset.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/friendsofsymfony/user-bundle/Resources/views/Resetting/reset.html.twig");
    }
}

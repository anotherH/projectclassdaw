<?php

/* EasyAdminBundle:default:field_datetimetz.html.twig */
class __TwigTemplate_07016e9aaa0b7b2c6e670505edb0bfee77a5475cb3ef4f8cbfb909b24fc1a809 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_277b04c12e8207fcf2b52946c39e6be61750c730263c7434f61297585fb39995 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_277b04c12e8207fcf2b52946c39e6be61750c730263c7434f61297585fb39995->enter($__internal_277b04c12e8207fcf2b52946c39e6be61750c730263c7434f61297585fb39995_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_datetimetz.html.twig"));

        $__internal_ef21105f371e65d2e8bc3df415c67446890f4d7953ae0296027cb49e2b3853fb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ef21105f371e65d2e8bc3df415c67446890f4d7953ae0296027cb49e2b3853fb->enter($__internal_ef21105f371e65d2e8bc3df415c67446890f4d7953ae0296027cb49e2b3853fb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_datetimetz.html.twig"));

        // line 1
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, ($context["value"] ?? $this->getContext($context, "value")), $this->getAttribute(($context["field_options"] ?? $this->getContext($context, "field_options")), "format", array())), "html", null, true);
        echo "
";
        
        $__internal_277b04c12e8207fcf2b52946c39e6be61750c730263c7434f61297585fb39995->leave($__internal_277b04c12e8207fcf2b52946c39e6be61750c730263c7434f61297585fb39995_prof);

        
        $__internal_ef21105f371e65d2e8bc3df415c67446890f4d7953ae0296027cb49e2b3853fb->leave($__internal_ef21105f371e65d2e8bc3df415c67446890f4d7953ae0296027cb49e2b3853fb_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:field_datetimetz.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ value|date(field_options.format) }}
", "EasyAdminBundle:default:field_datetimetz.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/field_datetimetz.html.twig");
    }
}

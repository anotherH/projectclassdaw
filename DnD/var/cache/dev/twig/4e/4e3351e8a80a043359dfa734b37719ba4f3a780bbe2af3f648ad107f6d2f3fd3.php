<?php

/* FOSUserBundle:Registration:check_email.html.twig */
class __TwigTemplate_8048e930cf2f28b0ab7148d82bebc21282c1115bf762ee0e50ba384aab2a95ac extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Registration:check_email.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0286f558d5f9dc4290722bd13e41adf8f8650e519ccb4ff4ed956c0e12444b46 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0286f558d5f9dc4290722bd13e41adf8f8650e519ccb4ff4ed956c0e12444b46->enter($__internal_0286f558d5f9dc4290722bd13e41adf8f8650e519ccb4ff4ed956c0e12444b46_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:check_email.html.twig"));

        $__internal_9dbd4ae092efd9b4b126e6740589d1c9ab2244daecd48fdbfcc76193ed055921 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9dbd4ae092efd9b4b126e6740589d1c9ab2244daecd48fdbfcc76193ed055921->enter($__internal_9dbd4ae092efd9b4b126e6740589d1c9ab2244daecd48fdbfcc76193ed055921_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:check_email.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0286f558d5f9dc4290722bd13e41adf8f8650e519ccb4ff4ed956c0e12444b46->leave($__internal_0286f558d5f9dc4290722bd13e41adf8f8650e519ccb4ff4ed956c0e12444b46_prof);

        
        $__internal_9dbd4ae092efd9b4b126e6740589d1c9ab2244daecd48fdbfcc76193ed055921->leave($__internal_9dbd4ae092efd9b4b126e6740589d1c9ab2244daecd48fdbfcc76193ed055921_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_99b08e8ac1a364972765a5bda5583ffb0adda39fba89ec0be8ce49803b1d0959 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_99b08e8ac1a364972765a5bda5583ffb0adda39fba89ec0be8ce49803b1d0959->enter($__internal_99b08e8ac1a364972765a5bda5583ffb0adda39fba89ec0be8ce49803b1d0959_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_b7be3d56db7405701b7aecea88fc77683368d970b60ef7294d8278b3e266c728 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b7be3d56db7405701b7aecea88fc77683368d970b60ef7294d8278b3e266c728->enter($__internal_b7be3d56db7405701b7aecea88fc77683368d970b60ef7294d8278b3e266c728_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.check_email", array("%email%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "email", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
";
        
        $__internal_b7be3d56db7405701b7aecea88fc77683368d970b60ef7294d8278b3e266c728->leave($__internal_b7be3d56db7405701b7aecea88fc77683368d970b60ef7294d8278b3e266c728_prof);

        
        $__internal_99b08e8ac1a364972765a5bda5583ffb0adda39fba89ec0be8ce49803b1d0959->leave($__internal_99b08e8ac1a364972765a5bda5583ffb0adda39fba89ec0be8ce49803b1d0959_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:check_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
    <p>{{ 'registration.check_email'|trans({'%email%': user.email}) }}</p>
{% endblock fos_user_content %}
", "FOSUserBundle:Registration:check_email.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/check_email.html.twig");
    }
}

<?php

/* WebProfilerBundle:Collector:exception.css.twig */
class __TwigTemplate_08f95c2acd9ced45875a9f670527743de8b8fa032c63876077f9cf45215a8f26 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_28609db0f843022653c3b5278b4ca851b1cc891d38b33df90525a4b513b2a426 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_28609db0f843022653c3b5278b4ca851b1cc891d38b33df90525a4b513b2a426->enter($__internal_28609db0f843022653c3b5278b4ca851b1cc891d38b33df90525a4b513b2a426_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:exception.css.twig"));

        $__internal_a6e03e2ed059784e3a0f9263a8f3ac00f3ecfe01d3da7a4f2cfd3347a0e03051 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a6e03e2ed059784e3a0f9263a8f3ac00f3ecfe01d3da7a4f2cfd3347a0e03051->enter($__internal_a6e03e2ed059784e3a0f9263a8f3ac00f3ecfe01d3da7a4f2cfd3347a0e03051_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:exception.css.twig"));

        // line 1
        echo ".sf-reset .traces {
    padding: 0 0 1em 1.5em;
}
.sf-reset .traces a {
    font-size: 14px;
}
.sf-reset .traces abbr {
    border-bottom-color: #AAA;
    padding-bottom: 2px;
}
.sf-reset .traces li {
    color: #222;
    font-size: 14px;
    padding: 5px 0;
    list-style-type: decimal;
    margin: 0 0 0 1em;
}
.sf-reset .traces li.selected {
    background: rgba(255, 255, 153, 0.5);
}

.sf-reset .traces ol li {
    font-size: 12px;
    color: #777;
}
.sf-reset #logs .traces li.error {
    color: #AA3333;
}
.sf-reset #logs .traces li.warning {
    background: #FFCC00;
}
.sf-reset .trace {
    border: 1px solid #DDD;
    background: #FFF;
    padding: 10px;
    overflow: auto;
    margin: 1em 0;
}
.sf-reset .trace code,
#traces-text pre {
    font-size: 13px;
}
.sf-reset .block-exception {
    margin-bottom: 2em;
    background-color: #FFF;
    border: 1px solid #EEE;
    padding: 28px;
    word-wrap: break-word;
    overflow: hidden;
}
.sf-reset .block-exception h1 {
    font-size: 21px;
    font-weight: normal;
    margin: 0 0 12px;
}
.sf-reset .block-exception .linked {
    margin-top: 1em;
}

.sf-reset .block {
    margin-bottom: 2em;
}
.sf-reset .block h2 {
    font-size: 16px;
}
.sf-reset .block-exception div {
    font-size: 14px;
}
.sf-reset .block-exception-detected .illustration-exception,
.sf-reset .block-exception-detected .text-exception {
    float: left;
}
.sf-reset .block-exception-detected .illustration-exception {
    width: 110px;
}
.sf-reset .block-exception-detected .text-exception {
    width: 650px;
    margin-left: 20px;
    padding: 30px 44px 24px 46px;
    position: relative;
}
.sf-reset .text-exception .open-quote,
.sf-reset .text-exception .close-quote {
    font-family: Arial, Helvetica, sans-serif;
    position: absolute;
    color: #C9C9C9;
    font-size: 8em;
}
.sf-reset .open-quote {
    top: 0;
    left: 0;
}
.sf-reset .close-quote {
    bottom: -0.5em;
    right: 50px;
}
.sf-reset .toggle {
    vertical-align: middle;
}
";
        
        $__internal_28609db0f843022653c3b5278b4ca851b1cc891d38b33df90525a4b513b2a426->leave($__internal_28609db0f843022653c3b5278b4ca851b1cc891d38b33df90525a4b513b2a426_prof);

        
        $__internal_a6e03e2ed059784e3a0f9263a8f3ac00f3ecfe01d3da7a4f2cfd3347a0e03051->leave($__internal_a6e03e2ed059784e3a0f9263a8f3ac00f3ecfe01d3da7a4f2cfd3347a0e03051_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:exception.css.twig";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source(".sf-reset .traces {
    padding: 0 0 1em 1.5em;
}
.sf-reset .traces a {
    font-size: 14px;
}
.sf-reset .traces abbr {
    border-bottom-color: #AAA;
    padding-bottom: 2px;
}
.sf-reset .traces li {
    color: #222;
    font-size: 14px;
    padding: 5px 0;
    list-style-type: decimal;
    margin: 0 0 0 1em;
}
.sf-reset .traces li.selected {
    background: rgba(255, 255, 153, 0.5);
}

.sf-reset .traces ol li {
    font-size: 12px;
    color: #777;
}
.sf-reset #logs .traces li.error {
    color: #AA3333;
}
.sf-reset #logs .traces li.warning {
    background: #FFCC00;
}
.sf-reset .trace {
    border: 1px solid #DDD;
    background: #FFF;
    padding: 10px;
    overflow: auto;
    margin: 1em 0;
}
.sf-reset .trace code,
#traces-text pre {
    font-size: 13px;
}
.sf-reset .block-exception {
    margin-bottom: 2em;
    background-color: #FFF;
    border: 1px solid #EEE;
    padding: 28px;
    word-wrap: break-word;
    overflow: hidden;
}
.sf-reset .block-exception h1 {
    font-size: 21px;
    font-weight: normal;
    margin: 0 0 12px;
}
.sf-reset .block-exception .linked {
    margin-top: 1em;
}

.sf-reset .block {
    margin-bottom: 2em;
}
.sf-reset .block h2 {
    font-size: 16px;
}
.sf-reset .block-exception div {
    font-size: 14px;
}
.sf-reset .block-exception-detected .illustration-exception,
.sf-reset .block-exception-detected .text-exception {
    float: left;
}
.sf-reset .block-exception-detected .illustration-exception {
    width: 110px;
}
.sf-reset .block-exception-detected .text-exception {
    width: 650px;
    margin-left: 20px;
    padding: 30px 44px 24px 46px;
    position: relative;
}
.sf-reset .text-exception .open-quote,
.sf-reset .text-exception .close-quote {
    font-family: Arial, Helvetica, sans-serif;
    position: absolute;
    color: #C9C9C9;
    font-size: 8em;
}
.sf-reset .open-quote {
    top: 0;
    left: 0;
}
.sf-reset .close-quote {
    bottom: -0.5em;
    right: 50px;
}
.sf-reset .toggle {
    vertical-align: middle;
}
", "WebProfilerBundle:Collector:exception.css.twig", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/exception.css.twig");
    }
}

<?php

/* EasyAdminBundle:default:new.html.twig */
class __TwigTemplate_b6af4a7c5a8e91b95b2d214735e0a71f53263144947b2a37697d7e3b08a55189 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'body_class' => array($this, 'block_body_class'),
            'content_title' => array($this, 'block_content_title'),
            'main' => array($this, 'block_main'),
            'entity_form' => array($this, 'block_entity_form'),
            'body_javascript' => array($this, 'block_body_javascript'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 7
        return $this->loadTemplate($this->getAttribute($this->getAttribute(($context["_entity_config"] ?? $this->getContext($context, "_entity_config")), "templates", array()), "layout", array()), "EasyAdminBundle:default:new.html.twig", 7);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_87be0f34e52e6a4c4eb3ae3d55b2710edbb7677a25b02d76a11e1a58c7cd7f52 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_87be0f34e52e6a4c4eb3ae3d55b2710edbb7677a25b02d76a11e1a58c7cd7f52->enter($__internal_87be0f34e52e6a4c4eb3ae3d55b2710edbb7677a25b02d76a11e1a58c7cd7f52_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:new.html.twig"));

        $__internal_7ba366d5e650a2f5f81c1773449b91dec39434b84ae94a94468e648ecb4defb1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7ba366d5e650a2f5f81c1773449b91dec39434b84ae94a94468e648ecb4defb1->enter($__internal_7ba366d5e650a2f5f81c1773449b91dec39434b84ae94a94468e648ecb4defb1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:new.html.twig"));

        // line 1
        $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->setTheme(($context["form"] ?? $this->getContext($context, "form")), $this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getBackendConfiguration("design.form_theme"));
        // line 3
        $context["_entity_config"] = $this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getEntityConfiguration($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "query", array()), "get", array(0 => "entity"), "method"));
        // line 4
        $context["__internal_2bb202d0daa502d48a424e25cf579fe358aaa2fa27c4714917dc185f427ef9ba"] = $this->getAttribute(($context["_entity_config"] ?? $this->getContext($context, "_entity_config")), "translation_domain", array());
        // line 5
        $context["_trans_parameters"] = array("%entity_name%" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute(($context["_entity_config"] ?? $this->getContext($context, "_entity_config")), "name", array()), array(),         // line 4
($context["__internal_2bb202d0daa502d48a424e25cf579fe358aaa2fa27c4714917dc185f427ef9ba"] ?? $this->getContext($context, "__internal_2bb202d0daa502d48a424e25cf579fe358aaa2fa27c4714917dc185f427ef9ba"))), "%entity_label%" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute(        // line 5
($context["_entity_config"] ?? $this->getContext($context, "_entity_config")), "label", array()), array(),         // line 4
($context["__internal_2bb202d0daa502d48a424e25cf579fe358aaa2fa27c4714917dc185f427ef9ba"] ?? $this->getContext($context, "__internal_2bb202d0daa502d48a424e25cf579fe358aaa2fa27c4714917dc185f427ef9ba"))));
        // line 7
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_87be0f34e52e6a4c4eb3ae3d55b2710edbb7677a25b02d76a11e1a58c7cd7f52->leave($__internal_87be0f34e52e6a4c4eb3ae3d55b2710edbb7677a25b02d76a11e1a58c7cd7f52_prof);

        
        $__internal_7ba366d5e650a2f5f81c1773449b91dec39434b84ae94a94468e648ecb4defb1->leave($__internal_7ba366d5e650a2f5f81c1773449b91dec39434b84ae94a94468e648ecb4defb1_prof);

    }

    // line 9
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_3f24d52dabe20e6a5312aca67acccb99eb9b4413815b5510494d663553371966 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3f24d52dabe20e6a5312aca67acccb99eb9b4413815b5510494d663553371966->enter($__internal_3f24d52dabe20e6a5312aca67acccb99eb9b4413815b5510494d663553371966_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        $__internal_18b5bc00c07fce1e595f0c2513e6566244503fb89391bba4691d33d173332ab1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_18b5bc00c07fce1e595f0c2513e6566244503fb89391bba4691d33d173332ab1->enter($__internal_18b5bc00c07fce1e595f0c2513e6566244503fb89391bba4691d33d173332ab1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        echo twig_escape_filter($this->env, ("easyadmin-new-" . $this->getAttribute(($context["_entity_config"] ?? $this->getContext($context, "_entity_config")), "name", array())), "html", null, true);
        
        $__internal_18b5bc00c07fce1e595f0c2513e6566244503fb89391bba4691d33d173332ab1->leave($__internal_18b5bc00c07fce1e595f0c2513e6566244503fb89391bba4691d33d173332ab1_prof);

        
        $__internal_3f24d52dabe20e6a5312aca67acccb99eb9b4413815b5510494d663553371966->leave($__internal_3f24d52dabe20e6a5312aca67acccb99eb9b4413815b5510494d663553371966_prof);

    }

    // line 10
    public function block_body_class($context, array $blocks = array())
    {
        $__internal_b99a9bff592479be79deb1b542c7bda18b0371cf9695241e469694dc51efedae = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b99a9bff592479be79deb1b542c7bda18b0371cf9695241e469694dc51efedae->enter($__internal_b99a9bff592479be79deb1b542c7bda18b0371cf9695241e469694dc51efedae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_class"));

        $__internal_4c23a84a7e5bbef381d54bf4023c90736d20465d66e9062864e3747ebf62e238 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4c23a84a7e5bbef381d54bf4023c90736d20465d66e9062864e3747ebf62e238->enter($__internal_4c23a84a7e5bbef381d54bf4023c90736d20465d66e9062864e3747ebf62e238_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_class"));

        echo twig_escape_filter($this->env, ("new new-" . twig_lower_filter($this->env, $this->getAttribute(($context["_entity_config"] ?? $this->getContext($context, "_entity_config")), "name", array()))), "html", null, true);
        
        $__internal_4c23a84a7e5bbef381d54bf4023c90736d20465d66e9062864e3747ebf62e238->leave($__internal_4c23a84a7e5bbef381d54bf4023c90736d20465d66e9062864e3747ebf62e238_prof);

        
        $__internal_b99a9bff592479be79deb1b542c7bda18b0371cf9695241e469694dc51efedae->leave($__internal_b99a9bff592479be79deb1b542c7bda18b0371cf9695241e469694dc51efedae_prof);

    }

    // line 12
    public function block_content_title($context, array $blocks = array())
    {
        $__internal_e48147e18afa124debb308163f89d0ecd531f1c8377eebf33a5b59597fe25e04 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e48147e18afa124debb308163f89d0ecd531f1c8377eebf33a5b59597fe25e04->enter($__internal_e48147e18afa124debb308163f89d0ecd531f1c8377eebf33a5b59597fe25e04_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_title"));

        $__internal_4e2038d22a58644793c164c6e6e92d1d87af7cab277c23b3157049fd8245ce6d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4e2038d22a58644793c164c6e6e92d1d87af7cab277c23b3157049fd8245ce6d->enter($__internal_4e2038d22a58644793c164c6e6e92d1d87af7cab277c23b3157049fd8245ce6d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_title"));

        // line 13
        ob_start();
        // line 14
        echo "    ";
        $context["_default_title"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("new.page_title", ($context["_trans_parameters"] ?? $this->getContext($context, "_trans_parameters")), "EasyAdminBundle");
        // line 15
        echo "    ";
        echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute(($context["_entity_config"] ?? null), "new", array(), "any", false, true), "title", array(), "any", true, true)) ? ($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute($this->getAttribute(($context["_entity_config"] ?? $this->getContext($context, "_entity_config")), "new", array()), "title", array()), ($context["_trans_parameters"] ?? $this->getContext($context, "_trans_parameters")),         // line 4
($context["__internal_2bb202d0daa502d48a424e25cf579fe358aaa2fa27c4714917dc185f427ef9ba"] ?? $this->getContext($context, "__internal_2bb202d0daa502d48a424e25cf579fe358aaa2fa27c4714917dc185f427ef9ba")))) : (        // line 15
($context["_default_title"] ?? $this->getContext($context, "_default_title")))), "html", null, true);
        echo "
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_4e2038d22a58644793c164c6e6e92d1d87af7cab277c23b3157049fd8245ce6d->leave($__internal_4e2038d22a58644793c164c6e6e92d1d87af7cab277c23b3157049fd8245ce6d_prof);

        
        $__internal_e48147e18afa124debb308163f89d0ecd531f1c8377eebf33a5b59597fe25e04->leave($__internal_e48147e18afa124debb308163f89d0ecd531f1c8377eebf33a5b59597fe25e04_prof);

    }

    // line 19
    public function block_main($context, array $blocks = array())
    {
        $__internal_de9759f62d26407b09bee7b23b3601dd22b414a2c1c16a020d1a56580c142a3d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_de9759f62d26407b09bee7b23b3601dd22b414a2c1c16a020d1a56580c142a3d->enter($__internal_de9759f62d26407b09bee7b23b3601dd22b414a2c1c16a020d1a56580c142a3d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        $__internal_a7540c2a4441f8470cdf60f8068d83072714d7bd08aca061c76b3101bc651830 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a7540c2a4441f8470cdf60f8068d83072714d7bd08aca061c76b3101bc651830->enter($__internal_a7540c2a4441f8470cdf60f8068d83072714d7bd08aca061c76b3101bc651830_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 20
        echo "    ";
        $this->displayBlock('entity_form', $context, $blocks);
        
        $__internal_a7540c2a4441f8470cdf60f8068d83072714d7bd08aca061c76b3101bc651830->leave($__internal_a7540c2a4441f8470cdf60f8068d83072714d7bd08aca061c76b3101bc651830_prof);

        
        $__internal_de9759f62d26407b09bee7b23b3601dd22b414a2c1c16a020d1a56580c142a3d->leave($__internal_de9759f62d26407b09bee7b23b3601dd22b414a2c1c16a020d1a56580c142a3d_prof);

    }

    public function block_entity_form($context, array $blocks = array())
    {
        $__internal_376abea88aa22609438dbe36b46f8807ccf1d3a451a4f598b378cd3db256e0c4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_376abea88aa22609438dbe36b46f8807ccf1d3a451a4f598b378cd3db256e0c4->enter($__internal_376abea88aa22609438dbe36b46f8807ccf1d3a451a4f598b378cd3db256e0c4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "entity_form"));

        $__internal_e0b8439ca3b1f97db7954c192af7572ade84844067959d5761c3a3bdaba3db53 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e0b8439ca3b1f97db7954c192af7572ade84844067959d5761c3a3bdaba3db53->enter($__internal_e0b8439ca3b1f97db7954c192af7572ade84844067959d5761c3a3bdaba3db53_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "entity_form"));

        // line 21
        echo "        ";
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form');
        echo "
    ";
        
        $__internal_e0b8439ca3b1f97db7954c192af7572ade84844067959d5761c3a3bdaba3db53->leave($__internal_e0b8439ca3b1f97db7954c192af7572ade84844067959d5761c3a3bdaba3db53_prof);

        
        $__internal_376abea88aa22609438dbe36b46f8807ccf1d3a451a4f598b378cd3db256e0c4->leave($__internal_376abea88aa22609438dbe36b46f8807ccf1d3a451a4f598b378cd3db256e0c4_prof);

    }

    // line 25
    public function block_body_javascript($context, array $blocks = array())
    {
        $__internal_1d30aadeed1e1cb44b1c2ea585d8fd9a3ea2571a8b648c6081604e48ea9274ec = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1d30aadeed1e1cb44b1c2ea585d8fd9a3ea2571a8b648c6081604e48ea9274ec->enter($__internal_1d30aadeed1e1cb44b1c2ea585d8fd9a3ea2571a8b648c6081604e48ea9274ec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_javascript"));

        $__internal_0abc95f7061fd2e468d7e1afd03e77fba927ee691702d0ff08c1f6891eb7ea9d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0abc95f7061fd2e468d7e1afd03e77fba927ee691702d0ff08c1f6891eb7ea9d->enter($__internal_0abc95f7061fd2e468d7e1afd03e77fba927ee691702d0ff08c1f6891eb7ea9d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_javascript"));

        // line 26
        echo "    ";
        $this->displayParentBlock("body_javascript", $context, $blocks);
        echo "

    <script type=\"text/javascript\">
        \$(function() {
            \$('.new-form').areYouSure({ 'message': '";
        // line 30
        echo twig_escape_filter($this->env, twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("form.are_you_sure", array(), "EasyAdminBundle"), "js"), "html", null, true);
        echo "' });

            \$('.form-actions').easyAdminSticky();
        });
    </script>

    ";
        // line 36
        echo twig_include($this->env, $context, "@EasyAdmin/default/includes/_select2_widget.html.twig");
        echo "
";
        
        $__internal_0abc95f7061fd2e468d7e1afd03e77fba927ee691702d0ff08c1f6891eb7ea9d->leave($__internal_0abc95f7061fd2e468d7e1afd03e77fba927ee691702d0ff08c1f6891eb7ea9d_prof);

        
        $__internal_1d30aadeed1e1cb44b1c2ea585d8fd9a3ea2571a8b648c6081604e48ea9274ec->leave($__internal_1d30aadeed1e1cb44b1c2ea585d8fd9a3ea2571a8b648c6081604e48ea9274ec_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  189 => 36,  180 => 30,  172 => 26,  163 => 25,  150 => 21,  131 => 20,  122 => 19,  109 => 15,  108 => 4,  106 => 15,  103 => 14,  101 => 13,  92 => 12,  74 => 10,  56 => 9,  46 => 7,  44 => 4,  43 => 5,  42 => 4,  41 => 5,  39 => 4,  37 => 3,  35 => 1,  23 => 7,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% form_theme form with easyadmin_config('design.form_theme') %}

{% set _entity_config = easyadmin_entity(app.request.query.get('entity')) %}
{% trans_default_domain _entity_config.translation_domain %}
{% set _trans_parameters = { '%entity_name%': _entity_config.name|trans, '%entity_label%': _entity_config.label|trans } %}

{% extends _entity_config.templates.layout %}

{% block body_id 'easyadmin-new-' ~ _entity_config.name %}
{% block body_class 'new new-' ~ _entity_config.name|lower %}

{% block content_title %}
{% spaceless %}
    {% set _default_title = 'new.page_title'|trans(_trans_parameters, 'EasyAdminBundle') %}
    {{ _entity_config.new.title is defined ? _entity_config.new.title|trans(_trans_parameters) : _default_title }}
{% endspaceless %}
{% endblock %}

{% block main %}
    {% block entity_form %}
        {{ form(form) }}
    {% endblock entity_form %}
{% endblock %}

{% block body_javascript %}
    {{ parent() }}

    <script type=\"text/javascript\">
        \$(function() {
            \$('.new-form').areYouSure({ 'message': '{{ 'form.are_you_sure'|trans({}, 'EasyAdminBundle')|e('js') }}' });

            \$('.form-actions').easyAdminSticky();
        });
    </script>

    {{ include('@EasyAdmin/default/includes/_select2_widget.html.twig') }}
{% endblock %}
", "EasyAdminBundle:default:new.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/new.html.twig");
    }
}

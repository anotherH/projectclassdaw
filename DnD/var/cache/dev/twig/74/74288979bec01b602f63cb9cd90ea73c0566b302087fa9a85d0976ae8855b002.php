<?php

/* FOSUserBundle:Group:edit.html.twig */
class __TwigTemplate_676fe93e9f2ad72c3c641c2f24ad956732e0782b51d564ba0a72d1ccb5c47740 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8493effe513cf13a24fbd16a212f43cd57502605cac971db33b2f06e34b211d7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8493effe513cf13a24fbd16a212f43cd57502605cac971db33b2f06e34b211d7->enter($__internal_8493effe513cf13a24fbd16a212f43cd57502605cac971db33b2f06e34b211d7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:edit.html.twig"));

        $__internal_659a375669208f08c9b62595f85223855e29ad20b0a66253fa13f6ec377f7712 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_659a375669208f08c9b62595f85223855e29ad20b0a66253fa13f6ec377f7712->enter($__internal_659a375669208f08c9b62595f85223855e29ad20b0a66253fa13f6ec377f7712_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8493effe513cf13a24fbd16a212f43cd57502605cac971db33b2f06e34b211d7->leave($__internal_8493effe513cf13a24fbd16a212f43cd57502605cac971db33b2f06e34b211d7_prof);

        
        $__internal_659a375669208f08c9b62595f85223855e29ad20b0a66253fa13f6ec377f7712->leave($__internal_659a375669208f08c9b62595f85223855e29ad20b0a66253fa13f6ec377f7712_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_082f5926d985a71cc3fac94ba0f8ef1a7308a40347f45520b75e4e5b0d962872 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_082f5926d985a71cc3fac94ba0f8ef1a7308a40347f45520b75e4e5b0d962872->enter($__internal_082f5926d985a71cc3fac94ba0f8ef1a7308a40347f45520b75e4e5b0d962872_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_4499deebb8a884f3bdcefaf955b4223555cd9d2a344c01051bffa57b39f70be3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4499deebb8a884f3bdcefaf955b4223555cd9d2a344c01051bffa57b39f70be3->enter($__internal_4499deebb8a884f3bdcefaf955b4223555cd9d2a344c01051bffa57b39f70be3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/edit_content.html.twig", "FOSUserBundle:Group:edit.html.twig", 4)->display($context);
        
        $__internal_4499deebb8a884f3bdcefaf955b4223555cd9d2a344c01051bffa57b39f70be3->leave($__internal_4499deebb8a884f3bdcefaf955b4223555cd9d2a344c01051bffa57b39f70be3_prof);

        
        $__internal_082f5926d985a71cc3fac94ba0f8ef1a7308a40347f45520b75e4e5b0d962872->leave($__internal_082f5926d985a71cc3fac94ba0f8ef1a7308a40347f45520b75e4e5b0d962872_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/edit_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:edit.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/friendsofsymfony/user-bundle/Resources/views/Group/edit.html.twig");
    }
}

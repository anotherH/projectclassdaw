<?php

/* @Framework/Form/collection_widget.html.php */
class __TwigTemplate_f5ad834e2cd6ed7d8b6c0c31469a7444f6225dbf4d02c76df98bfcf39255c406 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3731b827994d3db15c074959103927431f806bc8385ced0af064b71ab9c751a0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3731b827994d3db15c074959103927431f806bc8385ced0af064b71ab9c751a0->enter($__internal_3731b827994d3db15c074959103927431f806bc8385ced0af064b71ab9c751a0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        $__internal_f8c87689054a9f72cff952c60addf0c886da4fe3b05f748e68114d5f9159dc25 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f8c87689054a9f72cff952c60addf0c886da4fe3b05f748e68114d5f9159dc25->enter($__internal_f8c87689054a9f72cff952c60addf0c886da4fe3b05f748e68114d5f9159dc25_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        // line 1
        echo "<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
";
        
        $__internal_3731b827994d3db15c074959103927431f806bc8385ced0af064b71ab9c751a0->leave($__internal_3731b827994d3db15c074959103927431f806bc8385ced0af064b71ab9c751a0_prof);

        
        $__internal_f8c87689054a9f72cff952c60addf0c886da4fe3b05f748e68114d5f9159dc25->leave($__internal_f8c87689054a9f72cff952c60addf0c886da4fe3b05f748e68114d5f9159dc25_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/collection_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
", "@Framework/Form/collection_widget.html.php", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/collection_widget.html.php");
    }
}

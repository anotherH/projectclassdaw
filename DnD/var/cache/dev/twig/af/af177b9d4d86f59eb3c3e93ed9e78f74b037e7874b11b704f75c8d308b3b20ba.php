<?php

/* @FOSUser/layout.html.twig */
class __TwigTemplate_75bf9bef385b240c21a6f37773193c547da3f44d8d3f47a4e693e47ac23222e1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7ae82fbb108a0657803234b446b7dbc94dcd6caf62d87adf926ffdb35c01cdcf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7ae82fbb108a0657803234b446b7dbc94dcd6caf62d87adf926ffdb35c01cdcf->enter($__internal_7ae82fbb108a0657803234b446b7dbc94dcd6caf62d87adf926ffdb35c01cdcf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/layout.html.twig"));

        $__internal_e0a8c871b3e0f61d33af38669b215cc5e1a8c7afa454bfef291d9b28aa9ddace = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e0a8c871b3e0f61d33af38669b215cc5e1a8c7afa454bfef291d9b28aa9ddace->enter($__internal_e0a8c871b3e0f61d33af38669b215cc5e1a8c7afa454bfef291d9b28aa9ddace_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/layout.html.twig"));

        // line 1
        echo "<html>
    <head>
        ";
        // line 3
        $this->displayBlock('head', $context, $blocks);
        // line 9
        echo "    </head>
    <body>
        
        <div id=\"header\">
            <input type=\"text\" name=\"search\" placeholder=\"Search..\">
            
            <!-- Sign in -->
            Sign in
            
            <!-- Log in -->
            ";
        // line 19
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 20
            echo "                ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("layout.logged_in_as", array("%username%" => $this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array()), "username", array())), "FOSUserBundle"), "html", null, true);
            echo " |
                <a href=\"";
            // line 21
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_logout");
            echo "\">
                    ";
            // line 22
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("layout.logout", array(), "FOSUserBundle"), "html", null, true);
            echo "
                </a>
            ";
        } else {
            // line 25
            echo "                <a href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_login");
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("layout.login", array(), "FOSUserBundle"), "html", null, true);
            echo "</a>
            ";
        }
        // line 27
        echo "            
        </div>


        ";
        // line 31
        if ($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "hasPreviousSession", array())) {
            // line 32
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "session", array()), "flashbag", array()), "all", array(), "method"));
            foreach ($context['_seq'] as $context["type"] => $context["messages"]) {
                // line 33
                echo "                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($context["messages"]);
                foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                    // line 34
                    echo "                    <div class=\"flash-";
                    echo twig_escape_filter($this->env, $context["type"], "html", null, true);
                    echo "\">
                        ";
                    // line 35
                    echo twig_escape_filter($this->env, $context["message"], "html", null, true);
                    echo "
                    </div>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 38
                echo "            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['type'], $context['messages'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 39
            echo "        ";
        }
        // line 40
        echo "
        <div>
            ";
        // line 42
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 44
        echo "        </div>
        
         <div id=\"footer\">
            ";
        // line 47
        $this->displayBlock('footer', $context, $blocks);
        // line 50
        echo "        </div>
    </body>
</html>
";
        
        $__internal_7ae82fbb108a0657803234b446b7dbc94dcd6caf62d87adf926ffdb35c01cdcf->leave($__internal_7ae82fbb108a0657803234b446b7dbc94dcd6caf62d87adf926ffdb35c01cdcf_prof);

        
        $__internal_e0a8c871b3e0f61d33af38669b215cc5e1a8c7afa454bfef291d9b28aa9ddace->leave($__internal_e0a8c871b3e0f61d33af38669b215cc5e1a8c7afa454bfef291d9b28aa9ddace_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_4a2a8eb9699b61a331f6c4fbefdfee980afaca884b0eb9b441307bf0990d5545 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4a2a8eb9699b61a331f6c4fbefdfee980afaca884b0eb9b441307bf0990d5545->enter($__internal_4a2a8eb9699b61a331f6c4fbefdfee980afaca884b0eb9b441307bf0990d5545_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_4d03817111fde594d4553a2286f29dfed718a298ded090586cb8a82030ca5e99 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4d03817111fde594d4553a2286f29dfed718a298ded090586cb8a82030ca5e99->enter($__internal_4d03817111fde594d4553a2286f29dfed718a298ded090586cb8a82030ca5e99_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "        
            <meta charset=\"UTF-8\" />
            <link rel=\"stylesheet\" href=\"style.css\" />
            <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        
        $__internal_4d03817111fde594d4553a2286f29dfed718a298ded090586cb8a82030ca5e99->leave($__internal_4d03817111fde594d4553a2286f29dfed718a298ded090586cb8a82030ca5e99_prof);

        
        $__internal_4a2a8eb9699b61a331f6c4fbefdfee980afaca884b0eb9b441307bf0990d5545->leave($__internal_4a2a8eb9699b61a331f6c4fbefdfee980afaca884b0eb9b441307bf0990d5545_prof);

    }

    public function block_title($context, array $blocks = array())
    {
        $__internal_7a5cc7e804c7ba7f4d81eadf321013e5a579d2862d992b6ae062e3505c7757fd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7a5cc7e804c7ba7f4d81eadf321013e5a579d2862d992b6ae062e3505c7757fd->enter($__internal_7a5cc7e804c7ba7f4d81eadf321013e5a579d2862d992b6ae062e3505c7757fd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_33824cc48019fa5ab54f5f729681fc80e22ee7d0783d738a9d61922ad4f71772 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_33824cc48019fa5ab54f5f729681fc80e22ee7d0783d738a9d61922ad4f71772->enter($__internal_33824cc48019fa5ab54f5f729681fc80e22ee7d0783d738a9d61922ad4f71772_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "D&D";
        
        $__internal_33824cc48019fa5ab54f5f729681fc80e22ee7d0783d738a9d61922ad4f71772->leave($__internal_33824cc48019fa5ab54f5f729681fc80e22ee7d0783d738a9d61922ad4f71772_prof);

        
        $__internal_7a5cc7e804c7ba7f4d81eadf321013e5a579d2862d992b6ae062e3505c7757fd->leave($__internal_7a5cc7e804c7ba7f4d81eadf321013e5a579d2862d992b6ae062e3505c7757fd_prof);

    }

    // line 42
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_7c1a396045af01bf74442d8225aa38273f9291a9c7b8f059c7d1579bfdb4e22c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7c1a396045af01bf74442d8225aa38273f9291a9c7b8f059c7d1579bfdb4e22c->enter($__internal_7c1a396045af01bf74442d8225aa38273f9291a9c7b8f059c7d1579bfdb4e22c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_ac89a53342edc49a111ee9722dbf099457a78077f73c84339c4a323fe8a1c614 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ac89a53342edc49a111ee9722dbf099457a78077f73c84339c4a323fe8a1c614->enter($__internal_ac89a53342edc49a111ee9722dbf099457a78077f73c84339c4a323fe8a1c614_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 43
        echo "            ";
        
        $__internal_ac89a53342edc49a111ee9722dbf099457a78077f73c84339c4a323fe8a1c614->leave($__internal_ac89a53342edc49a111ee9722dbf099457a78077f73c84339c4a323fe8a1c614_prof);

        
        $__internal_7c1a396045af01bf74442d8225aa38273f9291a9c7b8f059c7d1579bfdb4e22c->leave($__internal_7c1a396045af01bf74442d8225aa38273f9291a9c7b8f059c7d1579bfdb4e22c_prof);

    }

    // line 47
    public function block_footer($context, array $blocks = array())
    {
        $__internal_cf060f6c34bfb52b8d239ee312221ad103a6099c0491859cca4dcfcc03b4db43 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cf060f6c34bfb52b8d239ee312221ad103a6099c0491859cca4dcfcc03b4db43->enter($__internal_cf060f6c34bfb52b8d239ee312221ad103a6099c0491859cca4dcfcc03b4db43_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        $__internal_dddf23afd6d3fdfe50197061872a76bea43eaff3b6f057d049c29078def782a1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dddf23afd6d3fdfe50197061872a76bea43eaff3b6f057d049c29078def782a1->enter($__internal_dddf23afd6d3fdfe50197061872a76bea43eaff3b6f057d049c29078def782a1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        // line 48
        echo "                Dungeons & Dragons | &copy; Copyright 2017 by Núria Alcalá, Eric Amorós i Sara Enriques.
            ";
        
        $__internal_dddf23afd6d3fdfe50197061872a76bea43eaff3b6f057d049c29078def782a1->leave($__internal_dddf23afd6d3fdfe50197061872a76bea43eaff3b6f057d049c29078def782a1_prof);

        
        $__internal_cf060f6c34bfb52b8d239ee312221ad103a6099c0491859cca4dcfcc03b4db43->leave($__internal_cf060f6c34bfb52b8d239ee312221ad103a6099c0491859cca4dcfcc03b4db43_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  210 => 48,  201 => 47,  191 => 43,  182 => 42,  153 => 7,  148 => 4,  139 => 3,  126 => 50,  124 => 47,  119 => 44,  117 => 42,  113 => 40,  110 => 39,  104 => 38,  95 => 35,  90 => 34,  85 => 33,  80 => 32,  78 => 31,  72 => 27,  64 => 25,  58 => 22,  54 => 21,  49 => 20,  47 => 19,  35 => 9,  33 => 3,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<html>
    <head>
        {% block head %}
        
            <meta charset=\"UTF-8\" />
            <link rel=\"stylesheet\" href=\"style.css\" />
            <title>{% block title %}D&D{% endblock %}</title>
        {% endblock %}
    </head>
    <body>
        
        <div id=\"header\">
            <input type=\"text\" name=\"search\" placeholder=\"Search..\">
            
            <!-- Sign in -->
            Sign in
            
            <!-- Log in -->
            {% if is_granted(\"IS_AUTHENTICATED_REMEMBERED\") %}
                {{ 'layout.logged_in_as'|trans({'%username%': app.user.username}, 'FOSUserBundle') }} |
                <a href=\"{{ path('fos_user_security_logout') }}\">
                    {{ 'layout.logout'|trans({}, 'FOSUserBundle') }}
                </a>
            {% else %}
                <a href=\"{{ path('fos_user_security_login') }}\">{{ 'layout.login'|trans({}, 'FOSUserBundle') }}</a>
            {% endif %}
            
        </div>


        {% if app.request.hasPreviousSession %}
            {% for type, messages in app.session.flashbag.all() %}
                {% for message in messages %}
                    <div class=\"flash-{{ type }}\">
                        {{ message }}
                    </div>
                {% endfor %}
            {% endfor %}
        {% endif %}

        <div>
            {% block fos_user_content %}
            {% endblock fos_user_content %}
        </div>
        
         <div id=\"footer\">
            {% block footer %}
                Dungeons & Dragons | &copy; Copyright 2017 by Núria Alcalá, Eric Amorós i Sara Enriques.
            {% endblock %}
        </div>
    </body>
</html>
", "@FOSUser/layout.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/friendsofsymfony/user-bundle/Resources/views/layout.html.twig");
    }
}

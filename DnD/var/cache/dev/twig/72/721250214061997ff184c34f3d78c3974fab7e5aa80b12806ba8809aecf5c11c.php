<?php

/* FOSUserBundle:Registration:register_content.html.twig */
class __TwigTemplate_531b5cd564bc1b58c748640b894aa98d08c3e7c46ae40ad921dfc2dbd397fc31 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'head' => array($this, 'block_head'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d205268a3f182b8ddfd9e019d9a4049215b0a4227ccccb4c0a0584bc8e368435 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d205268a3f182b8ddfd9e019d9a4049215b0a4227ccccb4c0a0584bc8e368435->enter($__internal_d205268a3f182b8ddfd9e019d9a4049215b0a4227ccccb4c0a0584bc8e368435_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:register_content.html.twig"));

        $__internal_1749c590c001c858020cc0f3a7431b7a25162b14cadc1b3e9b18f0e92bcefd72 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1749c590c001c858020cc0f3a7431b7a25162b14cadc1b3e9b18f0e92bcefd72->enter($__internal_1749c590c001c858020cc0f3a7431b7a25162b14cadc1b3e9b18f0e92bcefd72_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:register_content.html.twig"));

        // line 1
        echo "<html>
    <head>
        ";
        // line 3
        $this->displayBlock('head', $context, $blocks);
        // line 32
        echo "        
        
    </head>
    <body>

        <div id=\"wrapper\">
            ";
        // line 39
        echo "    
            ";
        // line 40
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start', array("method" => "post", "action" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_registration_register"), "attr" => array("class" => "fos_user_registration_register")));
        echo "
                ";
        // line 41
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
                <div>
                    <input id=\"register\" type=\"submit\" value=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
                </div>
            ";
        // line 45
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
        
        </div>
    </body>
</html>";
        
        $__internal_d205268a3f182b8ddfd9e019d9a4049215b0a4227ccccb4c0a0584bc8e368435->leave($__internal_d205268a3f182b8ddfd9e019d9a4049215b0a4227ccccb4c0a0584bc8e368435_prof);

        
        $__internal_1749c590c001c858020cc0f3a7431b7a25162b14cadc1b3e9b18f0e92bcefd72->leave($__internal_1749c590c001c858020cc0f3a7431b7a25162b14cadc1b3e9b18f0e92bcefd72_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_bc9d2790b40c43c4d6e7ef15c425d00dba43604e9b6912c2cc782d7152a1a680 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bc9d2790b40c43c4d6e7ef15c425d00dba43604e9b6912c2cc782d7152a1a680->enter($__internal_bc9d2790b40c43c4d6e7ef15c425d00dba43604e9b6912c2cc782d7152a1a680_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_6a98f68874e2006269bba7c66a0c0955e9f4a2ed99dd542d5fabc677932fd8fa = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6a98f68874e2006269bba7c66a0c0955e9f4a2ed99dd542d5fabc677932fd8fa->enter($__internal_6a98f68874e2006269bba7c66a0c0955e9f4a2ed99dd542d5fabc677932fd8fa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "          
            <style>
                body{
                    background:#FAFAFA;
                }
                
                
                 #wrapper{
                    text-align: center;
                }
                
                #register, #fos_user_registration_form_email, #fos_user_registration_form_username, #fos_user_registration_form_plainPassword_first, #fos_user_registration_form_plainPassword_second{
                    margin:5px;
                }
                
                /*FORM IDs
                
                #fos_user_registration_form
                #fos_user_registration_form_email
                #fos_user_registration_form_username
                #fos_user_registration_form_plainPassword_first
                #fos_user_registration_form_plainPassword_second
                
                */
                
            </style>
            
        ";
        
        $__internal_6a98f68874e2006269bba7c66a0c0955e9f4a2ed99dd542d5fabc677932fd8fa->leave($__internal_6a98f68874e2006269bba7c66a0c0955e9f4a2ed99dd542d5fabc677932fd8fa_prof);

        
        $__internal_bc9d2790b40c43c4d6e7ef15c425d00dba43604e9b6912c2cc782d7152a1a680->leave($__internal_bc9d2790b40c43c4d6e7ef15c425d00dba43604e9b6912c2cc782d7152a1a680_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:register_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  81 => 4,  72 => 3,  57 => 45,  52 => 43,  47 => 41,  43 => 40,  40 => 39,  32 => 32,  30 => 3,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<html>
    <head>
        {% block head %}
          
            <style>
                body{
                    background:#FAFAFA;
                }
                
                
                 #wrapper{
                    text-align: center;
                }
                
                #register, #fos_user_registration_form_email, #fos_user_registration_form_username, #fos_user_registration_form_plainPassword_first, #fos_user_registration_form_plainPassword_second{
                    margin:5px;
                }
                
                /*FORM IDs
                
                #fos_user_registration_form
                #fos_user_registration_form_email
                #fos_user_registration_form_username
                #fos_user_registration_form_plainPassword_first
                #fos_user_registration_form_plainPassword_second
                
                */
                
            </style>
            
        {% endblock %}
        
        
    </head>
    <body>

        <div id=\"wrapper\">
            {% trans_default_domain 'FOSUserBundle' %}
    
            {{ form_start(form, {'method': 'post', 'action': path('fos_user_registration_register'), 'attr': {'class': 'fos_user_registration_register'}}) }}
                {{ form_widget(form) }}
                <div>
                    <input id=\"register\" type=\"submit\" value=\"{{ 'registration.submit'|trans }}\" />
                </div>
            {{ form_end(form) }}
        
        </div>
    </body>
</html>", "FOSUserBundle:Registration:register_content.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/register_content.html.twig");
    }
}

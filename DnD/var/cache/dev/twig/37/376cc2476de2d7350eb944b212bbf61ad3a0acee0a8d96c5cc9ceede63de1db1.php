<?php

/* FOSUserBundle:Group:list.html.twig */
class __TwigTemplate_043e44f9466819a497aa43478b8989cc6457e29210bdb6ddcdd00638e0a4e5c8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:list.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_82729e384bfb513e9bc30ff76cfc2ab9a7aa5eef3e674b8f72062814912c7953 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_82729e384bfb513e9bc30ff76cfc2ab9a7aa5eef3e674b8f72062814912c7953->enter($__internal_82729e384bfb513e9bc30ff76cfc2ab9a7aa5eef3e674b8f72062814912c7953_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:list.html.twig"));

        $__internal_1086da850acee9be8d2f11ada7d50f66e9b1c8a9a7e5c85d7cd5bbdf45b4f1ff = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1086da850acee9be8d2f11ada7d50f66e9b1c8a9a7e5c85d7cd5bbdf45b4f1ff->enter($__internal_1086da850acee9be8d2f11ada7d50f66e9b1c8a9a7e5c85d7cd5bbdf45b4f1ff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_82729e384bfb513e9bc30ff76cfc2ab9a7aa5eef3e674b8f72062814912c7953->leave($__internal_82729e384bfb513e9bc30ff76cfc2ab9a7aa5eef3e674b8f72062814912c7953_prof);

        
        $__internal_1086da850acee9be8d2f11ada7d50f66e9b1c8a9a7e5c85d7cd5bbdf45b4f1ff->leave($__internal_1086da850acee9be8d2f11ada7d50f66e9b1c8a9a7e5c85d7cd5bbdf45b4f1ff_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_b6de776407812057bf0ea4641f749eedf376948188e34fd953f0ea943ea846a0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b6de776407812057bf0ea4641f749eedf376948188e34fd953f0ea943ea846a0->enter($__internal_b6de776407812057bf0ea4641f749eedf376948188e34fd953f0ea943ea846a0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_797f68c75ea9c763be5859a4f603c38590102f8e8ea2d406a9abdf3504fa51d7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_797f68c75ea9c763be5859a4f603c38590102f8e8ea2d406a9abdf3504fa51d7->enter($__internal_797f68c75ea9c763be5859a4f603c38590102f8e8ea2d406a9abdf3504fa51d7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/list_content.html.twig", "FOSUserBundle:Group:list.html.twig", 4)->display($context);
        
        $__internal_797f68c75ea9c763be5859a4f603c38590102f8e8ea2d406a9abdf3504fa51d7->leave($__internal_797f68c75ea9c763be5859a4f603c38590102f8e8ea2d406a9abdf3504fa51d7_prof);

        
        $__internal_b6de776407812057bf0ea4641f749eedf376948188e34fd953f0ea943ea846a0->leave($__internal_b6de776407812057bf0ea4641f749eedf376948188e34fd953f0ea943ea846a0_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/list_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:list.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/friendsofsymfony/user-bundle/Resources/views/Group/list.html.twig");
    }
}

<?php

/* @Framework/Form/search_widget.html.php */
class __TwigTemplate_8c405bd1ecbe53421023ba67db3e9dd56476232350e5cfa7b3e62eb0689b74ef extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ccf047b9836ee215f3d80bf69626e3d3b395e430042c3ffd59e694e265fa18fc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ccf047b9836ee215f3d80bf69626e3d3b395e430042c3ffd59e694e265fa18fc->enter($__internal_ccf047b9836ee215f3d80bf69626e3d3b395e430042c3ffd59e694e265fa18fc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        $__internal_669c96fafe41e68dc55dc78b956c3be98de37390ab4021145872219fc666ddf2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_669c96fafe41e68dc55dc78b956c3be98de37390ab4021145872219fc666ddf2->enter($__internal_669c96fafe41e68dc55dc78b956c3be98de37390ab4021145872219fc666ddf2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
";
        
        $__internal_ccf047b9836ee215f3d80bf69626e3d3b395e430042c3ffd59e694e265fa18fc->leave($__internal_ccf047b9836ee215f3d80bf69626e3d3b395e430042c3ffd59e694e265fa18fc_prof);

        
        $__internal_669c96fafe41e68dc55dc78b956c3be98de37390ab4021145872219fc666ddf2->leave($__internal_669c96fafe41e68dc55dc78b956c3be98de37390ab4021145872219fc666ddf2_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/search_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
", "@Framework/Form/search_widget.html.php", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/search_widget.html.php");
    }
}

<?php

/* @Framework/Form/form_widget_compound.html.php */
class __TwigTemplate_85ef73396def2f304d1248be7f19c5d2f4df2d2b758f73c0ab786f1bea7d1fbf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2d5a8b49a5043595bceaee997ff622b52ae359d82cfa10cb54ee20a73cafa018 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2d5a8b49a5043595bceaee997ff622b52ae359d82cfa10cb54ee20a73cafa018->enter($__internal_2d5a8b49a5043595bceaee997ff622b52ae359d82cfa10cb54ee20a73cafa018_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_compound.html.php"));

        $__internal_79080d3e053235f92806c5c4b4f13364975be36c45774817026118c1cde4993a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_79080d3e053235f92806c5c4b4f13364975be36c45774817026118c1cde4993a->enter($__internal_79080d3e053235f92806c5c4b4f13364975be36c45774817026118c1cde4993a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_compound.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</div>
";
        
        $__internal_2d5a8b49a5043595bceaee997ff622b52ae359d82cfa10cb54ee20a73cafa018->leave($__internal_2d5a8b49a5043595bceaee997ff622b52ae359d82cfa10cb54ee20a73cafa018_prof);

        
        $__internal_79080d3e053235f92806c5c4b4f13364975be36c45774817026118c1cde4993a->leave($__internal_79080d3e053235f92806c5c4b4f13364975be36c45774817026118c1cde4993a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</div>
", "@Framework/Form/form_widget_compound.html.php", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_widget_compound.html.php");
    }
}

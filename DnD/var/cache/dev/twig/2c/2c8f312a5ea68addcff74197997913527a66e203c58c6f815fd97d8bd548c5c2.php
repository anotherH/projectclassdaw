<?php

/* FOSUserBundle:Profile:edit.html.twig */
class __TwigTemplate_54c092d62fc64be8e8c52d827ac597a3a1c4d2e12366afc0937e09fdd5dc351f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Profile:edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1ecbdf1e1d46e58c8de0b2dcc64fca2f181c29e4a1971b4bb661d7c9f03a3fe2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1ecbdf1e1d46e58c8de0b2dcc64fca2f181c29e4a1971b4bb661d7c9f03a3fe2->enter($__internal_1ecbdf1e1d46e58c8de0b2dcc64fca2f181c29e4a1971b4bb661d7c9f03a3fe2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:edit.html.twig"));

        $__internal_5dc171da93b16709e087163c648bd5cb674fb15da8afb652285222db9c61f8af = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5dc171da93b16709e087163c648bd5cb674fb15da8afb652285222db9c61f8af->enter($__internal_5dc171da93b16709e087163c648bd5cb674fb15da8afb652285222db9c61f8af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1ecbdf1e1d46e58c8de0b2dcc64fca2f181c29e4a1971b4bb661d7c9f03a3fe2->leave($__internal_1ecbdf1e1d46e58c8de0b2dcc64fca2f181c29e4a1971b4bb661d7c9f03a3fe2_prof);

        
        $__internal_5dc171da93b16709e087163c648bd5cb674fb15da8afb652285222db9c61f8af->leave($__internal_5dc171da93b16709e087163c648bd5cb674fb15da8afb652285222db9c61f8af_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_f55f51e3731570e60631cf2faa904c744f1fcc2fac0593b995fc346d4fdbc55c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f55f51e3731570e60631cf2faa904c744f1fcc2fac0593b995fc346d4fdbc55c->enter($__internal_f55f51e3731570e60631cf2faa904c744f1fcc2fac0593b995fc346d4fdbc55c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_05e70a128bcea11e22ee4cc2aed30a8537c10d407be0e756760ad4b7aeb881d3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_05e70a128bcea11e22ee4cc2aed30a8537c10d407be0e756760ad4b7aeb881d3->enter($__internal_05e70a128bcea11e22ee4cc2aed30a8537c10d407be0e756760ad4b7aeb881d3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Profile/edit_content.html.twig", "FOSUserBundle:Profile:edit.html.twig", 4)->display($context);
        
        $__internal_05e70a128bcea11e22ee4cc2aed30a8537c10d407be0e756760ad4b7aeb881d3->leave($__internal_05e70a128bcea11e22ee4cc2aed30a8537c10d407be0e756760ad4b7aeb881d3_prof);

        
        $__internal_f55f51e3731570e60631cf2faa904c744f1fcc2fac0593b995fc346d4fdbc55c->leave($__internal_f55f51e3731570e60631cf2faa904c744f1fcc2fac0593b995fc346d4fdbc55c_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Profile/edit_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Profile:edit.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/friendsofsymfony/user-bundle/Resources/views/Profile/edit.html.twig");
    }
}

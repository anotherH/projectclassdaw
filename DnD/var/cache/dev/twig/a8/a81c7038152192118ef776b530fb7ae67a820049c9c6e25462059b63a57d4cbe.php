<?php

/* @Framework/Form/password_widget.html.php */
class __TwigTemplate_e07616a86b0a557cbbb9b23ce5435a1eef853d16535e39a4f14f3a76aa1916a1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_66833f7ae0d5755dd0e614be3aefcd43574539ed829aee58ae8d00ae56391c5e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_66833f7ae0d5755dd0e614be3aefcd43574539ed829aee58ae8d00ae56391c5e->enter($__internal_66833f7ae0d5755dd0e614be3aefcd43574539ed829aee58ae8d00ae56391c5e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        $__internal_2420f77e6a590d6b1a3d9591921472c05b5795353fc9e09517f623032ffdfa63 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2420f77e6a590d6b1a3d9591921472c05b5795353fc9e09517f623032ffdfa63->enter($__internal_2420f77e6a590d6b1a3d9591921472c05b5795353fc9e09517f623032ffdfa63_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'password')) ?>
";
        
        $__internal_66833f7ae0d5755dd0e614be3aefcd43574539ed829aee58ae8d00ae56391c5e->leave($__internal_66833f7ae0d5755dd0e614be3aefcd43574539ed829aee58ae8d00ae56391c5e_prof);

        
        $__internal_2420f77e6a590d6b1a3d9591921472c05b5795353fc9e09517f623032ffdfa63->leave($__internal_2420f77e6a590d6b1a3d9591921472c05b5795353fc9e09517f623032ffdfa63_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/password_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'password')) ?>
", "@Framework/Form/password_widget.html.php", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/password_widget.html.php");
    }
}

<?php

/* EasyAdminBundle:default:field_bigint.html.twig */
class __TwigTemplate_ed2442e96321bd3643fba21eb165ef3154f3e173febb91247317b735f700b313 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_95238ae4f028e633fddaf9b788c6b975c68267889ee6115af83c1ea32f617422 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_95238ae4f028e633fddaf9b788c6b975c68267889ee6115af83c1ea32f617422->enter($__internal_95238ae4f028e633fddaf9b788c6b975c68267889ee6115af83c1ea32f617422_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_bigint.html.twig"));

        $__internal_54177e33c6f443283528b79c60d535e5bd11177c6c02d46aa301a82aae60add4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_54177e33c6f443283528b79c60d535e5bd11177c6c02d46aa301a82aae60add4->enter($__internal_54177e33c6f443283528b79c60d535e5bd11177c6c02d46aa301a82aae60add4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_bigint.html.twig"));

        // line 1
        if ($this->getAttribute(($context["field_options"] ?? $this->getContext($context, "field_options")), "format", array())) {
            // line 2
            echo "    ";
            echo twig_escape_filter($this->env, sprintf($this->getAttribute(($context["field_options"] ?? $this->getContext($context, "field_options")), "format", array()), ($context["value"] ?? $this->getContext($context, "value"))), "html", null, true);
            echo "
";
        } else {
            // line 4
            echo "    ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, ($context["value"] ?? $this->getContext($context, "value"))), "html", null, true);
            echo "
";
        }
        
        $__internal_95238ae4f028e633fddaf9b788c6b975c68267889ee6115af83c1ea32f617422->leave($__internal_95238ae4f028e633fddaf9b788c6b975c68267889ee6115af83c1ea32f617422_prof);

        
        $__internal_54177e33c6f443283528b79c60d535e5bd11177c6c02d46aa301a82aae60add4->leave($__internal_54177e33c6f443283528b79c60d535e5bd11177c6c02d46aa301a82aae60add4_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:field_bigint.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  33 => 4,  27 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if field_options.format %}
    {{ field_options.format|format(value) }}
{% else %}
    {{ value|number_format }}
{% endif %}
", "EasyAdminBundle:default:field_bigint.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/field_bigint.html.twig");
    }
}

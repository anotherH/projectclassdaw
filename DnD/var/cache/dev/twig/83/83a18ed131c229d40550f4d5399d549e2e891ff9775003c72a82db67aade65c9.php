<?php

/* EasyAdminBundle:default:label_empty.html.twig */
class __TwigTemplate_5e58ceed53c7cd17102e8bc745bb0e173d826e599c64a4b2b05a9b5c211d8873 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_922b9737a7400edb635374e783e475c391b3fb94fe12ccf5d17202b1bd366598 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_922b9737a7400edb635374e783e475c391b3fb94fe12ccf5d17202b1bd366598->enter($__internal_922b9737a7400edb635374e783e475c391b3fb94fe12ccf5d17202b1bd366598_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:label_empty.html.twig"));

        $__internal_65aed7e21cf31fda90a04b2d66850867740142aa57c68631fe4735cc054de21c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_65aed7e21cf31fda90a04b2d66850867740142aa57c68631fe4735cc054de21c->enter($__internal_65aed7e21cf31fda90a04b2d66850867740142aa57c68631fe4735cc054de21c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:label_empty.html.twig"));

        // line 1
        echo "<span class=\"label label-empty\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("label.empty", array(), "EasyAdminBundle"), "html", null, true);
        echo "</span>
";
        
        $__internal_922b9737a7400edb635374e783e475c391b3fb94fe12ccf5d17202b1bd366598->leave($__internal_922b9737a7400edb635374e783e475c391b3fb94fe12ccf5d17202b1bd366598_prof);

        
        $__internal_65aed7e21cf31fda90a04b2d66850867740142aa57c68631fe4735cc054de21c->leave($__internal_65aed7e21cf31fda90a04b2d66850867740142aa57c68631fe4735cc054de21c_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:label_empty.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<span class=\"label label-empty\">{{ 'label.empty'|trans(domain = 'EasyAdminBundle') }}</span>
", "EasyAdminBundle:default:label_empty.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/label_empty.html.twig");
    }
}

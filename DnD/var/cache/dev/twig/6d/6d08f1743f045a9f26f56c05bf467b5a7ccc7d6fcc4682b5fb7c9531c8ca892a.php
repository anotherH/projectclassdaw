<?php

/* @Framework/Form/form.html.php */
class __TwigTemplate_317533931f93e8b32c0bae4357c472d4c3d1167d29839191e749bc5541015394 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cb42bd9f58a3cb6448b0fa2ba0d5fec740bf0b61317b14e7fac5acac166e05e4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cb42bd9f58a3cb6448b0fa2ba0d5fec740bf0b61317b14e7fac5acac166e05e4->enter($__internal_cb42bd9f58a3cb6448b0fa2ba0d5fec740bf0b61317b14e7fac5acac166e05e4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        $__internal_2bf1f763157af608e7aa7d4efc25158b47faa82d2e48b3159e05a86130f15b2e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2bf1f763157af608e7aa7d4efc25158b47faa82d2e48b3159e05a86130f15b2e->enter($__internal_2bf1f763157af608e7aa7d4efc25158b47faa82d2e48b3159e05a86130f15b2e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        // line 1
        echo "<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
";
        
        $__internal_cb42bd9f58a3cb6448b0fa2ba0d5fec740bf0b61317b14e7fac5acac166e05e4->leave($__internal_cb42bd9f58a3cb6448b0fa2ba0d5fec740bf0b61317b14e7fac5acac166e05e4_prof);

        
        $__internal_2bf1f763157af608e7aa7d4efc25158b47faa82d2e48b3159e05a86130f15b2e->leave($__internal_2bf1f763157af608e7aa7d4efc25158b47faa82d2e48b3159e05a86130f15b2e_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
", "@Framework/Form/form.html.php", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form.html.php");
    }
}

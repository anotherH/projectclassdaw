<?php

/* TwigBundle:Exception:traces.xml.twig */
class __TwigTemplate_46a55a24544375d644ff6fcfdc39bb294aa1d7163ac979f329d1ee74648cc24c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ad1d3fcee16a8871618bb553ec1edfb6fa8976bde7130ab1ed92c602e46c3066 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ad1d3fcee16a8871618bb553ec1edfb6fa8976bde7130ab1ed92c602e46c3066->enter($__internal_ad1d3fcee16a8871618bb553ec1edfb6fa8976bde7130ab1ed92c602e46c3066_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:traces.xml.twig"));

        $__internal_e6095579c0c1feb01976fd4c6150caa15aa4dd040fd3215747501e268bd22d99 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e6095579c0c1feb01976fd4c6150caa15aa4dd040fd3215747501e268bd22d99->enter($__internal_e6095579c0c1feb01976fd4c6150caa15aa4dd040fd3215747501e268bd22d99_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:traces.xml.twig"));

        // line 1
        echo "        <traces>
";
        // line 2
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "trace", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["trace"]) {
            // line 3
            echo "            <trace>
";
            // line 4
            $this->loadTemplate("@Twig/Exception/trace.txt.twig", "TwigBundle:Exception:traces.xml.twig", 4)->display(array("trace" => $context["trace"]));
            // line 5
            echo "
            </trace>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['trace'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 8
        echo "        </traces>
";
        
        $__internal_ad1d3fcee16a8871618bb553ec1edfb6fa8976bde7130ab1ed92c602e46c3066->leave($__internal_ad1d3fcee16a8871618bb553ec1edfb6fa8976bde7130ab1ed92c602e46c3066_prof);

        
        $__internal_e6095579c0c1feb01976fd4c6150caa15aa4dd040fd3215747501e268bd22d99->leave($__internal_e6095579c0c1feb01976fd4c6150caa15aa4dd040fd3215747501e268bd22d99_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:traces.xml.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 8,  37 => 5,  35 => 4,  32 => 3,  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("        <traces>
{% for trace in exception.trace %}
            <trace>
{% include '@Twig/Exception/trace.txt.twig' with { 'trace': trace } only %}

            </trace>
{% endfor %}
        </traces>
", "TwigBundle:Exception:traces.xml.twig", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/traces.xml.twig");
    }
}

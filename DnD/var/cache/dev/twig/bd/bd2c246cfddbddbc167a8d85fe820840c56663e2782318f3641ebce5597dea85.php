<?php

/* @Framework/Form/form_rows.html.php */
class __TwigTemplate_5f3ecd940f87bcc69d8ed2fce45bbe8b90aac3cbca7673ef6d6403dc75f2e091 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_140af4c1504c8eafc940f4499ca8c627d37faf5dd4b090f2df6ee4a4429e6548 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_140af4c1504c8eafc940f4499ca8c627d37faf5dd4b090f2df6ee4a4429e6548->enter($__internal_140af4c1504c8eafc940f4499ca8c627d37faf5dd4b090f2df6ee4a4429e6548_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        $__internal_4d3a8348d86cfb2d211694780e4321c993b98ff0dfae877557cd44514b3615e8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4d3a8348d86cfb2d211694780e4321c993b98ff0dfae877557cd44514b3615e8->enter($__internal_4d3a8348d86cfb2d211694780e4321c993b98ff0dfae877557cd44514b3615e8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
";
        
        $__internal_140af4c1504c8eafc940f4499ca8c627d37faf5dd4b090f2df6ee4a4429e6548->leave($__internal_140af4c1504c8eafc940f4499ca8c627d37faf5dd4b090f2df6ee4a4429e6548_prof);

        
        $__internal_4d3a8348d86cfb2d211694780e4321c993b98ff0dfae877557cd44514b3615e8->leave($__internal_4d3a8348d86cfb2d211694780e4321c993b98ff0dfae877557cd44514b3615e8_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rows.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
", "@Framework/Form/form_rows.html.php", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_rows.html.php");
    }
}

<?php

/* EasyAdminBundle:default:field_decimal.html.twig */
class __TwigTemplate_4c054609e3ad757bac5349197cfa17b2054d109e2a2eaef472914a656d7bfe19 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2b633494ae98e97b84783f2f9b4ad14a9bcce8f2c122705d0bcec73ede589cca = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2b633494ae98e97b84783f2f9b4ad14a9bcce8f2c122705d0bcec73ede589cca->enter($__internal_2b633494ae98e97b84783f2f9b4ad14a9bcce8f2c122705d0bcec73ede589cca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_decimal.html.twig"));

        $__internal_9cc59eb7e06f654c9fcd107cc06b8c3da3f28ca2a60cf5f3e2a59dd16c3b6394 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9cc59eb7e06f654c9fcd107cc06b8c3da3f28ca2a60cf5f3e2a59dd16c3b6394->enter($__internal_9cc59eb7e06f654c9fcd107cc06b8c3da3f28ca2a60cf5f3e2a59dd16c3b6394_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_decimal.html.twig"));

        // line 1
        if ($this->getAttribute(($context["field_options"] ?? $this->getContext($context, "field_options")), "format", array())) {
            // line 2
            echo "    ";
            echo twig_escape_filter($this->env, sprintf($this->getAttribute(($context["field_options"] ?? $this->getContext($context, "field_options")), "format", array()), ($context["value"] ?? $this->getContext($context, "value"))), "html", null, true);
            echo "
";
        } else {
            // line 4
            echo "    ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, ($context["value"] ?? $this->getContext($context, "value")), 2), "html", null, true);
            echo "
";
        }
        
        $__internal_2b633494ae98e97b84783f2f9b4ad14a9bcce8f2c122705d0bcec73ede589cca->leave($__internal_2b633494ae98e97b84783f2f9b4ad14a9bcce8f2c122705d0bcec73ede589cca_prof);

        
        $__internal_9cc59eb7e06f654c9fcd107cc06b8c3da3f28ca2a60cf5f3e2a59dd16c3b6394->leave($__internal_9cc59eb7e06f654c9fcd107cc06b8c3da3f28ca2a60cf5f3e2a59dd16c3b6394_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:field_decimal.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  33 => 4,  27 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if field_options.format %}
    {{ field_options.format|format(value) }}
{% else %}
    {{ value|number_format(2) }}
{% endif %}
", "EasyAdminBundle:default:field_decimal.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/field_decimal.html.twig");
    }
}

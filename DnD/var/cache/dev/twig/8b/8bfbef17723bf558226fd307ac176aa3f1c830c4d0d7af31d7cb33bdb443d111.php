<?php

/* EasyAdminBundle:default:field_boolean.html.twig */
class __TwigTemplate_4730dad912772f35eb17ae6420df643d47cc3fde4632fda79aae3f2e50b473a9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5c70897038caf02005913aa9acff5af9f86bcad6ec3de7120512388bc581bf41 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5c70897038caf02005913aa9acff5af9f86bcad6ec3de7120512388bc581bf41->enter($__internal_5c70897038caf02005913aa9acff5af9f86bcad6ec3de7120512388bc581bf41_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_boolean.html.twig"));

        $__internal_4be3bd4f08540840ff98829bc11f6d0ce10aaa7b162c5a0685b9640ca4ce797d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4be3bd4f08540840ff98829bc11f6d0ce10aaa7b162c5a0685b9640ca4ce797d->enter($__internal_4be3bd4f08540840ff98829bc11f6d0ce10aaa7b162c5a0685b9640ca4ce797d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_boolean.html.twig"));

        // line 2
        echo "
";
        // line 3
        if ((($context["value"] ?? $this->getContext($context, "value")) == true)) {
            // line 4
            echo "    <span class=\"label label-success\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("label.true", array(), "EasyAdminBundle"), "html", null, true);
            echo "</span>
";
        } else {
            // line 6
            echo "    <span class=\"label label-danger\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("label.false", array(), "EasyAdminBundle"), "html", null, true);
            echo "</span>
";
        }
        
        $__internal_5c70897038caf02005913aa9acff5af9f86bcad6ec3de7120512388bc581bf41->leave($__internal_5c70897038caf02005913aa9acff5af9f86bcad6ec3de7120512388bc581bf41_prof);

        
        $__internal_4be3bd4f08540840ff98829bc11f6d0ce10aaa7b162c5a0685b9640ca4ce797d->leave($__internal_4be3bd4f08540840ff98829bc11f6d0ce10aaa7b162c5a0685b9640ca4ce797d_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:field_boolean.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  36 => 6,  30 => 4,  28 => 3,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'EasyAdminBundle' %}

{% if value == true %}
    <span class=\"label label-success\">{{ 'label.true'|trans }}</span>
{% else %}
    <span class=\"label label-danger\">{{ 'label.false'|trans }}</span>
{% endif %}
", "EasyAdminBundle:default:field_boolean.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/field_boolean.html.twig");
    }
}

<?php

/* @Framework/Form/form_errors.html.php */
class __TwigTemplate_7fd6d3769c54945b3b45343fb45b2dac597455f083568558fd6176b7cc73b49f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e61a20ddedda5c4921d6f58e7c1e49d4beb0bdb6a1309071424bf4d129715b81 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e61a20ddedda5c4921d6f58e7c1e49d4beb0bdb6a1309071424bf4d129715b81->enter($__internal_e61a20ddedda5c4921d6f58e7c1e49d4beb0bdb6a1309071424bf4d129715b81_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        $__internal_d1da131e872b8aed6dc27403222634b3f2d2ff6b23cddac68829e52ffb45fb22 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d1da131e872b8aed6dc27403222634b3f2d2ff6b23cddac68829e52ffb45fb22->enter($__internal_d1da131e872b8aed6dc27403222634b3f2d2ff6b23cddac68829e52ffb45fb22_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        // line 1
        echo "<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
";
        
        $__internal_e61a20ddedda5c4921d6f58e7c1e49d4beb0bdb6a1309071424bf4d129715b81->leave($__internal_e61a20ddedda5c4921d6f58e7c1e49d4beb0bdb6a1309071424bf4d129715b81_prof);

        
        $__internal_d1da131e872b8aed6dc27403222634b3f2d2ff6b23cddac68829e52ffb45fb22->leave($__internal_d1da131e872b8aed6dc27403222634b3f2d2ff6b23cddac68829e52ffb45fb22_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_errors.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
", "@Framework/Form/form_errors.html.php", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_errors.html.php");
    }
}

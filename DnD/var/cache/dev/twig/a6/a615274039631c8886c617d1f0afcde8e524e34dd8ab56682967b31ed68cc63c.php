<?php

/* FOSUserBundle:Resetting:email.txt.twig */
class __TwigTemplate_4b3afe1f8057ce009a2978dac1479f51e87249a43225ec751361293204199123 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8d6f7bea68ff4ac2d873debca7284e96adf5d39003e6e7da8a8f8608352363a7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8d6f7bea68ff4ac2d873debca7284e96adf5d39003e6e7da8a8f8608352363a7->enter($__internal_8d6f7bea68ff4ac2d873debca7284e96adf5d39003e6e7da8a8f8608352363a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:email.txt.twig"));

        $__internal_89cfeb13889635fd9f56854fbe4bc71df8370a24912cb8e543431e86171f3101 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_89cfeb13889635fd9f56854fbe4bc71df8370a24912cb8e543431e86171f3101->enter($__internal_89cfeb13889635fd9f56854fbe4bc71df8370a24912cb8e543431e86171f3101_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('body_text', $context, $blocks);
        // line 13
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_8d6f7bea68ff4ac2d873debca7284e96adf5d39003e6e7da8a8f8608352363a7->leave($__internal_8d6f7bea68ff4ac2d873debca7284e96adf5d39003e6e7da8a8f8608352363a7_prof);

        
        $__internal_89cfeb13889635fd9f56854fbe4bc71df8370a24912cb8e543431e86171f3101->leave($__internal_89cfeb13889635fd9f56854fbe4bc71df8370a24912cb8e543431e86171f3101_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_59892145f82bf2edfb8c5e723096f87a5bab049d12e234719ea4ba30cb73d488 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_59892145f82bf2edfb8c5e723096f87a5bab049d12e234719ea4ba30cb73d488->enter($__internal_59892145f82bf2edfb8c5e723096f87a5bab049d12e234719ea4ba30cb73d488_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        $__internal_512d96e5509f37b5d9dcfb8d996d9b9249fdc692b7325d20f7ff7520a4a9f756 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_512d96e5509f37b5d9dcfb8d996d9b9249fdc692b7325d20f7ff7520a4a9f756->enter($__internal_512d96e5509f37b5d9dcfb8d996d9b9249fdc692b7325d20f7ff7520a4a9f756_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.email.subject", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array())), "FOSUserBundle");
        
        $__internal_512d96e5509f37b5d9dcfb8d996d9b9249fdc692b7325d20f7ff7520a4a9f756->leave($__internal_512d96e5509f37b5d9dcfb8d996d9b9249fdc692b7325d20f7ff7520a4a9f756_prof);

        
        $__internal_59892145f82bf2edfb8c5e723096f87a5bab049d12e234719ea4ba30cb73d488->leave($__internal_59892145f82bf2edfb8c5e723096f87a5bab049d12e234719ea4ba30cb73d488_prof);

    }

    // line 8
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_6a38ce5a50a79f2f6f0632c5d4986815a423d1043398dde9a6dd15eadc9b8772 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6a38ce5a50a79f2f6f0632c5d4986815a423d1043398dde9a6dd15eadc9b8772->enter($__internal_6a38ce5a50a79f2f6f0632c5d4986815a423d1043398dde9a6dd15eadc9b8772_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        $__internal_b4d24eceae5f008a685ceea2d0f150789912d1d8922d19e90edfaf910f20c9a7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b4d24eceae5f008a685ceea2d0f150789912d1d8922d19e90edfaf910f20c9a7->enter($__internal_b4d24eceae5f008a685ceea2d0f150789912d1d8922d19e90edfaf910f20c9a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.email.message", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => ($context["confirmationUrl"] ?? $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_b4d24eceae5f008a685ceea2d0f150789912d1d8922d19e90edfaf910f20c9a7->leave($__internal_b4d24eceae5f008a685ceea2d0f150789912d1d8922d19e90edfaf910f20c9a7_prof);

        
        $__internal_6a38ce5a50a79f2f6f0632c5d4986815a423d1043398dde9a6dd15eadc9b8772->leave($__internal_6a38ce5a50a79f2f6f0632c5d4986815a423d1043398dde9a6dd15eadc9b8772_prof);

    }

    // line 13
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_307b3fc2782ff5d8575855efdbf2e707fd2c562b1d197078b0353873bc0ac417 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_307b3fc2782ff5d8575855efdbf2e707fd2c562b1d197078b0353873bc0ac417->enter($__internal_307b3fc2782ff5d8575855efdbf2e707fd2c562b1d197078b0353873bc0ac417_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        $__internal_49e440e46bee6dbe9ed2657c771493c20c5cf85d5e0116d942cd673171dcfa48 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_49e440e46bee6dbe9ed2657c771493c20c5cf85d5e0116d942cd673171dcfa48->enter($__internal_49e440e46bee6dbe9ed2657c771493c20c5cf85d5e0116d942cd673171dcfa48_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_49e440e46bee6dbe9ed2657c771493c20c5cf85d5e0116d942cd673171dcfa48->leave($__internal_49e440e46bee6dbe9ed2657c771493c20c5cf85d5e0116d942cd673171dcfa48_prof);

        
        $__internal_307b3fc2782ff5d8575855efdbf2e707fd2c562b1d197078b0353873bc0ac417->leave($__internal_307b3fc2782ff5d8575855efdbf2e707fd2c562b1d197078b0353873bc0ac417_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  85 => 13,  73 => 10,  64 => 8,  54 => 4,  45 => 2,  35 => 13,  33 => 8,  30 => 7,  28 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}
{% block subject %}
{%- autoescape false -%}
{{ 'resetting.email.subject'|trans({'%username%': user.username}) }}
{%- endautoescape -%}
{% endblock %}

{% block body_text %}
{% autoescape false %}
{{ 'resetting.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{% endautoescape %}
{% endblock %}
{% block body_html %}{% endblock %}
", "FOSUserBundle:Resetting:email.txt.twig", "/home/a14eriamocob/public_html/DnD/vendor/friendsofsymfony/user-bundle/Resources/views/Resetting/email.txt.twig");
    }
}

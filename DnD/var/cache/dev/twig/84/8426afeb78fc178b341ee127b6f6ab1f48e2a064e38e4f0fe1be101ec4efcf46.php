<?php

/* FOSUserBundle:Group:new.html.twig */
class __TwigTemplate_f638b83a544d0914fc6ccac86c5e6968cdbcd28473f1a9ac0ff3daf194281a1a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:new.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_01625bac7f5d83bfd90615e86ab5c0b9fe42cc2d80b1af70cb3df79bfac74b0d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_01625bac7f5d83bfd90615e86ab5c0b9fe42cc2d80b1af70cb3df79bfac74b0d->enter($__internal_01625bac7f5d83bfd90615e86ab5c0b9fe42cc2d80b1af70cb3df79bfac74b0d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:new.html.twig"));

        $__internal_1fec4723152c1a6a7e8dc1be586de1c73b8d6ab725362f3789fc3f5f2ab922f6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1fec4723152c1a6a7e8dc1be586de1c73b8d6ab725362f3789fc3f5f2ab922f6->enter($__internal_1fec4723152c1a6a7e8dc1be586de1c73b8d6ab725362f3789fc3f5f2ab922f6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_01625bac7f5d83bfd90615e86ab5c0b9fe42cc2d80b1af70cb3df79bfac74b0d->leave($__internal_01625bac7f5d83bfd90615e86ab5c0b9fe42cc2d80b1af70cb3df79bfac74b0d_prof);

        
        $__internal_1fec4723152c1a6a7e8dc1be586de1c73b8d6ab725362f3789fc3f5f2ab922f6->leave($__internal_1fec4723152c1a6a7e8dc1be586de1c73b8d6ab725362f3789fc3f5f2ab922f6_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_ddd2f9632e8a0825943a27603d772217271a1364724ee084d10a13b7eb7ca5db = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ddd2f9632e8a0825943a27603d772217271a1364724ee084d10a13b7eb7ca5db->enter($__internal_ddd2f9632e8a0825943a27603d772217271a1364724ee084d10a13b7eb7ca5db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_42df7435dca59da4c0e967d38368894f146da257c038de10ddf482afac37a7e1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_42df7435dca59da4c0e967d38368894f146da257c038de10ddf482afac37a7e1->enter($__internal_42df7435dca59da4c0e967d38368894f146da257c038de10ddf482afac37a7e1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/new_content.html.twig", "FOSUserBundle:Group:new.html.twig", 4)->display($context);
        
        $__internal_42df7435dca59da4c0e967d38368894f146da257c038de10ddf482afac37a7e1->leave($__internal_42df7435dca59da4c0e967d38368894f146da257c038de10ddf482afac37a7e1_prof);

        
        $__internal_ddd2f9632e8a0825943a27603d772217271a1364724ee084d10a13b7eb7ca5db->leave($__internal_ddd2f9632e8a0825943a27603d772217271a1364724ee084d10a13b7eb7ca5db_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/new_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:new.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/friendsofsymfony/user-bundle/Resources/views/Group/new.html.twig");
    }
}

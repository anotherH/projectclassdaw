<?php

/* FOSUserBundle:ChangePassword:change_password.html.twig */
class __TwigTemplate_01cf36017731a32abe5af81dbd5828eaeac0702705b86c405740147b196ae113 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:ChangePassword:change_password.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5dd01bcd1a55bddfc212f78d877d70d00baf503a0d9e4a87ebbc7c9911951af7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5dd01bcd1a55bddfc212f78d877d70d00baf503a0d9e4a87ebbc7c9911951af7->enter($__internal_5dd01bcd1a55bddfc212f78d877d70d00baf503a0d9e4a87ebbc7c9911951af7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:ChangePassword:change_password.html.twig"));

        $__internal_a3b05066ce0cc7d1ce1ff549399d3ed4fa9f23a402fb51dc7ae7856e45c78632 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a3b05066ce0cc7d1ce1ff549399d3ed4fa9f23a402fb51dc7ae7856e45c78632->enter($__internal_a3b05066ce0cc7d1ce1ff549399d3ed4fa9f23a402fb51dc7ae7856e45c78632_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:ChangePassword:change_password.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5dd01bcd1a55bddfc212f78d877d70d00baf503a0d9e4a87ebbc7c9911951af7->leave($__internal_5dd01bcd1a55bddfc212f78d877d70d00baf503a0d9e4a87ebbc7c9911951af7_prof);

        
        $__internal_a3b05066ce0cc7d1ce1ff549399d3ed4fa9f23a402fb51dc7ae7856e45c78632->leave($__internal_a3b05066ce0cc7d1ce1ff549399d3ed4fa9f23a402fb51dc7ae7856e45c78632_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_bcc0218eac98132ef0dcf7df86fd9da33cd36fb1a9cae5ae81b993af403128d9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bcc0218eac98132ef0dcf7df86fd9da33cd36fb1a9cae5ae81b993af403128d9->enter($__internal_bcc0218eac98132ef0dcf7df86fd9da33cd36fb1a9cae5ae81b993af403128d9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_a20e9838233d4993bd7413d9f61bbdd495a760a2216098a9a781ae1c0099b2ba = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a20e9838233d4993bd7413d9f61bbdd495a760a2216098a9a781ae1c0099b2ba->enter($__internal_a20e9838233d4993bd7413d9f61bbdd495a760a2216098a9a781ae1c0099b2ba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/ChangePassword/change_password_content.html.twig", "FOSUserBundle:ChangePassword:change_password.html.twig", 4)->display($context);
        
        $__internal_a20e9838233d4993bd7413d9f61bbdd495a760a2216098a9a781ae1c0099b2ba->leave($__internal_a20e9838233d4993bd7413d9f61bbdd495a760a2216098a9a781ae1c0099b2ba_prof);

        
        $__internal_bcc0218eac98132ef0dcf7df86fd9da33cd36fb1a9cae5ae81b993af403128d9->leave($__internal_bcc0218eac98132ef0dcf7df86fd9da33cd36fb1a9cae5ae81b993af403128d9_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:ChangePassword:change_password.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/ChangePassword/change_password_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:ChangePassword:change_password.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/friendsofsymfony/user-bundle/Resources/views/ChangePassword/change_password.html.twig");
    }
}

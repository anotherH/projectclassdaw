<?php

/* FOSUserBundle:Registration:register.html.twig */
class __TwigTemplate_86c38b2fba49baf7a102be48da052535026a20385af4be6fd03edc153f6fcfea extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "FOSUserBundle:Registration:register.html.twig", 2);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_59e58ac22327db54f6723a9d16d4e389da98dbfb8a3bc35d126966ba1d88f46f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_59e58ac22327db54f6723a9d16d4e389da98dbfb8a3bc35d126966ba1d88f46f->enter($__internal_59e58ac22327db54f6723a9d16d4e389da98dbfb8a3bc35d126966ba1d88f46f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:register.html.twig"));

        $__internal_1cd537127327d171f6e26fdf52afb99b668f63eec87976854d410005ec56b0a5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1cd537127327d171f6e26fdf52afb99b668f63eec87976854d410005ec56b0a5->enter($__internal_1cd537127327d171f6e26fdf52afb99b668f63eec87976854d410005ec56b0a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:register.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_59e58ac22327db54f6723a9d16d4e389da98dbfb8a3bc35d126966ba1d88f46f->leave($__internal_59e58ac22327db54f6723a9d16d4e389da98dbfb8a3bc35d126966ba1d88f46f_prof);

        
        $__internal_1cd537127327d171f6e26fdf52afb99b668f63eec87976854d410005ec56b0a5->leave($__internal_1cd537127327d171f6e26fdf52afb99b668f63eec87976854d410005ec56b0a5_prof);

    }

    // line 4
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_fb8203e7dfdb231b1f0fceecbeb6138d52e54b229391cae346ca6ceb9d3afb66 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fb8203e7dfdb231b1f0fceecbeb6138d52e54b229391cae346ca6ceb9d3afb66->enter($__internal_fb8203e7dfdb231b1f0fceecbeb6138d52e54b229391cae346ca6ceb9d3afb66_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_809be4d422f6e540ed34259d1fd4f0650eee5632eb53bbcb52115a6ee7a7c69f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_809be4d422f6e540ed34259d1fd4f0650eee5632eb53bbcb52115a6ee7a7c69f->enter($__internal_809be4d422f6e540ed34259d1fd4f0650eee5632eb53bbcb52115a6ee7a7c69f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 5
        $this->loadTemplate("@FOSUser/Registration/register_content.html.twig", "FOSUserBundle:Registration:register.html.twig", 5)->display($context);
        
        $__internal_809be4d422f6e540ed34259d1fd4f0650eee5632eb53bbcb52115a6ee7a7c69f->leave($__internal_809be4d422f6e540ed34259d1fd4f0650eee5632eb53bbcb52115a6ee7a7c69f_prof);

        
        $__internal_fb8203e7dfdb231b1f0fceecbeb6138d52e54b229391cae346ca6ceb9d3afb66->leave($__internal_fb8203e7dfdb231b1f0fceecbeb6138d52e54b229391cae346ca6ceb9d3afb66_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 5,  40 => 4,  11 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#{% extends \"@FOSUser/layout.html.twig\" %}#}
{% extends 'base.html.twig' %}

{% block fos_user_content %}
{% include \"@FOSUser/Registration/register_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Registration:register.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/register.html.twig");
    }
}

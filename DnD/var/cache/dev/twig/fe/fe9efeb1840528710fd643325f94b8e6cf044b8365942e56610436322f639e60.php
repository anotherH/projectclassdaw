<?php

/* @Framework/Form/checkbox_widget.html.php */
class __TwigTemplate_01472f5f5ae60ac9240810d171e5edb1e9858ea85cbe03de33978bcc1b0d9383 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_82e122520a2b1f7bc226d6b797dc8c80d14b159459047d3d24f25a83d50aa230 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_82e122520a2b1f7bc226d6b797dc8c80d14b159459047d3d24f25a83d50aa230->enter($__internal_82e122520a2b1f7bc226d6b797dc8c80d14b159459047d3d24f25a83d50aa230_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/checkbox_widget.html.php"));

        $__internal_b5a28d5fdb25ec1e9e926c1e697cc7c0abe383176f6f3f60914a13aa5f3c6880 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b5a28d5fdb25ec1e9e926c1e697cc7c0abe383176f6f3f60914a13aa5f3c6880->enter($__internal_b5a28d5fdb25ec1e9e926c1e697cc7c0abe383176f6f3f60914a13aa5f3c6880_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/checkbox_widget.html.php"));

        // line 1
        echo "<input type=\"checkbox\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    <?php if (strlen(\$value) > 0): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?>
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_82e122520a2b1f7bc226d6b797dc8c80d14b159459047d3d24f25a83d50aa230->leave($__internal_82e122520a2b1f7bc226d6b797dc8c80d14b159459047d3d24f25a83d50aa230_prof);

        
        $__internal_b5a28d5fdb25ec1e9e926c1e697cc7c0abe383176f6f3f60914a13aa5f3c6880->leave($__internal_b5a28d5fdb25ec1e9e926c1e697cc7c0abe383176f6f3f60914a13aa5f3c6880_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/checkbox_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<input type=\"checkbox\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    <?php if (strlen(\$value) > 0): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?>
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
", "@Framework/Form/checkbox_widget.html.php", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/checkbox_widget.html.php");
    }
}

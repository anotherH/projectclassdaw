<?php

/* FOSUserBundle:Security:login.html.twig */
class __TwigTemplate_5486af040d797b2d0258187975dfcfd8586ca6fc496b149901003b068baa9f88 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "FOSUserBundle:Security:login.html.twig", 2);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3e0762aca675861e05f39fd8226c2901525fc75c2b340452e76b157ef8e2e775 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3e0762aca675861e05f39fd8226c2901525fc75c2b340452e76b157ef8e2e775->enter($__internal_3e0762aca675861e05f39fd8226c2901525fc75c2b340452e76b157ef8e2e775_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Security:login.html.twig"));

        $__internal_3cf796543d30762d06d5f749701fa81a9e92342e424bbe3c85b9e03cdc823de7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3cf796543d30762d06d5f749701fa81a9e92342e424bbe3c85b9e03cdc823de7->enter($__internal_3cf796543d30762d06d5f749701fa81a9e92342e424bbe3c85b9e03cdc823de7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Security:login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3e0762aca675861e05f39fd8226c2901525fc75c2b340452e76b157ef8e2e775->leave($__internal_3e0762aca675861e05f39fd8226c2901525fc75c2b340452e76b157ef8e2e775_prof);

        
        $__internal_3cf796543d30762d06d5f749701fa81a9e92342e424bbe3c85b9e03cdc823de7->leave($__internal_3cf796543d30762d06d5f749701fa81a9e92342e424bbe3c85b9e03cdc823de7_prof);

    }

    // line 4
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_a5e243acfd476df8cf3c7cac8b17aa93fa0c440457ee7ac1c7bc0b2e82d12604 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a5e243acfd476df8cf3c7cac8b17aa93fa0c440457ee7ac1c7bc0b2e82d12604->enter($__internal_a5e243acfd476df8cf3c7cac8b17aa93fa0c440457ee7ac1c7bc0b2e82d12604_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_e0d255ad24475e06e8a6b97712c60bbc6287ce3405ab203ee3e0ae7e5d6a85e5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e0d255ad24475e06e8a6b97712c60bbc6287ce3405ab203ee3e0ae7e5d6a85e5->enter($__internal_e0d255ad24475e06e8a6b97712c60bbc6287ce3405ab203ee3e0ae7e5d6a85e5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 5
        echo twig_include($this->env, $context, "@FOSUser/Security/login_content.html.twig");
        echo "
";
        
        $__internal_e0d255ad24475e06e8a6b97712c60bbc6287ce3405ab203ee3e0ae7e5d6a85e5->leave($__internal_e0d255ad24475e06e8a6b97712c60bbc6287ce3405ab203ee3e0ae7e5d6a85e5_prof);

        
        $__internal_a5e243acfd476df8cf3c7cac8b17aa93fa0c440457ee7ac1c7bc0b2e82d12604->leave($__internal_a5e243acfd476df8cf3c7cac8b17aa93fa0c440457ee7ac1c7bc0b2e82d12604_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Security:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 5,  40 => 4,  11 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#{% extends \"@FOSUser/layout.html.twig\" %}#}
{% extends 'base.html.twig' %}

{% block fos_user_content %}
{{ include('@FOSUser/Security/login_content.html.twig') }}
{% endblock fos_user_content %}
", "FOSUserBundle:Security:login.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/friendsofsymfony/user-bundle/Resources/views/Security/login.html.twig");
    }
}

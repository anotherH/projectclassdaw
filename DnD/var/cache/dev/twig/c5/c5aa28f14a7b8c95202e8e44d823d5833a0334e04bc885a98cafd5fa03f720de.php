<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_ba3a89d3aa50b5a96c821b3880e14a1a7bf075e66c0201c3d16db8eab4a75665 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f3e916af8c9ad7ac70dc10b3070ecc5da6a2e19b4819ea1c8cb8e317d19a093f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f3e916af8c9ad7ac70dc10b3070ecc5da6a2e19b4819ea1c8cb8e317d19a093f->enter($__internal_f3e916af8c9ad7ac70dc10b3070ecc5da6a2e19b4819ea1c8cb8e317d19a093f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_d245064fef0af942501cbec64d5c43b0a9186281bd967d1dc2b9ce99ba37739d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d245064fef0af942501cbec64d5c43b0a9186281bd967d1dc2b9ce99ba37739d->enter($__internal_d245064fef0af942501cbec64d5c43b0a9186281bd967d1dc2b9ce99ba37739d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f3e916af8c9ad7ac70dc10b3070ecc5da6a2e19b4819ea1c8cb8e317d19a093f->leave($__internal_f3e916af8c9ad7ac70dc10b3070ecc5da6a2e19b4819ea1c8cb8e317d19a093f_prof);

        
        $__internal_d245064fef0af942501cbec64d5c43b0a9186281bd967d1dc2b9ce99ba37739d->leave($__internal_d245064fef0af942501cbec64d5c43b0a9186281bd967d1dc2b9ce99ba37739d_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_9ed7f4663e68f7a74e6465fa6e5fc4fb9440e55cf134eb965d959cffdc471dbe = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9ed7f4663e68f7a74e6465fa6e5fc4fb9440e55cf134eb965d959cffdc471dbe->enter($__internal_9ed7f4663e68f7a74e6465fa6e5fc4fb9440e55cf134eb965d959cffdc471dbe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_2a2231241a702c9585aebfd95cf301d02332e201119c9e40f4b05a7ffe14ca46 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2a2231241a702c9585aebfd95cf301d02332e201119c9e40f4b05a7ffe14ca46->enter($__internal_2a2231241a702c9585aebfd95cf301d02332e201119c9e40f4b05a7ffe14ca46_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_2a2231241a702c9585aebfd95cf301d02332e201119c9e40f4b05a7ffe14ca46->leave($__internal_2a2231241a702c9585aebfd95cf301d02332e201119c9e40f4b05a7ffe14ca46_prof);

        
        $__internal_9ed7f4663e68f7a74e6465fa6e5fc4fb9440e55cf134eb965d959cffdc471dbe->leave($__internal_9ed7f4663e68f7a74e6465fa6e5fc4fb9440e55cf134eb965d959cffdc471dbe_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_faaa8c2af3879d961b52dece8854096bc24ba0137c00cda0ea2fbdb779582834 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_faaa8c2af3879d961b52dece8854096bc24ba0137c00cda0ea2fbdb779582834->enter($__internal_faaa8c2af3879d961b52dece8854096bc24ba0137c00cda0ea2fbdb779582834_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_41ea44dffdf5836a470237f6f4fba2443973d23e8374945bbfb1926f411262e1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_41ea44dffdf5836a470237f6f4fba2443973d23e8374945bbfb1926f411262e1->enter($__internal_41ea44dffdf5836a470237f6f4fba2443973d23e8374945bbfb1926f411262e1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_41ea44dffdf5836a470237f6f4fba2443973d23e8374945bbfb1926f411262e1->leave($__internal_41ea44dffdf5836a470237f6f4fba2443973d23e8374945bbfb1926f411262e1_prof);

        
        $__internal_faaa8c2af3879d961b52dece8854096bc24ba0137c00cda0ea2fbdb779582834->leave($__internal_faaa8c2af3879d961b52dece8854096bc24ba0137c00cda0ea2fbdb779582834_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_135d102f6263d615f21a715d8f3c54070cf273314ff8fecde1df3e6ef1668f3a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_135d102f6263d615f21a715d8f3c54070cf273314ff8fecde1df3e6ef1668f3a->enter($__internal_135d102f6263d615f21a715d8f3c54070cf273314ff8fecde1df3e6ef1668f3a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_c8dbda708b35022c86e3b29b799f5d101c62bee1f275b83e5e8b8738ad6a99a3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c8dbda708b35022c86e3b29b799f5d101c62bee1f275b83e5e8b8738ad6a99a3->enter($__internal_c8dbda708b35022c86e3b29b799f5d101c62bee1f275b83e5e8b8738ad6a99a3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_c8dbda708b35022c86e3b29b799f5d101c62bee1f275b83e5e8b8738ad6a99a3->leave($__internal_c8dbda708b35022c86e3b29b799f5d101c62bee1f275b83e5e8b8738ad6a99a3_prof);

        
        $__internal_135d102f6263d615f21a715d8f3c54070cf273314ff8fecde1df3e6ef1668f3a->leave($__internal_135d102f6263d615f21a715d8f3c54070cf273314ff8fecde1df3e6ef1668f3a_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/exception.html.twig");
    }
}

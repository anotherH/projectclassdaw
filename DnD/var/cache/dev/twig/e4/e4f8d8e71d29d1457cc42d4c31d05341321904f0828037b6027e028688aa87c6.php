<?php

/* EasyAdminBundle:default:field_smallint.html.twig */
class __TwigTemplate_62e539faf91391c661cae6bc309eb7db730bed1855b6b4a5814a973d4dd90c4c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4d7a62a1d4cfa6d18fb29acc0568a94f6d205c5d1d309d4dcb48aef8f471507c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4d7a62a1d4cfa6d18fb29acc0568a94f6d205c5d1d309d4dcb48aef8f471507c->enter($__internal_4d7a62a1d4cfa6d18fb29acc0568a94f6d205c5d1d309d4dcb48aef8f471507c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_smallint.html.twig"));

        $__internal_c35e4af60f3bcf3b4dd0ad8316f1ba669720d6444f88ec3ef81dea41ae63fe74 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c35e4af60f3bcf3b4dd0ad8316f1ba669720d6444f88ec3ef81dea41ae63fe74->enter($__internal_c35e4af60f3bcf3b4dd0ad8316f1ba669720d6444f88ec3ef81dea41ae63fe74_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_smallint.html.twig"));

        // line 1
        if ($this->getAttribute(($context["field_options"] ?? $this->getContext($context, "field_options")), "format", array())) {
            // line 2
            echo "    ";
            echo twig_escape_filter($this->env, sprintf($this->getAttribute(($context["field_options"] ?? $this->getContext($context, "field_options")), "format", array()), ($context["value"] ?? $this->getContext($context, "value"))), "html", null, true);
            echo "
";
        } else {
            // line 4
            echo "    ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, ($context["value"] ?? $this->getContext($context, "value"))), "html", null, true);
            echo "
";
        }
        
        $__internal_4d7a62a1d4cfa6d18fb29acc0568a94f6d205c5d1d309d4dcb48aef8f471507c->leave($__internal_4d7a62a1d4cfa6d18fb29acc0568a94f6d205c5d1d309d4dcb48aef8f471507c_prof);

        
        $__internal_c35e4af60f3bcf3b4dd0ad8316f1ba669720d6444f88ec3ef81dea41ae63fe74->leave($__internal_c35e4af60f3bcf3b4dd0ad8316f1ba669720d6444f88ec3ef81dea41ae63fe74_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:field_smallint.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  33 => 4,  27 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if field_options.format %}
    {{ field_options.format|format(value) }}
{% else %}
    {{ value|number_format }}
{% endif %}
", "EasyAdminBundle:default:field_smallint.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/field_smallint.html.twig");
    }
}

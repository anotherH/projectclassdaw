<?php

/* @Framework/Form/form_enctype.html.php */
class __TwigTemplate_f2b672721143cc43fe8d504323a1bec52717b367add01efc9408f373b0d1b996 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_64ba26c4ad440e443d7880c556b01600799891f18060e1190870f7be9e82f67b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_64ba26c4ad440e443d7880c556b01600799891f18060e1190870f7be9e82f67b->enter($__internal_64ba26c4ad440e443d7880c556b01600799891f18060e1190870f7be9e82f67b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        $__internal_c4f11e95a4e8397ee8231995402abdaecaed65d13b97adba0a9d8f23d7bff42c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c4f11e95a4e8397ee8231995402abdaecaed65d13b97adba0a9d8f23d7bff42c->enter($__internal_c4f11e95a4e8397ee8231995402abdaecaed65d13b97adba0a9d8f23d7bff42c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        // line 1
        echo "<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
";
        
        $__internal_64ba26c4ad440e443d7880c556b01600799891f18060e1190870f7be9e82f67b->leave($__internal_64ba26c4ad440e443d7880c556b01600799891f18060e1190870f7be9e82f67b_prof);

        
        $__internal_c4f11e95a4e8397ee8231995402abdaecaed65d13b97adba0a9d8f23d7bff42c->leave($__internal_c4f11e95a4e8397ee8231995402abdaecaed65d13b97adba0a9d8f23d7bff42c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_enctype.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
", "@Framework/Form/form_enctype.html.php", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_enctype.html.php");
    }
}

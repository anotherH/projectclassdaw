<?php

/* WebProfilerBundle:Profiler:open.html.twig */
class __TwigTemplate_cb44be8cc60b8881f8f6e0d3332c6aa924f8b1d8223564ac1fc3c96b5c42a5de extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/base.html.twig", "WebProfilerBundle:Profiler:open.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ed78cb5db92e90f9596a4792849fd349f5b40bd6ec0d3cb69259ce4af888e054 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ed78cb5db92e90f9596a4792849fd349f5b40bd6ec0d3cb69259ce4af888e054->enter($__internal_ed78cb5db92e90f9596a4792849fd349f5b40bd6ec0d3cb69259ce4af888e054_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:open.html.twig"));

        $__internal_4dc8e0d7aa2bfc01e0b622f93584f9ce66c8c96e95e419f6a654c157a8c71d1a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4dc8e0d7aa2bfc01e0b622f93584f9ce66c8c96e95e419f6a654c157a8c71d1a->enter($__internal_4dc8e0d7aa2bfc01e0b622f93584f9ce66c8c96e95e419f6a654c157a8c71d1a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:open.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ed78cb5db92e90f9596a4792849fd349f5b40bd6ec0d3cb69259ce4af888e054->leave($__internal_ed78cb5db92e90f9596a4792849fd349f5b40bd6ec0d3cb69259ce4af888e054_prof);

        
        $__internal_4dc8e0d7aa2bfc01e0b622f93584f9ce66c8c96e95e419f6a654c157a8c71d1a->leave($__internal_4dc8e0d7aa2bfc01e0b622f93584f9ce66c8c96e95e419f6a654c157a8c71d1a_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_d69e213234b11fc0b24d93b6183c3053276296c1a3d34243975b64bbe0398db5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d69e213234b11fc0b24d93b6183c3053276296c1a3d34243975b64bbe0398db5->enter($__internal_d69e213234b11fc0b24d93b6183c3053276296c1a3d34243975b64bbe0398db5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_51ae024d7a98873ba4ee3bc0cfb1fe27e442830178132850d6dfc3f8ce728636 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_51ae024d7a98873ba4ee3bc0cfb1fe27e442830178132850d6dfc3f8ce728636->enter($__internal_51ae024d7a98873ba4ee3bc0cfb1fe27e442830178132850d6dfc3f8ce728636_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        ";
        // line 5
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/open.css.twig");
        echo "
    </style>
";
        
        $__internal_51ae024d7a98873ba4ee3bc0cfb1fe27e442830178132850d6dfc3f8ce728636->leave($__internal_51ae024d7a98873ba4ee3bc0cfb1fe27e442830178132850d6dfc3f8ce728636_prof);

        
        $__internal_d69e213234b11fc0b24d93b6183c3053276296c1a3d34243975b64bbe0398db5->leave($__internal_d69e213234b11fc0b24d93b6183c3053276296c1a3d34243975b64bbe0398db5_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_0f900456c3a5ce74d06ae13d8a07cedf93fef7fd2ba9f98127bbab896e05ac7a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0f900456c3a5ce74d06ae13d8a07cedf93fef7fd2ba9f98127bbab896e05ac7a->enter($__internal_0f900456c3a5ce74d06ae13d8a07cedf93fef7fd2ba9f98127bbab896e05ac7a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_920022ed5f5ee397433d66cd317f0d66c9f4aa95675abd2e51520624d73bc5ac = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_920022ed5f5ee397433d66cd317f0d66c9f4aa95675abd2e51520624d73bc5ac->enter($__internal_920022ed5f5ee397433d66cd317f0d66c9f4aa95675abd2e51520624d73bc5ac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "<div class=\"header\">
    <h1>";
        // line 11
        echo twig_escape_filter($this->env, ($context["file"] ?? $this->getContext($context, "file")), "html", null, true);
        echo " <small>line ";
        echo twig_escape_filter($this->env, ($context["line"] ?? $this->getContext($context, "line")), "html", null, true);
        echo "</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/";
        // line 12
        echo twig_escape_filter($this->env, twig_constant("Symfony\\Component\\HttpKernel\\Kernel::VERSION"), "html", null, true);
        echo "/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    ";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->fileExcerpt(($context["filename"] ?? $this->getContext($context, "filename")), ($context["line"] ?? $this->getContext($context, "line")),  -1);
        echo "
</div>
";
        
        $__internal_920022ed5f5ee397433d66cd317f0d66c9f4aa95675abd2e51520624d73bc5ac->leave($__internal_920022ed5f5ee397433d66cd317f0d66c9f4aa95675abd2e51520624d73bc5ac_prof);

        
        $__internal_0f900456c3a5ce74d06ae13d8a07cedf93fef7fd2ba9f98127bbab896e05ac7a->leave($__internal_0f900456c3a5ce74d06ae13d8a07cedf93fef7fd2ba9f98127bbab896e05ac7a_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:open.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 15,  84 => 12,  78 => 11,  75 => 10,  66 => 9,  53 => 5,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/base.html.twig' %}

{% block head %}
    <style>
        {{ include('@WebProfiler/Profiler/open.css.twig') }}
    </style>
{% endblock %}

{% block body %}
<div class=\"header\">
    <h1>{{ file }} <small>line {{ line }}</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/{{ constant('Symfony\\\\Component\\\\HttpKernel\\\\Kernel::VERSION') }}/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    {{ filename|file_excerpt(line, -1) }}
</div>
{% endblock %}
", "WebProfilerBundle:Profiler:open.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/open.html.twig");
    }
}

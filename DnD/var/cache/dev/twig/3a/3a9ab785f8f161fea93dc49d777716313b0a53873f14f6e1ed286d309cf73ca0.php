<?php

/* EasyAdminBundle:default:label_null.html.twig */
class __TwigTemplate_a3127a51c1a6e132d1b32aee2965cb2d36517821dc3131e466e8fac7ba4ad271 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_34babf3dadc23aecdf7f65b6ff0212d391bfc46a200f5c3695ccf854b2c91fd2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_34babf3dadc23aecdf7f65b6ff0212d391bfc46a200f5c3695ccf854b2c91fd2->enter($__internal_34babf3dadc23aecdf7f65b6ff0212d391bfc46a200f5c3695ccf854b2c91fd2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:label_null.html.twig"));

        $__internal_fcb8ceee676276e35dded0ddd0d4cf5fdadd69057bb052ef07dd34ca08b0f7c9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fcb8ceee676276e35dded0ddd0d4cf5fdadd69057bb052ef07dd34ca08b0f7c9->enter($__internal_fcb8ceee676276e35dded0ddd0d4cf5fdadd69057bb052ef07dd34ca08b0f7c9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:label_null.html.twig"));

        // line 1
        echo "<span class=\"label\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("label.null", array(), "EasyAdminBundle"), "html", null, true);
        echo "</span>
";
        
        $__internal_34babf3dadc23aecdf7f65b6ff0212d391bfc46a200f5c3695ccf854b2c91fd2->leave($__internal_34babf3dadc23aecdf7f65b6ff0212d391bfc46a200f5c3695ccf854b2c91fd2_prof);

        
        $__internal_fcb8ceee676276e35dded0ddd0d4cf5fdadd69057bb052ef07dd34ca08b0f7c9->leave($__internal_fcb8ceee676276e35dded0ddd0d4cf5fdadd69057bb052ef07dd34ca08b0f7c9_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:label_null.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<span class=\"label\">{{ 'label.null'|trans(domain = 'EasyAdminBundle') }}</span>
", "EasyAdminBundle:default:label_null.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/label_null.html.twig");
    }
}

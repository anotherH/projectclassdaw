<?php

/* TwigBundle:Exception:error.txt.twig */
class __TwigTemplate_5b19fd94607ab992d52e5220744f4fe940b9c40592186555684bfe91c09d48d2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4ed6b7c396d5b0092aa66856f701ed28484fcad64bd309f7b0872638cb8cc16b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4ed6b7c396d5b0092aa66856f701ed28484fcad64bd309f7b0872638cb8cc16b->enter($__internal_4ed6b7c396d5b0092aa66856f701ed28484fcad64bd309f7b0872638cb8cc16b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.txt.twig"));

        $__internal_42af26c6c479179cfec32a2c1fbb7830ba8deb69cc30f749d080703b9893bf65 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_42af26c6c479179cfec32a2c1fbb7830ba8deb69cc30f749d080703b9893bf65->enter($__internal_42af26c6c479179cfec32a2c1fbb7830ba8deb69cc30f749d080703b9893bf65_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.txt.twig"));

        // line 1
        echo "Oops! An Error Occurred
=======================

The server returned a \"";
        // line 4
        echo ($context["status_code"] ?? $this->getContext($context, "status_code"));
        echo " ";
        echo ($context["status_text"] ?? $this->getContext($context, "status_text"));
        echo "\".

Something is broken. Please let us know what you were doing when this error occurred.
We will fix it as soon as possible. Sorry for any inconvenience caused.
";
        
        $__internal_4ed6b7c396d5b0092aa66856f701ed28484fcad64bd309f7b0872638cb8cc16b->leave($__internal_4ed6b7c396d5b0092aa66856f701ed28484fcad64bd309f7b0872638cb8cc16b_prof);

        
        $__internal_42af26c6c479179cfec32a2c1fbb7830ba8deb69cc30f749d080703b9893bf65->leave($__internal_42af26c6c479179cfec32a2c1fbb7830ba8deb69cc30f749d080703b9893bf65_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 4,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("Oops! An Error Occurred
=======================

The server returned a \"{{ status_code }} {{ status_text }}\".

Something is broken. Please let us know what you were doing when this error occurred.
We will fix it as soon as possible. Sorry for any inconvenience caused.
", "TwigBundle:Exception:error.txt.twig", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.txt.twig");
    }
}

<?php

/* @WebProfiler/Collector/ajax.html.twig */
class __TwigTemplate_ef2dde039e1a1bf307e2157676c5769fbfcbb433414c9d6ee8516fbda03802ab extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/ajax.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4a852474c585e86d4e8a3f17c6fd4e9c9e6c8125bf305116caa4be3e38ece3af = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4a852474c585e86d4e8a3f17c6fd4e9c9e6c8125bf305116caa4be3e38ece3af->enter($__internal_4a852474c585e86d4e8a3f17c6fd4e9c9e6c8125bf305116caa4be3e38ece3af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/ajax.html.twig"));

        $__internal_d5c2ad38de7e86f01cfcebed5374a347185951194104f7e1ae75e6b82005dfe1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d5c2ad38de7e86f01cfcebed5374a347185951194104f7e1ae75e6b82005dfe1->enter($__internal_d5c2ad38de7e86f01cfcebed5374a347185951194104f7e1ae75e6b82005dfe1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/ajax.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4a852474c585e86d4e8a3f17c6fd4e9c9e6c8125bf305116caa4be3e38ece3af->leave($__internal_4a852474c585e86d4e8a3f17c6fd4e9c9e6c8125bf305116caa4be3e38ece3af_prof);

        
        $__internal_d5c2ad38de7e86f01cfcebed5374a347185951194104f7e1ae75e6b82005dfe1->leave($__internal_d5c2ad38de7e86f01cfcebed5374a347185951194104f7e1ae75e6b82005dfe1_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_031bf84a022e15ff23efdcfdaac1d2a63c25004b02c82ebf92e29e17b2dabc9a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_031bf84a022e15ff23efdcfdaac1d2a63c25004b02c82ebf92e29e17b2dabc9a->enter($__internal_031bf84a022e15ff23efdcfdaac1d2a63c25004b02c82ebf92e29e17b2dabc9a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_0873e7b019b56c001cf2f318681886778ac489dcc34016a282ebfea46fb614a9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0873e7b019b56c001cf2f318681886778ac489dcc34016a282ebfea46fb614a9->enter($__internal_0873e7b019b56c001cf2f318681886778ac489dcc34016a282ebfea46fb614a9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        // line 4
        echo "    ";
        ob_start();
        // line 5
        echo "        ";
        echo twig_include($this->env, $context, "@WebProfiler/Icon/ajax.svg");
        echo "
        <span class=\"sf-toolbar-value sf-toolbar-ajax-requests\">0</span>
    ";
        $context["icon"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 8
        echo "
    ";
        // line 9
        $context["text"] = ('' === $tmp = "        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    ") ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 29
        echo "
    ";
        // line 30
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/toolbar_item.html.twig", array("link" => false));
        echo "
";
        
        $__internal_0873e7b019b56c001cf2f318681886778ac489dcc34016a282ebfea46fb614a9->leave($__internal_0873e7b019b56c001cf2f318681886778ac489dcc34016a282ebfea46fb614a9_prof);

        
        $__internal_031bf84a022e15ff23efdcfdaac1d2a63c25004b02c82ebf92e29e17b2dabc9a->leave($__internal_031bf84a022e15ff23efdcfdaac1d2a63c25004b02c82ebf92e29e17b2dabc9a_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/ajax.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 30,  82 => 29,  62 => 9,  59 => 8,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}
    {% set icon %}
        {{ include('@WebProfiler/Icon/ajax.svg') }}
        <span class=\"sf-toolbar-value sf-toolbar-ajax-requests\">0</span>
    {% endset %}

    {% set text %}
        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    {% endset %}

    {{ include('@WebProfiler/Profiler/toolbar_item.html.twig', { link: false }) }}
{% endblock %}
", "@WebProfiler/Collector/ajax.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/ajax.html.twig");
    }
}

<?php

/* TwigBundle:Exception:error.atom.twig */
class __TwigTemplate_9e5d49bd010c6e541f4eb07a90769c339e40910c252c219f581b775037f90cf5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2e5c6c000c9e9472550f2791274f82b030934d84a330bc0e2611dd2607751944 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2e5c6c000c9e9472550f2791274f82b030934d84a330bc0e2611dd2607751944->enter($__internal_2e5c6c000c9e9472550f2791274f82b030934d84a330bc0e2611dd2607751944_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.atom.twig"));

        $__internal_cc95cf60d917777cdff1df4f13e5d7f80da415460294d50c3a6bf2157884d337 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cc95cf60d917777cdff1df4f13e5d7f80da415460294d50c3a6bf2157884d337->enter($__internal_cc95cf60d917777cdff1df4f13e5d7f80da415460294d50c3a6bf2157884d337_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/error.xml.twig", "TwigBundle:Exception:error.atom.twig", 1)->display($context);
        
        $__internal_2e5c6c000c9e9472550f2791274f82b030934d84a330bc0e2611dd2607751944->leave($__internal_2e5c6c000c9e9472550f2791274f82b030934d84a330bc0e2611dd2607751944_prof);

        
        $__internal_cc95cf60d917777cdff1df4f13e5d7f80da415460294d50c3a6bf2157884d337->leave($__internal_cc95cf60d917777cdff1df4f13e5d7f80da415460294d50c3a6bf2157884d337_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.atom.twig";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include '@Twig/Exception/error.xml.twig' %}
", "TwigBundle:Exception:error.atom.twig", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.atom.twig");
    }
}

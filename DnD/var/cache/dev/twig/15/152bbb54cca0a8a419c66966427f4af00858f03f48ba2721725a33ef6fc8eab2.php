<?php

/* @Framework/Form/number_widget.html.php */
class __TwigTemplate_9ed32331b5a1ee48526d10d13699ac1b9c841c5fdde9b41aaab687a70ad68e4e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8c7aa4335b1101616ed9cc8c3ac744db50a1bdd57007f4b0ff0e60a26357a28d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8c7aa4335b1101616ed9cc8c3ac744db50a1bdd57007f4b0ff0e60a26357a28d->enter($__internal_8c7aa4335b1101616ed9cc8c3ac744db50a1bdd57007f4b0ff0e60a26357a28d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        $__internal_a3ac62bdfade26471dfe1a39c4fe824ac36ecf10d79914b81829921b1e4602c5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a3ac62bdfade26471dfe1a39c4fe824ac36ecf10d79914b81829921b1e4602c5->enter($__internal_a3ac62bdfade26471dfe1a39c4fe824ac36ecf10d79914b81829921b1e4602c5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?>
";
        
        $__internal_8c7aa4335b1101616ed9cc8c3ac744db50a1bdd57007f4b0ff0e60a26357a28d->leave($__internal_8c7aa4335b1101616ed9cc8c3ac744db50a1bdd57007f4b0ff0e60a26357a28d_prof);

        
        $__internal_a3ac62bdfade26471dfe1a39c4fe824ac36ecf10d79914b81829921b1e4602c5->leave($__internal_a3ac62bdfade26471dfe1a39c4fe824ac36ecf10d79914b81829921b1e4602c5_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/number_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?>
", "@Framework/Form/number_widget.html.php", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/number_widget.html.php");
    }
}

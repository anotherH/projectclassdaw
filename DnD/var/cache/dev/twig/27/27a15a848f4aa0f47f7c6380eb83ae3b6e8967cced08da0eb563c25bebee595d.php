<?php

/* @WebProfiler/Icon/form.svg */
class __TwigTemplate_78bdfa04ac0377a7ac67c4381198340c78c114fa2a268962602cb359911d114a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c5f661eb41617a824380ead2ac20b9cc01ed0e266c17f77ad7ed0e4796f84bec = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c5f661eb41617a824380ead2ac20b9cc01ed0e266c17f77ad7ed0e4796f84bec->enter($__internal_c5f661eb41617a824380ead2ac20b9cc01ed0e266c17f77ad7ed0e4796f84bec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/form.svg"));

        $__internal_5e0f1680166bf6bf6a512aa918fd1eb90677cb8973ed3f35482ed54d1a0e0e4a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5e0f1680166bf6bf6a512aa918fd1eb90677cb8973ed3f35482ed54d1a0e0e4a->enter($__internal_5e0f1680166bf6bf6a512aa918fd1eb90677cb8973ed3f35482ed54d1a0e0e4a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/form.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path fill=\"#AAAAAA\" d=\"M20.5,4H18V2.5C18,1.7,17.3,1,16.5,1h-9C6.7,1,6,1.7,6,2.5V4H3.5C2.7,4,2,4.7,2,5.5v16C2,22.3,2.7,23,3.5,
    23h17c0.8,0,1.5-0.7,1.5-1.5v-16C22,4.7,21.3,4,20.5,4z M9,4h6v1H9V4z M19,20H5V7h1.1c0.2,0.6,0.8,1,1.4,1h9c0.7,0,1.2-0.4,1.4-1H19
    V20z M17,11c0,0.6-0.4,1-1,1H8c-0.6,0-1-0.4-1-1s0.4-1,1-1h8C16.6,10,17,10.4,17,11z M17,14c0,0.6-0.4,1-1,1H8c-0.6,0-1-0.4-1-1
    s0.4-1,1-1h8C16.6,13,17,13.4,17,14z M13,17c0,0.6-0.4,1-1,1H8c-0.6,0-1-0.4-1-1s0.4-1,1-1h4C12.6,16,13,16.4,13,17z\"/>
</svg>
";
        
        $__internal_c5f661eb41617a824380ead2ac20b9cc01ed0e266c17f77ad7ed0e4796f84bec->leave($__internal_c5f661eb41617a824380ead2ac20b9cc01ed0e266c17f77ad7ed0e4796f84bec_prof);

        
        $__internal_5e0f1680166bf6bf6a512aa918fd1eb90677cb8973ed3f35482ed54d1a0e0e4a->leave($__internal_5e0f1680166bf6bf6a512aa918fd1eb90677cb8973ed3f35482ed54d1a0e0e4a_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Icon/form.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path fill=\"#AAAAAA\" d=\"M20.5,4H18V2.5C18,1.7,17.3,1,16.5,1h-9C6.7,1,6,1.7,6,2.5V4H3.5C2.7,4,2,4.7,2,5.5v16C2,22.3,2.7,23,3.5,
    23h17c0.8,0,1.5-0.7,1.5-1.5v-16C22,4.7,21.3,4,20.5,4z M9,4h6v1H9V4z M19,20H5V7h1.1c0.2,0.6,0.8,1,1.4,1h9c0.7,0,1.2-0.4,1.4-1H19
    V20z M17,11c0,0.6-0.4,1-1,1H8c-0.6,0-1-0.4-1-1s0.4-1,1-1h8C16.6,10,17,10.4,17,11z M17,14c0,0.6-0.4,1-1,1H8c-0.6,0-1-0.4-1-1
    s0.4-1,1-1h8C16.6,13,17,13.4,17,14z M13,17c0,0.6-0.4,1-1,1H8c-0.6,0-1-0.4-1-1s0.4-1,1-1h4C12.6,16,13,16.4,13,17z\"/>
</svg>
", "@WebProfiler/Icon/form.svg", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Icon/form.svg");
    }
}

<?php

/* EasyAdminBundle:default:field_guid.html.twig */
class __TwigTemplate_ac9b9e3dceaeff0563ef8319d4282caebea233bc002b5db3b1136d398b38b43a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_13db2e2de805466669bddf7a6753cf94bf2cffb273d705b73ac0b29c69cd7cbc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_13db2e2de805466669bddf7a6753cf94bf2cffb273d705b73ac0b29c69cd7cbc->enter($__internal_13db2e2de805466669bddf7a6753cf94bf2cffb273d705b73ac0b29c69cd7cbc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_guid.html.twig"));

        $__internal_b74b44f887972d9891b1cfe90fd6fb7e4905a3b8d699a14fc7004136b2f5419a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b74b44f887972d9891b1cfe90fd6fb7e4905a3b8d699a14fc7004136b2f5419a->enter($__internal_b74b44f887972d9891b1cfe90fd6fb7e4905a3b8d699a14fc7004136b2f5419a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_guid.html.twig"));

        // line 1
        if ((($context["view"] ?? $this->getContext($context, "view")) == "show")) {
            // line 2
            echo "    ";
            echo twig_escape_filter($this->env, ($context["value"] ?? $this->getContext($context, "value")), "html", null, true);
            echo "
";
        } else {
            // line 4
            echo "    ";
            echo twig_escape_filter($this->env, $this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->truncateText($this->env, ($context["value"] ?? $this->getContext($context, "value")), 7), "html", null, true);
            echo "
";
        }
        
        $__internal_13db2e2de805466669bddf7a6753cf94bf2cffb273d705b73ac0b29c69cd7cbc->leave($__internal_13db2e2de805466669bddf7a6753cf94bf2cffb273d705b73ac0b29c69cd7cbc_prof);

        
        $__internal_b74b44f887972d9891b1cfe90fd6fb7e4905a3b8d699a14fc7004136b2f5419a->leave($__internal_b74b44f887972d9891b1cfe90fd6fb7e4905a3b8d699a14fc7004136b2f5419a_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:field_guid.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  33 => 4,  27 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if view == 'show' %}
    {{ value }}
{% else %}
    {{ value|easyadmin_truncate(7) }}
{% endif %}
", "EasyAdminBundle:default:field_guid.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/field_guid.html.twig");
    }
}

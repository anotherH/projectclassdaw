<?php

/* @Framework/Form/container_attributes.html.php */
class __TwigTemplate_69b147b95bf6f5714e4137329d33a7b4a7728990e5a19cfd69c687c9fd2e5539 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e87e856a18773ae1ff86f0f857c9bd3769092cfc1a2aab0612b32ae07b446097 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e87e856a18773ae1ff86f0f857c9bd3769092cfc1a2aab0612b32ae07b446097->enter($__internal_e87e856a18773ae1ff86f0f857c9bd3769092cfc1a2aab0612b32ae07b446097_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        $__internal_1aad2a6ca660ff08e905380d58264d72d985936b90730473f981902fc16b6cbb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1aad2a6ca660ff08e905380d58264d72d985936b90730473f981902fc16b6cbb->enter($__internal_1aad2a6ca660ff08e905380d58264d72d985936b90730473f981902fc16b6cbb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
";
        
        $__internal_e87e856a18773ae1ff86f0f857c9bd3769092cfc1a2aab0612b32ae07b446097->leave($__internal_e87e856a18773ae1ff86f0f857c9bd3769092cfc1a2aab0612b32ae07b446097_prof);

        
        $__internal_1aad2a6ca660ff08e905380d58264d72d985936b90730473f981902fc16b6cbb->leave($__internal_1aad2a6ca660ff08e905380d58264d72d985936b90730473f981902fc16b6cbb_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
", "@Framework/Form/container_attributes.html.php", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/container_attributes.html.php");
    }
}

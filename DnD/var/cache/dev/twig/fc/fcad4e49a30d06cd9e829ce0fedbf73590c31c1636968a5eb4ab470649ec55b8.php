<?php

/* @Framework/Form/repeated_row.html.php */
class __TwigTemplate_573991d623297161ac27e7bb5012887b1794a828fe0c191a616c4af697cc2190 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_34072118ca54bbd6434a290c5f60b0f9895e73b5ce8fb7806aac2ef4c0d7d7b8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_34072118ca54bbd6434a290c5f60b0f9895e73b5ce8fb7806aac2ef4c0d7d7b8->enter($__internal_34072118ca54bbd6434a290c5f60b0f9895e73b5ce8fb7806aac2ef4c0d7d7b8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/repeated_row.html.php"));

        $__internal_317ea994fa1246077999ff150e79c13b40ff896b6e9ecf55d2f84a6823255c6b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_317ea994fa1246077999ff150e79c13b40ff896b6e9ecf55d2f84a6823255c6b->enter($__internal_317ea994fa1246077999ff150e79c13b40ff896b6e9ecf55d2f84a6823255c6b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/repeated_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_rows') ?>
";
        
        $__internal_34072118ca54bbd6434a290c5f60b0f9895e73b5ce8fb7806aac2ef4c0d7d7b8->leave($__internal_34072118ca54bbd6434a290c5f60b0f9895e73b5ce8fb7806aac2ef4c0d7d7b8_prof);

        
        $__internal_317ea994fa1246077999ff150e79c13b40ff896b6e9ecf55d2f84a6823255c6b->leave($__internal_317ea994fa1246077999ff150e79c13b40ff896b6e9ecf55d2f84a6823255c6b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/repeated_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_rows') ?>
", "@Framework/Form/repeated_row.html.php", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/repeated_row.html.php");
    }
}

<?php

/* TwigBundle:Exception:exception.atom.twig */
class __TwigTemplate_5855fcda2fccbcf5e5eba467cef60e74af4d2cb4a6fdfe1b0863d2f09f28052d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bc3cd80c05150917ab26255b1901c2c4b78dbc7e5cb38b407e95bed514afd423 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bc3cd80c05150917ab26255b1901c2c4b78dbc7e5cb38b407e95bed514afd423->enter($__internal_bc3cd80c05150917ab26255b1901c2c4b78dbc7e5cb38b407e95bed514afd423_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.atom.twig"));

        $__internal_7cd70c38fd74482f97208e4f63e01f098db41f96c2bede13cd212bc4a75b9636 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7cd70c38fd74482f97208e4f63e01f098db41f96c2bede13cd212bc4a75b9636->enter($__internal_7cd70c38fd74482f97208e4f63e01f098db41f96c2bede13cd212bc4a75b9636_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "TwigBundle:Exception:exception.atom.twig", 1)->display(array_merge($context, array("exception" => ($context["exception"] ?? $this->getContext($context, "exception")))));
        
        $__internal_bc3cd80c05150917ab26255b1901c2c4b78dbc7e5cb38b407e95bed514afd423->leave($__internal_bc3cd80c05150917ab26255b1901c2c4b78dbc7e5cb38b407e95bed514afd423_prof);

        
        $__internal_7cd70c38fd74482f97208e4f63e01f098db41f96c2bede13cd212bc4a75b9636->leave($__internal_7cd70c38fd74482f97208e4f63e01f098db41f96c2bede13cd212bc4a75b9636_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}
", "TwigBundle:Exception:exception.atom.twig", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.atom.twig");
    }
}

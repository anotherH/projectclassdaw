<?php

/* TwigBundle:Exception:traces.txt.twig */
class __TwigTemplate_ee73d25ebfc3e5f802eb1ed2c159b71912a1a6f77c0819857282366da50c98bd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8643dcab3dd6bedc766d093803576b08dd170a391b8a97a23454dbb7a3d462aa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8643dcab3dd6bedc766d093803576b08dd170a391b8a97a23454dbb7a3d462aa->enter($__internal_8643dcab3dd6bedc766d093803576b08dd170a391b8a97a23454dbb7a3d462aa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:traces.txt.twig"));

        $__internal_078d347a9e15f355f6025210d13c187afb6502c80320007f2b54c0e25f7fe1e2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_078d347a9e15f355f6025210d13c187afb6502c80320007f2b54c0e25f7fe1e2->enter($__internal_078d347a9e15f355f6025210d13c187afb6502c80320007f2b54c0e25f7fe1e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:traces.txt.twig"));

        // line 1
        if (twig_length_filter($this->env, $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "trace", array()))) {
            // line 2
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "trace", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["trace"]) {
                // line 3
                $this->loadTemplate("@Twig/Exception/trace.txt.twig", "TwigBundle:Exception:traces.txt.twig", 3)->display(array("trace" => $context["trace"]));
                // line 4
                echo "
";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['trace'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        
        $__internal_8643dcab3dd6bedc766d093803576b08dd170a391b8a97a23454dbb7a3d462aa->leave($__internal_8643dcab3dd6bedc766d093803576b08dd170a391b8a97a23454dbb7a3d462aa_prof);

        
        $__internal_078d347a9e15f355f6025210d13c187afb6502c80320007f2b54c0e25f7fe1e2->leave($__internal_078d347a9e15f355f6025210d13c187afb6502c80320007f2b54c0e25f7fe1e2_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:traces.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  33 => 4,  31 => 3,  27 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if exception.trace|length %}
{% for trace in exception.trace %}
{% include '@Twig/Exception/trace.txt.twig' with { 'trace': trace } only %}

{% endfor %}
{% endif %}
", "TwigBundle:Exception:traces.txt.twig", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/traces.txt.twig");
    }
}

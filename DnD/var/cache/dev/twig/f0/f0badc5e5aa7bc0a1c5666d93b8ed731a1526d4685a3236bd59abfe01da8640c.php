<?php

/* FOSUserBundle:Group:show.html.twig */
class __TwigTemplate_c0f226282baf5a4dfe89b76a9bcec2ef1a3a430ad449bb3af89ba2d9a132faf3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c1572648b94d517e8f5e2829b91c96b80e550a7dbe62b7488cc4f742b24de9c6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c1572648b94d517e8f5e2829b91c96b80e550a7dbe62b7488cc4f742b24de9c6->enter($__internal_c1572648b94d517e8f5e2829b91c96b80e550a7dbe62b7488cc4f742b24de9c6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show.html.twig"));

        $__internal_6d89665309b770b476a2e75ae9ede895fa131985e4f12fb8c0bf20bfc45cf026 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6d89665309b770b476a2e75ae9ede895fa131985e4f12fb8c0bf20bfc45cf026->enter($__internal_6d89665309b770b476a2e75ae9ede895fa131985e4f12fb8c0bf20bfc45cf026_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c1572648b94d517e8f5e2829b91c96b80e550a7dbe62b7488cc4f742b24de9c6->leave($__internal_c1572648b94d517e8f5e2829b91c96b80e550a7dbe62b7488cc4f742b24de9c6_prof);

        
        $__internal_6d89665309b770b476a2e75ae9ede895fa131985e4f12fb8c0bf20bfc45cf026->leave($__internal_6d89665309b770b476a2e75ae9ede895fa131985e4f12fb8c0bf20bfc45cf026_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_8868536809b55055c57ce5eba7a4b6a86d37222243a0f4082dec2119df689614 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8868536809b55055c57ce5eba7a4b6a86d37222243a0f4082dec2119df689614->enter($__internal_8868536809b55055c57ce5eba7a4b6a86d37222243a0f4082dec2119df689614_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_36f700b51bdeb1233641a9fdada4f7ea4a9989b851143bb1334030440038c173 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_36f700b51bdeb1233641a9fdada4f7ea4a9989b851143bb1334030440038c173->enter($__internal_36f700b51bdeb1233641a9fdada4f7ea4a9989b851143bb1334030440038c173_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/show_content.html.twig", "FOSUserBundle:Group:show.html.twig", 4)->display($context);
        
        $__internal_36f700b51bdeb1233641a9fdada4f7ea4a9989b851143bb1334030440038c173->leave($__internal_36f700b51bdeb1233641a9fdada4f7ea4a9989b851143bb1334030440038c173_prof);

        
        $__internal_8868536809b55055c57ce5eba7a4b6a86d37222243a0f4082dec2119df689614->leave($__internal_8868536809b55055c57ce5eba7a4b6a86d37222243a0f4082dec2119df689614_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/show_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:show.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/friendsofsymfony/user-bundle/Resources/views/Group/show.html.twig");
    }
}

<?php

/* FOSUserBundle:Profile:show_content.html.twig */
class __TwigTemplate_8a6faa15e18511d77a9eba6d097a289c49245c60dffa9643cfa049e7d9b9843a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b539ced8b7469edd0ee4fbb05bae9da6f095403ce80b99a49ff4682c55e9cd8b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b539ced8b7469edd0ee4fbb05bae9da6f095403ce80b99a49ff4682c55e9cd8b->enter($__internal_b539ced8b7469edd0ee4fbb05bae9da6f095403ce80b99a49ff4682c55e9cd8b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show_content.html.twig"));

        $__internal_764d8e4804b0babc85a67c526ab37074636b050ff430852d1f9bfc5eaeeaa4e9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_764d8e4804b0babc85a67c526ab37074636b050ff430852d1f9bfc5eaeeaa4e9->enter($__internal_764d8e4804b0babc85a67c526ab37074636b050ff430852d1f9bfc5eaeeaa4e9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show_content.html.twig"));

        // line 2
        echo "
<div class=\"fos_user_user_show\">
    <p>";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("profile.show.username", array(), "FOSUserBundle"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array()), "html", null, true);
        echo "</p>
    <p>";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("profile.show.email", array(), "FOSUserBundle"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "email", array()), "html", null, true);
        echo "</p>
</div>
";
        
        $__internal_b539ced8b7469edd0ee4fbb05bae9da6f095403ce80b99a49ff4682c55e9cd8b->leave($__internal_b539ced8b7469edd0ee4fbb05bae9da6f095403ce80b99a49ff4682c55e9cd8b_prof);

        
        $__internal_764d8e4804b0babc85a67c526ab37074636b050ff430852d1f9bfc5eaeeaa4e9->leave($__internal_764d8e4804b0babc85a67c526ab37074636b050ff430852d1f9bfc5eaeeaa4e9_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:show_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 5,  29 => 4,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}

<div class=\"fos_user_user_show\">
    <p>{{ 'profile.show.username'|trans }}: {{ user.username }}</p>
    <p>{{ 'profile.show.email'|trans }}: {{ user.email }}</p>
</div>
", "FOSUserBundle:Profile:show_content.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/friendsofsymfony/user-bundle/Resources/views/Profile/show_content.html.twig");
    }
}

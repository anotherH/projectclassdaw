<?php

/* EasyAdminBundle:default:field_tel.html.twig */
class __TwigTemplate_f5d2a8fbcd1015c43fdd4295da701170a18d3f26bbbb9ea5d8a7af50c0b9182e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_679b5bcda08dc4c3191ebe95b02e2b22f81622cf142b2173ffdcd85e16ee8e8b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_679b5bcda08dc4c3191ebe95b02e2b22f81622cf142b2173ffdcd85e16ee8e8b->enter($__internal_679b5bcda08dc4c3191ebe95b02e2b22f81622cf142b2173ffdcd85e16ee8e8b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_tel.html.twig"));

        $__internal_f96fd842eedeea42f83363b02ffa365aee3561cc53bd237a6a17b99191b0341b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f96fd842eedeea42f83363b02ffa365aee3561cc53bd237a6a17b99191b0341b->enter($__internal_f96fd842eedeea42f83363b02ffa365aee3561cc53bd237a6a17b99191b0341b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_tel.html.twig"));

        // line 1
        echo "<a href=\"tel:";
        echo twig_escape_filter($this->env, ($context["value"] ?? $this->getContext($context, "value")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, ($context["value"] ?? $this->getContext($context, "value")), "html", null, true);
        echo "</a>
";
        
        $__internal_679b5bcda08dc4c3191ebe95b02e2b22f81622cf142b2173ffdcd85e16ee8e8b->leave($__internal_679b5bcda08dc4c3191ebe95b02e2b22f81622cf142b2173ffdcd85e16ee8e8b_prof);

        
        $__internal_f96fd842eedeea42f83363b02ffa365aee3561cc53bd237a6a17b99191b0341b->leave($__internal_f96fd842eedeea42f83363b02ffa365aee3561cc53bd237a6a17b99191b0341b_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:field_tel.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<a href=\"tel:{{ value }}\">{{ value }}</a>
", "EasyAdminBundle:default:field_tel.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/field_tel.html.twig");
    }
}

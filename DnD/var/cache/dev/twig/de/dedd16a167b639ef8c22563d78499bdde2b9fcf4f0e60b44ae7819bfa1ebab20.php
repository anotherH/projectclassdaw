<?php

/* WebProfilerBundle:Profiler:ajax_layout.html.twig */
class __TwigTemplate_d64522183fca0c707acdcd5f1385152c3eacbe3aa07afcfcdca2be29ca2f3378 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_febb8f729db6704f7134381ae033a9b1b81eee99636cb52afa85481168d41bc9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_febb8f729db6704f7134381ae033a9b1b81eee99636cb52afa85481168d41bc9->enter($__internal_febb8f729db6704f7134381ae033a9b1b81eee99636cb52afa85481168d41bc9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        $__internal_b35df604cae10cf683198dc1f79b79bc4cd098019e22956039e77ade4536bd7a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b35df604cae10cf683198dc1f79b79bc4cd098019e22956039e77ade4536bd7a->enter($__internal_b35df604cae10cf683198dc1f79b79bc4cd098019e22956039e77ade4536bd7a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_febb8f729db6704f7134381ae033a9b1b81eee99636cb52afa85481168d41bc9->leave($__internal_febb8f729db6704f7134381ae033a9b1b81eee99636cb52afa85481168d41bc9_prof);

        
        $__internal_b35df604cae10cf683198dc1f79b79bc4cd098019e22956039e77ade4536bd7a->leave($__internal_b35df604cae10cf683198dc1f79b79bc4cd098019e22956039e77ade4536bd7a_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_5e1e8777273084742a4ba295f68d4cdd6521c41eac94d2b66228e5a2433c16d7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5e1e8777273084742a4ba295f68d4cdd6521c41eac94d2b66228e5a2433c16d7->enter($__internal_5e1e8777273084742a4ba295f68d4cdd6521c41eac94d2b66228e5a2433c16d7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_8e00f5dffe38d7ab7a6f81744fd9cf8f250c4e2a9c67efbb56e729d280557ddb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8e00f5dffe38d7ab7a6f81744fd9cf8f250c4e2a9c67efbb56e729d280557ddb->enter($__internal_8e00f5dffe38d7ab7a6f81744fd9cf8f250c4e2a9c67efbb56e729d280557ddb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_8e00f5dffe38d7ab7a6f81744fd9cf8f250c4e2a9c67efbb56e729d280557ddb->leave($__internal_8e00f5dffe38d7ab7a6f81744fd9cf8f250c4e2a9c67efbb56e729d280557ddb_prof);

        
        $__internal_5e1e8777273084742a4ba295f68d4cdd6521c41eac94d2b66228e5a2433c16d7->leave($__internal_5e1e8777273084742a4ba295f68d4cdd6521c41eac94d2b66228e5a2433c16d7_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% block panel '' %}
", "WebProfilerBundle:Profiler:ajax_layout.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/ajax_layout.html.twig");
    }
}

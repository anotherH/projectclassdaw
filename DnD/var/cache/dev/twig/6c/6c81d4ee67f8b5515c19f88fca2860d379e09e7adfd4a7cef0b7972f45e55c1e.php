<?php

/* FOSUserBundle:Registration:email.txt.twig */
class __TwigTemplate_ebd14e19b5f38dc94426350b0a7950288f74b1c158ff52a81df83108edf3cd7c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_da730c179dfa7e5614dab59f35903d98bca8ab86319d47af109b422041781928 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_da730c179dfa7e5614dab59f35903d98bca8ab86319d47af109b422041781928->enter($__internal_da730c179dfa7e5614dab59f35903d98bca8ab86319d47af109b422041781928_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:email.txt.twig"));

        $__internal_0cccc8e924fde8a1c7e7aceb61948d03dae2a64c5ed065e77c50dd1f39fd0c6b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0cccc8e924fde8a1c7e7aceb61948d03dae2a64c5ed065e77c50dd1f39fd0c6b->enter($__internal_0cccc8e924fde8a1c7e7aceb61948d03dae2a64c5ed065e77c50dd1f39fd0c6b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('body_text', $context, $blocks);
        // line 13
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_da730c179dfa7e5614dab59f35903d98bca8ab86319d47af109b422041781928->leave($__internal_da730c179dfa7e5614dab59f35903d98bca8ab86319d47af109b422041781928_prof);

        
        $__internal_0cccc8e924fde8a1c7e7aceb61948d03dae2a64c5ed065e77c50dd1f39fd0c6b->leave($__internal_0cccc8e924fde8a1c7e7aceb61948d03dae2a64c5ed065e77c50dd1f39fd0c6b_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_76b703c25116ec704fda8aa3b11705f09627992706c2450bb0569940330b1c9f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_76b703c25116ec704fda8aa3b11705f09627992706c2450bb0569940330b1c9f->enter($__internal_76b703c25116ec704fda8aa3b11705f09627992706c2450bb0569940330b1c9f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        $__internal_2121a18b8285aa43a624f0e916161fc6ca0c6cfc0b30c61dcd11564bd8e00596 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2121a18b8285aa43a624f0e916161fc6ca0c6cfc0b30c61dcd11564bd8e00596->enter($__internal_2121a18b8285aa43a624f0e916161fc6ca0c6cfc0b30c61dcd11564bd8e00596_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.email.subject", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => ($context["confirmationUrl"] ?? $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        
        $__internal_2121a18b8285aa43a624f0e916161fc6ca0c6cfc0b30c61dcd11564bd8e00596->leave($__internal_2121a18b8285aa43a624f0e916161fc6ca0c6cfc0b30c61dcd11564bd8e00596_prof);

        
        $__internal_76b703c25116ec704fda8aa3b11705f09627992706c2450bb0569940330b1c9f->leave($__internal_76b703c25116ec704fda8aa3b11705f09627992706c2450bb0569940330b1c9f_prof);

    }

    // line 8
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_8444d083461ee83a9de6b8e637fd1872ac9b053d19d5b26a0229f775d64597e9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8444d083461ee83a9de6b8e637fd1872ac9b053d19d5b26a0229f775d64597e9->enter($__internal_8444d083461ee83a9de6b8e637fd1872ac9b053d19d5b26a0229f775d64597e9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        $__internal_d7a53642825bdfe106fb3222ed3aa04d09a2b8fd229af638fbef3ca7d8d6dd83 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d7a53642825bdfe106fb3222ed3aa04d09a2b8fd229af638fbef3ca7d8d6dd83->enter($__internal_d7a53642825bdfe106fb3222ed3aa04d09a2b8fd229af638fbef3ca7d8d6dd83_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.email.message", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => ($context["confirmationUrl"] ?? $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_d7a53642825bdfe106fb3222ed3aa04d09a2b8fd229af638fbef3ca7d8d6dd83->leave($__internal_d7a53642825bdfe106fb3222ed3aa04d09a2b8fd229af638fbef3ca7d8d6dd83_prof);

        
        $__internal_8444d083461ee83a9de6b8e637fd1872ac9b053d19d5b26a0229f775d64597e9->leave($__internal_8444d083461ee83a9de6b8e637fd1872ac9b053d19d5b26a0229f775d64597e9_prof);

    }

    // line 13
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_ac4b4057887dcc475f13f20c8860d702a480219cb55d8d0fdb0633b7d0dea4c6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ac4b4057887dcc475f13f20c8860d702a480219cb55d8d0fdb0633b7d0dea4c6->enter($__internal_ac4b4057887dcc475f13f20c8860d702a480219cb55d8d0fdb0633b7d0dea4c6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        $__internal_52b79e35a51bc15275656463725722bd92d2c3e585aaa616c8c9d46beb4342b7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_52b79e35a51bc15275656463725722bd92d2c3e585aaa616c8c9d46beb4342b7->enter($__internal_52b79e35a51bc15275656463725722bd92d2c3e585aaa616c8c9d46beb4342b7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_52b79e35a51bc15275656463725722bd92d2c3e585aaa616c8c9d46beb4342b7->leave($__internal_52b79e35a51bc15275656463725722bd92d2c3e585aaa616c8c9d46beb4342b7_prof);

        
        $__internal_ac4b4057887dcc475f13f20c8860d702a480219cb55d8d0fdb0633b7d0dea4c6->leave($__internal_ac4b4057887dcc475f13f20c8860d702a480219cb55d8d0fdb0633b7d0dea4c6_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  85 => 13,  73 => 10,  64 => 8,  54 => 4,  45 => 2,  35 => 13,  33 => 8,  30 => 7,  28 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}
{% block subject %}
{%- autoescape false -%}
{{ 'registration.email.subject'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{%- endautoescape -%}
{% endblock %}

{% block body_text %}
{% autoescape false %}
{{ 'registration.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{% endautoescape %}
{% endblock %}
{% block body_html %}{% endblock %}
", "FOSUserBundle:Registration:email.txt.twig", "/home/a14eriamocob/public_html/DnD/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/email.txt.twig");
    }
}

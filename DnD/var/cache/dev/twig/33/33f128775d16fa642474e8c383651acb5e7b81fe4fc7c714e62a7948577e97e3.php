<?php

/* @Framework/FormTable/button_row.html.php */
class __TwigTemplate_a39cc41fd17c54efa691a7167d29e3b1f7dad551e902db53536cdeb5ef0533f7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7838f92d090dc6b4d98cb3d2df8b5ba55371bcb0788878960bdecbb8b469763d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7838f92d090dc6b4d98cb3d2df8b5ba55371bcb0788878960bdecbb8b469763d->enter($__internal_7838f92d090dc6b4d98cb3d2df8b5ba55371bcb0788878960bdecbb8b469763d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        $__internal_c931266527c654cd9c9b072f3a9bf30444bf5d9a0b88f0b6e75e527d27dd8cef = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c931266527c654cd9c9b072f3a9bf30444bf5d9a0b88f0b6e75e527d27dd8cef->enter($__internal_c931266527c654cd9c9b072f3a9bf30444bf5d9a0b88f0b6e75e527d27dd8cef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        // line 1
        echo "<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_7838f92d090dc6b4d98cb3d2df8b5ba55371bcb0788878960bdecbb8b469763d->leave($__internal_7838f92d090dc6b4d98cb3d2df8b5ba55371bcb0788878960bdecbb8b469763d_prof);

        
        $__internal_c931266527c654cd9c9b072f3a9bf30444bf5d9a0b88f0b6e75e527d27dd8cef->leave($__internal_c931266527c654cd9c9b072f3a9bf30444bf5d9a0b88f0b6e75e527d27dd8cef_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
", "@Framework/FormTable/button_row.html.php", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/button_row.html.php");
    }
}

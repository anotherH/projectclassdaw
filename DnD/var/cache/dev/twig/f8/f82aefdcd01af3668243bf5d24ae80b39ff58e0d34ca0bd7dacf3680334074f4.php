<?php

/* @Framework/Form/form_end.html.php */
class __TwigTemplate_f010117a464385624024b6110e65231f6e50e1a53beb1ba984b75d4159061bd4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b385d88d1ef93ae0bf3c19054e365b1df0124bf9f0fe7c3c29ffbf4785cdc6b1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b385d88d1ef93ae0bf3c19054e365b1df0124bf9f0fe7c3c29ffbf4785cdc6b1->enter($__internal_b385d88d1ef93ae0bf3c19054e365b1df0124bf9f0fe7c3c29ffbf4785cdc6b1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        $__internal_4feffc5a9ec7243ef13d68ff10cc7be07d9a3a4d91a2992c355be6cf8f602ee5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4feffc5a9ec7243ef13d68ff10cc7be07d9a3a4d91a2992c355be6cf8f602ee5->enter($__internal_4feffc5a9ec7243ef13d68ff10cc7be07d9a3a4d91a2992c355be6cf8f602ee5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        // line 1
        echo "<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
";
        
        $__internal_b385d88d1ef93ae0bf3c19054e365b1df0124bf9f0fe7c3c29ffbf4785cdc6b1->leave($__internal_b385d88d1ef93ae0bf3c19054e365b1df0124bf9f0fe7c3c29ffbf4785cdc6b1_prof);

        
        $__internal_4feffc5a9ec7243ef13d68ff10cc7be07d9a3a4d91a2992c355be6cf8f602ee5->leave($__internal_4feffc5a9ec7243ef13d68ff10cc7be07d9a3a4d91a2992c355be6cf8f602ee5_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_end.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
", "@Framework/Form/form_end.html.php", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_end.html.php");
    }
}

<?php

/* @Framework/Form/hidden_row.html.php */
class __TwigTemplate_0365aab3812d843ceff98f3d8d72e31f66f6a968fa6d9ac77004f850239ca741 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_685ae4b8766e4f55b98df91763a2b07c8cd6ca839425e1898fcb5de6523e809d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_685ae4b8766e4f55b98df91763a2b07c8cd6ca839425e1898fcb5de6523e809d->enter($__internal_685ae4b8766e4f55b98df91763a2b07c8cd6ca839425e1898fcb5de6523e809d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        $__internal_1e904a09ffbd83c6749a47dc33e6046ed88e283b106bccc2032b1d790302df22 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1e904a09ffbd83c6749a47dc33e6046ed88e283b106bccc2032b1d790302df22->enter($__internal_1e904a09ffbd83c6749a47dc33e6046ed88e283b106bccc2032b1d790302df22_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->widget(\$form) ?>
";
        
        $__internal_685ae4b8766e4f55b98df91763a2b07c8cd6ca839425e1898fcb5de6523e809d->leave($__internal_685ae4b8766e4f55b98df91763a2b07c8cd6ca839425e1898fcb5de6523e809d_prof);

        
        $__internal_1e904a09ffbd83c6749a47dc33e6046ed88e283b106bccc2032b1d790302df22->leave($__internal_1e904a09ffbd83c6749a47dc33e6046ed88e283b106bccc2032b1d790302df22_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->widget(\$form) ?>
", "@Framework/Form/hidden_row.html.php", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/hidden_row.html.php");
    }
}

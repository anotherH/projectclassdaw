<?php

/* TwigBundle:Exception:error.css.twig */
class __TwigTemplate_e40359884509614f530e48e6058c5b3ae38cf5071481d7bd2bfc13c4bae0751c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5b49c0b5758dd0c7be582defb66f5ead2cb43ffb76789fe7f487a179a4aa2c47 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5b49c0b5758dd0c7be582defb66f5ead2cb43ffb76789fe7f487a179a4aa2c47->enter($__internal_5b49c0b5758dd0c7be582defb66f5ead2cb43ffb76789fe7f487a179a4aa2c47_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.css.twig"));

        $__internal_51f8a4092be371804412c1eb853f7321519f9319014ea8315e6378a5243d7c32 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_51f8a4092be371804412c1eb853f7321519f9319014ea8315e6378a5243d7c32->enter($__internal_51f8a4092be371804412c1eb853f7321519f9319014ea8315e6378a5243d7c32_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "css", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "css", null, true);
        echo "

*/
";
        
        $__internal_5b49c0b5758dd0c7be582defb66f5ead2cb43ffb76789fe7f487a179a4aa2c47->leave($__internal_5b49c0b5758dd0c7be582defb66f5ead2cb43ffb76789fe7f487a179a4aa2c47_prof);

        
        $__internal_51f8a4092be371804412c1eb853f7321519f9319014ea8315e6378a5243d7c32->leave($__internal_51f8a4092be371804412c1eb853f7321519f9319014ea8315e6378a5243d7c32_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ status_code }} {{ status_text }}

*/
", "TwigBundle:Exception:error.css.twig", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.css.twig");
    }
}

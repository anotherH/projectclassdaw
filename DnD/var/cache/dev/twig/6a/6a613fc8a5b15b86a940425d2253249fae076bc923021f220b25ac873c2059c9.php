<?php

/* @Framework/Form/hidden_widget.html.php */
class __TwigTemplate_7238f964c4bd51c01c20a217c86c00a035e75db40b6d5f79193e499c0c6f4b89 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_261fd7235f9e3757a8aef5bf1362e5b14b25303d9299d36b9b8893b832fd36f6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_261fd7235f9e3757a8aef5bf1362e5b14b25303d9299d36b9b8893b832fd36f6->enter($__internal_261fd7235f9e3757a8aef5bf1362e5b14b25303d9299d36b9b8893b832fd36f6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        $__internal_1c448e39189626278d1ff39d48da4e2a1f991849f233ff954992db98db29b741 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1c448e39189626278d1ff39d48da4e2a1f991849f233ff954992db98db29b741->enter($__internal_1c448e39189626278d1ff39d48da4e2a1f991849f233ff954992db98db29b741_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
";
        
        $__internal_261fd7235f9e3757a8aef5bf1362e5b14b25303d9299d36b9b8893b832fd36f6->leave($__internal_261fd7235f9e3757a8aef5bf1362e5b14b25303d9299d36b9b8893b832fd36f6_prof);

        
        $__internal_1c448e39189626278d1ff39d48da4e2a1f991849f233ff954992db98db29b741->leave($__internal_1c448e39189626278d1ff39d48da4e2a1f991849f233ff954992db98db29b741_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
", "@Framework/Form/hidden_widget.html.php", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/hidden_widget.html.php");
    }
}

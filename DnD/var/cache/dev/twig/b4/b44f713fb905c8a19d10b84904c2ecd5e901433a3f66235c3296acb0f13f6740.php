<?php

/* EasyAdminBundle:default:show.html.twig */
class __TwigTemplate_bcd5659ae4c933d45dd6fc49e5f600ca62120b12784eae83dd7edc8bdbe2c6aa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'body_class' => array($this, 'block_body_class'),
            'content_title' => array($this, 'block_content_title'),
            'main' => array($this, 'block_main'),
            'item_actions' => array($this, 'block_item_actions'),
            'delete_form' => array($this, 'block_delete_form'),
            'body_javascript' => array($this, 'block_body_javascript'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 7
        return $this->loadTemplate($this->getAttribute($this->getAttribute(($context["_entity_config"] ?? $this->getContext($context, "_entity_config")), "templates", array()), "layout", array()), "EasyAdminBundle:default:show.html.twig", 7);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5588147b76b5f33e196ee81a808db3f67229aba311006d56f30455afde6cd646 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5588147b76b5f33e196ee81a808db3f67229aba311006d56f30455afde6cd646->enter($__internal_5588147b76b5f33e196ee81a808db3f67229aba311006d56f30455afde6cd646_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:show.html.twig"));

        $__internal_dcb57ad2c7d9251170b948b6e3f650fb61f6d398d4bdcfb7dce38b89f6e415f1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dcb57ad2c7d9251170b948b6e3f650fb61f6d398d4bdcfb7dce38b89f6e415f1->enter($__internal_dcb57ad2c7d9251170b948b6e3f650fb61f6d398d4bdcfb7dce38b89f6e415f1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:show.html.twig"));

        // line 1
        $context["_entity_config"] = $this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getEntityConfiguration($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "query", array()), "get", array(0 => "entity"), "method"));
        // line 3
        $context["_entity_id"] = ("" . $this->getAttribute(($context["entity"] ?? $this->getContext($context, "entity")), $this->getAttribute(($context["_entity_config"] ?? $this->getContext($context, "_entity_config")), "primary_key_field_name", array())));
        // line 4
        $context["__internal_ebeb925f5613f091a80b3dd4e1a09b3341c0f50a7e8c34dd6b85b0fb57d331b3"] = $this->getAttribute(($context["_entity_config"] ?? $this->getContext($context, "_entity_config")), "translation_domain", array());
        // line 5
        $context["_trans_parameters"] = array("%entity_name%" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute(($context["_entity_config"] ?? $this->getContext($context, "_entity_config")), "name", array()), array(),         // line 4
($context["__internal_ebeb925f5613f091a80b3dd4e1a09b3341c0f50a7e8c34dd6b85b0fb57d331b3"] ?? $this->getContext($context, "__internal_ebeb925f5613f091a80b3dd4e1a09b3341c0f50a7e8c34dd6b85b0fb57d331b3"))), "%entity_label%" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute(        // line 5
($context["_entity_config"] ?? $this->getContext($context, "_entity_config")), "label", array()), array(),         // line 4
($context["__internal_ebeb925f5613f091a80b3dd4e1a09b3341c0f50a7e8c34dd6b85b0fb57d331b3"] ?? $this->getContext($context, "__internal_ebeb925f5613f091a80b3dd4e1a09b3341c0f50a7e8c34dd6b85b0fb57d331b3"))), "%entity_id%" =>         // line 5
($context["_entity_id"] ?? $this->getContext($context, "_entity_id")));
        // line 7
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5588147b76b5f33e196ee81a808db3f67229aba311006d56f30455afde6cd646->leave($__internal_5588147b76b5f33e196ee81a808db3f67229aba311006d56f30455afde6cd646_prof);

        
        $__internal_dcb57ad2c7d9251170b948b6e3f650fb61f6d398d4bdcfb7dce38b89f6e415f1->leave($__internal_dcb57ad2c7d9251170b948b6e3f650fb61f6d398d4bdcfb7dce38b89f6e415f1_prof);

    }

    // line 9
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_8f81f6d1a96c773ec29eeb7003a3424cfd9db5487b4eee6bf01ba2b20fe75e9f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8f81f6d1a96c773ec29eeb7003a3424cfd9db5487b4eee6bf01ba2b20fe75e9f->enter($__internal_8f81f6d1a96c773ec29eeb7003a3424cfd9db5487b4eee6bf01ba2b20fe75e9f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        $__internal_d81b52897b3adb5f7fdfa4fc0a15086d58be11bca41916b3050f69112060e97f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d81b52897b3adb5f7fdfa4fc0a15086d58be11bca41916b3050f69112060e97f->enter($__internal_d81b52897b3adb5f7fdfa4fc0a15086d58be11bca41916b3050f69112060e97f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        echo twig_escape_filter($this->env, ((("easyadmin-show-" . $this->getAttribute(($context["_entity_config"] ?? $this->getContext($context, "_entity_config")), "name", array())) . "-") . ($context["_entity_id"] ?? $this->getContext($context, "_entity_id"))), "html", null, true);
        
        $__internal_d81b52897b3adb5f7fdfa4fc0a15086d58be11bca41916b3050f69112060e97f->leave($__internal_d81b52897b3adb5f7fdfa4fc0a15086d58be11bca41916b3050f69112060e97f_prof);

        
        $__internal_8f81f6d1a96c773ec29eeb7003a3424cfd9db5487b4eee6bf01ba2b20fe75e9f->leave($__internal_8f81f6d1a96c773ec29eeb7003a3424cfd9db5487b4eee6bf01ba2b20fe75e9f_prof);

    }

    // line 10
    public function block_body_class($context, array $blocks = array())
    {
        $__internal_b1b09ec6e15cef87459f0ee0e2419c46761e54283a062d18798803ee1165d3a1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b1b09ec6e15cef87459f0ee0e2419c46761e54283a062d18798803ee1165d3a1->enter($__internal_b1b09ec6e15cef87459f0ee0e2419c46761e54283a062d18798803ee1165d3a1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_class"));

        $__internal_818d5ad262516fdecb1a75e0d490e5269a6eda16d9f206c0085b0ef68fe175a6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_818d5ad262516fdecb1a75e0d490e5269a6eda16d9f206c0085b0ef68fe175a6->enter($__internal_818d5ad262516fdecb1a75e0d490e5269a6eda16d9f206c0085b0ef68fe175a6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_class"));

        echo twig_escape_filter($this->env, ("show show-" . twig_lower_filter($this->env, $this->getAttribute(($context["_entity_config"] ?? $this->getContext($context, "_entity_config")), "name", array()))), "html", null, true);
        
        $__internal_818d5ad262516fdecb1a75e0d490e5269a6eda16d9f206c0085b0ef68fe175a6->leave($__internal_818d5ad262516fdecb1a75e0d490e5269a6eda16d9f206c0085b0ef68fe175a6_prof);

        
        $__internal_b1b09ec6e15cef87459f0ee0e2419c46761e54283a062d18798803ee1165d3a1->leave($__internal_b1b09ec6e15cef87459f0ee0e2419c46761e54283a062d18798803ee1165d3a1_prof);

    }

    // line 12
    public function block_content_title($context, array $blocks = array())
    {
        $__internal_a1b40b84d8c7b7cd5b168208e8ee484c00a7a076b3f61f98da175d90f7366adb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a1b40b84d8c7b7cd5b168208e8ee484c00a7a076b3f61f98da175d90f7366adb->enter($__internal_a1b40b84d8c7b7cd5b168208e8ee484c00a7a076b3f61f98da175d90f7366adb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_title"));

        $__internal_58262f5eb9a2b573c5e2b740bf805aa3dc96add1979d5bff4e106c6372267eea = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_58262f5eb9a2b573c5e2b740bf805aa3dc96add1979d5bff4e106c6372267eea->enter($__internal_58262f5eb9a2b573c5e2b740bf805aa3dc96add1979d5bff4e106c6372267eea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_title"));

        // line 13
        ob_start();
        // line 14
        echo "    ";
        $context["_default_title"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("show.page_title", ($context["_trans_parameters"] ?? $this->getContext($context, "_trans_parameters")), "EasyAdminBundle");
        // line 15
        echo "    ";
        echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute(($context["_entity_config"] ?? null), "show", array(), "any", false, true), "title", array(), "any", true, true)) ? ($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute($this->getAttribute(($context["_entity_config"] ?? $this->getContext($context, "_entity_config")), "show", array()), "title", array()), ($context["_trans_parameters"] ?? $this->getContext($context, "_trans_parameters")),         // line 4
($context["__internal_ebeb925f5613f091a80b3dd4e1a09b3341c0f50a7e8c34dd6b85b0fb57d331b3"] ?? $this->getContext($context, "__internal_ebeb925f5613f091a80b3dd4e1a09b3341c0f50a7e8c34dd6b85b0fb57d331b3")))) : (        // line 15
($context["_default_title"] ?? $this->getContext($context, "_default_title")))), "html", null, true);
        echo "
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_58262f5eb9a2b573c5e2b740bf805aa3dc96add1979d5bff4e106c6372267eea->leave($__internal_58262f5eb9a2b573c5e2b740bf805aa3dc96add1979d5bff4e106c6372267eea_prof);

        
        $__internal_a1b40b84d8c7b7cd5b168208e8ee484c00a7a076b3f61f98da175d90f7366adb->leave($__internal_a1b40b84d8c7b7cd5b168208e8ee484c00a7a076b3f61f98da175d90f7366adb_prof);

    }

    // line 19
    public function block_main($context, array $blocks = array())
    {
        $__internal_7b09a3acd7ab0230e1674c29504aec27485ddffee9370ed222d8b9ecee6b669a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7b09a3acd7ab0230e1674c29504aec27485ddffee9370ed222d8b9ecee6b669a->enter($__internal_7b09a3acd7ab0230e1674c29504aec27485ddffee9370ed222d8b9ecee6b669a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        $__internal_56f028099b77b367a8b369ce857829a9946a5951a1064017d953ef9f697feec8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_56f028099b77b367a8b369ce857829a9946a5951a1064017d953ef9f697feec8->enter($__internal_56f028099b77b367a8b369ce857829a9946a5951a1064017d953ef9f697feec8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 20
        echo "    <div class=\"form-horizontal\">
        ";
        // line 21
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["fields"] ?? $this->getContext($context, "fields")));
        foreach ($context['_seq'] as $context["field"] => $context["metadata"]) {
            // line 22
            echo "            <div class=\"form-group field-";
            echo twig_escape_filter($this->env, twig_lower_filter($this->env, (($this->getAttribute($context["metadata"], "type", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($context["metadata"], "type", array()), "default")) : ("default"))), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, (($this->getAttribute($context["metadata"], "css_class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($context["metadata"], "css_class", array()), "")) : ("")), "html", null, true);
            echo "\">
                <label class=\"col-sm-2 control-label\">
                    ";
            // line 24
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((($this->getAttribute($context["metadata"], "label", array())) ? ($this->getAttribute($context["metadata"], "label", array())) : ($this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->humanize($context["field"]))), ($context["_trans_parameters"] ?? $this->getContext($context, "_trans_parameters")),             // line 4
($context["__internal_ebeb925f5613f091a80b3dd4e1a09b3341c0f50a7e8c34dd6b85b0fb57d331b3"] ?? $this->getContext($context, "__internal_ebeb925f5613f091a80b3dd4e1a09b3341c0f50a7e8c34dd6b85b0fb57d331b3")));
            // line 24
            echo "
                </label>
                <div class=\"col-sm-10\">
                    <div class=\"form-control\">
                        ";
            // line 28
            echo $this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->renderEntityField($this->env, "show", $this->getAttribute(($context["_entity_config"] ?? $this->getContext($context, "_entity_config")), "name", array()), ($context["entity"] ?? $this->getContext($context, "entity")), $context["metadata"]);
            echo "
                    </div>

                    ";
            // line 31
            if (((($this->getAttribute($context["metadata"], "help", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($context["metadata"], "help", array()), "")) : ("")) != "")) {
                // line 32
                echo "                        <span class=\"help-block\"><i class=\"fa fa-info-circle\"></i> ";
                echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute($context["metadata"], "help", array()), array(),                 // line 4
($context["__internal_ebeb925f5613f091a80b3dd4e1a09b3341c0f50a7e8c34dd6b85b0fb57d331b3"] ?? $this->getContext($context, "__internal_ebeb925f5613f091a80b3dd4e1a09b3341c0f50a7e8c34dd6b85b0fb57d331b3")));
                // line 32
                echo "</span>
                    ";
            }
            // line 34
            echo "                </div>
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['field'], $context['metadata'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 37
        echo "
        <div class=\"form-group form-actions\">
            <div class=\"col-sm-10 col-sm-offset-2\">
            ";
        // line 40
        $this->displayBlock('item_actions', $context, $blocks);
        // line 52
        echo "            </div>
        </div>
    </div>

    ";
        // line 56
        $this->displayBlock('delete_form', $context, $blocks);
        
        $__internal_56f028099b77b367a8b369ce857829a9946a5951a1064017d953ef9f697feec8->leave($__internal_56f028099b77b367a8b369ce857829a9946a5951a1064017d953ef9f697feec8_prof);

        
        $__internal_7b09a3acd7ab0230e1674c29504aec27485ddffee9370ed222d8b9ecee6b669a->leave($__internal_7b09a3acd7ab0230e1674c29504aec27485ddffee9370ed222d8b9ecee6b669a_prof);

    }

    // line 40
    public function block_item_actions($context, array $blocks = array())
    {
        $__internal_516e3b06e946fc0bf2e89ae5c8acf90c7aab7b48b6dc6f7389754a4dc8561162 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_516e3b06e946fc0bf2e89ae5c8acf90c7aab7b48b6dc6f7389754a4dc8561162->enter($__internal_516e3b06e946fc0bf2e89ae5c8acf90c7aab7b48b6dc6f7389754a4dc8561162_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "item_actions"));

        $__internal_10efcf0a7a9e06cf34067f5be858db3e4b3c8e09c8dfac23f35bd99ec62fec64 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_10efcf0a7a9e06cf34067f5be858db3e4b3c8e09c8dfac23f35bd99ec62fec64->enter($__internal_10efcf0a7a9e06cf34067f5be858db3e4b3c8e09c8dfac23f35bd99ec62fec64_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "item_actions"));

        // line 41
        echo "                ";
        $context["_show_actions"] = $this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getActionsForItem("show", $this->getAttribute(($context["_entity_config"] ?? $this->getContext($context, "_entity_config")), "name", array()));
        // line 42
        echo "                ";
        $context["_request_parameters"] = array("entity" => $this->getAttribute(($context["_entity_config"] ?? $this->getContext($context, "_entity_config")), "name", array()), "referer" => $this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "query", array()), "get", array(0 => "referer"), "method"));
        // line 43
        echo "
                ";
        // line 44
        echo twig_include($this->env, $context, "@EasyAdmin/default/includes/_actions.html.twig", array("actions" =>         // line 45
($context["_show_actions"] ?? $this->getContext($context, "_show_actions")), "request_parameters" =>         // line 46
($context["_request_parameters"] ?? $this->getContext($context, "_request_parameters")), "translation_domain" => $this->getAttribute(        // line 47
($context["_entity_config"] ?? $this->getContext($context, "_entity_config")), "translation_domain", array()), "trans_parameters" =>         // line 48
($context["_trans_parameters"] ?? $this->getContext($context, "_trans_parameters")), "item_id" =>         // line 49
($context["_entity_id"] ?? $this->getContext($context, "_entity_id"))), false);
        // line 50
        echo "
            ";
        
        $__internal_10efcf0a7a9e06cf34067f5be858db3e4b3c8e09c8dfac23f35bd99ec62fec64->leave($__internal_10efcf0a7a9e06cf34067f5be858db3e4b3c8e09c8dfac23f35bd99ec62fec64_prof);

        
        $__internal_516e3b06e946fc0bf2e89ae5c8acf90c7aab7b48b6dc6f7389754a4dc8561162->leave($__internal_516e3b06e946fc0bf2e89ae5c8acf90c7aab7b48b6dc6f7389754a4dc8561162_prof);

    }

    // line 56
    public function block_delete_form($context, array $blocks = array())
    {
        $__internal_4be49cd7ff5dfd436660ac7ee8578771ab2b639b92a23251b5f6c78da39a96af = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4be49cd7ff5dfd436660ac7ee8578771ab2b639b92a23251b5f6c78da39a96af->enter($__internal_4be49cd7ff5dfd436660ac7ee8578771ab2b639b92a23251b5f6c78da39a96af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "delete_form"));

        $__internal_b748f37bf3315f8dab32a91dfad0bcc0226b75229a648b02a661d80e05235992 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b748f37bf3315f8dab32a91dfad0bcc0226b75229a648b02a661d80e05235992->enter($__internal_b748f37bf3315f8dab32a91dfad0bcc0226b75229a648b02a661d80e05235992_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "delete_form"));

        // line 57
        echo "        ";
        echo twig_include($this->env, $context, "@EasyAdmin/default/includes/_delete_form.html.twig", array("view" => "show", "referer" => $this->getAttribute($this->getAttribute($this->getAttribute(        // line 59
($context["app"] ?? $this->getContext($context, "app")), "request", array()), "query", array()), "get", array(0 => "referer", 1 => ""), "method"), "delete_form" =>         // line 60
($context["delete_form"] ?? $this->getContext($context, "delete_form")), "_translation_domain" => $this->getAttribute(        // line 61
($context["_entity_config"] ?? $this->getContext($context, "_entity_config")), "translation_domain", array()), "_trans_parameters" =>         // line 62
($context["_trans_parameters"] ?? $this->getContext($context, "_trans_parameters")), "_entity_config" =>         // line 63
($context["_entity_config"] ?? $this->getContext($context, "_entity_config"))), false);
        // line 64
        echo "
    ";
        
        $__internal_b748f37bf3315f8dab32a91dfad0bcc0226b75229a648b02a661d80e05235992->leave($__internal_b748f37bf3315f8dab32a91dfad0bcc0226b75229a648b02a661d80e05235992_prof);

        
        $__internal_4be49cd7ff5dfd436660ac7ee8578771ab2b639b92a23251b5f6c78da39a96af->leave($__internal_4be49cd7ff5dfd436660ac7ee8578771ab2b639b92a23251b5f6c78da39a96af_prof);

    }

    // line 68
    public function block_body_javascript($context, array $blocks = array())
    {
        $__internal_bac87d0a51430727353d8b6e69aea8b3a49c0dfec181b2a5b896ed0056ef4989 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bac87d0a51430727353d8b6e69aea8b3a49c0dfec181b2a5b896ed0056ef4989->enter($__internal_bac87d0a51430727353d8b6e69aea8b3a49c0dfec181b2a5b896ed0056ef4989_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_javascript"));

        $__internal_818916fcb3c7a18f505f6ea92ffd9e1dfb52e680bb2a79eb0294347d69d2d443 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_818916fcb3c7a18f505f6ea92ffd9e1dfb52e680bb2a79eb0294347d69d2d443->enter($__internal_818916fcb3c7a18f505f6ea92ffd9e1dfb52e680bb2a79eb0294347d69d2d443_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_javascript"));

        // line 69
        echo "    ";
        $this->displayParentBlock("body_javascript", $context, $blocks);
        echo "

    <script type=\"text/javascript\">
        \$(function() {
            \$('a.action-delete').on('click', function(e) {
                e.preventDefault();

                \$('#modal-delete').modal({ backdrop: true, keyboard: true })
                    .off('click', '#modal-delete-button')
                    .on('click', '#modal-delete-button', function () {
                        \$('#delete-form').trigger('submit');
                    });
            });
        });
    </script>
";
        
        $__internal_818916fcb3c7a18f505f6ea92ffd9e1dfb52e680bb2a79eb0294347d69d2d443->leave($__internal_818916fcb3c7a18f505f6ea92ffd9e1dfb52e680bb2a79eb0294347d69d2d443_prof);

        
        $__internal_bac87d0a51430727353d8b6e69aea8b3a49c0dfec181b2a5b896ed0056ef4989->leave($__internal_bac87d0a51430727353d8b6e69aea8b3a49c0dfec181b2a5b896ed0056ef4989_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  277 => 69,  268 => 68,  257 => 64,  255 => 63,  254 => 62,  253 => 61,  252 => 60,  251 => 59,  249 => 57,  240 => 56,  229 => 50,  227 => 49,  226 => 48,  225 => 47,  224 => 46,  223 => 45,  222 => 44,  219 => 43,  216 => 42,  213 => 41,  204 => 40,  194 => 56,  188 => 52,  186 => 40,  181 => 37,  173 => 34,  169 => 32,  167 => 4,  165 => 32,  163 => 31,  157 => 28,  151 => 24,  149 => 4,  148 => 24,  140 => 22,  136 => 21,  133 => 20,  124 => 19,  111 => 15,  110 => 4,  108 => 15,  105 => 14,  103 => 13,  94 => 12,  76 => 10,  58 => 9,  48 => 7,  46 => 5,  45 => 4,  44 => 5,  43 => 4,  42 => 5,  40 => 4,  38 => 3,  36 => 1,  24 => 7,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% set _entity_config = easyadmin_entity(app.request.query.get('entity')) %}
{# the empty string concatenation is needed when the primary key is an object (e.g. an Uuid object) #}
{% set _entity_id = '' ~ attribute(entity, _entity_config.primary_key_field_name) %}
{% trans_default_domain _entity_config.translation_domain %}
{% set _trans_parameters = { '%entity_name%': _entity_config.name|trans, '%entity_label%': _entity_config.label|trans, '%entity_id%': _entity_id } %}

{% extends _entity_config.templates.layout %}

{% block body_id 'easyadmin-show-' ~ _entity_config.name ~ '-' ~ _entity_id %}
{% block body_class 'show show-' ~ _entity_config.name|lower %}

{% block content_title %}
{% spaceless %}
    {% set _default_title = 'show.page_title'|trans(_trans_parameters, 'EasyAdminBundle') %}
    {{ _entity_config.show.title is defined ? _entity_config.show.title|trans(_trans_parameters) : _default_title }}
{% endspaceless %}
{% endblock %}

{% block main %}
    <div class=\"form-horizontal\">
        {% for field, metadata in fields %}
            <div class=\"form-group field-{{ metadata.type|default('default')|lower }} {{ metadata.css_class|default('') }}\">
                <label class=\"col-sm-2 control-label\">
                    {{ (metadata.label ?: field|humanize)|trans(_trans_parameters)|raw }}
                </label>
                <div class=\"col-sm-10\">
                    <div class=\"form-control\">
                        {{ easyadmin_render_field_for_show_view(_entity_config.name, entity, metadata) }}
                    </div>

                    {% if metadata.help|default('') != '' %}
                        <span class=\"help-block\"><i class=\"fa fa-info-circle\"></i> {{ metadata.help|trans|raw }}</span>
                    {% endif %}
                </div>
            </div>
        {% endfor %}

        <div class=\"form-group form-actions\">
            <div class=\"col-sm-10 col-sm-offset-2\">
            {% block item_actions %}
                {% set _show_actions = easyadmin_get_actions_for_show_item(_entity_config.name) %}
                {% set _request_parameters = { entity: _entity_config.name, referer: app.request.query.get('referer') } %}

                {{ include('@EasyAdmin/default/includes/_actions.html.twig', {
                    actions: _show_actions,
                    request_parameters: _request_parameters,
                    translation_domain: _entity_config.translation_domain,
                    trans_parameters: _trans_parameters,
                    item_id: _entity_id
                }, with_context = false) }}
            {% endblock item_actions %}
            </div>
        </div>
    </div>

    {% block delete_form %}
        {{ include('@EasyAdmin/default/includes/_delete_form.html.twig', {
            view: 'show',
            referer: app.request.query.get('referer', ''),
            delete_form: delete_form,
            _translation_domain: _entity_config.translation_domain,
            _trans_parameters: _trans_parameters,
            _entity_config: _entity_config,
        }, with_context = false) }}
    {% endblock delete_form %}
{% endblock %}

{% block body_javascript %}
    {{ parent() }}

    <script type=\"text/javascript\">
        \$(function() {
            \$('a.action-delete').on('click', function(e) {
                e.preventDefault();

                \$('#modal-delete').modal({ backdrop: true, keyboard: true })
                    .off('click', '#modal-delete-button')
                    .on('click', '#modal-delete-button', function () {
                        \$('#delete-form').trigger('submit');
                    });
            });
        });
    </script>
{% endblock %}
", "EasyAdminBundle:default:show.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/show.html.twig");
    }
}

<?php

/* @Framework/Form/reset_widget.html.php */
class __TwigTemplate_989499d4026475e5b74054cdf0fa37caacedc01dc61bc4f6971b5b4514560642 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b18091f358428190a3adbd66ad5449959bca758e5a3526144caedb72cc96e49d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b18091f358428190a3adbd66ad5449959bca758e5a3526144caedb72cc96e49d->enter($__internal_b18091f358428190a3adbd66ad5449959bca758e5a3526144caedb72cc96e49d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        $__internal_5d51e7566083638af501473fcae17dc87d81f9a4ccba74b2dc1df40753b23f14 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5d51e7566083638af501473fcae17dc87d81f9a4ccba74b2dc1df40753b23f14->enter($__internal_5d51e7566083638af501473fcae17dc87d81f9a4ccba74b2dc1df40753b23f14_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
";
        
        $__internal_b18091f358428190a3adbd66ad5449959bca758e5a3526144caedb72cc96e49d->leave($__internal_b18091f358428190a3adbd66ad5449959bca758e5a3526144caedb72cc96e49d_prof);

        
        $__internal_5d51e7566083638af501473fcae17dc87d81f9a4ccba74b2dc1df40753b23f14->leave($__internal_5d51e7566083638af501473fcae17dc87d81f9a4ccba74b2dc1df40753b23f14_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/reset_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
", "@Framework/Form/reset_widget.html.php", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/reset_widget.html.php");
    }
}

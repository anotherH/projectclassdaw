<?php

/* base.html.twig */
class __TwigTemplate_2e4c9f4a3070437f86c9d6a27199ef58c21bc55486994ea9dc89bfb93376a104 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b6a3c6ac2997aaae622c299a3adcfd73631cb74bc4442fb1ab8ce58061e7d6a6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b6a3c6ac2997aaae622c299a3adcfd73631cb74bc4442fb1ab8ce58061e7d6a6->enter($__internal_b6a3c6ac2997aaae622c299a3adcfd73631cb74bc4442fb1ab8ce58061e7d6a6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_3de06c3665f86df1aefe82cdded1ae4b2ed3e640ada518177bc5513cb8c6754a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3de06c3665f86df1aefe82cdded1ae4b2ed3e640ada518177bc5513cb8c6754a->enter($__internal_3de06c3665f86df1aefe82cdded1ae4b2ed3e640ada518177bc5513cb8c6754a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>

        <script
            src=\"https://code.jquery.com/jquery-3.2.1.min.js\"
            integrity=\"sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=\"
        crossorigin=\"anonymous\"></script>

        <!-- navbar, buttons and searchbar from https://bootsnipp.com/>-->
        <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\">

        <link rel=\"stylesheet\" href=\"https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css\">

        ";
        // line 18
        echo "        <script src=\"https://code.jquery.com/ui/1.12.1/jquery-ui.js\"></script>

        <!-- Latest compiled and minified JavaScript -->
        <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\" integrity=\"sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa\" crossorigin=\"anonymous\"></script>

        <!-- Main Quill library -->
        <script src=\"//cdn.quilljs.com/1.2.4/quill.js\"></script>
        <script src=\"//cdn.quilljs.com/1.2.4/quill.min.js\"></script>

        <!-- Theme included stylesheets -->
        <link href=\"//cdn.quilljs.com/1.2.4/quill.snow.css\" rel=\"stylesheet\">
        <link href=\"//cdn.quilljs.com/1.2.4/quill.bubble.css\" rel=\"stylesheet\">
        <style>
        ";
        // line 31
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 32
        echo "
        header .input-group{
            content: '';
            position: relative;
            display: block;
            height: 90px;
            width: auto;
            background: url(https://placeholdit.imgix.net/~text?txtsize=55&txt=1000%C3%9790&w=1000&h=90) transparent;
            background-position-x: center;
            background-repeat: no-repeat;
            padding-top: 30px;
        }

        header{
            background-color: #9d9d9d;   
        }

        .footer{
            background-color: #9d9d9d;
            width: 100%;
            height: 100px;
            text-align: center;
        }


        .footer::before{
            clear: both;
        }

        .footer  .input-group{
            text-align: center;
            background:  transparent;
        }

        span.copyright {
            font-size: 12px;
            display: block;
            text-align: center;
            -webkit-transform: translateX(-50%);
            -moz-transform: translateX(-50%);
            -ms-transform: translateX(-50%);
            -o-transform: translateX(-50%);
            transform: translateX(-50%);

            color: #6e6e6e;
        }

        .space-ad-90{height:90px;}
        .space-50{height:50px;}

        .main-content{
            border: 1px solid black;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
        }

        /*search input from : https://bootsnipp.com/snippets/featured/stylish-search-box many thanks*/
        .search-query {
            padding-right: 3px;
            padding-right: 4px \\9;
            padding-left: 3px;
            padding-left: 4px \\9;
            /* IE7-8 doesn't have border-radius, so don't indent the padding */
            margin-bottom: 0;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
        }

        .search {
            margin-right: 2%;
        }

        .input-group button{
            height: 34px;
            position: relative;
        }

        .login-box{
            margin-left: 10px;
        }

        .form-control,.input-group-btn{
            display:block !important;   
        }

        /** nav bar **/

        .navbar-collapse{
            width:100%;
        }
        .navbar-nav{
            box-shadow: 0 3px 5px rgba(0,0,0,0.5) !important;
            background: #0F0F0F;
            width: 100%;
            text-align: center;
            border-radius: 2px;
            overflow: hidden;
            white-space: nowrap;
        }

        .navbar-nav > li{
            float:none;
            display: inline-block;
            width: 19%;
        }

        .navbar-nav > li > a {
            color: #9d9d9d;
            border-bottom: 1px solid  #080808;
        }

        .navbar-nav > li > a:hover,
        .navbar-nav > li > a:focus {
            color: #fff;
            background-color: transparent;
            border-bottom: 1px solid #f62329;
            background: url(\"http://s31.postimg.org/9k42omozb/arrow.png\") bottom;
            background-repeat: no-repeat;
        }

        .navbar-nav > .active > a,
        .navbar-nav > .active > a:hover,
        .navbar-nav > .active > a:focus {
            color: #fff;
            background-color: #080808;
            border-bottom: 1px solid #f62329;
            background: url(\"http://s31.postimg.org/9k42omozb/arrow.png\") bottom;
            background-repeat: no-repeat;
        }

    </style>
    <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        // line 165
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
</head>
<body>
    <header role=\"banner\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"input-group col-md-12 col-lg-12 col-sm-12 col-xs-12\">
                    <div class=\"search col-md-offset-7 col-md-3  col-sm-offset-6 col-sm-3\">
                        <input type=\"text\" class=\"search-query form-control\" placeholder=\"Search\" />
                        <span class=\"input-group-btn\">
                            <button class=\"btn btn-danger\" type=\"button\"> <!-- cambiar btn-danger si se quiere dar estilo -->
                                <span class=\" glyphicon glyphicon-search\"></span>
                            </button>
                        </span>
                    </div>
                    <span class=\"login-box\">
                        <button onclick=\"location.href = '";
        // line 181
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_login");
        echo "'\"  class=\"btn btn-primary\" /> login </button>
                        <button onclick=\"location.href = '";
        // line 182
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_registration_register");
        echo "'\"  class=\"btn btn-primary\" /> join </button>
                    </span>
                </div>
            </div>
        </div>
    </header>

    <div class=\"col-md-offset-2 col-md-8 col-lg-offset-2 col-lg-8 col-sm-12 col-xs-12 container\">

        <div class=\"row space-ad-90 col-md-12 col-lg-12 col-sm-12 col-xs-12\"></div>

        <div class=\"collapse navbar-collapse col-md-12 col-lg-12 col-sm-12 col-xs-12 row\">
            <ul class=\"nav navbar-nav col-md-12 col-lg-12 col-sm-12 col-xs-12\">
                <li><a href=\"";
        // line 195
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\">Home</a></li>
                <li><a href=\"";
        // line 196
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("createbuild");
        echo "\">Create Guide</a></li>
                <li><a href=\"#\">Search Guide</a></li>
                <li><a href=\"/featSearcher\">Feats</a></li>
                <li><a href=\"/prestigeSearcher\">Prestige Classes</a></li>
            </ul>
        </div>

        <div class=\"row space-50  col-md-12 col-lg-12 col-sm-12 col-xs-12\"></div>

        <div class=\"main-content col-md-12 col-lg-12 col-sm-12 col-xs-12row\">

        ";
        // line 207
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 208
        echo "    ";
        $this->displayBlock('body', $context, $blocks);
        // line 209
        echo "</div>

<div class=\"row space-ad-90  col-md-12 col-lg-12 col-sm-12 col-xs-12\"></div>
</div>

<div role=\"footer\" class=\"footer col-md-12 col-lg-12 col-sm-12 col-xs-12\">
    <div class=\"container\">
        <div class=\"input-group col-md-12 col-lg-12 col-sm-12 col-xs-12\">
            <p>nombres R DAW</p>
            <p class=\"copyright\">Requires the use of a Roleplaying Game Core Book published by Wizards of the Coast, Inc.</p>
        </div>
    </div>

    ";
        // line 222
        $this->displayBlock('javascripts', $context, $blocks);
        // line 223
        echo "</body>
</html>
";
        
        $__internal_b6a3c6ac2997aaae622c299a3adcfd73631cb74bc4442fb1ab8ce58061e7d6a6->leave($__internal_b6a3c6ac2997aaae622c299a3adcfd73631cb74bc4442fb1ab8ce58061e7d6a6_prof);

        
        $__internal_3de06c3665f86df1aefe82cdded1ae4b2ed3e640ada518177bc5513cb8c6754a->leave($__internal_3de06c3665f86df1aefe82cdded1ae4b2ed3e640ada518177bc5513cb8c6754a_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_1d8d06d66b2debd0e4d112b73383112c5703915c4fc811908146d03e44a76580 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1d8d06d66b2debd0e4d112b73383112c5703915c4fc811908146d03e44a76580->enter($__internal_1d8d06d66b2debd0e4d112b73383112c5703915c4fc811908146d03e44a76580_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_b4ef0a8549720e30d506a885f731d6b6ab42629e73baf2bec276363444fa0fdd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b4ef0a8549720e30d506a885f731d6b6ab42629e73baf2bec276363444fa0fdd->enter($__internal_b4ef0a8549720e30d506a885f731d6b6ab42629e73baf2bec276363444fa0fdd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_b4ef0a8549720e30d506a885f731d6b6ab42629e73baf2bec276363444fa0fdd->leave($__internal_b4ef0a8549720e30d506a885f731d6b6ab42629e73baf2bec276363444fa0fdd_prof);

        
        $__internal_1d8d06d66b2debd0e4d112b73383112c5703915c4fc811908146d03e44a76580->leave($__internal_1d8d06d66b2debd0e4d112b73383112c5703915c4fc811908146d03e44a76580_prof);

    }

    // line 31
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_b92625d4207a2a3bd082c062a8a25f4527cc0725037de937bf0d7c9181dab37d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b92625d4207a2a3bd082c062a8a25f4527cc0725037de937bf0d7c9181dab37d->enter($__internal_b92625d4207a2a3bd082c062a8a25f4527cc0725037de937bf0d7c9181dab37d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_6586b1ad8922d7475fdde7a3433763180adcf7a1691b8c768f69b010538e194f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6586b1ad8922d7475fdde7a3433763180adcf7a1691b8c768f69b010538e194f->enter($__internal_6586b1ad8922d7475fdde7a3433763180adcf7a1691b8c768f69b010538e194f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_6586b1ad8922d7475fdde7a3433763180adcf7a1691b8c768f69b010538e194f->leave($__internal_6586b1ad8922d7475fdde7a3433763180adcf7a1691b8c768f69b010538e194f_prof);

        
        $__internal_b92625d4207a2a3bd082c062a8a25f4527cc0725037de937bf0d7c9181dab37d->leave($__internal_b92625d4207a2a3bd082c062a8a25f4527cc0725037de937bf0d7c9181dab37d_prof);

    }

    // line 207
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_ae27397c80e467d764d09ba0bd91386926c9046194dce27552dbd33929c4d5d9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ae27397c80e467d764d09ba0bd91386926c9046194dce27552dbd33929c4d5d9->enter($__internal_ae27397c80e467d764d09ba0bd91386926c9046194dce27552dbd33929c4d5d9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_b14ea1645df099afb2a85df730ee5f6a7af4b08c09c755306e9b273515149c7f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b14ea1645df099afb2a85df730ee5f6a7af4b08c09c755306e9b273515149c7f->enter($__internal_b14ea1645df099afb2a85df730ee5f6a7af4b08c09c755306e9b273515149c7f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        
        $__internal_b14ea1645df099afb2a85df730ee5f6a7af4b08c09c755306e9b273515149c7f->leave($__internal_b14ea1645df099afb2a85df730ee5f6a7af4b08c09c755306e9b273515149c7f_prof);

        
        $__internal_ae27397c80e467d764d09ba0bd91386926c9046194dce27552dbd33929c4d5d9->leave($__internal_ae27397c80e467d764d09ba0bd91386926c9046194dce27552dbd33929c4d5d9_prof);

    }

    // line 208
    public function block_body($context, array $blocks = array())
    {
        $__internal_bef836d3000ad5f3ce30cf45097200e66e28eeb980019c2d985da5743499c072 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bef836d3000ad5f3ce30cf45097200e66e28eeb980019c2d985da5743499c072->enter($__internal_bef836d3000ad5f3ce30cf45097200e66e28eeb980019c2d985da5743499c072_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_8c8a6fee0afd3d1637bd9e4745db430b7a201ceaefc8b4d23d2c41486dc1827e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8c8a6fee0afd3d1637bd9e4745db430b7a201ceaefc8b4d23d2c41486dc1827e->enter($__internal_8c8a6fee0afd3d1637bd9e4745db430b7a201ceaefc8b4d23d2c41486dc1827e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_8c8a6fee0afd3d1637bd9e4745db430b7a201ceaefc8b4d23d2c41486dc1827e->leave($__internal_8c8a6fee0afd3d1637bd9e4745db430b7a201ceaefc8b4d23d2c41486dc1827e_prof);

        
        $__internal_bef836d3000ad5f3ce30cf45097200e66e28eeb980019c2d985da5743499c072->leave($__internal_bef836d3000ad5f3ce30cf45097200e66e28eeb980019c2d985da5743499c072_prof);

    }

    // line 222
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_48833e386989cf76f1d57c17c429bfa8f31c739a2f3546b0c6d821defeed75db = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_48833e386989cf76f1d57c17c429bfa8f31c739a2f3546b0c6d821defeed75db->enter($__internal_48833e386989cf76f1d57c17c429bfa8f31c739a2f3546b0c6d821defeed75db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_283e2ec1ad324bd64c9d460f4c71967adb31b9f2557d75c78ae7929289711540 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_283e2ec1ad324bd64c9d460f4c71967adb31b9f2557d75c78ae7929289711540->enter($__internal_283e2ec1ad324bd64c9d460f4c71967adb31b9f2557d75c78ae7929289711540_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        echo "    ";
        
        $__internal_283e2ec1ad324bd64c9d460f4c71967adb31b9f2557d75c78ae7929289711540->leave($__internal_283e2ec1ad324bd64c9d460f4c71967adb31b9f2557d75c78ae7929289711540_prof);

        
        $__internal_48833e386989cf76f1d57c17c429bfa8f31c739a2f3546b0c6d821defeed75db->leave($__internal_48833e386989cf76f1d57c17c429bfa8f31c739a2f3546b0c6d821defeed75db_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  363 => 222,  346 => 208,  329 => 207,  312 => 31,  294 => 5,  282 => 223,  280 => 222,  265 => 209,  262 => 208,  260 => 207,  246 => 196,  242 => 195,  226 => 182,  222 => 181,  203 => 165,  68 => 32,  66 => 31,  51 => 18,  36 => 5,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>

        <script
            src=\"https://code.jquery.com/jquery-3.2.1.min.js\"
            integrity=\"sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=\"
        crossorigin=\"anonymous\"></script>

        <!-- navbar, buttons and searchbar from https://bootsnipp.com/>-->
        <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\">

        <link rel=\"stylesheet\" href=\"https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css\">

        {# JQUERY lib required for the autocomplete #}
        <script src=\"https://code.jquery.com/ui/1.12.1/jquery-ui.js\"></script>

        <!-- Latest compiled and minified JavaScript -->
        <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\" integrity=\"sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa\" crossorigin=\"anonymous\"></script>

        <!-- Main Quill library -->
        <script src=\"//cdn.quilljs.com/1.2.4/quill.js\"></script>
        <script src=\"//cdn.quilljs.com/1.2.4/quill.min.js\"></script>

        <!-- Theme included stylesheets -->
        <link href=\"//cdn.quilljs.com/1.2.4/quill.snow.css\" rel=\"stylesheet\">
        <link href=\"//cdn.quilljs.com/1.2.4/quill.bubble.css\" rel=\"stylesheet\">
        <style>
        {% block stylesheets %}{% endblock %}

        header .input-group{
            content: '';
            position: relative;
            display: block;
            height: 90px;
            width: auto;
            background: url(https://placeholdit.imgix.net/~text?txtsize=55&txt=1000%C3%9790&w=1000&h=90) transparent;
            background-position-x: center;
            background-repeat: no-repeat;
            padding-top: 30px;
        }

        header{
            background-color: #9d9d9d;   
        }

        .footer{
            background-color: #9d9d9d;
            width: 100%;
            height: 100px;
            text-align: center;
        }


        .footer::before{
            clear: both;
        }

        .footer  .input-group{
            text-align: center;
            background:  transparent;
        }

        span.copyright {
            font-size: 12px;
            display: block;
            text-align: center;
            -webkit-transform: translateX(-50%);
            -moz-transform: translateX(-50%);
            -ms-transform: translateX(-50%);
            -o-transform: translateX(-50%);
            transform: translateX(-50%);

            color: #6e6e6e;
        }

        .space-ad-90{height:90px;}
        .space-50{height:50px;}

        .main-content{
            border: 1px solid black;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
        }

        /*search input from : https://bootsnipp.com/snippets/featured/stylish-search-box many thanks*/
        .search-query {
            padding-right: 3px;
            padding-right: 4px \\9;
            padding-left: 3px;
            padding-left: 4px \\9;
            /* IE7-8 doesn't have border-radius, so don't indent the padding */
            margin-bottom: 0;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
        }

        .search {
            margin-right: 2%;
        }

        .input-group button{
            height: 34px;
            position: relative;
        }

        .login-box{
            margin-left: 10px;
        }

        .form-control,.input-group-btn{
            display:block !important;   
        }

        /** nav bar **/

        .navbar-collapse{
            width:100%;
        }
        .navbar-nav{
            box-shadow: 0 3px 5px rgba(0,0,0,0.5) !important;
            background: #0F0F0F;
            width: 100%;
            text-align: center;
            border-radius: 2px;
            overflow: hidden;
            white-space: nowrap;
        }

        .navbar-nav > li{
            float:none;
            display: inline-block;
            width: 19%;
        }

        .navbar-nav > li > a {
            color: #9d9d9d;
            border-bottom: 1px solid  #080808;
        }

        .navbar-nav > li > a:hover,
        .navbar-nav > li > a:focus {
            color: #fff;
            background-color: transparent;
            border-bottom: 1px solid #f62329;
            background: url(\"http://s31.postimg.org/9k42omozb/arrow.png\") bottom;
            background-repeat: no-repeat;
        }

        .navbar-nav > .active > a,
        .navbar-nav > .active > a:hover,
        .navbar-nav > .active > a:focus {
            color: #fff;
            background-color: #080808;
            border-bottom: 1px solid #f62329;
            background: url(\"http://s31.postimg.org/9k42omozb/arrow.png\") bottom;
            background-repeat: no-repeat;
        }

    </style>
    <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
</head>
<body>
    <header role=\"banner\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"input-group col-md-12 col-lg-12 col-sm-12 col-xs-12\">
                    <div class=\"search col-md-offset-7 col-md-3  col-sm-offset-6 col-sm-3\">
                        <input type=\"text\" class=\"search-query form-control\" placeholder=\"Search\" />
                        <span class=\"input-group-btn\">
                            <button class=\"btn btn-danger\" type=\"button\"> <!-- cambiar btn-danger si se quiere dar estilo -->
                                <span class=\" glyphicon glyphicon-search\"></span>
                            </button>
                        </span>
                    </div>
                    <span class=\"login-box\">
                        <button onclick=\"location.href = '{{ path('fos_user_security_login') }}'\"  class=\"btn btn-primary\" /> login </button>
                        <button onclick=\"location.href = '{{ path('fos_user_registration_register') }}'\"  class=\"btn btn-primary\" /> join </button>
                    </span>
                </div>
            </div>
        </div>
    </header>

    <div class=\"col-md-offset-2 col-md-8 col-lg-offset-2 col-lg-8 col-sm-12 col-xs-12 container\">

        <div class=\"row space-ad-90 col-md-12 col-lg-12 col-sm-12 col-xs-12\"></div>

        <div class=\"collapse navbar-collapse col-md-12 col-lg-12 col-sm-12 col-xs-12 row\">
            <ul class=\"nav navbar-nav col-md-12 col-lg-12 col-sm-12 col-xs-12\">
                <li><a href=\"{{ path('homepage') }}\">Home</a></li>
                <li><a href=\"{{ path('createbuild') }}\">Create Guide</a></li>
                <li><a href=\"#\">Search Guide</a></li>
                <li><a href=\"/featSearcher\">Feats</a></li>
                <li><a href=\"/prestigeSearcher\">Prestige Classes</a></li>
            </ul>
        </div>

        <div class=\"row space-50  col-md-12 col-lg-12 col-sm-12 col-xs-12\"></div>

        <div class=\"main-content col-md-12 col-lg-12 col-sm-12 col-xs-12row\">

        {% block fos_user_content %}{% endblock %}
    {% block body %}{% endblock %}
</div>

<div class=\"row space-ad-90  col-md-12 col-lg-12 col-sm-12 col-xs-12\"></div>
</div>

<div role=\"footer\" class=\"footer col-md-12 col-lg-12 col-sm-12 col-xs-12\">
    <div class=\"container\">
        <div class=\"input-group col-md-12 col-lg-12 col-sm-12 col-xs-12\">
            <p>nombres R DAW</p>
            <p class=\"copyright\">Requires the use of a Roleplaying Game Core Book published by Wizards of the Coast, Inc.</p>
        </div>
    </div>

    {% block javascripts %}    {% endblock %}
</body>
</html>
", "base.html.twig", "/home/a14eriamocob/public_html/DnD/app/Resources/views/base.html.twig");
    }
}

<?php

/* FOSUserBundle:Group:show_content.html.twig */
class __TwigTemplate_6da0e2b65b7735f1e1a9f69a6d2367d079a2c935eea5cd92780ac3d7e7e7904f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b5abf314bca58cee71e64b1d722b214bd9b29f50c2266ae69d45dfee122f4c65 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b5abf314bca58cee71e64b1d722b214bd9b29f50c2266ae69d45dfee122f4c65->enter($__internal_b5abf314bca58cee71e64b1d722b214bd9b29f50c2266ae69d45dfee122f4c65_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show_content.html.twig"));

        $__internal_13e55af3c21b5cc7cf722c1d494f05b3853797281840180b179eb690b14c35f7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_13e55af3c21b5cc7cf722c1d494f05b3853797281840180b179eb690b14c35f7->enter($__internal_13e55af3c21b5cc7cf722c1d494f05b3853797281840180b179eb690b14c35f7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show_content.html.twig"));

        // line 2
        echo "
<div class=\"fos_user_group_show\">
    <p>";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("group.show.name", array(), "FOSUserBundle"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["group"] ?? $this->getContext($context, "group")), "getName", array(), "method"), "html", null, true);
        echo "</p>
</div>
";
        
        $__internal_b5abf314bca58cee71e64b1d722b214bd9b29f50c2266ae69d45dfee122f4c65->leave($__internal_b5abf314bca58cee71e64b1d722b214bd9b29f50c2266ae69d45dfee122f4c65_prof);

        
        $__internal_13e55af3c21b5cc7cf722c1d494f05b3853797281840180b179eb690b14c35f7->leave($__internal_13e55af3c21b5cc7cf722c1d494f05b3853797281840180b179eb690b14c35f7_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:show_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 4,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}

<div class=\"fos_user_group_show\">
    <p>{{ 'group.show.name'|trans }}: {{ group.getName() }}</p>
</div>
", "FOSUserBundle:Group:show_content.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/friendsofsymfony/user-bundle/Resources/views/Group/show_content.html.twig");
    }
}

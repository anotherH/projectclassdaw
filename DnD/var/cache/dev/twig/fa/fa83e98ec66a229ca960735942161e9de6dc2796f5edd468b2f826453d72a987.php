<?php

/* TwigBundle:Exception:exception.rdf.twig */
class __TwigTemplate_175ee0a4dd804429ff044a99ef6938ed31390db3d857ba90d2478e0a9565f25f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_14b7c9fd7cebe9c9a3a812e18a3675dcf40076bb4bfee25079799c9d51469eba = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_14b7c9fd7cebe9c9a3a812e18a3675dcf40076bb4bfee25079799c9d51469eba->enter($__internal_14b7c9fd7cebe9c9a3a812e18a3675dcf40076bb4bfee25079799c9d51469eba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        $__internal_256bdfe479a2af011477cd004c379ac859c2567d30ca065af6efe6768060a32c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_256bdfe479a2af011477cd004c379ac859c2567d30ca065af6efe6768060a32c->enter($__internal_256bdfe479a2af011477cd004c379ac859c2567d30ca065af6efe6768060a32c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "TwigBundle:Exception:exception.rdf.twig", 1)->display(array_merge($context, array("exception" => ($context["exception"] ?? $this->getContext($context, "exception")))));
        
        $__internal_14b7c9fd7cebe9c9a3a812e18a3675dcf40076bb4bfee25079799c9d51469eba->leave($__internal_14b7c9fd7cebe9c9a3a812e18a3675dcf40076bb4bfee25079799c9d51469eba_prof);

        
        $__internal_256bdfe479a2af011477cd004c379ac859c2567d30ca065af6efe6768060a32c->leave($__internal_256bdfe479a2af011477cd004c379ac859c2567d30ca065af6efe6768060a32c_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}
", "TwigBundle:Exception:exception.rdf.twig", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.rdf.twig");
    }
}

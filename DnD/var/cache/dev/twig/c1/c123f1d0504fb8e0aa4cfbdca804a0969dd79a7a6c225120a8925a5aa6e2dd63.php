<?php

/* @Framework/Form/form_rest.html.php */
class __TwigTemplate_f19410ae8da91b24d80d6e12cb95dc2cbfb15744267e9d8d3876fb53c0cd14cf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0b27e4fae2cac12628c71b0ecb0caedeac5cd3d75d1eaa711c682dc83170b691 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0b27e4fae2cac12628c71b0ecb0caedeac5cd3d75d1eaa711c682dc83170b691->enter($__internal_0b27e4fae2cac12628c71b0ecb0caedeac5cd3d75d1eaa711c682dc83170b691_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        $__internal_8bc1ca8779f4645c11e16686f41675556cb04374551e807a4fbf9e81d5a875f8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8bc1ca8779f4645c11e16686f41675556cb04374551e807a4fbf9e81d5a875f8->enter($__internal_8bc1ca8779f4645c11e16686f41675556cb04374551e807a4fbf9e81d5a875f8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
";
        
        $__internal_0b27e4fae2cac12628c71b0ecb0caedeac5cd3d75d1eaa711c682dc83170b691->leave($__internal_0b27e4fae2cac12628c71b0ecb0caedeac5cd3d75d1eaa711c682dc83170b691_prof);

        
        $__internal_8bc1ca8779f4645c11e16686f41675556cb04374551e807a4fbf9e81d5a875f8->leave($__internal_8bc1ca8779f4645c11e16686f41675556cb04374551e807a4fbf9e81d5a875f8_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rest.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
", "@Framework/Form/form_rest.html.php", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_rest.html.php");
    }
}

<?php

/* @Framework/Form/datetime_widget.html.php */
class __TwigTemplate_a7e2b4e416f54f13ca9317ff557d5863e90b49ff5e8faec617d65cbd0b016517 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2800ecfec8a45cd87883b10732eef37a25df3f2243facad5743beb19e7028129 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2800ecfec8a45cd87883b10732eef37a25df3f2243facad5743beb19e7028129->enter($__internal_2800ecfec8a45cd87883b10732eef37a25df3f2243facad5743beb19e7028129_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/datetime_widget.html.php"));

        $__internal_5139281cd1c890e0f12d1cfe130b34117625bfd4c70461a4f31eb175755e39cc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5139281cd1c890e0f12d1cfe130b34117625bfd4c70461a4f31eb175755e39cc->enter($__internal_5139281cd1c890e0f12d1cfe130b34117625bfd4c70461a4f31eb175755e39cc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/datetime_widget.html.php"));

        // line 1
        echo "<?php if (\$widget == 'single_text'): ?>
    <?php echo \$view['form']->block(\$form, 'form_widget_simple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
        <?php echo \$view['form']->widget(\$form['date']).' '.\$view['form']->widget(\$form['time']) ?>
    </div>
<?php endif ?>
";
        
        $__internal_2800ecfec8a45cd87883b10732eef37a25df3f2243facad5743beb19e7028129->leave($__internal_2800ecfec8a45cd87883b10732eef37a25df3f2243facad5743beb19e7028129_prof);

        
        $__internal_5139281cd1c890e0f12d1cfe130b34117625bfd4c70461a4f31eb175755e39cc->leave($__internal_5139281cd1c890e0f12d1cfe130b34117625bfd4c70461a4f31eb175755e39cc_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/datetime_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$widget == 'single_text'): ?>
    <?php echo \$view['form']->block(\$form, 'form_widget_simple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
        <?php echo \$view['form']->widget(\$form['date']).' '.\$view['form']->widget(\$form['time']) ?>
    </div>
<?php endif ?>
", "@Framework/Form/datetime_widget.html.php", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/datetime_widget.html.php");
    }
}

<?php

/* @Framework/Form/form_widget_simple.html.php */
class __TwigTemplate_1672b6ff318eca35a25856b83fa4eca0e0c2d1f1d5ae41b28fe6bde97ffb9898 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_252463f59710145436f87e99fb61f5646ea3a4b9f414eee2cd2e837185ac6db7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_252463f59710145436f87e99fb61f5646ea3a4b9f414eee2cd2e837185ac6db7->enter($__internal_252463f59710145436f87e99fb61f5646ea3a4b9f414eee2cd2e837185ac6db7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_simple.html.php"));

        $__internal_6fba4bd1aae9a3bfcd9a0ac859bc6f44e3b27375bd6421bd6c2af9b1bdd0b980 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6fba4bd1aae9a3bfcd9a0ac859bc6f44e3b27375bd6421bd6c2af9b1bdd0b980->enter($__internal_6fba4bd1aae9a3bfcd9a0ac859bc6f44e3b27375bd6421bd6c2af9b1bdd0b980_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_simple.html.php"));

        // line 1
        echo "<input type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'text' ?>\" <?php echo \$view['form']->block(\$form, 'widget_attributes') ?><?php if (!empty(\$value) || is_numeric(\$value)): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?> />
";
        
        $__internal_252463f59710145436f87e99fb61f5646ea3a4b9f414eee2cd2e837185ac6db7->leave($__internal_252463f59710145436f87e99fb61f5646ea3a4b9f414eee2cd2e837185ac6db7_prof);

        
        $__internal_6fba4bd1aae9a3bfcd9a0ac859bc6f44e3b27375bd6421bd6c2af9b1bdd0b980->leave($__internal_6fba4bd1aae9a3bfcd9a0ac859bc6f44e3b27375bd6421bd6c2af9b1bdd0b980_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_simple.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<input type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'text' ?>\" <?php echo \$view['form']->block(\$form, 'widget_attributes') ?><?php if (!empty(\$value) || is_numeric(\$value)): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?> />
", "@Framework/Form/form_widget_simple.html.php", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_widget_simple.html.php");
    }
}

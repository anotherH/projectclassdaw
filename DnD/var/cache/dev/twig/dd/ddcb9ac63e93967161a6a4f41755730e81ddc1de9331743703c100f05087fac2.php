<?php

/* @Framework/Form/submit_widget.html.php */
class __TwigTemplate_e2b3a36c720cfa6a80f7f0ef81777f4e66edf3c1606a6f78d54be06261b901b5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b8abb4aed856a28af89197ce1b1679bb18d9e6b87bf8baeba477e16775618ac8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b8abb4aed856a28af89197ce1b1679bb18d9e6b87bf8baeba477e16775618ac8->enter($__internal_b8abb4aed856a28af89197ce1b1679bb18d9e6b87bf8baeba477e16775618ac8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        $__internal_c8047e40ef9a9cb097c76fd6680247203e9d20908409259bd6c2b2bc0533e6a4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c8047e40ef9a9cb097c76fd6680247203e9d20908409259bd6c2b2bc0533e6a4->enter($__internal_c8047e40ef9a9cb097c76fd6680247203e9d20908409259bd6c2b2bc0533e6a4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'submit')) ?>
";
        
        $__internal_b8abb4aed856a28af89197ce1b1679bb18d9e6b87bf8baeba477e16775618ac8->leave($__internal_b8abb4aed856a28af89197ce1b1679bb18d9e6b87bf8baeba477e16775618ac8_prof);

        
        $__internal_c8047e40ef9a9cb097c76fd6680247203e9d20908409259bd6c2b2bc0533e6a4->leave($__internal_c8047e40ef9a9cb097c76fd6680247203e9d20908409259bd6c2b2bc0533e6a4_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/submit_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'submit')) ?>
", "@Framework/Form/submit_widget.html.php", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/submit_widget.html.php");
    }
}

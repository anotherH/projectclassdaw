<?php

/* EasyAdminBundle:default:layout.html.twig */
class __TwigTemplate_8cac7662bbe4116fcfb3d2cb28acb448023b883cccc65c3a7b89a6e742d88bf2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'page_title' => array($this, 'block_page_title'),
            'head_stylesheets' => array($this, 'block_head_stylesheets'),
            'head_favicon' => array($this, 'block_head_favicon'),
            'head_javascript' => array($this, 'block_head_javascript'),
            'adminlte_options' => array($this, 'block_adminlte_options'),
            'body' => array($this, 'block_body'),
            'body_id' => array($this, 'block_body_id'),
            'body_class' => array($this, 'block_body_class'),
            'wrapper' => array($this, 'block_wrapper'),
            'header' => array($this, 'block_header'),
            'header_logo' => array($this, 'block_header_logo'),
            'header_custom_menu' => array($this, 'block_header_custom_menu'),
            'user_menu' => array($this, 'block_user_menu'),
            'user_menu_dropdown' => array($this, 'block_user_menu_dropdown'),
            'sidebar' => array($this, 'block_sidebar'),
            'main_menu_wrapper' => array($this, 'block_main_menu_wrapper'),
            'content' => array($this, 'block_content'),
            'flash_messages' => array($this, 'block_flash_messages'),
            'content_header' => array($this, 'block_content_header'),
            'content_title' => array($this, 'block_content_title'),
            'content_help' => array($this, 'block_content_help'),
            'main' => array($this, 'block_main'),
            'body_javascript' => array($this, 'block_body_javascript'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d475f76520094cdcd31c90c732c6a40f15d5f994adfa31eea5206f1daf7320aa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d475f76520094cdcd31c90c732c6a40f15d5f994adfa31eea5206f1daf7320aa->enter($__internal_d475f76520094cdcd31c90c732c6a40f15d5f994adfa31eea5206f1daf7320aa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:layout.html.twig"));

        $__internal_a6db37452686f42ac659ce5f405b7149403e9ecf5ae7b66a86f859b1c12c4dab = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a6db37452686f42ac659ce5f405b7149403e9ecf5ae7b66a86f859b1c12c4dab->enter($__internal_a6db37452686f42ac659ce5f405b7149403e9ecf5ae7b66a86f859b1c12c4dab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"";
        // line 2
        echo twig_escape_filter($this->env, _twig_default_filter(twig_first($this->env, twig_split_filter($this->env, $this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "locale", array()), "_")), "en"), "html", null, true);
        echo "\">
    <head>
        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
        <meta name=\"robots\" content=\"noindex, nofollow, noarchive, nosnippet, noodp, noimageindex, notranslate, nocache\" />
        <meta content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\" name=\"viewport\">
        <meta name=\"generator\" content=\"EasyAdmin\" />

        <title>";
        // line 10
        $this->displayBlock('page_title', $context, $blocks);
        echo "</title>

        ";
        // line 12
        $this->displayBlock('head_stylesheets', $context, $blocks);
        // line 18
        echo "
        ";
        // line 19
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getBackendConfiguration("design.assets.css"));
        foreach ($context['_seq'] as $context["_key"] => $context["css_asset"]) {
            // line 20
            echo "            <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($context["css_asset"]), "html", null, true);
            echo "\">
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['css_asset'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "
        ";
        // line 23
        $this->displayBlock('head_favicon', $context, $blocks);
        // line 27
        echo "
        ";
        // line 28
        $this->displayBlock('head_javascript', $context, $blocks);
        // line 45
        echo "
        ";
        // line 46
        if ($this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getBackendConfiguration("design.rtl")) {
            // line 47
            echo "            <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/easyadmin/stylesheet/bootstrap-rtl.min.css"), "html", null, true);
            echo "\">
            <link rel=\"stylesheet\" href=\"";
            // line 48
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/easyadmin/stylesheet/adminlte-rtl.min.css"), "html", null, true);
            echo "\">
        ";
        }
        // line 50
        echo "
        <!--[if lt IE 9]>
            <script src=\"";
        // line 52
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/easyadmin/stylesheet/html5shiv.min.css"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/easyadmin/stylesheet/respond.min.css"), "html", null, true);
        echo "\"></script>
        <![endif]-->
    </head>

    ";
        // line 57
        $this->displayBlock('body', $context, $blocks);
        // line 167
        echo "</html>
";
        
        $__internal_d475f76520094cdcd31c90c732c6a40f15d5f994adfa31eea5206f1daf7320aa->leave($__internal_d475f76520094cdcd31c90c732c6a40f15d5f994adfa31eea5206f1daf7320aa_prof);

        
        $__internal_a6db37452686f42ac659ce5f405b7149403e9ecf5ae7b66a86f859b1c12c4dab->leave($__internal_a6db37452686f42ac659ce5f405b7149403e9ecf5ae7b66a86f859b1c12c4dab_prof);

    }

    // line 10
    public function block_page_title($context, array $blocks = array())
    {
        $__internal_03f728a8cd5a63ddd1d68f9678da3dac87d83c4266ef56d95789a28ddad9c5e2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_03f728a8cd5a63ddd1d68f9678da3dac87d83c4266ef56d95789a28ddad9c5e2->enter($__internal_03f728a8cd5a63ddd1d68f9678da3dac87d83c4266ef56d95789a28ddad9c5e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page_title"));

        $__internal_acbce7e7a0dc68cd83f36e2d120a6c9c661eca860324ce51bae120a49aa16769 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_acbce7e7a0dc68cd83f36e2d120a6c9c661eca860324ce51bae120a49aa16769->enter($__internal_acbce7e7a0dc68cd83f36e2d120a6c9c661eca860324ce51bae120a49aa16769_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page_title"));

        echo strip_tags(        $this->renderBlock("content_title", $context, $blocks));
        
        $__internal_acbce7e7a0dc68cd83f36e2d120a6c9c661eca860324ce51bae120a49aa16769->leave($__internal_acbce7e7a0dc68cd83f36e2d120a6c9c661eca860324ce51bae120a49aa16769_prof);

        
        $__internal_03f728a8cd5a63ddd1d68f9678da3dac87d83c4266ef56d95789a28ddad9c5e2->leave($__internal_03f728a8cd5a63ddd1d68f9678da3dac87d83c4266ef56d95789a28ddad9c5e2_prof);

    }

    // line 12
    public function block_head_stylesheets($context, array $blocks = array())
    {
        $__internal_dbee468a24f69c85fe9ad2b9bc1c3f7050087c66b2d7b63dac335a324e36bfbb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dbee468a24f69c85fe9ad2b9bc1c3f7050087c66b2d7b63dac335a324e36bfbb->enter($__internal_dbee468a24f69c85fe9ad2b9bc1c3f7050087c66b2d7b63dac335a324e36bfbb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head_stylesheets"));

        $__internal_8410047522fecbc894204167205afe304f5a23af92eeafd69cc1ea54e229acf7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8410047522fecbc894204167205afe304f5a23af92eeafd69cc1ea54e229acf7->enter($__internal_8410047522fecbc894204167205afe304f5a23af92eeafd69cc1ea54e229acf7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head_stylesheets"));

        // line 13
        echo "            <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/easyadmin/stylesheet/easyadmin-all.min.css"), "html", null, true);
        echo "\">
            <style>
                ";
        // line 15
        echo $this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getBackendConfiguration("_internal.custom_css");
        echo "
            </style>
        ";
        
        $__internal_8410047522fecbc894204167205afe304f5a23af92eeafd69cc1ea54e229acf7->leave($__internal_8410047522fecbc894204167205afe304f5a23af92eeafd69cc1ea54e229acf7_prof);

        
        $__internal_dbee468a24f69c85fe9ad2b9bc1c3f7050087c66b2d7b63dac335a324e36bfbb->leave($__internal_dbee468a24f69c85fe9ad2b9bc1c3f7050087c66b2d7b63dac335a324e36bfbb_prof);

    }

    // line 23
    public function block_head_favicon($context, array $blocks = array())
    {
        $__internal_f9446a30abc3f6abb424df1051c24922dee9d60838524a88eaa4116b4aca483c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f9446a30abc3f6abb424df1051c24922dee9d60838524a88eaa4116b4aca483c->enter($__internal_f9446a30abc3f6abb424df1051c24922dee9d60838524a88eaa4116b4aca483c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head_favicon"));

        $__internal_5ce788a791f19e403b07c855fffa127493f8d3176d9a04587fb553171009540e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5ce788a791f19e403b07c855fffa127493f8d3176d9a04587fb553171009540e->enter($__internal_5ce788a791f19e403b07c855fffa127493f8d3176d9a04587fb553171009540e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head_favicon"));

        // line 24
        echo "            ";
        $context["favicon"] = $this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getBackendConfiguration("design.assets.favicon");
        // line 25
        echo "            <link rel=\"icon\" type=\"";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["favicon"] ?? $this->getContext($context, "favicon")), "mime_type", array()), "html", null, true);
        echo "\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($this->getAttribute(($context["favicon"] ?? $this->getContext($context, "favicon")), "path", array())), "html", null, true);
        echo "\" />
        ";
        
        $__internal_5ce788a791f19e403b07c855fffa127493f8d3176d9a04587fb553171009540e->leave($__internal_5ce788a791f19e403b07c855fffa127493f8d3176d9a04587fb553171009540e_prof);

        
        $__internal_f9446a30abc3f6abb424df1051c24922dee9d60838524a88eaa4116b4aca483c->leave($__internal_f9446a30abc3f6abb424df1051c24922dee9d60838524a88eaa4116b4aca483c_prof);

    }

    // line 28
    public function block_head_javascript($context, array $blocks = array())
    {
        $__internal_f114bbd076860b7c6af375bf1041175db82682c4b3bc67378c45c0ab2e82291b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f114bbd076860b7c6af375bf1041175db82682c4b3bc67378c45c0ab2e82291b->enter($__internal_f114bbd076860b7c6af375bf1041175db82682c4b3bc67378c45c0ab2e82291b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head_javascript"));

        $__internal_bfa2354011a94874c3e5f8a0a72659fcac6796473d451f48803d50d4655abe90 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bfa2354011a94874c3e5f8a0a72659fcac6796473d451f48803d50d4655abe90->enter($__internal_bfa2354011a94874c3e5f8a0a72659fcac6796473d451f48803d50d4655abe90_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head_javascript"));

        // line 29
        echo "            ";
        $this->displayBlock('adminlte_options', $context, $blocks);
        // line 42
        echo "
            <script src=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/easyadmin/javascript/easyadmin-all.min.js"), "html", null, true);
        echo "\"></script>
        ";
        
        $__internal_bfa2354011a94874c3e5f8a0a72659fcac6796473d451f48803d50d4655abe90->leave($__internal_bfa2354011a94874c3e5f8a0a72659fcac6796473d451f48803d50d4655abe90_prof);

        
        $__internal_f114bbd076860b7c6af375bf1041175db82682c4b3bc67378c45c0ab2e82291b->leave($__internal_f114bbd076860b7c6af375bf1041175db82682c4b3bc67378c45c0ab2e82291b_prof);

    }

    // line 29
    public function block_adminlte_options($context, array $blocks = array())
    {
        $__internal_966d53e6ef0d36ea2f9a07851a1add9f3ce7b1039acdecc095aa754e42c47045 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_966d53e6ef0d36ea2f9a07851a1add9f3ce7b1039acdecc095aa754e42c47045->enter($__internal_966d53e6ef0d36ea2f9a07851a1add9f3ce7b1039acdecc095aa754e42c47045_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "adminlte_options"));

        $__internal_6958307f7019ee90e46fd71f66393a4223bc2f20ce990cd374608f71b502cfa4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6958307f7019ee90e46fd71f66393a4223bc2f20ce990cd374608f71b502cfa4->enter($__internal_6958307f7019ee90e46fd71f66393a4223bc2f20ce990cd374608f71b502cfa4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "adminlte_options"));

        // line 30
        echo "                <script type=\"text/javascript\">
                    var AdminLTEOptions = {
                        animationSpeed: 'normal',
                        sidebarExpandOnHover: false,
                        enableBoxRefresh: false,
                        enableBSToppltip: false,
                        enableFastclick: false,
                        enableControlSidebar: false,
                        enableBoxWidget: false
                    };
                </script>
            ";
        
        $__internal_6958307f7019ee90e46fd71f66393a4223bc2f20ce990cd374608f71b502cfa4->leave($__internal_6958307f7019ee90e46fd71f66393a4223bc2f20ce990cd374608f71b502cfa4_prof);

        
        $__internal_966d53e6ef0d36ea2f9a07851a1add9f3ce7b1039acdecc095aa754e42c47045->leave($__internal_966d53e6ef0d36ea2f9a07851a1add9f3ce7b1039acdecc095aa754e42c47045_prof);

    }

    // line 57
    public function block_body($context, array $blocks = array())
    {
        $__internal_05172761d191efbcf913b6c899969e5122d0b746f185823a3f47e9ec46ea5591 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_05172761d191efbcf913b6c899969e5122d0b746f185823a3f47e9ec46ea5591->enter($__internal_05172761d191efbcf913b6c899969e5122d0b746f185823a3f47e9ec46ea5591_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_c299657ea4392ae8738b9efd0b71dabcaf8ba80688049905840282d517561a09 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c299657ea4392ae8738b9efd0b71dabcaf8ba80688049905840282d517561a09->enter($__internal_c299657ea4392ae8738b9efd0b71dabcaf8ba80688049905840282d517561a09_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 58
        echo "    <body id=\"";
        $this->displayBlock('body_id', $context, $blocks);
        echo "\" class=\"easyadmin sidebar-mini ";
        $this->displayBlock('body_class', $context, $blocks);
        echo " ";
        echo (($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "cookies", array()), "has", array(0 => "_easyadmin_navigation_iscollapsed"), "method")) ? ("sidebar-collapse") : (""));
        echo "\">
        <div class=\"wrapper\">
        ";
        // line 60
        $this->displayBlock('wrapper', $context, $blocks);
        // line 158
        echo "        </div>

        ";
        // line 160
        $this->displayBlock('body_javascript', $context, $blocks);
        // line 161
        echo "
        ";
        // line 162
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getBackendConfiguration("design.assets.js"));
        foreach ($context['_seq'] as $context["_key"] => $context["js_asset"]) {
            // line 163
            echo "            <script src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($context["js_asset"]), "html", null, true);
            echo "\"></script>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['js_asset'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 165
        echo "    </body>
    ";
        
        $__internal_c299657ea4392ae8738b9efd0b71dabcaf8ba80688049905840282d517561a09->leave($__internal_c299657ea4392ae8738b9efd0b71dabcaf8ba80688049905840282d517561a09_prof);

        
        $__internal_05172761d191efbcf913b6c899969e5122d0b746f185823a3f47e9ec46ea5591->leave($__internal_05172761d191efbcf913b6c899969e5122d0b746f185823a3f47e9ec46ea5591_prof);

    }

    // line 58
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_e1204fe158f2f21bdae7d0137fb139e05fddb4dba22100e9478f43d1863a59fb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e1204fe158f2f21bdae7d0137fb139e05fddb4dba22100e9478f43d1863a59fb->enter($__internal_e1204fe158f2f21bdae7d0137fb139e05fddb4dba22100e9478f43d1863a59fb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        $__internal_626f00228b78b9056d236334178ad12497665d5df638493172ff761bf31d7ce7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_626f00228b78b9056d236334178ad12497665d5df638493172ff761bf31d7ce7->enter($__internal_626f00228b78b9056d236334178ad12497665d5df638493172ff761bf31d7ce7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        
        $__internal_626f00228b78b9056d236334178ad12497665d5df638493172ff761bf31d7ce7->leave($__internal_626f00228b78b9056d236334178ad12497665d5df638493172ff761bf31d7ce7_prof);

        
        $__internal_e1204fe158f2f21bdae7d0137fb139e05fddb4dba22100e9478f43d1863a59fb->leave($__internal_e1204fe158f2f21bdae7d0137fb139e05fddb4dba22100e9478f43d1863a59fb_prof);

    }

    public function block_body_class($context, array $blocks = array())
    {
        $__internal_8080aca48ce756f7d477616db743ce8f11f39d41315205437d9410395f7996fb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8080aca48ce756f7d477616db743ce8f11f39d41315205437d9410395f7996fb->enter($__internal_8080aca48ce756f7d477616db743ce8f11f39d41315205437d9410395f7996fb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_class"));

        $__internal_bf42fbb1ce4108a60fd430dc1a5906fbb576bb6600307fbc839ba36ad587441e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bf42fbb1ce4108a60fd430dc1a5906fbb576bb6600307fbc839ba36ad587441e->enter($__internal_bf42fbb1ce4108a60fd430dc1a5906fbb576bb6600307fbc839ba36ad587441e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_class"));

        
        $__internal_bf42fbb1ce4108a60fd430dc1a5906fbb576bb6600307fbc839ba36ad587441e->leave($__internal_bf42fbb1ce4108a60fd430dc1a5906fbb576bb6600307fbc839ba36ad587441e_prof);

        
        $__internal_8080aca48ce756f7d477616db743ce8f11f39d41315205437d9410395f7996fb->leave($__internal_8080aca48ce756f7d477616db743ce8f11f39d41315205437d9410395f7996fb_prof);

    }

    // line 60
    public function block_wrapper($context, array $blocks = array())
    {
        $__internal_139ab9eef59293202d9d4ec1c6bf3b1ec0416ef351bcb9540157118b525f7302 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_139ab9eef59293202d9d4ec1c6bf3b1ec0416ef351bcb9540157118b525f7302->enter($__internal_139ab9eef59293202d9d4ec1c6bf3b1ec0416ef351bcb9540157118b525f7302_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "wrapper"));

        $__internal_bfdb2f2fc8731075e87073a7eabbf9b3f2ffbb97e777fef2069645fe388d6168 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bfdb2f2fc8731075e87073a7eabbf9b3f2ffbb97e777fef2069645fe388d6168->enter($__internal_bfdb2f2fc8731075e87073a7eabbf9b3f2ffbb97e777fef2069645fe388d6168_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "wrapper"));

        // line 61
        echo "            <header class=\"main-header\">
            ";
        // line 62
        $this->displayBlock('header', $context, $blocks);
        // line 115
        echo "            </header>

            <aside class=\"main-sidebar\">
            ";
        // line 118
        $this->displayBlock('sidebar', $context, $blocks);
        // line 129
        echo "            </aside>

            <div class=\"content-wrapper\">
            ";
        // line 132
        $this->displayBlock('content', $context, $blocks);
        // line 156
        echo "            </div>
        ";
        
        $__internal_bfdb2f2fc8731075e87073a7eabbf9b3f2ffbb97e777fef2069645fe388d6168->leave($__internal_bfdb2f2fc8731075e87073a7eabbf9b3f2ffbb97e777fef2069645fe388d6168_prof);

        
        $__internal_139ab9eef59293202d9d4ec1c6bf3b1ec0416ef351bcb9540157118b525f7302->leave($__internal_139ab9eef59293202d9d4ec1c6bf3b1ec0416ef351bcb9540157118b525f7302_prof);

    }

    // line 62
    public function block_header($context, array $blocks = array())
    {
        $__internal_9856ac8a48f0c2a0acdce3ec4bdd2b8b1f535b5f586deaddf626f83dee16c81a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9856ac8a48f0c2a0acdce3ec4bdd2b8b1f535b5f586deaddf626f83dee16c81a->enter($__internal_9856ac8a48f0c2a0acdce3ec4bdd2b8b1f535b5f586deaddf626f83dee16c81a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        $__internal_a12a91a7de30717b9ab598c8f1e1c9f3b7e41de8f87a4c4a134cee43ff1f2411 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a12a91a7de30717b9ab598c8f1e1c9f3b7e41de8f87a4c4a134cee43ff1f2411->enter($__internal_a12a91a7de30717b9ab598c8f1e1c9f3b7e41de8f87a4c4a134cee43ff1f2411_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 63
        echo "                <nav class=\"navbar\" role=\"navigation\">
                    <a href=\"#\" class=\"sidebar-toggle\" data-toggle=\"offcanvas\" role=\"button\">
                        <span class=\"sr-only\">";
        // line 65
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("toggle_navigation", array(), "EasyAdminBundle"), "html", null, true);
        echo "</span>
                    </a>

                    <div id=\"header-logo\">
                        ";
        // line 69
        $this->displayBlock('header_logo', $context, $blocks);
        // line 74
        echo "                    </div>

                    <div class=\"navbar-custom-menu\">
                    ";
        // line 77
        $this->displayBlock('header_custom_menu', $context, $blocks);
        // line 112
        echo "                    </div>
                </nav>
            ";
        
        $__internal_a12a91a7de30717b9ab598c8f1e1c9f3b7e41de8f87a4c4a134cee43ff1f2411->leave($__internal_a12a91a7de30717b9ab598c8f1e1c9f3b7e41de8f87a4c4a134cee43ff1f2411_prof);

        
        $__internal_9856ac8a48f0c2a0acdce3ec4bdd2b8b1f535b5f586deaddf626f83dee16c81a->leave($__internal_9856ac8a48f0c2a0acdce3ec4bdd2b8b1f535b5f586deaddf626f83dee16c81a_prof);

    }

    // line 69
    public function block_header_logo($context, array $blocks = array())
    {
        $__internal_4624d4b44b1c4ac400b52cb083b9143f8b87599cd70408e14f1d7da338dcfab7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4624d4b44b1c4ac400b52cb083b9143f8b87599cd70408e14f1d7da338dcfab7->enter($__internal_4624d4b44b1c4ac400b52cb083b9143f8b87599cd70408e14f1d7da338dcfab7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header_logo"));

        $__internal_d44a6a4d9a3863c34c62df5b517599ebde619f08790eb3cc2693b3555e0a85fd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d44a6a4d9a3863c34c62df5b517599ebde619f08790eb3cc2693b3555e0a85fd->enter($__internal_d44a6a4d9a3863c34c62df5b517599ebde619f08790eb3cc2693b3555e0a85fd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header_logo"));

        // line 70
        echo "                            <a class=\"logo ";
        echo (((twig_length_filter($this->env, $this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getBackendConfiguration("site_name")) > 14)) ? ("logo-long") : (""));
        echo "\" title=\"";
        echo twig_escape_filter($this->env, strip_tags($this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getBackendConfiguration("site_name")), "html", null, true);
        echo "\" href=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("easyadmin");
        echo "\">
                                ";
        // line 71
        echo $this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getBackendConfiguration("site_name");
        echo "
                            </a>
                        ";
        
        $__internal_d44a6a4d9a3863c34c62df5b517599ebde619f08790eb3cc2693b3555e0a85fd->leave($__internal_d44a6a4d9a3863c34c62df5b517599ebde619f08790eb3cc2693b3555e0a85fd_prof);

        
        $__internal_4624d4b44b1c4ac400b52cb083b9143f8b87599cd70408e14f1d7da338dcfab7->leave($__internal_4624d4b44b1c4ac400b52cb083b9143f8b87599cd70408e14f1d7da338dcfab7_prof);

    }

    // line 77
    public function block_header_custom_menu($context, array $blocks = array())
    {
        $__internal_cbf5240a1d46730a5883716452b08d8884b1d25aa078bbce5cb882b6eb2eb8d7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cbf5240a1d46730a5883716452b08d8884b1d25aa078bbce5cb882b6eb2eb8d7->enter($__internal_cbf5240a1d46730a5883716452b08d8884b1d25aa078bbce5cb882b6eb2eb8d7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header_custom_menu"));

        $__internal_cec968c072d680b023442007508c7495f545c97687f076dfe6335c5c6d512c36 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cec968c072d680b023442007508c7495f545c97687f076dfe6335c5c6d512c36->enter($__internal_cec968c072d680b023442007508c7495f545c97687f076dfe6335c5c6d512c36_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header_custom_menu"));

        // line 78
        echo "                        ";
        $context["_logout_path"] = $this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getLogoutPath();
        // line 79
        echo "                        <ul class=\"nav navbar-nav\">
                            <li class=\"dropdown user user-menu\">
                                ";
        // line 81
        $this->displayBlock('user_menu', $context, $blocks);
        // line 109
        echo "                            </li>
                        </ul>
                    ";
        
        $__internal_cec968c072d680b023442007508c7495f545c97687f076dfe6335c5c6d512c36->leave($__internal_cec968c072d680b023442007508c7495f545c97687f076dfe6335c5c6d512c36_prof);

        
        $__internal_cbf5240a1d46730a5883716452b08d8884b1d25aa078bbce5cb882b6eb2eb8d7->leave($__internal_cbf5240a1d46730a5883716452b08d8884b1d25aa078bbce5cb882b6eb2eb8d7_prof);

    }

    // line 81
    public function block_user_menu($context, array $blocks = array())
    {
        $__internal_9e3a6365ea16daa19b30c5371782d78578ed5240a86b1aea009bb193398163f7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9e3a6365ea16daa19b30c5371782d78578ed5240a86b1aea009bb193398163f7->enter($__internal_9e3a6365ea16daa19b30c5371782d78578ed5240a86b1aea009bb193398163f7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "user_menu"));

        $__internal_22f128e611024f3ff557f1c6635ac4542a7c2f9898d27c0e6a2173aafde63e1e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_22f128e611024f3ff557f1c6635ac4542a7c2f9898d27c0e6a2173aafde63e1e->enter($__internal_22f128e611024f3ff557f1c6635ac4542a7c2f9898d27c0e6a2173aafde63e1e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "user_menu"));

        // line 82
        echo "                                    <span class=\"sr-only\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user.logged_in_as", array(), "EasyAdminBundle"), "html", null, true);
        echo "</span>

                                    ";
        // line 84
        if (((($this->getAttribute(($context["app"] ?? null), "user", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["app"] ?? null), "user", array()), false)) : (false)) == false)) {
            // line 85
            echo "                                        <i class=\"hidden-xs fa fa-user-times\"></i>
                                        ";
            // line 86
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user.anonymous", array(), "EasyAdminBundle"), "html", null, true);
            echo "
                                    ";
        } elseif ( !        // line 87
($context["_logout_path"] ?? $this->getContext($context, "_logout_path"))) {
            // line 88
            echo "                                        <i class=\"hidden-xs fa fa-user\"></i>
                                        ";
            // line 89
            echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute(($context["app"] ?? null), "user", array(), "any", false, true), "username", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute(($context["app"] ?? null), "user", array(), "any", false, true), "username", array()), $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user.unnamed", array(), "EasyAdminBundle"))) : ($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user.unnamed", array(), "EasyAdminBundle"))), "html", null, true);
            echo "
                                    ";
        } else {
            // line 91
            echo "                                        <div class=\"btn-group\">
                                            <button type=\"button\" class=\"btn\" data-toggle=\"dropdown\">
                                                <i class=\"hidden-xs fa fa-user\"></i>
                                                ";
            // line 94
            echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute(($context["app"] ?? null), "user", array(), "any", false, true), "username", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute(($context["app"] ?? null), "user", array(), "any", false, true), "username", array()), $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user.unnamed", array(), "EasyAdminBundle"))) : ($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user.unnamed", array(), "EasyAdminBundle"))), "html", null, true);
            echo "
                                            </button>
                                            <button type=\"button\" class=\"btn dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\">
                                                <span class=\"caret\"></span>
                                            </button>
                                            <ul class=\"dropdown-menu\" role=\"menu\">
                                                ";
            // line 100
            $this->displayBlock('user_menu_dropdown', $context, $blocks);
            // line 105
            echo "                                            </ul>
                                        </div>
                                    ";
        }
        // line 108
        echo "                                ";
        
        $__internal_22f128e611024f3ff557f1c6635ac4542a7c2f9898d27c0e6a2173aafde63e1e->leave($__internal_22f128e611024f3ff557f1c6635ac4542a7c2f9898d27c0e6a2173aafde63e1e_prof);

        
        $__internal_9e3a6365ea16daa19b30c5371782d78578ed5240a86b1aea009bb193398163f7->leave($__internal_9e3a6365ea16daa19b30c5371782d78578ed5240a86b1aea009bb193398163f7_prof);

    }

    // line 100
    public function block_user_menu_dropdown($context, array $blocks = array())
    {
        $__internal_96597807846e78d9d018dddec33f7bb002734993ba5a003d51c62e20fc169207 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_96597807846e78d9d018dddec33f7bb002734993ba5a003d51c62e20fc169207->enter($__internal_96597807846e78d9d018dddec33f7bb002734993ba5a003d51c62e20fc169207_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "user_menu_dropdown"));

        $__internal_8aa968caa568dd9c36b32d5f45eb8dee929f560645d3309a96df1eb3cb22188e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8aa968caa568dd9c36b32d5f45eb8dee929f560645d3309a96df1eb3cb22188e->enter($__internal_8aa968caa568dd9c36b32d5f45eb8dee929f560645d3309a96df1eb3cb22188e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "user_menu_dropdown"));

        // line 101
        echo "                                                <li>
                                                    <a href=\"";
        // line 102
        echo twig_escape_filter($this->env, ($context["_logout_path"] ?? $this->getContext($context, "_logout_path")), "html", null, true);
        echo "\"><i class=\"fa fa-sign-out\"></i> ";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user.signout", array(), "EasyAdminBundle"), "html", null, true);
        echo "</a>
                                                </li>
                                                ";
        
        $__internal_8aa968caa568dd9c36b32d5f45eb8dee929f560645d3309a96df1eb3cb22188e->leave($__internal_8aa968caa568dd9c36b32d5f45eb8dee929f560645d3309a96df1eb3cb22188e_prof);

        
        $__internal_96597807846e78d9d018dddec33f7bb002734993ba5a003d51c62e20fc169207->leave($__internal_96597807846e78d9d018dddec33f7bb002734993ba5a003d51c62e20fc169207_prof);

    }

    // line 118
    public function block_sidebar($context, array $blocks = array())
    {
        $__internal_73721b910285df215f0c84ce5e87fa28967b0e62a573ada87f0481898a8f5da1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_73721b910285df215f0c84ce5e87fa28967b0e62a573ada87f0481898a8f5da1->enter($__internal_73721b910285df215f0c84ce5e87fa28967b0e62a573ada87f0481898a8f5da1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        $__internal_55265ad878027262caa36f9062372e5e3f97991897bbd804ea0bc81437b5ad7a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_55265ad878027262caa36f9062372e5e3f97991897bbd804ea0bc81437b5ad7a->enter($__internal_55265ad878027262caa36f9062372e5e3f97991897bbd804ea0bc81437b5ad7a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        // line 119
        echo "                <section class=\"sidebar\">
                    ";
        // line 120
        $this->displayBlock('main_menu_wrapper', $context, $blocks);
        // line 127
        echo "                </section>
            ";
        
        $__internal_55265ad878027262caa36f9062372e5e3f97991897bbd804ea0bc81437b5ad7a->leave($__internal_55265ad878027262caa36f9062372e5e3f97991897bbd804ea0bc81437b5ad7a_prof);

        
        $__internal_73721b910285df215f0c84ce5e87fa28967b0e62a573ada87f0481898a8f5da1->leave($__internal_73721b910285df215f0c84ce5e87fa28967b0e62a573ada87f0481898a8f5da1_prof);

    }

    // line 120
    public function block_main_menu_wrapper($context, array $blocks = array())
    {
        $__internal_5183009fecb5068cd891b043a63c1a30c1ff41adcb0f1cc7a45cb23588c81a2b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5183009fecb5068cd891b043a63c1a30c1ff41adcb0f1cc7a45cb23588c81a2b->enter($__internal_5183009fecb5068cd891b043a63c1a30c1ff41adcb0f1cc7a45cb23588c81a2b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_menu_wrapper"));

        $__internal_d677cee50b4e2c26dda5de6613d62788db52458ea38afd97317ed52be1bc3891 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d677cee50b4e2c26dda5de6613d62788db52458ea38afd97317ed52be1bc3891->enter($__internal_d677cee50b4e2c26dda5de6613d62788db52458ea38afd97317ed52be1bc3891_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_menu_wrapper"));

        // line 121
        echo "                        ";
        echo twig_include($this->env, $context, array(0 => ((        // line 122
array_key_exists("_entity_config", $context)) ? ($this->getAttribute($this->getAttribute(($context["_entity_config"] ?? $this->getContext($context, "_entity_config")), "templates", array()), "menu", array())) : ("")), 1 => $this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getBackendConfiguration("design.templates.menu"), 2 => "@EasyAdmin/default/menu.html.twig"));
        // line 125
        echo "
                    ";
        
        $__internal_d677cee50b4e2c26dda5de6613d62788db52458ea38afd97317ed52be1bc3891->leave($__internal_d677cee50b4e2c26dda5de6613d62788db52458ea38afd97317ed52be1bc3891_prof);

        
        $__internal_5183009fecb5068cd891b043a63c1a30c1ff41adcb0f1cc7a45cb23588c81a2b->leave($__internal_5183009fecb5068cd891b043a63c1a30c1ff41adcb0f1cc7a45cb23588c81a2b_prof);

    }

    // line 132
    public function block_content($context, array $blocks = array())
    {
        $__internal_0d31b670dcc96e07ba9fe412a83227962ac5ac2cf555b4737db5cfe74d25badb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0d31b670dcc96e07ba9fe412a83227962ac5ac2cf555b4737db5cfe74d25badb->enter($__internal_0d31b670dcc96e07ba9fe412a83227962ac5ac2cf555b4737db5cfe74d25badb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_08e5e1a83e25f317b6c63803d465a30952b4aedaafea6ef6b91d5490278113b5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_08e5e1a83e25f317b6c63803d465a30952b4aedaafea6ef6b91d5490278113b5->enter($__internal_08e5e1a83e25f317b6c63803d465a30952b4aedaafea6ef6b91d5490278113b5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 133
        echo "                ";
        $this->displayBlock('flash_messages', $context, $blocks);
        // line 136
        echo "
                <section class=\"content-header\">
                ";
        // line 138
        $this->displayBlock('content_header', $context, $blocks);
        // line 141
        echo "                ";
        $this->displayBlock('content_help', $context, $blocks);
        // line 150
        echo "                </section>

                <section id=\"main\" class=\"content\">
                    ";
        // line 153
        $this->displayBlock('main', $context, $blocks);
        // line 154
        echo "                </section>
            ";
        
        $__internal_08e5e1a83e25f317b6c63803d465a30952b4aedaafea6ef6b91d5490278113b5->leave($__internal_08e5e1a83e25f317b6c63803d465a30952b4aedaafea6ef6b91d5490278113b5_prof);

        
        $__internal_0d31b670dcc96e07ba9fe412a83227962ac5ac2cf555b4737db5cfe74d25badb->leave($__internal_0d31b670dcc96e07ba9fe412a83227962ac5ac2cf555b4737db5cfe74d25badb_prof);

    }

    // line 133
    public function block_flash_messages($context, array $blocks = array())
    {
        $__internal_f58920d52bc70fe967051adc980324aa7f813aaec62609a7dc69db763613d9df = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f58920d52bc70fe967051adc980324aa7f813aaec62609a7dc69db763613d9df->enter($__internal_f58920d52bc70fe967051adc980324aa7f813aaec62609a7dc69db763613d9df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "flash_messages"));

        $__internal_e7129a4332a6ed5c61b7815e2b93b4340ec6e5e51f9e4930cbed1b7a7f5b02d9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e7129a4332a6ed5c61b7815e2b93b4340ec6e5e51f9e4930cbed1b7a7f5b02d9->enter($__internal_e7129a4332a6ed5c61b7815e2b93b4340ec6e5e51f9e4930cbed1b7a7f5b02d9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "flash_messages"));

        // line 134
        echo "                    ";
        echo twig_include($this->env, $context, ((array_key_exists("_entity_config", $context)) ? ($this->getAttribute($this->getAttribute(($context["_entity_config"] ?? $this->getContext($context, "_entity_config")), "templates", array()), "flash_messages", array())) : ("@EasyAdmin/default/flash_messages.html.twig")));
        echo "
                ";
        
        $__internal_e7129a4332a6ed5c61b7815e2b93b4340ec6e5e51f9e4930cbed1b7a7f5b02d9->leave($__internal_e7129a4332a6ed5c61b7815e2b93b4340ec6e5e51f9e4930cbed1b7a7f5b02d9_prof);

        
        $__internal_f58920d52bc70fe967051adc980324aa7f813aaec62609a7dc69db763613d9df->leave($__internal_f58920d52bc70fe967051adc980324aa7f813aaec62609a7dc69db763613d9df_prof);

    }

    // line 138
    public function block_content_header($context, array $blocks = array())
    {
        $__internal_5b9c1529fa5a3d69d0aca1c28e0e7b2b1339ff889fd8187ab4c79113900460d3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5b9c1529fa5a3d69d0aca1c28e0e7b2b1339ff889fd8187ab4c79113900460d3->enter($__internal_5b9c1529fa5a3d69d0aca1c28e0e7b2b1339ff889fd8187ab4c79113900460d3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_header"));

        $__internal_41f75b385d7467f27bf243ecf6721594626946ba1a40a02101334940a24b9b00 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_41f75b385d7467f27bf243ecf6721594626946ba1a40a02101334940a24b9b00->enter($__internal_41f75b385d7467f27bf243ecf6721594626946ba1a40a02101334940a24b9b00_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_header"));

        // line 139
        echo "                    <h1 class=\"title\">";
        $this->displayBlock('content_title', $context, $blocks);
        echo "</h1>
                ";
        
        $__internal_41f75b385d7467f27bf243ecf6721594626946ba1a40a02101334940a24b9b00->leave($__internal_41f75b385d7467f27bf243ecf6721594626946ba1a40a02101334940a24b9b00_prof);

        
        $__internal_5b9c1529fa5a3d69d0aca1c28e0e7b2b1339ff889fd8187ab4c79113900460d3->leave($__internal_5b9c1529fa5a3d69d0aca1c28e0e7b2b1339ff889fd8187ab4c79113900460d3_prof);

    }

    public function block_content_title($context, array $blocks = array())
    {
        $__internal_b9ebeb15711c3b06c0db21588b7e95a3c6ecaad2d704880e64f500aa7490c8df = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b9ebeb15711c3b06c0db21588b7e95a3c6ecaad2d704880e64f500aa7490c8df->enter($__internal_b9ebeb15711c3b06c0db21588b7e95a3c6ecaad2d704880e64f500aa7490c8df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_title"));

        $__internal_e1612888dd55669d7906413891d86848c0caa9d74d0a0ee6c14b47fcf85d471f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e1612888dd55669d7906413891d86848c0caa9d74d0a0ee6c14b47fcf85d471f->enter($__internal_e1612888dd55669d7906413891d86848c0caa9d74d0a0ee6c14b47fcf85d471f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_title"));

        
        $__internal_e1612888dd55669d7906413891d86848c0caa9d74d0a0ee6c14b47fcf85d471f->leave($__internal_e1612888dd55669d7906413891d86848c0caa9d74d0a0ee6c14b47fcf85d471f_prof);

        
        $__internal_b9ebeb15711c3b06c0db21588b7e95a3c6ecaad2d704880e64f500aa7490c8df->leave($__internal_b9ebeb15711c3b06c0db21588b7e95a3c6ecaad2d704880e64f500aa7490c8df_prof);

    }

    // line 141
    public function block_content_help($context, array $blocks = array())
    {
        $__internal_0d73e9ac2e1c00c72013f98c2aef7ce6c74798545ff7fe4719530984b65c4fec = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0d73e9ac2e1c00c72013f98c2aef7ce6c74798545ff7fe4719530984b65c4fec->enter($__internal_0d73e9ac2e1c00c72013f98c2aef7ce6c74798545ff7fe4719530984b65c4fec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_help"));

        $__internal_86eec2799066aad7c60d30f07c92c23cac762e688757e34957b2a270752b3a90 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_86eec2799066aad7c60d30f07c92c23cac762e688757e34957b2a270752b3a90->enter($__internal_86eec2799066aad7c60d30f07c92c23cac762e688757e34957b2a270752b3a90_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_help"));

        // line 142
        echo "                    ";
        if ((array_key_exists("_entity_config", $context) && (($this->getAttribute($this->getAttribute(($context["_entity_config"] ?? null), $this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "query", array()), "get", array(0 => "action"), "method"), array(), "array", false, true), "help", array(), "array", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute(($context["_entity_config"] ?? null), $this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "query", array()), "get", array(0 => "action"), "method"), array(), "array", false, true), "help", array(), "array"), false)) : (false)))) {
            // line 143
            echo "                        <div class=\"box box-widget help-entity\">
                            <div class=\"box-body\">
                                ";
            // line 145
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute($this->getAttribute(($context["_entity_config"] ?? $this->getContext($context, "_entity_config")), $this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "query", array()), "get", array(0 => "action"), "method"), array(), "array"), "help", array(), "array"));
            echo "
                            </div>
                        </div>
                    ";
        }
        // line 149
        echo "                ";
        
        $__internal_86eec2799066aad7c60d30f07c92c23cac762e688757e34957b2a270752b3a90->leave($__internal_86eec2799066aad7c60d30f07c92c23cac762e688757e34957b2a270752b3a90_prof);

        
        $__internal_0d73e9ac2e1c00c72013f98c2aef7ce6c74798545ff7fe4719530984b65c4fec->leave($__internal_0d73e9ac2e1c00c72013f98c2aef7ce6c74798545ff7fe4719530984b65c4fec_prof);

    }

    // line 153
    public function block_main($context, array $blocks = array())
    {
        $__internal_bc32b49f21893f70e7c7747a30d591091e57278ed684d56b8b44328b69c68fac = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bc32b49f21893f70e7c7747a30d591091e57278ed684d56b8b44328b69c68fac->enter($__internal_bc32b49f21893f70e7c7747a30d591091e57278ed684d56b8b44328b69c68fac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        $__internal_1ad43c93fdd58ba14ae178c15b8c4d652957eb53bffc41aa466191145508ed39 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1ad43c93fdd58ba14ae178c15b8c4d652957eb53bffc41aa466191145508ed39->enter($__internal_1ad43c93fdd58ba14ae178c15b8c4d652957eb53bffc41aa466191145508ed39_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        
        $__internal_1ad43c93fdd58ba14ae178c15b8c4d652957eb53bffc41aa466191145508ed39->leave($__internal_1ad43c93fdd58ba14ae178c15b8c4d652957eb53bffc41aa466191145508ed39_prof);

        
        $__internal_bc32b49f21893f70e7c7747a30d591091e57278ed684d56b8b44328b69c68fac->leave($__internal_bc32b49f21893f70e7c7747a30d591091e57278ed684d56b8b44328b69c68fac_prof);

    }

    // line 160
    public function block_body_javascript($context, array $blocks = array())
    {
        $__internal_b3da52459a8fba944ab47dcf6a32f6ad11541c1c3776de9df8965c2184a0e374 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b3da52459a8fba944ab47dcf6a32f6ad11541c1c3776de9df8965c2184a0e374->enter($__internal_b3da52459a8fba944ab47dcf6a32f6ad11541c1c3776de9df8965c2184a0e374_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_javascript"));

        $__internal_da307583a8179bddfafefd6b086689526a04ceedcaaccfd1568b2344f864988b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_da307583a8179bddfafefd6b086689526a04ceedcaaccfd1568b2344f864988b->enter($__internal_da307583a8179bddfafefd6b086689526a04ceedcaaccfd1568b2344f864988b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_javascript"));

        
        $__internal_da307583a8179bddfafefd6b086689526a04ceedcaaccfd1568b2344f864988b->leave($__internal_da307583a8179bddfafefd6b086689526a04ceedcaaccfd1568b2344f864988b_prof);

        
        $__internal_b3da52459a8fba944ab47dcf6a32f6ad11541c1c3776de9df8965c2184a0e374->leave($__internal_b3da52459a8fba944ab47dcf6a32f6ad11541c1c3776de9df8965c2184a0e374_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  786 => 160,  769 => 153,  759 => 149,  752 => 145,  748 => 143,  745 => 142,  736 => 141,  707 => 139,  698 => 138,  685 => 134,  676 => 133,  665 => 154,  663 => 153,  658 => 150,  655 => 141,  653 => 138,  649 => 136,  646 => 133,  637 => 132,  626 => 125,  624 => 122,  622 => 121,  613 => 120,  602 => 127,  600 => 120,  597 => 119,  588 => 118,  573 => 102,  570 => 101,  561 => 100,  551 => 108,  546 => 105,  544 => 100,  535 => 94,  530 => 91,  525 => 89,  522 => 88,  520 => 87,  516 => 86,  513 => 85,  511 => 84,  505 => 82,  496 => 81,  484 => 109,  482 => 81,  478 => 79,  475 => 78,  466 => 77,  453 => 71,  444 => 70,  435 => 69,  423 => 112,  421 => 77,  416 => 74,  414 => 69,  407 => 65,  403 => 63,  394 => 62,  383 => 156,  381 => 132,  376 => 129,  374 => 118,  369 => 115,  367 => 62,  364 => 61,  355 => 60,  322 => 58,  311 => 165,  302 => 163,  298 => 162,  295 => 161,  293 => 160,  289 => 158,  287 => 60,  277 => 58,  268 => 57,  247 => 30,  238 => 29,  226 => 43,  223 => 42,  220 => 29,  211 => 28,  196 => 25,  193 => 24,  184 => 23,  171 => 15,  165 => 13,  156 => 12,  138 => 10,  127 => 167,  125 => 57,  118 => 53,  114 => 52,  110 => 50,  105 => 48,  100 => 47,  98 => 46,  95 => 45,  93 => 28,  90 => 27,  88 => 23,  85 => 22,  76 => 20,  72 => 19,  69 => 18,  67 => 12,  62 => 10,  51 => 2,  48 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html lang=\"{{ app.request.locale|split('_')|first|default('en') }}\">
    <head>
        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
        <meta name=\"robots\" content=\"noindex, nofollow, noarchive, nosnippet, noodp, noimageindex, notranslate, nocache\" />
        <meta content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\" name=\"viewport\">
        <meta name=\"generator\" content=\"EasyAdmin\" />

        <title>{% block page_title %}{{ block('content_title')|striptags|raw }}{% endblock %}</title>

        {% block head_stylesheets %}
            <link rel=\"stylesheet\" href=\"{{ asset('bundles/easyadmin/stylesheet/easyadmin-all.min.css') }}\">
            <style>
                {{ easyadmin_config('_internal.custom_css')|raw }}
            </style>
        {% endblock %}

        {% for css_asset in easyadmin_config('design.assets.css') %}
            <link rel=\"stylesheet\" href=\"{{ asset(css_asset) }}\">
        {% endfor %}

        {% block head_favicon %}
            {% set favicon = easyadmin_config('design.assets.favicon') %}
            <link rel=\"icon\" type=\"{{ favicon.mime_type }}\" href=\"{{ asset(favicon.path) }}\" />
        {% endblock %}

        {% block head_javascript %}
            {% block adminlte_options %}
                <script type=\"text/javascript\">
                    var AdminLTEOptions = {
                        animationSpeed: 'normal',
                        sidebarExpandOnHover: false,
                        enableBoxRefresh: false,
                        enableBSToppltip: false,
                        enableFastclick: false,
                        enableControlSidebar: false,
                        enableBoxWidget: false
                    };
                </script>
            {% endblock %}

            <script src=\"{{ asset('bundles/easyadmin/javascript/easyadmin-all.min.js') }}\"></script>
        {% endblock head_javascript %}

        {% if easyadmin_config('design.rtl') %}
            <link rel=\"stylesheet\" href=\"{{ asset('bundles/easyadmin/stylesheet/bootstrap-rtl.min.css') }}\">
            <link rel=\"stylesheet\" href=\"{{ asset('bundles/easyadmin/stylesheet/adminlte-rtl.min.css') }}\">
        {% endif %}

        <!--[if lt IE 9]>
            <script src=\"{{ asset('bundles/easyadmin/stylesheet/html5shiv.min.css') }}\"></script>
            <script src=\"{{ asset('bundles/easyadmin/stylesheet/respond.min.css') }}\"></script>
        <![endif]-->
    </head>

    {% block body %}
    <body id=\"{% block body_id %}{% endblock %}\" class=\"easyadmin sidebar-mini {% block body_class %}{% endblock %} {{ app.request.cookies.has('_easyadmin_navigation_iscollapsed') ? 'sidebar-collapse' }}\">
        <div class=\"wrapper\">
        {% block wrapper %}
            <header class=\"main-header\">
            {% block header %}
                <nav class=\"navbar\" role=\"navigation\">
                    <a href=\"#\" class=\"sidebar-toggle\" data-toggle=\"offcanvas\" role=\"button\">
                        <span class=\"sr-only\">{{ 'toggle_navigation'|trans(domain = 'EasyAdminBundle') }}</span>
                    </a>

                    <div id=\"header-logo\">
                        {% block header_logo %}
                            <a class=\"logo {{ easyadmin_config('site_name')|length > 14 ? 'logo-long' }}\" title=\"{{ easyadmin_config('site_name')|striptags }}\" href=\"{{ path('easyadmin') }}\">
                                {{ easyadmin_config('site_name')|raw }}
                            </a>
                        {% endblock header_logo %}
                    </div>

                    <div class=\"navbar-custom-menu\">
                    {% block header_custom_menu %}
                        {% set _logout_path = easyadmin_logout_path() %}
                        <ul class=\"nav navbar-nav\">
                            <li class=\"dropdown user user-menu\">
                                {% block user_menu %}
                                    <span class=\"sr-only\">{{ 'user.logged_in_as'|trans(domain = 'EasyAdminBundle') }}</span>

                                    {% if app.user|default(false) == false %}
                                        <i class=\"hidden-xs fa fa-user-times\"></i>
                                        {{ 'user.anonymous'|trans(domain = 'EasyAdminBundle') }}
                                    {% elseif not _logout_path %}
                                        <i class=\"hidden-xs fa fa-user\"></i>
                                        {{ app.user.username|default('user.unnamed'|trans(domain = 'EasyAdminBundle')) }}
                                    {% else %}
                                        <div class=\"btn-group\">
                                            <button type=\"button\" class=\"btn\" data-toggle=\"dropdown\">
                                                <i class=\"hidden-xs fa fa-user\"></i>
                                                {{ app.user.username|default('user.unnamed'|trans(domain = 'EasyAdminBundle')) }}
                                            </button>
                                            <button type=\"button\" class=\"btn dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\">
                                                <span class=\"caret\"></span>
                                            </button>
                                            <ul class=\"dropdown-menu\" role=\"menu\">
                                                {% block user_menu_dropdown %}
                                                <li>
                                                    <a href=\"{{ _logout_path }}\"><i class=\"fa fa-sign-out\"></i> {{ 'user.signout'|trans(domain = 'EasyAdminBundle') }}</a>
                                                </li>
                                                {% endblock user_menu_dropdown %}
                                            </ul>
                                        </div>
                                    {% endif %}
                                {% endblock user_menu %}
                            </li>
                        </ul>
                    {% endblock header_custom_menu %}
                    </div>
                </nav>
            {% endblock header %}
            </header>

            <aside class=\"main-sidebar\">
            {% block sidebar %}
                <section class=\"sidebar\">
                    {% block main_menu_wrapper %}
                        {{ include([
                            _entity_config is defined ? _entity_config.templates.menu,
                            easyadmin_config('design.templates.menu'),
                            '@EasyAdmin/default/menu.html.twig'
                        ]) }}
                    {% endblock main_menu_wrapper %}
                </section>
            {% endblock sidebar %}
            </aside>

            <div class=\"content-wrapper\">
            {% block content %}
                {% block flash_messages %}
                    {{ include(_entity_config is defined ? _entity_config.templates.flash_messages : '@EasyAdmin/default/flash_messages.html.twig') }}
                {% endblock flash_messages %}

                <section class=\"content-header\">
                {% block content_header %}
                    <h1 class=\"title\">{% block content_title %}{% endblock %}</h1>
                {% endblock content_header %}
                {% block content_help %}
                    {% if _entity_config is defined and _entity_config[app.request.query.get('action')]['help']|default(false) %}
                        <div class=\"box box-widget help-entity\">
                            <div class=\"box-body\">
                                {{ _entity_config[app.request.query.get('action')]['help']|trans|raw }}
                            </div>
                        </div>
                    {% endif %}
                {% endblock content_help %}
                </section>

                <section id=\"main\" class=\"content\">
                    {% block main %}{% endblock %}
                </section>
            {% endblock content %}
            </div>
        {% endblock wrapper %}
        </div>

        {% block body_javascript %}{% endblock body_javascript %}

        {% for js_asset in easyadmin_config('design.assets.js') %}
            <script src=\"{{ asset(js_asset) }}\"></script>
        {% endfor %}
    </body>
    {% endblock body %}
</html>
", "EasyAdminBundle:default:layout.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/layout.html.twig");
    }
}

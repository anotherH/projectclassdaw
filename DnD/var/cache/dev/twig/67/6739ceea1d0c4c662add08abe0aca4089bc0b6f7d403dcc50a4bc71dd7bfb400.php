<?php

/* EasyAdminBundle:default:field_text.html.twig */
class __TwigTemplate_e7042cb8dfe165cf3f85bb9825d834d0da2c3c43608b334980083da794680f8f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8acb3479939619914eff9e966f80b2e56fbf322d2786430165444917531ca9a8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8acb3479939619914eff9e966f80b2e56fbf322d2786430165444917531ca9a8->enter($__internal_8acb3479939619914eff9e966f80b2e56fbf322d2786430165444917531ca9a8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_text.html.twig"));

        $__internal_917eecfad075c1d82bf669ecd98abf66d94f6c4f4f3b8f6f16a1dd5ae5a94056 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_917eecfad075c1d82bf669ecd98abf66d94f6c4f4f3b8f6f16a1dd5ae5a94056->enter($__internal_917eecfad075c1d82bf669ecd98abf66d94f6c4f4f3b8f6f16a1dd5ae5a94056_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_text.html.twig"));

        // line 1
        if ((($context["view"] ?? $this->getContext($context, "view")) == "show")) {
            // line 2
            echo "    ";
            echo nl2br(twig_escape_filter($this->env, ($context["value"] ?? $this->getContext($context, "value")), "html", null, true));
            echo "
";
        } else {
            // line 4
            echo "    ";
            echo twig_escape_filter($this->env, $this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->truncateText($this->env, ($context["value"] ?? $this->getContext($context, "value"))), "html", null, true);
            echo "
";
        }
        
        $__internal_8acb3479939619914eff9e966f80b2e56fbf322d2786430165444917531ca9a8->leave($__internal_8acb3479939619914eff9e966f80b2e56fbf322d2786430165444917531ca9a8_prof);

        
        $__internal_917eecfad075c1d82bf669ecd98abf66d94f6c4f4f3b8f6f16a1dd5ae5a94056->leave($__internal_917eecfad075c1d82bf669ecd98abf66d94f6c4f4f3b8f6f16a1dd5ae5a94056_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:field_text.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  33 => 4,  27 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if view == 'show' %}
    {{ value|nl2br }}
{% else %}
    {{ value|easyadmin_truncate }}
{% endif %}
", "EasyAdminBundle:default:field_text.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/field_text.html.twig");
    }
}

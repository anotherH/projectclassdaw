<?php

/* EasyAdminBundle:default:field_id.html.twig */
class __TwigTemplate_7a51377143531c13cd1e232fdb53b07c2e8a3bd44422dbcb06ef2e3b9a1177c0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2bc1de1184dc039ecfada49670f308b546d9f1e928464983c1cf9e816edcb93a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2bc1de1184dc039ecfada49670f308b546d9f1e928464983c1cf9e816edcb93a->enter($__internal_2bc1de1184dc039ecfada49670f308b546d9f1e928464983c1cf9e816edcb93a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_id.html.twig"));

        $__internal_f53341d4a16055fd61009ee3bc7a4dec60e1bbb305fb69a6335eefde7643a4f0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f53341d4a16055fd61009ee3bc7a4dec60e1bbb305fb69a6335eefde7643a4f0->enter($__internal_f53341d4a16055fd61009ee3bc7a4dec60e1bbb305fb69a6335eefde7643a4f0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_id.html.twig"));

        // line 2
        echo twig_escape_filter($this->env, ($context["value"] ?? $this->getContext($context, "value")), "html", null, true);
        echo "
";
        
        $__internal_2bc1de1184dc039ecfada49670f308b546d9f1e928464983c1cf9e816edcb93a->leave($__internal_2bc1de1184dc039ecfada49670f308b546d9f1e928464983c1cf9e816edcb93a_prof);

        
        $__internal_f53341d4a16055fd61009ee3bc7a4dec60e1bbb305fb69a6335eefde7643a4f0->leave($__internal_f53341d4a16055fd61009ee3bc7a4dec60e1bbb305fb69a6335eefde7643a4f0_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:field_id.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# this field template is used to avoid formatting the special ID attribute as a number #}
{{ value }}
", "EasyAdminBundle:default:field_id.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/field_id.html.twig");
    }
}

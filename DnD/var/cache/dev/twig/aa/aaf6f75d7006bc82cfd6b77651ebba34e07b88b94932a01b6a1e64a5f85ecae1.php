<?php

/* EasyAdminBundle:default:field_raw.html.twig */
class __TwigTemplate_b7352ae1b2d65d90bdb46edd4fbc7d46779f7ccbbf2820c99a1cd62327959d6c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a4eeeb589b56fefe5a95a7a93fa5555c173b6c6a4e7f0b769063c0f5d42d5ed8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a4eeeb589b56fefe5a95a7a93fa5555c173b6c6a4e7f0b769063c0f5d42d5ed8->enter($__internal_a4eeeb589b56fefe5a95a7a93fa5555c173b6c6a4e7f0b769063c0f5d42d5ed8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_raw.html.twig"));

        $__internal_92aa2ed9a2a2e55c5aa842e3a04ad1fcf31cd2bd4f30fe156c64a3965507dd01 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_92aa2ed9a2a2e55c5aa842e3a04ad1fcf31cd2bd4f30fe156c64a3965507dd01->enter($__internal_92aa2ed9a2a2e55c5aa842e3a04ad1fcf31cd2bd4f30fe156c64a3965507dd01_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_raw.html.twig"));

        // line 1
        echo ($context["value"] ?? $this->getContext($context, "value"));
        echo "
";
        
        $__internal_a4eeeb589b56fefe5a95a7a93fa5555c173b6c6a4e7f0b769063c0f5d42d5ed8->leave($__internal_a4eeeb589b56fefe5a95a7a93fa5555c173b6c6a4e7f0b769063c0f5d42d5ed8_prof);

        
        $__internal_92aa2ed9a2a2e55c5aa842e3a04ad1fcf31cd2bd4f30fe156c64a3965507dd01->leave($__internal_92aa2ed9a2a2e55c5aa842e3a04ad1fcf31cd2bd4f30fe156c64a3965507dd01_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:field_raw.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ value|raw }}
", "EasyAdminBundle:default:field_raw.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/field_raw.html.twig");
    }
}

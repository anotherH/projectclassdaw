<?php

/* WebProfilerBundle:Profiler:header.html.twig */
class __TwigTemplate_198fa432800bc2703558b6d33f6503c8c0312aed30e5d581e658dbf72ff53b1d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cc74ea513737b218996ab0888156ece95e773ebad7f8532d3b9d929cf99d1533 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cc74ea513737b218996ab0888156ece95e773ebad7f8532d3b9d929cf99d1533->enter($__internal_cc74ea513737b218996ab0888156ece95e773ebad7f8532d3b9d929cf99d1533_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:header.html.twig"));

        $__internal_331c705c68e97eca973c6cb8971fadf9b7fbf587d9fb4bb198923a5b51cae85a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_331c705c68e97eca973c6cb8971fadf9b7fbf587d9fb4bb198923a5b51cae85a->enter($__internal_331c705c68e97eca973c6cb8971fadf9b7fbf587d9fb4bb198923a5b51cae85a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:header.html.twig"));

        // line 1
        echo "<div id=\"header\">
    <div class=\"container\">
        <h1>";
        // line 3
        echo twig_include($this->env, $context, "@WebProfiler/Icon/symfony.svg");
        echo " Symfony <span>Profiler</span></h1>

        <div class=\"search\">
            <form method=\"get\" action=\"https://symfony.com/search\" target=\"_blank\">
                <div class=\"form-row\">
                    <input name=\"q\" id=\"search-id\" type=\"search\" placeholder=\"search on symfony.com\">
                    <button type=\"submit\" class=\"btn\">Search</button>
                </div>
           </form>
        </div>
    </div>
</div>
";
        
        $__internal_cc74ea513737b218996ab0888156ece95e773ebad7f8532d3b9d929cf99d1533->leave($__internal_cc74ea513737b218996ab0888156ece95e773ebad7f8532d3b9d929cf99d1533_prof);

        
        $__internal_331c705c68e97eca973c6cb8971fadf9b7fbf587d9fb4bb198923a5b51cae85a->leave($__internal_331c705c68e97eca973c6cb8971fadf9b7fbf587d9fb4bb198923a5b51cae85a_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 3,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div id=\"header\">
    <div class=\"container\">
        <h1>{{ include('@WebProfiler/Icon/symfony.svg') }} Symfony <span>Profiler</span></h1>

        <div class=\"search\">
            <form method=\"get\" action=\"https://symfony.com/search\" target=\"_blank\">
                <div class=\"form-row\">
                    <input name=\"q\" id=\"search-id\" type=\"search\" placeholder=\"search on symfony.com\">
                    <button type=\"submit\" class=\"btn\">Search</button>
                </div>
           </form>
        </div>
    </div>
</div>
", "WebProfilerBundle:Profiler:header.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/header.html.twig");
    }
}

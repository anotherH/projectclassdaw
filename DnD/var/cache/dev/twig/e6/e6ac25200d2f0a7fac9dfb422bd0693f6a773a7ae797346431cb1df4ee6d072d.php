<?php

/* @Framework/Form/textarea_widget.html.php */
class __TwigTemplate_477963f022e64e9a078aab670de74c725805a10d6a036ee03c6b9988b5af6af5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2aa4c0ec9e5f06134cf62a698f5ba06dba4ae9c2aff866414c9e78db8e6a0868 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2aa4c0ec9e5f06134cf62a698f5ba06dba4ae9c2aff866414c9e78db8e6a0868->enter($__internal_2aa4c0ec9e5f06134cf62a698f5ba06dba4ae9c2aff866414c9e78db8e6a0868_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        $__internal_85e0b535e14374edbbf812687a842ddfcce1f733c65a42e486a6a5bd06edb37b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_85e0b535e14374edbbf812687a842ddfcce1f733c65a42e486a6a5bd06edb37b->enter($__internal_85e0b535e14374edbbf812687a842ddfcce1f733c65a42e486a6a5bd06edb37b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        // line 1
        echo "<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
";
        
        $__internal_2aa4c0ec9e5f06134cf62a698f5ba06dba4ae9c2aff866414c9e78db8e6a0868->leave($__internal_2aa4c0ec9e5f06134cf62a698f5ba06dba4ae9c2aff866414c9e78db8e6a0868_prof);

        
        $__internal_85e0b535e14374edbbf812687a842ddfcce1f733c65a42e486a6a5bd06edb37b->leave($__internal_85e0b535e14374edbbf812687a842ddfcce1f733c65a42e486a6a5bd06edb37b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/textarea_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
", "@Framework/Form/textarea_widget.html.php", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/textarea_widget.html.php");
    }
}

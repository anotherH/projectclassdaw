<?php

/* EasyAdminBundle:default:label_inaccessible.html.twig */
class __TwigTemplate_625fa18bc76ca5fdb7485140a24a1b2a10f998685298c711a9039ef3f1557e27 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2ead3051ecda769f706c7a3c45fc1990699a9ffdb1cfe279236ac299c120c984 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2ead3051ecda769f706c7a3c45fc1990699a9ffdb1cfe279236ac299c120c984->enter($__internal_2ead3051ecda769f706c7a3c45fc1990699a9ffdb1cfe279236ac299c120c984_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:label_inaccessible.html.twig"));

        $__internal_63f9c34933ce566968fa28b31912a44416589b926db42949f0d2a13bd61122c9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_63f9c34933ce566968fa28b31912a44416589b926db42949f0d2a13bd61122c9->enter($__internal_63f9c34933ce566968fa28b31912a44416589b926db42949f0d2a13bd61122c9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:label_inaccessible.html.twig"));

        // line 2
        echo "
<span class=\"label label-danger\" title=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("label.inaccessible.explanation", array(), "EasyAdminBundle"), "html", null, true);
        echo "\">
    ";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("label.inaccessible", array(), "EasyAdminBundle"), "html", null, true);
        echo "
</span>
";
        
        $__internal_2ead3051ecda769f706c7a3c45fc1990699a9ffdb1cfe279236ac299c120c984->leave($__internal_2ead3051ecda769f706c7a3c45fc1990699a9ffdb1cfe279236ac299c120c984_prof);

        
        $__internal_63f9c34933ce566968fa28b31912a44416589b926db42949f0d2a13bd61122c9->leave($__internal_63f9c34933ce566968fa28b31912a44416589b926db42949f0d2a13bd61122c9_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:label_inaccessible.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  32 => 4,  28 => 3,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'EasyAdminBundle' %}

<span class=\"label label-danger\" title=\"{{ 'label.inaccessible.explanation'|trans }}\">
    {{ 'label.inaccessible'|trans }}
</span>
", "EasyAdminBundle:default:label_inaccessible.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/label_inaccessible.html.twig");
    }
}

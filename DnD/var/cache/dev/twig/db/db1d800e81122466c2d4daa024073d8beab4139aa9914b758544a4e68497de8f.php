<?php

/* @Framework/Form/form_widget.html.php */
class __TwigTemplate_96c2c5dd79f0b8fb1baa99e2a993b7bda424b4e04ba9dfea5080c97893b01399 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7c21494016f842e4c35ce5112405f8a7ece7b7bc440e9497ecc15b7fbee2e9cc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7c21494016f842e4c35ce5112405f8a7ece7b7bc440e9497ecc15b7fbee2e9cc->enter($__internal_7c21494016f842e4c35ce5112405f8a7ece7b7bc440e9497ecc15b7fbee2e9cc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        $__internal_407b495e5cd03025ad0e545eb7af478115a68007d621a042636136c54c43d6bc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_407b495e5cd03025ad0e545eb7af478115a68007d621a042636136c54c43d6bc->enter($__internal_407b495e5cd03025ad0e545eb7af478115a68007d621a042636136c54c43d6bc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        // line 1
        echo "<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
";
        
        $__internal_7c21494016f842e4c35ce5112405f8a7ece7b7bc440e9497ecc15b7fbee2e9cc->leave($__internal_7c21494016f842e4c35ce5112405f8a7ece7b7bc440e9497ecc15b7fbee2e9cc_prof);

        
        $__internal_407b495e5cd03025ad0e545eb7af478115a68007d621a042636136c54c43d6bc->leave($__internal_407b495e5cd03025ad0e545eb7af478115a68007d621a042636136c54c43d6bc_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
", "@Framework/Form/form_widget.html.php", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_widget.html.php");
    }
}

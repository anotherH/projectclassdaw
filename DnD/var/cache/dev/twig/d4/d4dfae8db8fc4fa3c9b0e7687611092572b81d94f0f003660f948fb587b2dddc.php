<?php

/* default/index.html.twig */
class __TwigTemplate_c29f80dbd9badb19aeaf88ba37bd19aa6947a659d7ff86d687bb66b83ebfbda0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "default/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5379096ec005a59a5431ec56601ea7bf9ac8992fd1671c8f786aa35b5ea821b1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5379096ec005a59a5431ec56601ea7bf9ac8992fd1671c8f786aa35b5ea821b1->enter($__internal_5379096ec005a59a5431ec56601ea7bf9ac8992fd1671c8f786aa35b5ea821b1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/index.html.twig"));

        $__internal_4ee7fd14ec4665bd743e95c54de4fb1b87858f19d1a6927ca4281e547ec87fd7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4ee7fd14ec4665bd743e95c54de4fb1b87858f19d1a6927ca4281e547ec87fd7->enter($__internal_4ee7fd14ec4665bd743e95c54de4fb1b87858f19d1a6927ca4281e547ec87fd7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5379096ec005a59a5431ec56601ea7bf9ac8992fd1671c8f786aa35b5ea821b1->leave($__internal_5379096ec005a59a5431ec56601ea7bf9ac8992fd1671c8f786aa35b5ea821b1_prof);

        
        $__internal_4ee7fd14ec4665bd743e95c54de4fb1b87858f19d1a6927ca4281e547ec87fd7->leave($__internal_4ee7fd14ec4665bd743e95c54de4fb1b87858f19d1a6927ca4281e547ec87fd7_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_e0f10019da2d30e0bc36c0ffe3a8e522a1ee0846e2d8cc0970abb41f91ea0eff = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e0f10019da2d30e0bc36c0ffe3a8e522a1ee0846e2d8cc0970abb41f91ea0eff->enter($__internal_e0f10019da2d30e0bc36c0ffe3a8e522a1ee0846e2d8cc0970abb41f91ea0eff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_1d94fed0ad9ddffeb01c3d847c72b9cb769d3f2379ef755c12787af61c381880 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1d94fed0ad9ddffeb01c3d847c72b9cb769d3f2379ef755c12787af61c381880->enter($__internal_1d94fed0ad9ddffeb01c3d847c72b9cb769d3f2379ef755c12787af61c381880_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    hola
";
        
        $__internal_1d94fed0ad9ddffeb01c3d847c72b9cb769d3f2379ef755c12787af61c381880->leave($__internal_1d94fed0ad9ddffeb01c3d847c72b9cb769d3f2379ef755c12787af61c381880_prof);

        
        $__internal_e0f10019da2d30e0bc36c0ffe3a8e522a1ee0846e2d8cc0970abb41f91ea0eff->leave($__internal_e0f10019da2d30e0bc36c0ffe3a8e522a1ee0846e2d8cc0970abb41f91ea0eff_prof);

    }

    // line 7
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_9ce379e93511a85fc3f10274cbcbad88810edba07c97dfcb7812be169eba222f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9ce379e93511a85fc3f10274cbcbad88810edba07c97dfcb7812be169eba222f->enter($__internal_9ce379e93511a85fc3f10274cbcbad88810edba07c97dfcb7812be169eba222f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_2ff88f8630c928ce2eab4c2d80c2acb950bf88f7b0e9b08e176f8dcfb49817ca = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2ff88f8630c928ce2eab4c2d80c2acb950bf88f7b0e9b08e176f8dcfb49817ca->enter($__internal_2ff88f8630c928ce2eab4c2d80c2acb950bf88f7b0e9b08e176f8dcfb49817ca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 8
        echo "
";
        
        $__internal_2ff88f8630c928ce2eab4c2d80c2acb950bf88f7b0e9b08e176f8dcfb49817ca->leave($__internal_2ff88f8630c928ce2eab4c2d80c2acb950bf88f7b0e9b08e176f8dcfb49817ca_prof);

        
        $__internal_9ce379e93511a85fc3f10274cbcbad88810edba07c97dfcb7812be169eba222f->leave($__internal_9ce379e93511a85fc3f10274cbcbad88810edba07c97dfcb7812be169eba222f_prof);

    }

    public function getTemplateName()
    {
        return "default/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  70 => 8,  61 => 7,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    hola
{% endblock %}

{% block stylesheets %}

{% endblock %}
", "default/index.html.twig", "/home/a14eriamocob/public_html/DnD/app/Resources/views/default/index.html.twig");
    }
}

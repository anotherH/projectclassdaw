<?php

/* EasyAdminBundle:default:exception.html.twig */
class __TwigTemplate_a8afdecd605b7ddc0f1e90eb614029750f0d65926014a045f7a48b77627ed539 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'body_class' => array($this, 'block_body_class'),
            'page_title' => array($this, 'block_page_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 3
        return $this->loadTemplate(array(0 => (($this->getAttribute($this->getAttribute(($context["_entity_config"] ?? null), "templates", array(), "any", false, true), "layout", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute(($context["_entity_config"] ?? null), "templates", array(), "any", false, true), "layout", array()), "")) : ("")), 1 => $this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getBackendConfiguration("design.templates.layout"), 2 => "@EasyAdmin/default/layout.html.twig"), "EasyAdminBundle:default:exception.html.twig", 3);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f5905d332d0184fe34ef895fb0b02684e7d8e9cf203c6b1bd0f4e90d10bc4283 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f5905d332d0184fe34ef895fb0b02684e7d8e9cf203c6b1bd0f4e90d10bc4283->enter($__internal_f5905d332d0184fe34ef895fb0b02684e7d8e9cf203c6b1bd0f4e90d10bc4283_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:exception.html.twig"));

        $__internal_b75fdc8b927fa00d4b9f5ba2fb4b9330b69b50c94cfad8d2675bcc9d405304d5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b75fdc8b927fa00d4b9f5ba2fb4b9330b69b50c94cfad8d2675bcc9d405304d5->enter($__internal_b75fdc8b927fa00d4b9f5ba2fb4b9330b69b50c94cfad8d2675bcc9d405304d5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:exception.html.twig"));

        // line 1
        $context["_entity_config"] = $this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getEntityConfiguration($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "query", array()), "get", array(0 => "entity"), "method"));
        // line 3
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f5905d332d0184fe34ef895fb0b02684e7d8e9cf203c6b1bd0f4e90d10bc4283->leave($__internal_f5905d332d0184fe34ef895fb0b02684e7d8e9cf203c6b1bd0f4e90d10bc4283_prof);

        
        $__internal_b75fdc8b927fa00d4b9f5ba2fb4b9330b69b50c94cfad8d2675bcc9d405304d5->leave($__internal_b75fdc8b927fa00d4b9f5ba2fb4b9330b69b50c94cfad8d2675bcc9d405304d5_prof);

    }

    // line 8
    public function block_body_class($context, array $blocks = array())
    {
        $__internal_7f0be58c612ff59946bde9d910500f8fc22d15e39467b041ee05853a768d7fa8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7f0be58c612ff59946bde9d910500f8fc22d15e39467b041ee05853a768d7fa8->enter($__internal_7f0be58c612ff59946bde9d910500f8fc22d15e39467b041ee05853a768d7fa8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_class"));

        $__internal_e7825bbfe28188550985049e07172b4832fd5521d129b9e17cee8c981544effd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e7825bbfe28188550985049e07172b4832fd5521d129b9e17cee8c981544effd->enter($__internal_e7825bbfe28188550985049e07172b4832fd5521d129b9e17cee8c981544effd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_class"));

        echo "error";
        
        $__internal_e7825bbfe28188550985049e07172b4832fd5521d129b9e17cee8c981544effd->leave($__internal_e7825bbfe28188550985049e07172b4832fd5521d129b9e17cee8c981544effd_prof);

        
        $__internal_7f0be58c612ff59946bde9d910500f8fc22d15e39467b041ee05853a768d7fa8->leave($__internal_7f0be58c612ff59946bde9d910500f8fc22d15e39467b041ee05853a768d7fa8_prof);

    }

    // line 9
    public function block_page_title($context, array $blocks = array())
    {
        $__internal_ab8818f66b1d7397357c8643e68e851e2180d1f1cad7aae34adc50f456371c1d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ab8818f66b1d7397357c8643e68e851e2180d1f1cad7aae34adc50f456371c1d->enter($__internal_ab8818f66b1d7397357c8643e68e851e2180d1f1cad7aae34adc50f456371c1d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page_title"));

        $__internal_b0c7b03ecabd71b84ab933bdb272ce26ceb982b88d6b79b95167850189278629 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b0c7b03ecabd71b84ab933bdb272ce26ceb982b88d6b79b95167850189278629->enter($__internal_b0c7b03ecabd71b84ab933bdb272ce26ceb982b88d6b79b95167850189278629_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page_title"));

        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->transchoice("errors", 1, array(), "EasyAdminBundle"), "html", null, true);
        
        $__internal_b0c7b03ecabd71b84ab933bdb272ce26ceb982b88d6b79b95167850189278629->leave($__internal_b0c7b03ecabd71b84ab933bdb272ce26ceb982b88d6b79b95167850189278629_prof);

        
        $__internal_ab8818f66b1d7397357c8643e68e851e2180d1f1cad7aae34adc50f456371c1d->leave($__internal_ab8818f66b1d7397357c8643e68e851e2180d1f1cad7aae34adc50f456371c1d_prof);

    }

    // line 11
    public function block_content($context, array $blocks = array())
    {
        $__internal_fee2a3df9fff6eb1eec631b2918b72588f8052358fd46c79158fb9f96981fe6c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fee2a3df9fff6eb1eec631b2918b72588f8052358fd46c79158fb9f96981fe6c->enter($__internal_fee2a3df9fff6eb1eec631b2918b72588f8052358fd46c79158fb9f96981fe6c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_c6685ddeaa6dfb8d53fe1c60f67b6ab4d26cbc30648109bf1102f5e225204483 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c6685ddeaa6dfb8d53fe1c60f67b6ab4d26cbc30648109bf1102f5e225204483->enter($__internal_c6685ddeaa6dfb8d53fe1c60f67b6ab4d26cbc30648109bf1102f5e225204483_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 12
        echo "    <section id=\"main\" class=\"content\">
        <div class=\"error-description\">
            <h1><i class=\"fa fa-exclamation-circle\"></i> ";
        // line 14
        $this->displayBlock("page_title", $context, $blocks);
        echo "</h1>

            <div class=\"error-message\">
                ";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "publicMessage", array()), $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "translationParameters", array()), "EasyAdminBundle"), "html", null, true);
        echo "
            </div>
        </div>
    </section>
";
        
        $__internal_c6685ddeaa6dfb8d53fe1c60f67b6ab4d26cbc30648109bf1102f5e225204483->leave($__internal_c6685ddeaa6dfb8d53fe1c60f67b6ab4d26cbc30648109bf1102f5e225204483_prof);

        
        $__internal_fee2a3df9fff6eb1eec631b2918b72588f8052358fd46c79158fb9f96981fe6c->leave($__internal_fee2a3df9fff6eb1eec631b2918b72588f8052358fd46c79158fb9f96981fe6c_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 17,  93 => 14,  89 => 12,  80 => 11,  62 => 9,  44 => 8,  34 => 3,  32 => 1,  20 => 3,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% set _entity_config = easyadmin_entity(app.request.query.get('entity')) %}
{% extends [
    _entity_config.templates.layout|default(''),
    easyadmin_config('design.templates.layout'),
    '@EasyAdmin/default/layout.html.twig'
] %}

{% block body_class 'error' %}
{% block page_title %}{{ 'errors'|transchoice(1, {}, 'EasyAdminBundle') }}{% endblock %}

{% block content %}
    <section id=\"main\" class=\"content\">
        <div class=\"error-description\">
            <h1><i class=\"fa fa-exclamation-circle\"></i> {{ block('page_title') }}</h1>

            <div class=\"error-message\">
                {{ exception.publicMessage|trans(exception.translationParameters, 'EasyAdminBundle') }}
            </div>
        </div>
    </section>
{% endblock %}
", "EasyAdminBundle:default:exception.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/exception.html.twig");
    }
}

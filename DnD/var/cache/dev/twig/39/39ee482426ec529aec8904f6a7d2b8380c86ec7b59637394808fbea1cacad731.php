<?php

/* EasyAdminBundle:default:field_string.html.twig */
class __TwigTemplate_f7e63ad9362bb395368bac30ae807d8a5d92ee516d06264de44069a143400af0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_30d0642bc25e4f0e62ff76427216f37cbd89d162efa64015bff471c5ffbbb11b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_30d0642bc25e4f0e62ff76427216f37cbd89d162efa64015bff471c5ffbbb11b->enter($__internal_30d0642bc25e4f0e62ff76427216f37cbd89d162efa64015bff471c5ffbbb11b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_string.html.twig"));

        $__internal_f729949e4568e6961574bcb84960b0f7e8c0d75411a5a50160bd12764a981655 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f729949e4568e6961574bcb84960b0f7e8c0d75411a5a50160bd12764a981655->enter($__internal_f729949e4568e6961574bcb84960b0f7e8c0d75411a5a50160bd12764a981655_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_string.html.twig"));

        // line 1
        if ((($context["view"] ?? $this->getContext($context, "view")) == "show")) {
            // line 2
            echo "    ";
            echo nl2br(twig_escape_filter($this->env, ($context["value"] ?? $this->getContext($context, "value")), "html", null, true));
            echo "
";
        } else {
            // line 4
            echo "    ";
            echo twig_escape_filter($this->env, $this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->truncateText($this->env, ($context["value"] ?? $this->getContext($context, "value"))), "html", null, true);
            echo "
";
        }
        
        $__internal_30d0642bc25e4f0e62ff76427216f37cbd89d162efa64015bff471c5ffbbb11b->leave($__internal_30d0642bc25e4f0e62ff76427216f37cbd89d162efa64015bff471c5ffbbb11b_prof);

        
        $__internal_f729949e4568e6961574bcb84960b0f7e8c0d75411a5a50160bd12764a981655->leave($__internal_f729949e4568e6961574bcb84960b0f7e8c0d75411a5a50160bd12764a981655_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:field_string.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  33 => 4,  27 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if view == 'show' %}
    {{ value|nl2br }}
{% else %}
    {{ value|easyadmin_truncate }}
{% endif %}
", "EasyAdminBundle:default:field_string.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/field_string.html.twig");
    }
}

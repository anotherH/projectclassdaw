<?php

/* TwigBundle:Exception:exception.css.twig */
class __TwigTemplate_561be98ff907db6f302ea1d00e55221d617eeb246acbdb3ee552ece9cc8aa982 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6c1f5929265c8de480191e577bba7f6b2252c70024483f5e2edc57d7503661ed = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6c1f5929265c8de480191e577bba7f6b2252c70024483f5e2edc57d7503661ed->enter($__internal_6c1f5929265c8de480191e577bba7f6b2252c70024483f5e2edc57d7503661ed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.css.twig"));

        $__internal_121975b1800a1ab19014ef5062fd20553a1db7548cd8fb36759227f627871b8f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_121975b1800a1ab19014ef5062fd20553a1db7548cd8fb36759227f627871b8f->enter($__internal_121975b1800a1ab19014ef5062fd20553a1db7548cd8fb36759227f627871b8f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("@Twig/Exception/exception.txt.twig", "TwigBundle:Exception:exception.css.twig", 2)->display(array_merge($context, array("exception" => ($context["exception"] ?? $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_6c1f5929265c8de480191e577bba7f6b2252c70024483f5e2edc57d7503661ed->leave($__internal_6c1f5929265c8de480191e577bba7f6b2252c70024483f5e2edc57d7503661ed_prof);

        
        $__internal_121975b1800a1ab19014ef5062fd20553a1db7548cd8fb36759227f627871b8f->leave($__internal_121975b1800a1ab19014ef5062fd20553a1db7548cd8fb36759227f627871b8f_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 3,  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{% include '@Twig/Exception/exception.txt.twig' with { 'exception': exception } %}
*/
", "TwigBundle:Exception:exception.css.twig", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.css.twig");
    }
}

<?php

/* @Framework/FormTable/hidden_row.html.php */
class __TwigTemplate_5dfeba48bf098b99277839070551083af984a2acf05cbfc3785e352e4a3e77b2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3b988960b729644ca5870fef881c3ec78a5228687d7223d01f3e20ade2191c46 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3b988960b729644ca5870fef881c3ec78a5228687d7223d01f3e20ade2191c46->enter($__internal_3b988960b729644ca5870fef881c3ec78a5228687d7223d01f3e20ade2191c46_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        $__internal_4bb6715271365b93e639c3f18491d824f7a33b6a831a5b69deb4b6eebf1cce7f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4bb6715271365b93e639c3f18491d824f7a33b6a831a5b69deb4b6eebf1cce7f->enter($__internal_4bb6715271365b93e639c3f18491d824f7a33b6a831a5b69deb4b6eebf1cce7f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        // line 1
        echo "<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_3b988960b729644ca5870fef881c3ec78a5228687d7223d01f3e20ade2191c46->leave($__internal_3b988960b729644ca5870fef881c3ec78a5228687d7223d01f3e20ade2191c46_prof);

        
        $__internal_4bb6715271365b93e639c3f18491d824f7a33b6a831a5b69deb4b6eebf1cce7f->leave($__internal_4bb6715271365b93e639c3f18491d824f7a33b6a831a5b69deb4b6eebf1cce7f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
", "@Framework/FormTable/hidden_row.html.php", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/hidden_row.html.php");
    }
}

<?php

/* @Framework/Form/url_widget.html.php */
class __TwigTemplate_e3ad3fb74f3e7618afbc1c9bd9f0682b977f43fb1b660066612edb3048290363 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_973fcc7a752ecd14606df8824b305ebe60407b98b2a2b65a808f4e2f3caedd08 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_973fcc7a752ecd14606df8824b305ebe60407b98b2a2b65a808f4e2f3caedd08->enter($__internal_973fcc7a752ecd14606df8824b305ebe60407b98b2a2b65a808f4e2f3caedd08_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        $__internal_892a279c445ed13b08efe2ecbf8cbb2a66efb773b0cb0fd708fbd281c95dd879 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_892a279c445ed13b08efe2ecbf8cbb2a66efb773b0cb0fd708fbd281c95dd879->enter($__internal_892a279c445ed13b08efe2ecbf8cbb2a66efb773b0cb0fd708fbd281c95dd879_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'url')) ?>
";
        
        $__internal_973fcc7a752ecd14606df8824b305ebe60407b98b2a2b65a808f4e2f3caedd08->leave($__internal_973fcc7a752ecd14606df8824b305ebe60407b98b2a2b65a808f4e2f3caedd08_prof);

        
        $__internal_892a279c445ed13b08efe2ecbf8cbb2a66efb773b0cb0fd708fbd281c95dd879->leave($__internal_892a279c445ed13b08efe2ecbf8cbb2a66efb773b0cb0fd708fbd281c95dd879_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/url_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'url')) ?>
", "@Framework/Form/url_widget.html.php", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/url_widget.html.php");
    }
}

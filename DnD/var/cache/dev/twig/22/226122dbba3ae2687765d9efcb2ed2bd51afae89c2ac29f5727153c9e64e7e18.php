<?php

/* EasyAdminBundle:default:field_time.html.twig */
class __TwigTemplate_ba2199d68925772b4f8cfe7f1f10527e680267e924715551ca24f09bbfec4bc7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8032c8d88669c9dfebf1bad3d9ee807134345355b7b4c776e46654cb23d34bbf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8032c8d88669c9dfebf1bad3d9ee807134345355b7b4c776e46654cb23d34bbf->enter($__internal_8032c8d88669c9dfebf1bad3d9ee807134345355b7b4c776e46654cb23d34bbf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_time.html.twig"));

        $__internal_b2b15df85b76d10d47356482c0ee7951fa9f8997670538929947a21954f6c88c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b2b15df85b76d10d47356482c0ee7951fa9f8997670538929947a21954f6c88c->enter($__internal_b2b15df85b76d10d47356482c0ee7951fa9f8997670538929947a21954f6c88c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_time.html.twig"));

        // line 1
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, ($context["value"] ?? $this->getContext($context, "value")), $this->getAttribute(($context["field_options"] ?? $this->getContext($context, "field_options")), "format", array())), "html", null, true);
        echo "
";
        
        $__internal_8032c8d88669c9dfebf1bad3d9ee807134345355b7b4c776e46654cb23d34bbf->leave($__internal_8032c8d88669c9dfebf1bad3d9ee807134345355b7b4c776e46654cb23d34bbf_prof);

        
        $__internal_b2b15df85b76d10d47356482c0ee7951fa9f8997670538929947a21954f6c88c->leave($__internal_b2b15df85b76d10d47356482c0ee7951fa9f8997670538929947a21954f6c88c_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:field_time.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ value|date(field_options.format) }}
", "EasyAdminBundle:default:field_time.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/field_time.html.twig");
    }
}

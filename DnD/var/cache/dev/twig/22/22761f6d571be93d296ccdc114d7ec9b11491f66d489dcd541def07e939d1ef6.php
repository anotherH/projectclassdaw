<?php

/* EasyAdminBundle:default:field_image.html.twig */
class __TwigTemplate_c10dd1510891158c2f34368aad502ae71ac8a4c32182b5ea917b1ee97bde2d33 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_15f80e6e8779cdf122f46519ab4ab0d78e980f593ea704c71d2d7d0c7f3a7642 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_15f80e6e8779cdf122f46519ab4ab0d78e980f593ea704c71d2d7d0c7f3a7642->enter($__internal_15f80e6e8779cdf122f46519ab4ab0d78e980f593ea704c71d2d7d0c7f3a7642_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_image.html.twig"));

        $__internal_c5675b8d891d07b2ef5e6f4d36c584ecc9a70c9beb9efa28444aaf633dc05051 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c5675b8d891d07b2ef5e6f4d36c584ecc9a70c9beb9efa28444aaf633dc05051->enter($__internal_c5675b8d891d07b2ef5e6f4d36c584ecc9a70c9beb9efa28444aaf633dc05051_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_image.html.twig"));

        // line 1
        echo "<a href=\"#\" class=\"easyadmin-thumbnail\" data-featherlight=\"#easyadmin-lightbox-";
        echo twig_escape_filter($this->env, ($context["uuid"] ?? $this->getContext($context, "uuid")), "html", null, true);
        echo "\" data-featherlight-close-on-click=\"anywhere\">
    <img src=\"";
        // line 2
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl(($context["value"] ?? $this->getContext($context, "value"))), "html", null, true);
        echo "\">
</a>

<div id=\"easyadmin-lightbox-";
        // line 5
        echo twig_escape_filter($this->env, ($context["uuid"] ?? $this->getContext($context, "uuid")), "html", null, true);
        echo "\" class=\"easyadmin-lightbox\">
    <img src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl(($context["value"] ?? $this->getContext($context, "value"))), "html", null, true);
        echo "\">
</div>
";
        
        $__internal_15f80e6e8779cdf122f46519ab4ab0d78e980f593ea704c71d2d7d0c7f3a7642->leave($__internal_15f80e6e8779cdf122f46519ab4ab0d78e980f593ea704c71d2d7d0c7f3a7642_prof);

        
        $__internal_c5675b8d891d07b2ef5e6f4d36c584ecc9a70c9beb9efa28444aaf633dc05051->leave($__internal_c5675b8d891d07b2ef5e6f4d36c584ecc9a70c9beb9efa28444aaf633dc05051_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:field_image.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 6,  36 => 5,  30 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<a href=\"#\" class=\"easyadmin-thumbnail\" data-featherlight=\"#easyadmin-lightbox-{{ uuid }}\" data-featherlight-close-on-click=\"anywhere\">
    <img src=\"{{ asset(value) }}\">
</a>

<div id=\"easyadmin-lightbox-{{ uuid }}\" class=\"easyadmin-lightbox\">
    <img src=\"{{ asset(value) }}\">
</div>
", "EasyAdminBundle:default:field_image.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/field_image.html.twig");
    }
}

<?php

/* EasyAdminBundle:default:field_datetime.html.twig */
class __TwigTemplate_04cdcc55b5a6360e2f44dbc9d58de2d9aa600377fbcfa9a2bb3f36f338155ad6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b6443493795143327bc2fdd5bcc56f354dae7ccc1b0cd31b08a94109dcc2208a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b6443493795143327bc2fdd5bcc56f354dae7ccc1b0cd31b08a94109dcc2208a->enter($__internal_b6443493795143327bc2fdd5bcc56f354dae7ccc1b0cd31b08a94109dcc2208a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_datetime.html.twig"));

        $__internal_d7cee4cbb9860991b72806e311fdacd41aae8e6075c1ab62e2d7e76cc2190560 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d7cee4cbb9860991b72806e311fdacd41aae8e6075c1ab62e2d7e76cc2190560->enter($__internal_d7cee4cbb9860991b72806e311fdacd41aae8e6075c1ab62e2d7e76cc2190560_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_datetime.html.twig"));

        // line 1
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, ($context["value"] ?? $this->getContext($context, "value")), $this->getAttribute(($context["field_options"] ?? $this->getContext($context, "field_options")), "format", array())), "html", null, true);
        echo "
";
        
        $__internal_b6443493795143327bc2fdd5bcc56f354dae7ccc1b0cd31b08a94109dcc2208a->leave($__internal_b6443493795143327bc2fdd5bcc56f354dae7ccc1b0cd31b08a94109dcc2208a_prof);

        
        $__internal_d7cee4cbb9860991b72806e311fdacd41aae8e6075c1ab62e2d7e76cc2190560->leave($__internal_d7cee4cbb9860991b72806e311fdacd41aae8e6075c1ab62e2d7e76cc2190560_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:field_datetime.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ value|date(field_options.format) }}
", "EasyAdminBundle:default:field_datetime.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/field_datetime.html.twig");
    }
}

<?php

/* @Framework/Form/integer_widget.html.php */
class __TwigTemplate_66e38fa3fd1f77eebb1fb7f39c06106afed76e01e021a26c77da9b475bc2c242 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6b6c822e9f9df42e2066013d4ec9f397f058523e413031f5091353e03198e1fc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6b6c822e9f9df42e2066013d4ec9f397f058523e413031f5091353e03198e1fc->enter($__internal_6b6c822e9f9df42e2066013d4ec9f397f058523e413031f5091353e03198e1fc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        $__internal_8c8c30147e883a5523036cf9408fa2298b4dd427ee5bd941d9c4cffcb6700439 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8c8c30147e883a5523036cf9408fa2298b4dd427ee5bd941d9c4cffcb6700439->enter($__internal_8c8c30147e883a5523036cf9408fa2298b4dd427ee5bd941d9c4cffcb6700439_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
";
        
        $__internal_6b6c822e9f9df42e2066013d4ec9f397f058523e413031f5091353e03198e1fc->leave($__internal_6b6c822e9f9df42e2066013d4ec9f397f058523e413031f5091353e03198e1fc_prof);

        
        $__internal_8c8c30147e883a5523036cf9408fa2298b4dd427ee5bd941d9c4cffcb6700439->leave($__internal_8c8c30147e883a5523036cf9408fa2298b4dd427ee5bd941d9c4cffcb6700439_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/integer_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
", "@Framework/Form/integer_widget.html.php", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/integer_widget.html.php");
    }
}

<?php

/* FOSUserBundle:Resetting:request.html.twig */
class __TwigTemplate_9ed3e037a2c9f8edbe6a5dd0a4c1daf9ec7b20db0a71fdc154c7aa597280b4c1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:request.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c059351efaa3113a03eea2d3da505027899258d7ff2f2ee36786c62d88401bef = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c059351efaa3113a03eea2d3da505027899258d7ff2f2ee36786c62d88401bef->enter($__internal_c059351efaa3113a03eea2d3da505027899258d7ff2f2ee36786c62d88401bef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:request.html.twig"));

        $__internal_ce8e4e539a4693e333e03484be4bf9c09104aee8c42b5aad7106516621d6fd98 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ce8e4e539a4693e333e03484be4bf9c09104aee8c42b5aad7106516621d6fd98->enter($__internal_ce8e4e539a4693e333e03484be4bf9c09104aee8c42b5aad7106516621d6fd98_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:request.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c059351efaa3113a03eea2d3da505027899258d7ff2f2ee36786c62d88401bef->leave($__internal_c059351efaa3113a03eea2d3da505027899258d7ff2f2ee36786c62d88401bef_prof);

        
        $__internal_ce8e4e539a4693e333e03484be4bf9c09104aee8c42b5aad7106516621d6fd98->leave($__internal_ce8e4e539a4693e333e03484be4bf9c09104aee8c42b5aad7106516621d6fd98_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_30f22c38e6db429652279c803d057b28404581b7b3a5cf6a39961438bf96ca44 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_30f22c38e6db429652279c803d057b28404581b7b3a5cf6a39961438bf96ca44->enter($__internal_30f22c38e6db429652279c803d057b28404581b7b3a5cf6a39961438bf96ca44_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_5088dbceda2abcfd18916b5c872e686450d58a50fc654403e1aa7cec5280c4bf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5088dbceda2abcfd18916b5c872e686450d58a50fc654403e1aa7cec5280c4bf->enter($__internal_5088dbceda2abcfd18916b5c872e686450d58a50fc654403e1aa7cec5280c4bf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Resetting/request_content.html.twig", "FOSUserBundle:Resetting:request.html.twig", 4)->display($context);
        
        $__internal_5088dbceda2abcfd18916b5c872e686450d58a50fc654403e1aa7cec5280c4bf->leave($__internal_5088dbceda2abcfd18916b5c872e686450d58a50fc654403e1aa7cec5280c4bf_prof);

        
        $__internal_30f22c38e6db429652279c803d057b28404581b7b3a5cf6a39961438bf96ca44->leave($__internal_30f22c38e6db429652279c803d057b28404581b7b3a5cf6a39961438bf96ca44_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:request.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Resetting/request_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Resetting:request.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/friendsofsymfony/user-bundle/Resources/views/Resetting/request.html.twig");
    }
}

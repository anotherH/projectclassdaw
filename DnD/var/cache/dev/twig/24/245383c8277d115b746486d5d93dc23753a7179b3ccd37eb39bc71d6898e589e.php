<?php

/* @Framework/FormTable/form_widget_compound.html.php */
class __TwigTemplate_978d2bb3bda4386d9d3fe70bdf26b48b81ff92a8f24320b2d487c5d3ec3dc937 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b843eaaa55f94cd244ed296ce0ef63d0db9b424100898c3e8b9911b963a51c24 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b843eaaa55f94cd244ed296ce0ef63d0db9b424100898c3e8b9911b963a51c24->enter($__internal_b843eaaa55f94cd244ed296ce0ef63d0db9b424100898c3e8b9911b963a51c24_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_widget_compound.html.php"));

        $__internal_fd4ecf59099f2ac5591e654f39a3cf9fce29ff9d261866dd25e946e2431d5357 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fd4ecf59099f2ac5591e654f39a3cf9fce29ff9d261866dd25e946e2431d5357->enter($__internal_fd4ecf59099f2ac5591e654f39a3cf9fce29ff9d261866dd25e946e2431d5357_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_widget_compound.html.php"));

        // line 1
        echo "<table <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <tr>
        <td colspan=\"2\">
            <?php echo \$view['form']->errors(\$form) ?>
        </td>
    </tr>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</table>
";
        
        $__internal_b843eaaa55f94cd244ed296ce0ef63d0db9b424100898c3e8b9911b963a51c24->leave($__internal_b843eaaa55f94cd244ed296ce0ef63d0db9b424100898c3e8b9911b963a51c24_prof);

        
        $__internal_fd4ecf59099f2ac5591e654f39a3cf9fce29ff9d261866dd25e946e2431d5357->leave($__internal_fd4ecf59099f2ac5591e654f39a3cf9fce29ff9d261866dd25e946e2431d5357_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<table <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <tr>
        <td colspan=\"2\">
            <?php echo \$view['form']->errors(\$form) ?>
        </td>
    </tr>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</table>
", "@Framework/FormTable/form_widget_compound.html.php", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/form_widget_compound.html.php");
    }
}

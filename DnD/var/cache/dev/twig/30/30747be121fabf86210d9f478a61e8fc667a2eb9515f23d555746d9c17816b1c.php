<?php

/* @Framework/Form/percent_widget.html.php */
class __TwigTemplate_c1308a9bdaadf82a23db58252b66b8a60f8a863ea7ccd1ff4fb27ee414142979 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f4951f779223c21eb7f752209a54c118f268158721ec42e4e47c8bcf783b3d85 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f4951f779223c21eb7f752209a54c118f268158721ec42e4e47c8bcf783b3d85->enter($__internal_f4951f779223c21eb7f752209a54c118f268158721ec42e4e47c8bcf783b3d85_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        $__internal_4b49d1aed64bdb00c286a5b5b38f5af3838e2805b754581a44ebd3fbc9a4f731 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4b49d1aed64bdb00c286a5b5b38f5af3838e2805b754581a44ebd3fbc9a4f731->enter($__internal_4b49d1aed64bdb00c286a5b5b38f5af3838e2805b754581a44ebd3fbc9a4f731_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?> %
";
        
        $__internal_f4951f779223c21eb7f752209a54c118f268158721ec42e4e47c8bcf783b3d85->leave($__internal_f4951f779223c21eb7f752209a54c118f268158721ec42e4e47c8bcf783b3d85_prof);

        
        $__internal_4b49d1aed64bdb00c286a5b5b38f5af3838e2805b754581a44ebd3fbc9a4f731->leave($__internal_4b49d1aed64bdb00c286a5b5b38f5af3838e2805b754581a44ebd3fbc9a4f731_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/percent_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?> %
", "@Framework/Form/percent_widget.html.php", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/percent_widget.html.php");
    }
}

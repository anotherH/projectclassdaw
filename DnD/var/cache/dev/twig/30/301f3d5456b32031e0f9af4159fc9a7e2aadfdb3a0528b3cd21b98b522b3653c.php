<?php

/* FOSUserBundle:Profile:show.html.twig */
class __TwigTemplate_0fc998c07cdf4fa6d9157c6bcef60cb36e823095e31283884e74d4f2c01123d5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Profile:show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8781b5046243cdca812fcc129332f2333fe38cfb37818a3aa79965da15aec4fb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8781b5046243cdca812fcc129332f2333fe38cfb37818a3aa79965da15aec4fb->enter($__internal_8781b5046243cdca812fcc129332f2333fe38cfb37818a3aa79965da15aec4fb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show.html.twig"));

        $__internal_36239b960743f050a50af124a526f586fc69fff788db3f88636d36c1caa008a3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_36239b960743f050a50af124a526f586fc69fff788db3f88636d36c1caa008a3->enter($__internal_36239b960743f050a50af124a526f586fc69fff788db3f88636d36c1caa008a3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8781b5046243cdca812fcc129332f2333fe38cfb37818a3aa79965da15aec4fb->leave($__internal_8781b5046243cdca812fcc129332f2333fe38cfb37818a3aa79965da15aec4fb_prof);

        
        $__internal_36239b960743f050a50af124a526f586fc69fff788db3f88636d36c1caa008a3->leave($__internal_36239b960743f050a50af124a526f586fc69fff788db3f88636d36c1caa008a3_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_b12b6ad8a0d3117c478aab1763467470ce986679ad57899579e441cd971a6412 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b12b6ad8a0d3117c478aab1763467470ce986679ad57899579e441cd971a6412->enter($__internal_b12b6ad8a0d3117c478aab1763467470ce986679ad57899579e441cd971a6412_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_0b86e4d635282fd8ba8c5a47a059e7566e3fde81cdfaeaf79129228f8974ba41 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0b86e4d635282fd8ba8c5a47a059e7566e3fde81cdfaeaf79129228f8974ba41->enter($__internal_0b86e4d635282fd8ba8c5a47a059e7566e3fde81cdfaeaf79129228f8974ba41_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Profile/show_content.html.twig", "FOSUserBundle:Profile:show.html.twig", 4)->display($context);
        
        $__internal_0b86e4d635282fd8ba8c5a47a059e7566e3fde81cdfaeaf79129228f8974ba41->leave($__internal_0b86e4d635282fd8ba8c5a47a059e7566e3fde81cdfaeaf79129228f8974ba41_prof);

        
        $__internal_b12b6ad8a0d3117c478aab1763467470ce986679ad57899579e441cd971a6412->leave($__internal_b12b6ad8a0d3117c478aab1763467470ce986679ad57899579e441cd971a6412_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Profile/show_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Profile:show.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/friendsofsymfony/user-bundle/Resources/views/Profile/show.html.twig");
    }
}

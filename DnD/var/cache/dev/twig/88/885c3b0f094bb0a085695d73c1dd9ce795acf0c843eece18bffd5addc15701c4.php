<?php

/* @Framework/Form/radio_widget.html.php */
class __TwigTemplate_0da07fac9bb53f821971db3ab16730bbd82b288cc7c009882ffbb6e7f3bb7fbf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3fc24ddb66409eacbd7183175bff1df112b5455be83cc36e61f6286d926bf85a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3fc24ddb66409eacbd7183175bff1df112b5455be83cc36e61f6286d926bf85a->enter($__internal_3fc24ddb66409eacbd7183175bff1df112b5455be83cc36e61f6286d926bf85a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        $__internal_df94eb87318c37e78cc539dc2e572f5e66325c4c7447feb674d0649e357992af = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_df94eb87318c37e78cc539dc2e572f5e66325c4c7447feb674d0649e357992af->enter($__internal_df94eb87318c37e78cc539dc2e572f5e66325c4c7447feb674d0649e357992af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        // line 1
        echo "<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_3fc24ddb66409eacbd7183175bff1df112b5455be83cc36e61f6286d926bf85a->leave($__internal_3fc24ddb66409eacbd7183175bff1df112b5455be83cc36e61f6286d926bf85a_prof);

        
        $__internal_df94eb87318c37e78cc539dc2e572f5e66325c4c7447feb674d0649e357992af->leave($__internal_df94eb87318c37e78cc539dc2e572f5e66325c4c7447feb674d0649e357992af_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/radio_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
", "@Framework/Form/radio_widget.html.php", "/home/a14eriamocob/public_html/DnD/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/radio_widget.html.php");
    }
}

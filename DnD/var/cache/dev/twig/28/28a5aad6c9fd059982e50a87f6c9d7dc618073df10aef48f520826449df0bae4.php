<?php

/* EasyAdminBundle:default:field_date.html.twig */
class __TwigTemplate_faeb8a27acd07079534945b9aa04e4fbc4dd9c0835504654d44fbc2db5994560 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c9160c1932f42b0dfb9eb90673d8680c704f8a0466f93fc1afc1123aa0eb2f8f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c9160c1932f42b0dfb9eb90673d8680c704f8a0466f93fc1afc1123aa0eb2f8f->enter($__internal_c9160c1932f42b0dfb9eb90673d8680c704f8a0466f93fc1afc1123aa0eb2f8f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_date.html.twig"));

        $__internal_a6edf3505b4c998fcefcf22743eebd7c5e6fc652418fdb710a2b5779556be215 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a6edf3505b4c998fcefcf22743eebd7c5e6fc652418fdb710a2b5779556be215->enter($__internal_a6edf3505b4c998fcefcf22743eebd7c5e6fc652418fdb710a2b5779556be215_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_date.html.twig"));

        // line 1
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, ($context["value"] ?? $this->getContext($context, "value")), $this->getAttribute(($context["field_options"] ?? $this->getContext($context, "field_options")), "format", array())), "html", null, true);
        echo "
";
        
        $__internal_c9160c1932f42b0dfb9eb90673d8680c704f8a0466f93fc1afc1123aa0eb2f8f->leave($__internal_c9160c1932f42b0dfb9eb90673d8680c704f8a0466f93fc1afc1123aa0eb2f8f_prof);

        
        $__internal_a6edf3505b4c998fcefcf22743eebd7c5e6fc652418fdb710a2b5779556be215->leave($__internal_a6edf3505b4c998fcefcf22743eebd7c5e6fc652418fdb710a2b5779556be215_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:field_date.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ value|date(field_options.format) }}
", "EasyAdminBundle:default:field_date.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/field_date.html.twig");
    }
}

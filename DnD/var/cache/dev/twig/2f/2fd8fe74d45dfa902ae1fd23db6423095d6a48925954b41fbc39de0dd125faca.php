<?php

/* EasyAdminBundle:default:edit.html.twig */
class __TwigTemplate_d03927ba49135a41e6d5ebf0deb403536ba367cb0b81b8bd0a837cd8ff7ab407 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'body_class' => array($this, 'block_body_class'),
            'content_title' => array($this, 'block_content_title'),
            'main' => array($this, 'block_main'),
            'entity_form' => array($this, 'block_entity_form'),
            'delete_form' => array($this, 'block_delete_form'),
            'body_javascript' => array($this, 'block_body_javascript'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 8
        return $this->loadTemplate($this->getAttribute($this->getAttribute(($context["_entity_config"] ?? $this->getContext($context, "_entity_config")), "templates", array()), "layout", array()), "EasyAdminBundle:default:edit.html.twig", 8);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8ac83039741fe1d0628bb76018cee9b906d43cb0223e83fe7e9637b8738f4c11 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8ac83039741fe1d0628bb76018cee9b906d43cb0223e83fe7e9637b8738f4c11->enter($__internal_8ac83039741fe1d0628bb76018cee9b906d43cb0223e83fe7e9637b8738f4c11_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:edit.html.twig"));

        $__internal_afaea31507d89d8103b426999b71a422ac90bae9f1be305d794313d72bb5f70b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_afaea31507d89d8103b426999b71a422ac90bae9f1be305d794313d72bb5f70b->enter($__internal_afaea31507d89d8103b426999b71a422ac90bae9f1be305d794313d72bb5f70b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:edit.html.twig"));

        // line 1
        $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->setTheme(($context["form"] ?? $this->getContext($context, "form")), $this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getBackendConfiguration("design.form_theme"));
        // line 3
        $context["_entity_config"] = $this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getEntityConfiguration($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "query", array()), "get", array(0 => "entity"), "method"));
        // line 4
        $context["_entity_id"] = $this->getAttribute(($context["entity"] ?? $this->getContext($context, "entity")), $this->getAttribute(($context["_entity_config"] ?? $this->getContext($context, "_entity_config")), "primary_key_field_name", array()));
        // line 5
        $context["__internal_f101db7cc6e1236f6e94c927a46cefb416ffd6f3135273f86b74a3fcf469278d"] = $this->getAttribute(($context["_entity_config"] ?? $this->getContext($context, "_entity_config")), "translation_domain", array());
        // line 6
        $context["_trans_parameters"] = array("%entity_name%" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute(($context["_entity_config"] ?? $this->getContext($context, "_entity_config")), "name", array()), array(),         // line 5
($context["__internal_f101db7cc6e1236f6e94c927a46cefb416ffd6f3135273f86b74a3fcf469278d"] ?? $this->getContext($context, "__internal_f101db7cc6e1236f6e94c927a46cefb416ffd6f3135273f86b74a3fcf469278d"))), "%entity_label%" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute(        // line 6
($context["_entity_config"] ?? $this->getContext($context, "_entity_config")), "label", array()), array(),         // line 5
($context["__internal_f101db7cc6e1236f6e94c927a46cefb416ffd6f3135273f86b74a3fcf469278d"] ?? $this->getContext($context, "__internal_f101db7cc6e1236f6e94c927a46cefb416ffd6f3135273f86b74a3fcf469278d"))), "%entity_id%" =>         // line 6
($context["_entity_id"] ?? $this->getContext($context, "_entity_id")));
        // line 8
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8ac83039741fe1d0628bb76018cee9b906d43cb0223e83fe7e9637b8738f4c11->leave($__internal_8ac83039741fe1d0628bb76018cee9b906d43cb0223e83fe7e9637b8738f4c11_prof);

        
        $__internal_afaea31507d89d8103b426999b71a422ac90bae9f1be305d794313d72bb5f70b->leave($__internal_afaea31507d89d8103b426999b71a422ac90bae9f1be305d794313d72bb5f70b_prof);

    }

    // line 10
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_8dd3a9d7f37b996b6cc4fbbe69e6f330033cbd8b1075310332b69a20d9bcddc3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8dd3a9d7f37b996b6cc4fbbe69e6f330033cbd8b1075310332b69a20d9bcddc3->enter($__internal_8dd3a9d7f37b996b6cc4fbbe69e6f330033cbd8b1075310332b69a20d9bcddc3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        $__internal_59dfb2b347ec7db25dcb0fbb56fbd4aa9691b773c35cc9b1ecf0ea6340217a4f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_59dfb2b347ec7db25dcb0fbb56fbd4aa9691b773c35cc9b1ecf0ea6340217a4f->enter($__internal_59dfb2b347ec7db25dcb0fbb56fbd4aa9691b773c35cc9b1ecf0ea6340217a4f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        echo twig_escape_filter($this->env, ((("easyadmin-edit-" . $this->getAttribute(($context["_entity_config"] ?? $this->getContext($context, "_entity_config")), "name", array())) . "-") . ($context["_entity_id"] ?? $this->getContext($context, "_entity_id"))), "html", null, true);
        
        $__internal_59dfb2b347ec7db25dcb0fbb56fbd4aa9691b773c35cc9b1ecf0ea6340217a4f->leave($__internal_59dfb2b347ec7db25dcb0fbb56fbd4aa9691b773c35cc9b1ecf0ea6340217a4f_prof);

        
        $__internal_8dd3a9d7f37b996b6cc4fbbe69e6f330033cbd8b1075310332b69a20d9bcddc3->leave($__internal_8dd3a9d7f37b996b6cc4fbbe69e6f330033cbd8b1075310332b69a20d9bcddc3_prof);

    }

    // line 11
    public function block_body_class($context, array $blocks = array())
    {
        $__internal_d0384675cfd10a18e886d7383307c0dde16100aee82eb6bcbff33bc930b2b5cd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d0384675cfd10a18e886d7383307c0dde16100aee82eb6bcbff33bc930b2b5cd->enter($__internal_d0384675cfd10a18e886d7383307c0dde16100aee82eb6bcbff33bc930b2b5cd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_class"));

        $__internal_77eb793446d5556fe218303dd0dfce2c86110d1698e519ae93b4f3b1003881aa = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_77eb793446d5556fe218303dd0dfce2c86110d1698e519ae93b4f3b1003881aa->enter($__internal_77eb793446d5556fe218303dd0dfce2c86110d1698e519ae93b4f3b1003881aa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_class"));

        echo twig_escape_filter($this->env, ("edit edit-" . twig_lower_filter($this->env, $this->getAttribute(($context["_entity_config"] ?? $this->getContext($context, "_entity_config")), "name", array()))), "html", null, true);
        
        $__internal_77eb793446d5556fe218303dd0dfce2c86110d1698e519ae93b4f3b1003881aa->leave($__internal_77eb793446d5556fe218303dd0dfce2c86110d1698e519ae93b4f3b1003881aa_prof);

        
        $__internal_d0384675cfd10a18e886d7383307c0dde16100aee82eb6bcbff33bc930b2b5cd->leave($__internal_d0384675cfd10a18e886d7383307c0dde16100aee82eb6bcbff33bc930b2b5cd_prof);

    }

    // line 13
    public function block_content_title($context, array $blocks = array())
    {
        $__internal_34773f70dbf4a75797dd73ad4486fcebeeff244bdd4d801e2066541739905c9d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_34773f70dbf4a75797dd73ad4486fcebeeff244bdd4d801e2066541739905c9d->enter($__internal_34773f70dbf4a75797dd73ad4486fcebeeff244bdd4d801e2066541739905c9d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_title"));

        $__internal_9a58bd1a1cb03d50390ae80bba61ce705531358d35c3a286aca9703081a1be72 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9a58bd1a1cb03d50390ae80bba61ce705531358d35c3a286aca9703081a1be72->enter($__internal_9a58bd1a1cb03d50390ae80bba61ce705531358d35c3a286aca9703081a1be72_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_title"));

        // line 14
        ob_start();
        // line 15
        echo "    ";
        $context["_default_title"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("edit.page_title", ($context["_trans_parameters"] ?? $this->getContext($context, "_trans_parameters")), "EasyAdminBundle");
        // line 16
        echo "    ";
        echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute(($context["_entity_config"] ?? null), "edit", array(), "any", false, true), "title", array(), "any", true, true)) ? ($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute($this->getAttribute(($context["_entity_config"] ?? $this->getContext($context, "_entity_config")), "edit", array()), "title", array()), ($context["_trans_parameters"] ?? $this->getContext($context, "_trans_parameters")),         // line 5
($context["__internal_f101db7cc6e1236f6e94c927a46cefb416ffd6f3135273f86b74a3fcf469278d"] ?? $this->getContext($context, "__internal_f101db7cc6e1236f6e94c927a46cefb416ffd6f3135273f86b74a3fcf469278d")))) : (        // line 16
($context["_default_title"] ?? $this->getContext($context, "_default_title")))), "html", null, true);
        echo "
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_9a58bd1a1cb03d50390ae80bba61ce705531358d35c3a286aca9703081a1be72->leave($__internal_9a58bd1a1cb03d50390ae80bba61ce705531358d35c3a286aca9703081a1be72_prof);

        
        $__internal_34773f70dbf4a75797dd73ad4486fcebeeff244bdd4d801e2066541739905c9d->leave($__internal_34773f70dbf4a75797dd73ad4486fcebeeff244bdd4d801e2066541739905c9d_prof);

    }

    // line 20
    public function block_main($context, array $blocks = array())
    {
        $__internal_1ebfb8222c9d61c0261743e4706e9bd9666639baef2634fd77ecf54763709293 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1ebfb8222c9d61c0261743e4706e9bd9666639baef2634fd77ecf54763709293->enter($__internal_1ebfb8222c9d61c0261743e4706e9bd9666639baef2634fd77ecf54763709293_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        $__internal_67171abf0c1150ab1c8474c5869dad17c17c6cf43df6eab9fd74e75594edf602 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_67171abf0c1150ab1c8474c5869dad17c17c6cf43df6eab9fd74e75594edf602->enter($__internal_67171abf0c1150ab1c8474c5869dad17c17c6cf43df6eab9fd74e75594edf602_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 21
        echo "    ";
        $this->displayBlock('entity_form', $context, $blocks);
        // line 24
        echo "
    ";
        // line 25
        $this->displayBlock('delete_form', $context, $blocks);
        
        $__internal_67171abf0c1150ab1c8474c5869dad17c17c6cf43df6eab9fd74e75594edf602->leave($__internal_67171abf0c1150ab1c8474c5869dad17c17c6cf43df6eab9fd74e75594edf602_prof);

        
        $__internal_1ebfb8222c9d61c0261743e4706e9bd9666639baef2634fd77ecf54763709293->leave($__internal_1ebfb8222c9d61c0261743e4706e9bd9666639baef2634fd77ecf54763709293_prof);

    }

    // line 21
    public function block_entity_form($context, array $blocks = array())
    {
        $__internal_371d7c9f65dee4f8b678223ef300c1f36740852e42561fd68af51024ab4cdf01 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_371d7c9f65dee4f8b678223ef300c1f36740852e42561fd68af51024ab4cdf01->enter($__internal_371d7c9f65dee4f8b678223ef300c1f36740852e42561fd68af51024ab4cdf01_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "entity_form"));

        $__internal_8dc4b0e334ba9e29b428a2f8dc0666d81c36f974c5b81dc8ba088bca41ab1646 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8dc4b0e334ba9e29b428a2f8dc0666d81c36f974c5b81dc8ba088bca41ab1646->enter($__internal_8dc4b0e334ba9e29b428a2f8dc0666d81c36f974c5b81dc8ba088bca41ab1646_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "entity_form"));

        // line 22
        echo "        ";
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form');
        echo "
    ";
        
        $__internal_8dc4b0e334ba9e29b428a2f8dc0666d81c36f974c5b81dc8ba088bca41ab1646->leave($__internal_8dc4b0e334ba9e29b428a2f8dc0666d81c36f974c5b81dc8ba088bca41ab1646_prof);

        
        $__internal_371d7c9f65dee4f8b678223ef300c1f36740852e42561fd68af51024ab4cdf01->leave($__internal_371d7c9f65dee4f8b678223ef300c1f36740852e42561fd68af51024ab4cdf01_prof);

    }

    // line 25
    public function block_delete_form($context, array $blocks = array())
    {
        $__internal_3ada3b058ab03f572f6e71a098dbecda939ec28637d402d8321f7f710b50b70c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3ada3b058ab03f572f6e71a098dbecda939ec28637d402d8321f7f710b50b70c->enter($__internal_3ada3b058ab03f572f6e71a098dbecda939ec28637d402d8321f7f710b50b70c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "delete_form"));

        $__internal_032d6674600d90e3869a816b22b9a761a0cc92f4d240f4bba9ec6bf538d0540d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_032d6674600d90e3869a816b22b9a761a0cc92f4d240f4bba9ec6bf538d0540d->enter($__internal_032d6674600d90e3869a816b22b9a761a0cc92f4d240f4bba9ec6bf538d0540d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "delete_form"));

        // line 26
        echo "        ";
        echo twig_include($this->env, $context, "@EasyAdmin/default/includes/_delete_form.html.twig", array("view" => "edit", "referer" => $this->getAttribute($this->getAttribute($this->getAttribute(        // line 28
($context["app"] ?? $this->getContext($context, "app")), "request", array()), "query", array()), "get", array(0 => "referer", 1 => ""), "method"), "delete_form" =>         // line 29
($context["delete_form"] ?? $this->getContext($context, "delete_form")), "_translation_domain" => $this->getAttribute(        // line 30
($context["_entity_config"] ?? $this->getContext($context, "_entity_config")), "translation_domain", array()), "_trans_parameters" =>         // line 31
($context["_trans_parameters"] ?? $this->getContext($context, "_trans_parameters")), "_entity_config" =>         // line 32
($context["_entity_config"] ?? $this->getContext($context, "_entity_config"))), false);
        // line 33
        echo "
    ";
        
        $__internal_032d6674600d90e3869a816b22b9a761a0cc92f4d240f4bba9ec6bf538d0540d->leave($__internal_032d6674600d90e3869a816b22b9a761a0cc92f4d240f4bba9ec6bf538d0540d_prof);

        
        $__internal_3ada3b058ab03f572f6e71a098dbecda939ec28637d402d8321f7f710b50b70c->leave($__internal_3ada3b058ab03f572f6e71a098dbecda939ec28637d402d8321f7f710b50b70c_prof);

    }

    // line 37
    public function block_body_javascript($context, array $blocks = array())
    {
        $__internal_7ed24d8e4b08521e220bf0a26ccd18217186f78ccb427224557cecff95336b77 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7ed24d8e4b08521e220bf0a26ccd18217186f78ccb427224557cecff95336b77->enter($__internal_7ed24d8e4b08521e220bf0a26ccd18217186f78ccb427224557cecff95336b77_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_javascript"));

        $__internal_0abad8d8856f4694b240892d1dc1a3d4aab5387593e56f3e9601bf7adb92d8dc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0abad8d8856f4694b240892d1dc1a3d4aab5387593e56f3e9601bf7adb92d8dc->enter($__internal_0abad8d8856f4694b240892d1dc1a3d4aab5387593e56f3e9601bf7adb92d8dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_javascript"));

        // line 38
        echo "    ";
        $this->displayParentBlock("body_javascript", $context, $blocks);
        echo "

    <script type=\"text/javascript\">
        \$(function() {
            \$('.edit-form').areYouSure({ 'message': '";
        // line 42
        echo twig_escape_filter($this->env, twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("form.are_you_sure", array(), "EasyAdminBundle"), "js"), "html", null, true);
        echo "' });

            \$('.form-actions').easyAdminSticky();

            \$('a.action-delete').on('click', function(e) {
                e.preventDefault();

                \$('#modal-delete').modal({ backdrop: true, keyboard: true })
                    .off('click', '#modal-delete-button')
                    .on('click', '#modal-delete-button', function () {
                        \$('#delete-form').trigger('submit');
                    });
            });
        });
    </script>

    ";
        // line 58
        echo twig_include($this->env, $context, "@EasyAdmin/default/includes/_select2_widget.html.twig");
        echo "
";
        
        $__internal_0abad8d8856f4694b240892d1dc1a3d4aab5387593e56f3e9601bf7adb92d8dc->leave($__internal_0abad8d8856f4694b240892d1dc1a3d4aab5387593e56f3e9601bf7adb92d8dc_prof);

        
        $__internal_7ed24d8e4b08521e220bf0a26ccd18217186f78ccb427224557cecff95336b77->leave($__internal_7ed24d8e4b08521e220bf0a26ccd18217186f78ccb427224557cecff95336b77_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  237 => 58,  218 => 42,  210 => 38,  201 => 37,  190 => 33,  188 => 32,  187 => 31,  186 => 30,  185 => 29,  184 => 28,  182 => 26,  173 => 25,  160 => 22,  151 => 21,  141 => 25,  138 => 24,  135 => 21,  126 => 20,  113 => 16,  112 => 5,  110 => 16,  107 => 15,  105 => 14,  96 => 13,  78 => 11,  60 => 10,  50 => 8,  48 => 6,  47 => 5,  46 => 6,  45 => 5,  44 => 6,  42 => 5,  40 => 4,  38 => 3,  36 => 1,  24 => 8,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% form_theme form with easyadmin_config('design.form_theme') %}

{% set _entity_config = easyadmin_entity(app.request.query.get('entity')) %}
{% set _entity_id = attribute(entity, _entity_config.primary_key_field_name) %}
{% trans_default_domain _entity_config.translation_domain %}
{% set _trans_parameters = { '%entity_name%': _entity_config.name|trans, '%entity_label%': _entity_config.label|trans, '%entity_id%': _entity_id } %}

{% extends _entity_config.templates.layout %}

{% block body_id 'easyadmin-edit-' ~ _entity_config.name ~ '-' ~ _entity_id %}
{% block body_class 'edit edit-' ~ _entity_config.name|lower %}

{% block content_title %}
{% spaceless %}
    {% set _default_title = 'edit.page_title'|trans(_trans_parameters, 'EasyAdminBundle') %}
    {{ _entity_config.edit.title is defined ? _entity_config.edit.title|trans(_trans_parameters) : _default_title }}
{% endspaceless %}
{% endblock %}

{% block main %}
    {% block entity_form %}
        {{ form(form) }}
    {% endblock entity_form %}

    {% block delete_form %}
        {{ include('@EasyAdmin/default/includes/_delete_form.html.twig', {
            view: 'edit',
            referer: app.request.query.get('referer', ''),
            delete_form: delete_form,
            _translation_domain: _entity_config.translation_domain,
            _trans_parameters: _trans_parameters,
            _entity_config: _entity_config,
        }, with_context = false) }}
    {% endblock delete_form %}
{% endblock %}

{% block body_javascript %}
    {{ parent() }}

    <script type=\"text/javascript\">
        \$(function() {
            \$('.edit-form').areYouSure({ 'message': '{{ 'form.are_you_sure'|trans({}, 'EasyAdminBundle')|e('js') }}' });

            \$('.form-actions').easyAdminSticky();

            \$('a.action-delete').on('click', function(e) {
                e.preventDefault();

                \$('#modal-delete').modal({ backdrop: true, keyboard: true })
                    .off('click', '#modal-delete-button')
                    .on('click', '#modal-delete-button', function () {
                        \$('#delete-form').trigger('submit');
                    });
            });
        });
    </script>

    {{ include('@EasyAdmin/default/includes/_select2_widget.html.twig') }}
{% endblock %}
", "EasyAdminBundle:default:edit.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/edit.html.twig");
    }
}

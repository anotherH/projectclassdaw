<?php

/* EasyAdminBundle:default:field_email.html.twig */
class __TwigTemplate_285f9bcbd5f3fe5e173f823d78a6b24e3a83f5e4bbb779640583bfc1c75513c1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9103a4d811ae144bc6bc797e4cd75ebbf3ac4d9af4e4b4d5b4be0c97a33a8ae8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9103a4d811ae144bc6bc797e4cd75ebbf3ac4d9af4e4b4d5b4be0c97a33a8ae8->enter($__internal_9103a4d811ae144bc6bc797e4cd75ebbf3ac4d9af4e4b4d5b4be0c97a33a8ae8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_email.html.twig"));

        $__internal_11674e4ce4f1031aef8c23acead952b4a117c4633c2c2605a05ea59dffe4f93e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_11674e4ce4f1031aef8c23acead952b4a117c4633c2c2605a05ea59dffe4f93e->enter($__internal_11674e4ce4f1031aef8c23acead952b4a117c4633c2c2605a05ea59dffe4f93e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_email.html.twig"));

        // line 1
        if ((($context["view"] ?? $this->getContext($context, "view")) == "show")) {
            // line 2
            echo "    <a href=\"mailto:";
            echo twig_escape_filter($this->env, ($context["value"] ?? $this->getContext($context, "value")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, ($context["value"] ?? $this->getContext($context, "value")), "html", null, true);
            echo "</a>
";
        } else {
            // line 4
            echo "    <a href=\"mailto:";
            echo twig_escape_filter($this->env, ($context["value"] ?? $this->getContext($context, "value")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->truncateText($this->env, ($context["value"] ?? $this->getContext($context, "value"))), "html", null, true);
            echo "</a>
";
        }
        
        $__internal_9103a4d811ae144bc6bc797e4cd75ebbf3ac4d9af4e4b4d5b4be0c97a33a8ae8->leave($__internal_9103a4d811ae144bc6bc797e4cd75ebbf3ac4d9af4e4b4d5b4be0c97a33a8ae8_prof);

        
        $__internal_11674e4ce4f1031aef8c23acead952b4a117c4633c2c2605a05ea59dffe4f93e->leave($__internal_11674e4ce4f1031aef8c23acead952b4a117c4633c2c2605a05ea59dffe4f93e_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:field_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 4,  27 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if view == 'show' %}
    <a href=\"mailto:{{ value }}\">{{ value }}</a>
{% else %}
    <a href=\"mailto:{{ value }}\">{{ value|easyadmin_truncate }}</a>
{% endif %}
", "EasyAdminBundle:default:field_email.html.twig", "/home/a14eriamocob/public_html/DnD/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/field_email.html.twig");
    }
}

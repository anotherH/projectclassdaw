
INSERT INTO PrestigeClass(name, hitDie, reqRace, reqBaseAttack, reqFeats, reqSpells, skillPointsPerLvl, shortDescription) VALUES('Arcane Archer', 'd8', 'Elf,Half-elf',6, 'Point Blank Shot, Precise Shot, Weapon Focus (longbow or shortbow)', 'Ability to cast 1st-level arcane spells.', 4, 'Arcane Archers are feared combatants on the battlefield, capable of imbuing their ammunition with magical properties and even casting spells through their arrows.');

INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Craft'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Arcane Archer'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Hide'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Arcane Archer'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Listen'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Arcane Archer'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Move Silently'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Arcane Archer'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Ride'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Arcane Archer'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Spot'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Arcane Archer'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Survival'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Arcane Archer'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Use Rope'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Arcane Archer'));


INSERT INTO PrestigeClass(name, hitDie, reqAlignment, reqSpells, reqSpecial, skillPointsPerLvl, shortDescription) VALUES('Arcane Trickster', 'd4', 'Chaotic,Neutral', 'Ability to cast mage hand and at least one arcane spell of 3rd level or higher', 'Sneak attack +2d6', 4,'Arcane tricksters tend to use a seat-of-the-pants approach to adventuring, loading up on spells that improve their stealth and mobility.');

INSERT INTO ReqPrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Decipher Script'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Arcane Trickster'), 7 );
INSERT INTO ReqPrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Disable Device'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Arcane Trickster'), 7 );
INSERT INTO ReqPrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Escape Artist'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Arcane Trickster'), 7 );
INSERT INTO ReqPrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Knowledge (arcana)'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Arcane Trickster'), 4 );

INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Appraise'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Arcane Trickster'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Balance'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Arcane Trickster'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Bluff'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Arcane Trickster'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Climb'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Arcane Trickster'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Concentration'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Arcane Trickster'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Craft'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Arcane Trickster'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Decipher Script'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Arcane Trickster'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Diplomacy'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Arcane Trickster'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Disable Device'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Arcane Trickster'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Disguise'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Arcane Trickster'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Escape Artist'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Arcane Trickster'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Gather Information'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Arcane Trickster'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Hide'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Arcane Trickster'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Jump'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Arcane Trickster'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Knowledge'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Arcane Trickster'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Listen'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Arcane Trickster'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Move Silently'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Arcane Trickster'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Open Lock'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Arcane Trickster'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Profession'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Arcane Trickster'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Sense Motive'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Arcane Trickster'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Search'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Arcane Trickster'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Sleight of Hand'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Arcane Trickster'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Speak Language'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Arcane Trickster'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Spellcraft'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Arcane Trickster'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Spot'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Arcane Trickster'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Swim'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Arcane Trickster'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Tumble'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Arcane Trickster'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Use Rope'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Arcane Trickster'));


INSERT INTO PrestigeClass(name, hitDie, reqFeats, reqSpells, skillPointsPerLvl, shortDescription) VALUES('Archmage', 'd4', 'Skill Focus (Spellcraft), Spell Focus in two schools of magic', 'Ability to cast 7th level arcane spells, knowledge of 5th-level or higher spells from at least five schools.', 2, 'An archmage gains strange powers and the ability to alter spells in remarkable ways, but must sacrifice some of her spell capability in order to master these arcane secrets.');

INSERT INTO ReqPrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Knowledge (arcana)'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Archmage'), 15 );
INSERT INTO ReqPrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Spellcraft'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Archmage'), 15 );

INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Concentration'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Archmage'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Craft (alchemy)'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Archmage'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Knowledge'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Archmage'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Profession'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Archmage'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Search'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Archmage'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Spellcraft'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Archmage'));

INSERT INTO PrestigeClass(name, hitDie, reqAlignment, reqSpecial, skillPointsPerLvl, shortDescription) VALUES('Assassin', 'd6', 'Evil', 'The character must kill someone for no other reason than to join the assassins.', 'The assassin is the master of dealing quick, lethal blows. Assassins also excel at infiltration and disguise.');

INSERT INTO ReqPrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Disguise'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Assassin'), 4 );
INSERT INTO ReqPrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Hide'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Assassin'), 8 );
INSERT INTO ReqPrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Move Silently'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Assassin'), 8 );

INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Balance'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Assassin'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Bluff'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Assassin'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Climb'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Assassin'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Craft'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Assassin'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Decipher Script'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Assassin'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Diplomacy'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Assassin'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Disable Device'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Assassin'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Disguise'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Assassin'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Escape Artist'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Assassin'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Forgery'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Assassin'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Gather Information'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Assassin'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Hide'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Assassin'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Intimidate'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Assassin'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Jump'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Assassin'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Listen'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Assassin'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Move Silently'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Assassin'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Open Lock'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Assassin'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Search'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Assassin'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Sense Motive'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Assassin'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Sleight of Hand'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Assassin'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Spot'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Assassin'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Swim'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Assassin'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Tumble'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Assassin'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Use Magic Device'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Assassin'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Use Rope'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Assassin'));


INSERT INTO PrestigeClass(name, hitDie, reqBaseAttack, reqFeats, reqAlignment, reqSpecial, skillPointsPerLvl,shortDescription) VALUES('Blackguard', 'd10', '6', 'Cleave, Improved Sunder, Power Attack', 'Evil', 'The character must have made peaceful contact with an evil outsider who was summoned by him or someone else.', 2, 'Some people call these villains antipaladins due to their completely evil nature. The blackguard has many options available to him—sending forth dark minions and servants to do his bidding, attacking with stealth and honorless guile, or straightforward smiting of the forces of good that stand in his way.');

INSERT INTO ReqPrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Hide'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Blackguard'), 5 );
INSERT INTO ReqPrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Knowledge (religion)'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Blackguard'), 2 );

INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Concentration'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Blackguard'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Craft'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Blackguard'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Diplomacy'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Blackguard'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Handle Animal'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Blackguard'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Heal'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Blackguard'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Hide'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Blackguard'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Intimidate'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Blackguard'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Knowledge (religion)'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Blackguard'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Profession'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Blackguard'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Ride'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Blackguard'));

INSERT INTO PrestigeClass(name, hitDie, reqRace, reqSpells, reqSpecial, reqLanguages, skillPointsPerLvl, shortDescription) VALUES('Dragon Disciple', 'd12', 'Any nondragon', 'Ability to cast arcane spells without preparation', 'The player chooses a dragon variety when taking the first level in this prestige class, subject to the DM’s approval.', 'Draconic', 2, 'They use their magical power as a catalyst to ignite their dragon blood, realizing its fullest potential.');

INSERT INTO ReqPrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Knowledge (arcana)'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Dragon Disciple'), 8 );

INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Concentration'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Dragon Disciple'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Craft'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Dragon Disciple'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Diplomacy'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Dragon Disciple'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Escape Artist'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Dragon Disciple'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Gather'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Dragon Disciple'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Information'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Dragon Disciple'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Knowledge'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Dragon Disciple'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Listen'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Dragon Disciple'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Profession'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Dragon Disciple'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Search'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Dragon Disciple'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Speak'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Dragon Disciple'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Language'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Dragon Disciple'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Spellcraft'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Dragon Disciple'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Spot'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Dragon Disciple'));

INSERT INTO PrestigeClass(name, hitDie, reqBaseAttack, reqFeats, skillPointsPerLvl, shortDescription) VALUES('Duelist', 'd10', 6, 'Dodge,Mobility,Weapon Finesse', 4, 'The duelist (sometimes known as the swashbuckler) is a nimble, intelligent fighter trained in making precise attacks with light weapons, such as the rapier. She always takes full advantage of her quick reflexes and wits in a fight.');

INSERT INTO ReqPrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Perform'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Duelist'), 3);
INSERT INTO ReqPrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Tumble'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Duelist'), 5 );

INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Balance'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Duelist'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Bluff'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Duelist'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Escape Artist'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Duelist'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Jump'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Duelist'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Listen'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Duelist'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Perform'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Duelist'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Sense Motive'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Duelist'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Spot'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Duelist'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Tumble'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Duelist'));

INSERT INTO PrestigeClass(name, hitDie, reqRace, reqBaseAttack, reqFeats, reqAlignment, skillPointsPerLvl, shortDescription) VALUES('Dwarven Defender', 'd12', 'Dwarf', 7, 'Dodge, Endurance, Toughness', 'Lawful', 2, 'As the name might imply, this character is a skilled combatant trained in the arts of defense. A line of dwarven defenders is a far better defense than a 10- foot-thick wall of stone, and much more dangerous');

INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Craft'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Dwarven Defender'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Listen'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Dwarven Defender'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Sense Motive'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Dwarven Defender'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Spot'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Dwarven Defender'));

INSERT INTO PrestigeClass(name, hitDie, reqSpells, reqWeaponProficiency, skillPointsPerLvl, shortDescription) VALUES('Eldritch Knight', 'd6', 'Able to cast 3rd-level arcane spells.', 'Must be proficient with all martial weapons.', 2 'Studying the martial and arcane arts to equal degree, the eldritch knight is a versatile combatant who can cast a fireball on her foes or charge them with sword drawn.');

INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Concentration'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'ldritch Knight'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Craft'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'ldritch Knight'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Decipher Script'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'ldritch Knight'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Jump'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'ldritch Knight'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Knowledge (arcana)'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'ldritch Knight'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Knowledge (nobility and royalty)'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'ldritch Knight'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Ride'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'ldritch Knight'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Sense Motive'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'ldritch Knight'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Spellcraft'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'ldritch Knight'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Swim'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'ldritch Knight'));

INSERT INTO PrestigeClass(name, hitDie, reqFeats, reqSpells, skillPointsPerLvl, shortDescription) VALUES('Hierophant', 'd8', 'Able to cast 7th-level divine spells.', 'Any metamagic feat', 2, 'A divine spellcaster who rises high in the service of his deity gains access to spells and abilities of which lesser faithful can only dream.');

INSERT INTO ReqPrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Knowledge (religion)'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Hierophant'), 15);
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Concentration'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Hierophant'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Craft'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Hierophant'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Diplomacy'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Hierophant'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Heal'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Hierophant'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Knowledge (arcana)'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Hierophant'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Knowledge (religion)'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Hierophant'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Profession'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Hierophant'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Spellcraft'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Hierophant'));

INSERT INTO PrestigeClass(name, hitDie, reqRace, reqFeats, skillPointsPerLvl, shortDescription) VALUES('Horizon Walker', 'd8', 'Endurance', 4, 'The horizon walker is an unceasing traveler to the universe’s most dangerous places. As her journeys take her from place to place, she adapts to become one with her environment.');

INSERT INTO ReqPrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Knowledge (geography)'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Horizon Walker'), 8);
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Balance'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Horizon Walker'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Climb'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Horizon Walker'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Diplomacy'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Horizon Walker'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Handle Animal'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Horizon Walker'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Hide'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Horizon Walker'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Knowledge (geography)'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Horizon Walker'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Listen'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Horizon Walker'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Move Silently'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Horizon Walker'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Profession'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Horizon Walker'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Ride'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Horizon Walker'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Speak Language'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Horizon Walker'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Spot'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Horizon Walker'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Survival'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Horizon Walker'));

INSERT INTO PrestigeClass(name, hitDie, reqFeats, reqSpells, skillPointsPerLvl, shortDescription) VALUES('Loremaster', 'd4', 'Any three metamagic or item creation feats, Skill Focus (Knowledge [any individual Knowledge skill]', 'Able to cast seven different divination spells, one of which must be 3rd level or higher', 4, 'Loremasters are spellcasters who concentrate on knowledge, valuing lore and secrets over gold. They uncover secrets that they then use to better themselves mentally, physically, and spiritually.');

INSERT INTO ReqPrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Knowledge'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Loremaster'), 10);
INSERT INTO ReqPrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Knowledge'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Loremaster'), 10);

INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Appraise'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Loremaster'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Concentration'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Loremaster'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Craft (alchemy)'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Loremaster'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Decipher Script'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Loremaster'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Gather Information'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Loremaster'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Handle Animals'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Loremaster'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Heal'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Loremaster'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Knowledge'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Loremaster'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Perform'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Loremaster'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Profession'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Loremaster'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Speak Language'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Loremaster'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Spellcraft'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Loremaster'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Use Magic Device'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Loremaster'));

INSERT INTO PrestigeClass(name, hitDie, reqSpells, skillPointsPerLvl, shortDescription) VALUES('Mystic Theurge', 'd4', 'Able to cast 2nd-level divine spells and 2nd-level arcane spells.', 2, 'Blurring the line between divine and arcane, mystic theurges draw power from divine sources and musty tomes alike.');

INSERT INTO ReqPrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Knowledge (arcana)'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Mystic Theurge'), 6);
INSERT INTO ReqPrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Knowledge (religion)'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Mystic Theurge'), 6);

INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Concentration'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Mystic Theurge'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Craft'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Mystic Theurge'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Decipher Script'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Mystic Theurge'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Knowledge (arcana)'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Mystic Theurge'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Knowledge (religion)'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Mystic Theurge'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Profession'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Mystic Theurge'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Sense Motive'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Mystic Theurge'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Spellcraft'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Mystic Theurge'));

INSERT INTO PrestigeClass(name, hitDie, reqRace, reqFeats, reqSpells, reqAlignment, reqSpecial, reqLanguages,reqWeaponProficiency, skillPointsPerLvl, shortDescription) VALUES('Red Wizard', 'd4', 'Human', 'Tattoo Focus, three metamagic feats or item creation feats', 'Able to cast 3rd-level arcane spells', 'Neutral,Evil', 'from Thay',  2, 'The Red Wizards are the masters of Thay, the would-be magical overlords of the land of Faerûn. They focus on a school of magic more intently than any specialist, achieving incredible mastery of magic within a very narrow focus.');

INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Spellcraft'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Red Wizard'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Bluff'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Red Wizard'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Concentration'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Red Wizard'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Craft'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Red Wizard'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Intimidate'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Red Wizard'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Knowledge'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Red Wizard'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Profession'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Red Wizard'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Spellcraft'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Red Wizard'));

INSERT INTO Feat (featName, featType, prerequisites, benefit, shortDescription, isFighterChoice, isGainedMultipleTimes, itsEffectsStack) VALUES ('Tattoo Focus', 'Special','Specialized in a school of magic','Add +1 to the DC for all saving throws against spells from your specialized school. You get a +1 bonus on caster level checks (1d20 + caster level) to overcome a creature’s spell resistance when casting spells from that school','Add +1 to all saving throws and +1 on caster lvl checks from spells from your specialized shool ','FALSE','FALSE','FALSE');

INSERT INTO PrestigeClass(name, hitDie,  reqFeats, skillPointsPerLvl, shortDescription) VALUES('Shadowdancer', 'd8', 'Combat Reflexes, Dodge, Mobility', 6, 'Operating in the border between light and darkness, shadowdancers are nimble artists of deception');

INSERT INTO ReqPrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Move Silently'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Shadowdancer'), 6);
INSERT INTO ReqPrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Hide'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Shadowdancer'), 6);
INSERT INTO ReqPrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Perform (dance)'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Shadowdancer'), 6);

INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Balance'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Red Wizard'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Bluff'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Red Wizard'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Decipher Script'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Red Wizard'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Diplomacy'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Red Wizard'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Disguise'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Red Wizard'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Escape Artist'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Red Wizard'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Hide'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Red Wizard'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Jump'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Red Wizard'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Listen'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Red Wizard'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Move Silently'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Red Wizard'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Perform'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Red Wizard'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Profession'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Red Wizard'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Search'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Red Wizard'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Sleight of Hand'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Red Wizard'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Spot'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Red Wizard'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Tumble'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Red Wizard'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Use Rope'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Red Wizard'));

INSERT INTO PrestigeClass(name, hitDie, reqFeats, reqSpells, skillPointsPerLvl, shortDescription) VALUES('Thaumaturgist', 'd4', 'Spell Focus (conjuration)', 'Able to cast lesser planar ally', 2, 'The thaumaturgist reaches out with divine power to other planes of existence, calling creatures there to do his bidding.');

INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Concentration'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Thaumaturgist'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Craft'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Thaumaturgist'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Diplomacy'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Thaumaturgist'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Knowledge (religion)'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Thaumaturgist'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Knowledge (the planes)'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Thaumaturgist'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Profession'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Thaumaturgist'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Sense Motive'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Thaumaturgist'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Speak Language'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Thaumaturgist'));
INSERT INTO PrstClassSkills((SELECT id_skill FROM SKILL WHERE skillName = 'Spellcraft'), (SELECT id_prestigeClass FROM PrestigeClass WHERE name = 'Thaumaturgist'));




--------------------------------------------------Sara --------------------------------------------------------------------
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Arcane Archer'), 1,1,2,2,0,'Enhance arrow +1');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Arcane Archer'), 2,2,3,3,0,'Imbue arrow');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Arcane Archer'), 3,3,3,3,1,'Enhance arrow +2');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Arcane Archer'), 4,4,4,4,1,'Seeker arrow');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Arcane Archer'), 5,5,4,4,1,'Enhance arrow +3');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Arcane Archer'), 6,6,5,5,2,'Phase arrow');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Arcane Archer'), 7,7,5,5,2,'Enhance arrow +4');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Arcane Archer'), 8,8,6,6,2,'Hail of arrows');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Arcane Archer'), 9,9,6,6,3,'Enhance arrow +5');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Arcane Archer'), 10,10,7,7,3,'Arrow of death');

INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special, Spells1, Spells2, Spells3, Spells4) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Assassin'), 1,0,0,2,0,'Sneak attack +1d6, death attack, poison use, spells',0,null,null,null);
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special, Spells1, Spells2, Spells3, Spells4) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Assassin'), 2,1,0,3,0,'+1 save against poison, uncanny dodge',1,null,null,null);
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special, Spells1, Spells2, Spells3, Spells4) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Assassin'), 3,2,1,3,1,'Sneak attack +2d6',2,0,null,null);
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special, Spells1, Spells2, Spells3, Spells4) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Assassin'), 4,3,1,4,1,'+2 save against poison',3,1,null,null);
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special, Spells1, Spells2, Spells3, Spells4) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Assassin'), 5,3,1,4,1,'Improved uncanny dodge, sneak attack +3d6',3,2,0,null);
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special, Spells1, Spells2, Spells3, Spells4) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Assassin'), 6,4,2,5,2,'+3 save against poison',3,3,1,null);
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special, Spells1, Spells2, Spells3, Spells4) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Assassin'), 7,5,2,5,2,'Sneak attack +4d6',3,3,2,0);
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special, Spells1, Spells2, Spells3, Spells4) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Assassin'), 8,6,2,6,2,'+4 save against poison, hide in plain sight',3,3,3,1);
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special, Spells1, Spells2, Spells3, Spells4) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Assassin'), 9,6,3,6,3,'Sneak attack +5d6',3,3,3,2);
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special, Spells1, Spells2, Spells3, Spells4) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Assassin'), 10,7,3,7,3,'+5 save against poison',3,3,3,3);

INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Duelist'), 1,1,0,2,0,'Canny defense');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Duelist'), 2,2,0,3,0,'Improved reaction +2');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Duelist'), 3,3,1,3,1,'Enhanced mobility');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Duelist'), 4,4,1,4,1,'Grace');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Duelist'), 5,5,1,4,1,'Precise strike +1d6');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Duelist'), 6,6,2,5,2,'Acrobatic charge');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Duelist'), 7,7,2,5,2,'Elaborate parry');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Duelist'), 8,8,2,6,2,'Improved reaction +4');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Duelist'), 9,9,3,6,3,'Deflect Arrows');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Duelist'), 10,10,3,7,3,'Precise strike +2d6');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Eldritch Knight'), 1,1,2,0,0,'Bonus feat');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Eldritch Knight'), 2,2,3,0,0,'+1 level of existing arcane spellcasting class');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Eldritch Knight'), 3,3,3,1,1,'+1 level of existing arcane spellcasting class');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Eldritch Knight'), 4,4,4,1,1,'+1 level of existing arcane spellcasting class');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Eldritch Knight'), 5,5,4,1,1,'+1 level of existing arcane spellcasting class');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Eldritch Knight'), 6,6,5,2,2,'+1 level of existing arcane spellcasting class');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Eldritch Knight'), 7,7,5,2,2,'+1 level of existing arcane spellcasting class');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Eldritch Knight'), 8,8,6,2,2,'+1 level of existing arcane spellcasting class');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Eldritch Knight'), 9,9,6,3,3,'+1 level of existing arcane spellcasting class');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Eldritch Knight'), 10,10,7,3,3,'+1 level of existing arcane spellcasting class');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special,SpellsPerDay) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Loremaster'),1,0,0,0,2,'Secret','+1 level of existing class');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special,SpellsPerDay) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Loremaster'),2,1,0,0,3,'Lore','+1 level of existing class');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special,SpellsPerDay) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Loremaster'),3,1,1,1,3,'Secret','+1 level of existing class');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special,SpellsPerDay) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Loremaster'),4,2,1,1,4,'Bonus language','+1 level of existing class');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special,SpellsPerDay) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Loremaster'),5,2,1,1,4,'Secret','+1 level of existing class');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special,SpellsPerDay) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Loremaster'),6,3,2,2,5,'Greater lore','+1 level of existing class');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special,SpellsPerDay) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Loremaster'),7,3,2,2,5,'Secret','+1 level of existing class');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special,SpellsPerDay) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Loremaster'),8,4,2,2,6,'Bonus language','+1 level of existing class');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special,SpellsPerDay) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Loremaster'),9,4,3,3,6,'Secret','+1 level of existing class');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special,SpellsPerDay) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Loremaster'),10,5,3,3,7,'True lore','+1 level of existing class');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special,SpellsPerDay) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Red Wizard'),1,0,0,0,2,'Enhanced specialization, specialist defense +1','+1 level of existing arcane spellcasting class');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special,SpellsPerDay) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Red Wizard'),2,1,0,0,3,'Spell power +1','+1 level of existing arcane spellcasting class');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special,SpellsPerDay) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Red Wizard'),3,1,1,1,3,'Specialist defense +2','+1 level of existing arcane spellcasting class');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special,SpellsPerDay) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Red Wizard'),4,2,1,1,4,'Spell power +2','+1 level of existing arcane spellcasting class');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special,SpellsPerDay) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Red Wizard'),5,2,1,1,4,'Bonus feat, circle leader','+1 level of existing arcane spellcasting class');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special,SpellsPerDay) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Red Wizard'),6,3,2,2,5,'Spell power +3','+1 level of existing arcane spellcasting class');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special,SpellsPerDay) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Red Wizard'),7,3,2,2,5,'Specialist defense +3, scribe tattoo','+1 level of existing arcane spellcasting class');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special,SpellsPerDay) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Red Wizard'),8,4,2,2,6,'Spell power +4','+1 level of existing arcane spellcasting class');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special,SpellsPerDay) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Red Wizard'),9,4,3,3,6,'Specialist defense +4','+1 level of existing arcane spellcasting class');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special,SpellsPerDay) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Red Wizard'),10,5,3,3,7,'Great circle leader, spell power +5','+1 level of existing arcane spellcasting class');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special,SpellsPerDay) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Thaumaturgist'),1,0,0,0,2,'Improved ally','+1 level of existing spellcasting class');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special,SpellsPerDay) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Thaumaturgist'),2,1,0,0,3,'Augment Summoning','+1 level of existing spellcasting class');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special,SpellsPerDay) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Thaumaturgist'),3,1,1,1,3,'Extended summoning','+1 level of existing spellcasting class');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special,SpellsPerDay) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Thaumaturgist'),4,2,1,1,4,'Contingent conjuration','+1 level of existing spellcasting class');
INSERT INTO PrestigeClassPerLvl (id_prestigeClass_fk, lvl,AttackBonus, FortSave, RefSave , WillSave, Special,SpellsPerDay) VALUES ( (select id_prestigeClass from PrestigeClass where name = 'Thaumaturgist'),5,2,1,1,4,'Planar cohort','+1 level of existing spellcasting class');


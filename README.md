#D&D Build Master

##Objetivos  

Una página de builds de personajes del juego Dungeons And Dragons, versión 3.5

##Equipo  

Eric Amoros, Sara Henriques, Núria Alcalá

##Estado  

=========

###Como instalar el proyecto:

####Para crear la base de datos:

Modificar el fichero parameters.yml con el host, nombre de la base de datos, usuario con permisos para la base de datos y su contraseña para establecer una conexión con la base de datos deseada o dejar los parámetros por defecto, en cuyo caso no hará falta seguir con los siguientes pasos.

Dentro de la carpeta del proyecto  
	- php bin/console doctrine:generate:entities AppBundle/Entity  
	- php bin/console doctrine:schema:update --force  

Insertar los datos dentro de la base de datos  
	- mysql -u [nombreUsuario] -p [nombreBaseDatos] < DnD/DnD_bd_inserts.sql


##verificar que el composer.json sale la ruta correcta en name!!